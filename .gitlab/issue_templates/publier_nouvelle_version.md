Pour passer de la version *SNAPSHOT* à la version stable :

- [ ] créer une demande de fusion et une branche à partir du ticket
- [ ] changer la version dans `pom.xml`
  ```sh
  mvn versions:set -DnewVersion=2.1.0
  mvn versions:commit
  ```
- [ ] mettre à jour `src/site/markdown/release-notes-fr.md`
- [ ] mettre à jour les fichiers de métadonnées du projet
- [ ] fusionner
- [ ] déployer sur Archiva
- [ ] [créer une étiquette](https://forgemia.inra.fr/agroclim/Indicators/indicators-java/-/tags/new) à partir de `main` en renseignant les points notables de la version
- [ ] [créer une *release*](https://forgemia.inra.fr/agroclim/Indicators/indicators-java/-/releases/new) à partir de l'étiquette et en incluant le message de l'étiquette annotée
- [ ] [créer le jalon suivant](https://forgemia.inra.fr/agroclim/Indicators/indicators-java/-/milestones/new)

Passer en version *SNAPSHOT* suivante dès que la *release* a été créée.
