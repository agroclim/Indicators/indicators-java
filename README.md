# Indicators Java library

Library to provide agro- and eco-climatic indicators in Java.

The library is used in the free software [GETARI](https://agroclim.inrae.fr/getari/) and the web application [SICLIMA](https://agroclim.inrae.fr/siclima/) throw the computation workflow SEASON.

## 🧐 Features

It contains predefined indicators : 89 daily and 48 hourly.

Documentation is available [online](https://forgemia.inra.fr/agroclim/Indicators/indicators-java).

For the last changes, see [`src/site/markdown/release-notes-fr.md`](src/site/markdown/release-notes-fr.md) or [online](https://agroclim.pages.mia.inra.fr/Indicators/indicators-java/release-notes-fr.html).

## 🛠️ Tech Stack

- [Maven](https://maven.apache.org/)
- [JAXB](https://eclipse-ee4j.github.io/jaxb-ri/)
- [JEXL](https://commons.apache.org/proper/commons-jexl/)
- [Log4j2](https://logging.apache.org/log4j/2.x/index.html)
- [Lombok](https://projectlombok.org/)

[pom2metadata](https://forgemia.inra.fr/olivier.maury/pom2metadata) can be used
to update the metadata project files (`AUTHORS.md`, `CITATION.cff`, `codemeta.json`, `publiccode.yml`):

```
cd /tmp
git clone https://forgemia.inra.fr/olivier.maury/pom2metadata
pip install pom2metadata
cd path/to/Indicators
pom2metadata
```

## 🛠️ Install

```bash
mvn install
```

## Authors

See [`AUTHORS.md`](AUTHORS.md) file.

## License

See [`License.md`](License.md) file.
