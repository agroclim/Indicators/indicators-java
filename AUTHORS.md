# Authors

## David Delannoy

## Jérémie Décome
- GitLab : [@jeremie.decome](https://forgemia.inra.fr/jeremie.decome)
- [ORCID](https://orcid.org/0000-0002-7780-8548)

## Marc Lagier

## Olivier Maury
- GitLab : [@olivier.maury](https://forgemia.inra.fr/olivier.maury)
- [ORCID](https://orcid.org/0000-0001-9016-9720)
