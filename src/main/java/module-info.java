/*
 * Copyright (C) 2020 INRAE AgroClim
 *
 * This file is part of Indicators.
 *
 * Indicators is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Indicators is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Indicators. If not, see <http://www.gnu.org/licenses/>.
 */
module fr.inrae.agroclim.indicators {
    requires lombok;
    requires com.fasterxml.jackson.dataformat.csv;
    requires com.fasterxml.jackson.databind;
    requires com.fasterxml.jackson.annotation;
    requires com.fasterxml.jackson.core;
    requires transitive jakarta.xml.bind;
    requires transitive java.desktop;
    requires org.apache.commons.jexl3;
    requires org.apache.logging.log4j;
    requires org.apache.logging.log4j.core;
    requires org.glassfish.jaxb.runtime;
    requires org.json;

    exports fr.inrae.agroclim.indicators.exception;
    exports fr.inrae.agroclim.indicators.exception.type;
    exports fr.inrae.agroclim.indicators.model;
    exports fr.inrae.agroclim.indicators.model.criteria;
    exports fr.inrae.agroclim.indicators.model.criteria.visitor;
    exports fr.inrae.agroclim.indicators.model.data;
    exports fr.inrae.agroclim.indicators.model.data.climate;
    exports fr.inrae.agroclim.indicators.model.data.phenology;
    exports fr.inrae.agroclim.indicators.model.data.soil;
    exports fr.inrae.agroclim.indicators.model.function.aggregation;
    exports fr.inrae.agroclim.indicators.model.function.listener;
    exports fr.inrae.agroclim.indicators.model.function.normalization;
    exports fr.inrae.agroclim.indicators.model.indicator;
    exports fr.inrae.agroclim.indicators.model.indicator.listener;
    exports fr.inrae.agroclim.indicators.model.result;
    exports fr.inrae.agroclim.indicators.resources;
    exports fr.inrae.agroclim.indicators.util;
    exports fr.inrae.agroclim.indicators.xml;

    opens fr.inrae.agroclim.indicators.model to jakarta.xml.bind;
    opens fr.inrae.agroclim.indicators.model.criteria to jakarta.xml.bind;
    opens fr.inrae.agroclim.indicators.model.data to com.fasterxml.jackson.databind, jakarta.xml.bind;
    opens fr.inrae.agroclim.indicators.model.data.climate to jakarta.xml.bind;
    opens fr.inrae.agroclim.indicators.model.data.phenology to jakarta.xml.bind;
    opens fr.inrae.agroclim.indicators.model.data.soil to org.glassfish.jaxb.runtime;
    opens fr.inrae.agroclim.indicators.model.function.aggregation to jakarta.xml.bind;
    opens fr.inrae.agroclim.indicators.model.function.listener to jakarta.xml.bind;
    opens fr.inrae.agroclim.indicators.model.function.normalization to jakarta.xml.bind;
    opens fr.inrae.agroclim.indicators.model.indicator to jakarta.xml.bind;
}
