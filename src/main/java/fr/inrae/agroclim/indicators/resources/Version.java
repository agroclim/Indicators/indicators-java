/*
 * This file is part of Indicators.
 *
 * Indicators is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Indicators is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Indicators. If not, see <https://www.gnu.org/licenses/>.
 */
package fr.inrae.agroclim.indicators.resources;

import java.time.LocalDateTime;
import java.util.Locale;

/**
 * Information on version of Indicators.
 *
 * @author Olivier Maury
 */
public final class Version {

    /**
     * Resources from .properties file.
     */
    private static final Resources RESOURCES = new Resources(
            "fr.inrae.agroclim.indicators.resources.version",
            Locale.getDefault()
            );

    /**
     * @return date of JAR building
     */
    public static LocalDateTime getBuildDate() {
        return RESOURCES.getLocalDateTime("build.date");
    }

    /**
     * @return Subversion revision number
     */
    public static Integer getRevision() {
        try {
            return Integer.parseInt(RESOURCES.getString("build.number"));
        } catch (final NumberFormatException e) {
            return -1;
        }
    }

    /**
     * @param key property key in version info
     * @return message value or exclamation message
     */
    public static String getString(final String key) {
        return RESOURCES.getString(key);
    }

    /**
     * Get build version.
     *
     * @return version
     */
    public static String getVersion() {
        return getString("version");
    }

    /**
     * Get build version and build date of library.
     *
     * @return version and date
     */
    public static String getVersionAndBuildDate() {
        return RESOURCES.getVersionAndBuildDate();
    }

    /**
     * No constructor for utility class.
     */
    private Version() {
    }
}
