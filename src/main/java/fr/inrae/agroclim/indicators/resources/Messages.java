/*
 * This file is part of Indicators.
 *
 * Indicators is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Indicators is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Indicators. If not, see <https://www.gnu.org/licenses/>.
 */
package fr.inrae.agroclim.indicators.resources;

import java.util.Locale;

/**
 * Messages for the user.
 *
 * Last change $Date$
 *
 * @author $Author$
 * @version $Revision$
 */
public final class Messages {

    /**
     * Resources from .properties file.
     */
    private static final I18n RESOURCES = new I18n(
            "fr.inrae.agroclim.indicators.resources.messages",
            Locale.getDefault()
    );

    /**
     * Return message with inlined arguments.
     *
     * @param plural value for plural form
     * @param key message key
     * @param messageArguments arguments for the message.
     * @return message with arguments or exclamation message
     */
    public static String format(final int plural, final String key,
            final Object... messageArguments) {
        return RESOURCES.format(plural, key, messageArguments);
    }

    /**
     * Return message with inlined arguments.
     *
     * @param key
     *            message key
     * @param messageArguments
     *            arguments for the message.
     * @return message with arguments or exclamation message
     */
    public static String format(final String key,
            final Object... messageArguments) {
        return RESOURCES.format(key, messageArguments);
    }

    /**
     * Retrieve message from key.
     *
     * @param key
     *            message key
     * @return message value or exclamation message
     */
    public static String get(final String key) {
        return RESOURCES.get(key);
    }

    /**
     * Refresh bundle using current locale.
     */
    public static void refresh() {
        RESOURCES.setLocale(Locale.getDefault());
    }

    /**
     * No constructor.
     */
    private Messages() {
    }
}
