/*
 * Copyright (C) 2021 INRAE AgroClim
 *
 * This file is part of Indicators.
 *
 * Indicators is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Indicators is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Indicators. If not, see <http://www.gnu.org/licenses/>.
 */
package fr.inrae.agroclim.indicators.xml;

import java.io.InputStream;
import java.util.Map;

import javax.xml.XMLConstants;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.transform.Source;
import javax.xml.transform.sax.SAXSource;

import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;

import jakarta.xml.bind.JAXBContext;
import jakarta.xml.bind.JAXBException;
import jakarta.xml.bind.Unmarshaller;
import lombok.Setter;

/**
 * Builder pattern to configure JAXB Unmarshaller.
 *
 * Last changed : $Date$
 *
 * @author $Author$
 * @version $Revision$
 */
public final class UnmarshallerBuilder {

    /**
     * Public ID of doctype ⮕  Resource for the doctype file.
     */
    @Setter
    private Map<String, InputStream> dtds;

    /**
     * The class related to the object to serialize.
     */
    @Setter
    private Class<?>[] classesToBeBound;

    /**
     * @return JAXB unmarshaller
     * @throws JAXBException error while getting context or creating unmarshaller
     */
    public Unmarshaller build() throws JAXBException {
        final JAXBContext context = JAXBContext.newInstance(classesToBeBound);
        return context.createUnmarshaller();
    }

    /**
     * @param inputStream file stream
     * @return source to unmarshal
     * @throws ParserConfigurationException while creating SAX Parser
     * @throws org.xml.sax.SAXException while creating SAX Parser
     */
    public Source buildSource(final InputStream inputStream) throws ParserConfigurationException, SAXException {
        final SAXParserFactory parserFactory = SAXParserFactory.newInstance();
        parserFactory.setNamespaceAware(true);
        final SAXParser parser = parserFactory.newSAXParser();
        parser.setProperty(XMLConstants.ACCESS_EXTERNAL_DTD, "");
        parser.setProperty(XMLConstants.ACCESS_EXTERNAL_SCHEMA, "");
        final XMLReader xmlreader = parser.getXMLReader();
        if (dtds != null) {
            final DtdResolver dtdResolver = new DtdResolver();
            dtdResolver.setDtds(dtds);
            xmlreader.setEntityResolver(dtdResolver);
        }
        final InputSource inputSource = new InputSource(inputStream);
        return new SAXSource(xmlreader, inputSource);

    }
}
