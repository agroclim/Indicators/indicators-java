package fr.inrae.agroclim.indicators.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import fr.inrae.agroclim.indicators.exception.ErrorType;
import fr.inrae.agroclim.indicators.resources.I18n;

/**
 * Helper methods to ensure all implementations of {@link ErrorType} are well defined.
 *
 * @author Olivier Maury
 */
public interface ErrorTypeUtils {

    /**
     * Get duplicated codes for the Enum.
     *
     * @param clazz enum class
     * @return duplicated codes
     */
    static Set<String> getDuplicatedCodes(final Class<? extends Enum<?>> clazz) {
        final Set<String> duplicates = new HashSet<>();
        final Set<String> codes = new HashSet<>();
        final Enum<?>[] values = clazz.getEnumConstants();
        for (final Enum<?> value : values) {
            final ErrorType k = (ErrorType) value;
            if (codes.contains(k.getSubCode())) {
                duplicates.add(k.getSubCode());
            }
            codes.add(k.getSubCode());
        }
        return duplicates;
    }

    /**
     * Get missing translations for the Enum in the bundle for the locales.
     *
     * @param clazz      enum class
     * @param bundleName bundle name for the Properties file.
     * @param locales    locales of translations
     * @return missing translations
     */
    static Map<Locale, Map<Class<?>, List<String>>> getMissingTranslations(final Class<? extends Enum<?>> clazz,
            final String bundleName, final List<Locale> locales) {
        final Map<Locale, Map<Class<?>, List<String>>> missing = new HashMap<>();
        for (final Locale locale : locales) {
            final I18n i18n = new I18n(bundleName, locale);
            final Enum<?>[] values = clazz.getEnumConstants();
            for (final Enum<?> value : values) {
                final ErrorType k = (ErrorType) value;
                final String tr = i18n.get(k.getI18nKey());
                if (tr.startsWith("!") && tr.endsWith("!")) {
                    missing.computeIfAbsent(locale, l -> new HashMap<>());
                    missing.get(locale).computeIfAbsent(clazz, l -> new ArrayList<>());
                    missing.get(locale).get(clazz).add(k.getI18nKey());
                }
            }
        }
        return missing;
    }
}
