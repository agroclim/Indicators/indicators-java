/*
 * Copyright (C) 2021 INRAE AgroClim
 *
 * This file is part of Indicators.
 *
 * Indicators is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Indicators is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Indicators. If not, see <http://www.gnu.org/licenses/>.
 */
package fr.inrae.agroclim.indicators.util;

import java.util.Locale;

import fr.inrae.agroclim.indicators.model.Nameable;
import fr.inrae.agroclim.indicators.resources.Resources;

/**
 * Utils to get localized name of an enum.
 *
 * Last changed : $Date$
 *
 * @author $Author$
 * @version $Revision$
 */
public final class NameableEnumUtils {

    /**
     * No constructor for util class.
     */
    private NameableEnumUtils() {
    }

    /**
     * @param e enum to localize for the current locale
     * @return localized name for the model type
     */
    public static String getName(final Enum<? extends Nameable> e) {
        return getName(e, Locale.getDefault());
    }

    /**
     * @param e enum to localize
     * @param locale locale for the localized name
     * @return localized name for the model type
     */
    public static String getName(final Enum<? extends Nameable> e, final Locale locale) {
        final Resources res = new Resources("fr.inrae.agroclim.indicators.resources.messages", locale);
        final String prefix = e.getClass().getSimpleName();
        return res.getString(prefix + "." + e.name() + ".name");
    }
}
