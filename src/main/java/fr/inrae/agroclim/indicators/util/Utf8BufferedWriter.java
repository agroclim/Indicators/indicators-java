package fr.inrae.agroclim.indicators.util;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;

/**
 * Shortcut class to write UTF-8 files.
 *
 * @author Olivier Maury
 */
public class Utf8BufferedWriter extends BufferedWriter {

    /**
     * Constructor.
     *
     * @param file file to write
     * @throws FileNotFoundException if file does not exist.
     */
    public Utf8BufferedWriter(final File file) throws FileNotFoundException {
        super(new OutputStreamWriter(new FileOutputStream(file, true), StandardCharsets.UTF_8));
    }

    /**
     * Constructor.
     *
     * @param path path of file to write
     * @throws FileNotFoundException if file does not exist.
     */
    public Utf8BufferedWriter(final Path path) throws FileNotFoundException {
        super(new OutputStreamWriter(new FileOutputStream(path.toFile(), false), StandardCharsets.UTF_8));
    }

    /**
     * Constructor.
     *
     * @param fileName system-dependent name of file to write
     * @throws FileNotFoundException if file does not exist.
     */
    public Utf8BufferedWriter(final String fileName) throws FileNotFoundException {
        super(new OutputStreamWriter(new FileOutputStream(new File(fileName), true), StandardCharsets.UTF_8));
    }
}
