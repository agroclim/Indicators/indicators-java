package fr.inrae.agroclim.indicators.util;

/**
 * Doublet contains 2 items.
 *
 * Last changed : $Date$
 *
 * @author Olivier Maury
 * @param first  first item
 * @param second second item
 * @param <T> class of first item
 * @param <U> class of second item
 */
public record Doublet<T, U>(T first, U second) {

    /**
     * Return a Doublet with 2 items.
     *
     * @param first  first item
     * @param second second item
     * @param <T>    class of first item
     * @param <U>    class of second item
     * @return instantiated doublet.
     */
    public static <T, U> Doublet<T, U> of(final T first, final U second) {
        return new Doublet<>(first, second);
    }
}
