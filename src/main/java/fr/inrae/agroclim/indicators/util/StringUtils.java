/*
 * This file is part of Indicators.
 *
 * Indicators is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Indicators is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Indicators. If not, see <https://www.gnu.org/licenses/>.
 */
package fr.inrae.agroclim.indicators.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.regex.Pattern;

/**
 * String manipulation utilities.
 *
 * Last changed : $Date$
 *
 * @author $Author$
 * @version $Revision$
 */
public final class StringUtils {

    /**
     * Check if the string does not contain text.
     *
     * @param string value to check
     * @return null, empty, only spaces
     */
    public static boolean isBlank(final String string) {
        return string == null || string.isEmpty() || string.matches("\\s*");
    }

    /**
     * check if the string contains only digits.
     *
     * @param string value to check
     * @return the string is a number
     */
    public static boolean isNumeric(final String string) {
        if (isBlank(string)) {
            return false;
        }
        for (char c : string.toCharArray()) {
            if (!Character.isDigit(c)) {
                return false;
            }
        }
        return true;
    }

    /**
     * Join object representations inserting delimiter between them.
     *
     * @param objects
     *            objects to join their representation
     * @param delimiter
     *            delimiter between strings
     * @return the joined object representations
     */
    public static String join(final List<? extends Object> objects,
            final String delimiter) {
        if (objects == null) {
            return "";
        }
        StringBuilder sb = new StringBuilder();
        boolean first = true;
        for (final Object object : objects) {
            if (!first) {
                sb.append(delimiter);
            } else {
                first = false;
            }
            if (object == null) {
                sb.append("null");
            } else {
                sb.append(object.toString());
            }
        }
        return sb.toString();
    }

    /**
     * Join strings inserting delimiter between them.
     *
     * NB : for pure Java, Java 8 has StringJoiner and String.join(), there is
     * also org.apache.commons.lang.StringUtils, com.google.common.base.Joiner .
     *
     * @param strings
     *            strings to join
     * @param delimiter
     *            delimiter between strings
     * @return the joined String
     */
    public static String join(final String[] strings, final String delimiter) {
        if (strings == null) {
            return "";
        }
        StringBuilder sb = new StringBuilder();
        boolean first = true;
        for (final String string : strings) {
            if (!first) {
                sb.append(delimiter);
            } else {
                first = false;
            }
            sb.append(string);
        }
        return sb.toString();
    }

    /**
     * Join name of elements of the enum.
     *
     * @param enumClass
     *            class of the enum
     * @param delimiter
     *            delimiter between strings
     * @return the joined element names
     */
    public static String joinEnumNames(
            final Class<? extends Enum<?>> enumClass, final String delimiter) {
        StringBuilder sb = new StringBuilder();
        boolean first = true;
        for (final Enum<?> value : enumClass.getEnumConstants()) {
            if (!first) {
                sb.append(delimiter);
            } else {
                first = false;
            }
            sb.append(value.name());
        }
        return sb.toString();
    }

    /**
     * Remove trailing values which are null at the end of the array.
     *
     * @param values values to purge
     * @return values without the null values at the end
     */
    public static String[] removeTrailingNull(final String[] values) {
        List<String> purged = new ArrayList<>();
        boolean last = true;
        for (int i = values.length - 1; i >= 0; i--) {
            if (last) {
                if (StringUtils.isBlank(values[i])) {
                    continue;
                }
                last = false;
            }
            purged.add(values[i]);
        }
        Collections.reverse(purged);
        String[] purgedArr = new String[purged.size()];
        purgedArr = purged.toArray(purgedArr);
        return purgedArr;
    }
    /**
     * Check if text is a DOI reference.
     * @param text text to check
     * @return {@code true} DOI or not ({@code false})
     */
    public static boolean isDoiRef(final String text) {
        final String patternText = "\\b(10\\.\\d{4,}(?:\\.\\d+)*\\/(?:(?![\\\"&\\'])\\S)+)\\b";
        return Pattern.compile(patternText).matcher(text).matches();
    }

    /**
     * No constructor on helper class.
     */
    private StringUtils() {
    }
}
