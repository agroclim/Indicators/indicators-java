package fr.inrae.agroclim.indicators.exception.type;

import fr.inrae.agroclim.indicators.exception.ErrorCategory;
import fr.inrae.agroclim.indicators.exception.IndicatorsErrorCategory;
import fr.inrae.agroclim.indicators.model.data.ResourceManager;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * Keys from messages.properties used to warn about errors in {@link ResourceManager}.
 *
 * @author Olivier Maury
 */
@RequiredArgsConstructor
public enum ResourceErrorType implements CommonErrorType {
    /**
     * Climate, topic.
     *
     * No parameter.
     */
    CLIMATE("100", null),
    /**
     * No climate data.
     *
     * No parameter.
     */
    CLIMATE_EMPTY("110", CLIMATE),
    /**
     * No climate data for the phase.
     *
     * parameter 0 : start stage name parameter 1 : end stage name parameter 2 : start stage day parameter 3 : end stage
     * day parameter 4 : year parameter 5 : first available day parameter 6 ! last available day
     */
    CLIMATE_EMPTY_FOR_PHASE("111", CLIMATE_EMPTY),
    /**
     * Not enough data.
     *
     * No parameter.
     */
    CLIMATE_SIZE_WRONG("101", CLIMATE),
    /**
     * Years of climate, topic.
     *
     * No parameter.
     */
    CLIMATE_YEARS("120", null),
    /**
     * No years of climate.
     *
     * No parameter.
     */
    CLIMATE_YEARS_EMPTY("121", CLIMATE_YEARS),
    /**
     * Not enough data.
     *
     * No parameter.
     */
    CLIMATE_YEARS_MISSING("122", CLIMATE_YEARS),
    /**
     * Phenology, topic.
     *
     * No parameter.
     */
    PHENO("200", null),
    /**
     * No phenological data.
     *
     * No parameter.
     */
    PHENO_EMPTY("201", PHENO),
    /**
     * Years of phenology, topic.
     *
     * No parameter.
     */
    PHENO_YEARS("210", PHENO),
    /**
     * No years of phenology.
     *
     * No parameter.
     */
    PHENO_YEARS_EMPTY("211", PHENO_YEARS),
    /**
     * Setting not set.
     *
     * No parameter.
     */
    RESOURCES_CROPDEVELOPMENT_YEARS("001", null),
    /**
     * Soil, topic.
     *
     * No parameter.
     */
    SOIL("300", null),
    /**
     * Not enough data.
     *
     * No parameter.
     */
    SOIL_SIZE_WRONG("301", SOIL),
    /**
     * Variables, topic.
     *
     * No parameter.
     */
    VARIABLES("400", null),
    /**
     * No variable.
     *
     * No parameter.
     */
    VARIABLES_EMPTY("401", VARIABLES),
    /**
     * No variale.
     *
     * No parameter.
     */
    VARIABLES_MISSING("402", VARIABLES);
    /**
     * Subcode for the error.
     */
    @Getter
    private final String subCode;
    /**
     * Parent refers to the resource part.
     */
    @Getter
    private final ResourceErrorType parent;

    @Override
    public ErrorCategory getCategory() {
        return IndicatorsErrorCategory.RESOURCES;
    }

}
