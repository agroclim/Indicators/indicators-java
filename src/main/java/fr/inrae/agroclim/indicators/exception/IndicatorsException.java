package fr.inrae.agroclim.indicators.exception;

import java.io.Serializable;
import java.util.List;

/**
 * An exception with {@link ErrorMessage} to describe the error in the Indicators library to the user.
 *
 * @author Olivier Maury
 */
public class IndicatorsException extends ErrorMessageException {
    /**
     * Path of .property resource.
     */
    private static final String BUNDLE_NAME = "fr.inrae.agroclim.indicators.resources.messages";

    /**
     * UUID for Serializable.
     */
    private static final long serialVersionUID = 6030595237342400006L;

    /**
     * Constructor.
     *
     * @param errorType Error type.
     * @param arguments Arguments for the message.
     */
    public IndicatorsException(final ErrorType errorType, final Serializable... arguments) {
        super(new ErrorMessage(BUNDLE_NAME, errorType, List.of(arguments)));
    }

    /**
     * Constructor.
     *
     * @param errorType Error type.
     * @param arguments Arguments for the message.
     * @param  cause the cause (which is saved for later retrieval by the
     *         {@link #getCause()} method).  (A {@code null} value is
     *         permitted, and indicates that the cause is nonexistent or
     *         unknown.)
     */
    public IndicatorsException(final ErrorType errorType, final Throwable cause, final Serializable... arguments) {
        super(new ErrorMessage(BUNDLE_NAME, errorType, List.of(arguments)), cause);
    }
}
