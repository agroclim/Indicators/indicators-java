package fr.inrae.agroclim.indicators.exception;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * An exception with {@link ErrorMessage} to describe the error to the user.
 *
 * @author omaury
 */
@RequiredArgsConstructor
public abstract class ErrorMessageException extends Exception {
    /**
     * UUID for Serializable.
     */
    private static final long serialVersionUID = 6030595237342400005L;

    /**
     * The object with details.
     */
    @Getter
    private final ErrorMessage errorMessage;

    /**
     * Constructor.
     *
     * @param message The object with details.
     * @param  cause the cause (which is saved for later retrieval by the
     *         {@link #getCause()} method).  (A {@code null} value is
     *         permitted, and indicates that the cause is nonexistent or
     *         unknown.)

     */
    public ErrorMessageException(final ErrorMessage message, final Throwable cause) {
        super("", cause);
        this.errorMessage = message;
    }

    @Override
    public final String getMessage() {
        return errorMessage.getMessage();
    }
}
