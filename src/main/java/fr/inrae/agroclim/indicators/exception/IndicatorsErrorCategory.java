package fr.inrae.agroclim.indicators.exception;

import fr.inrae.agroclim.indicators.model.data.ResourceManager;
import fr.inrae.agroclim.indicators.model.indicator.Indicator;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * Category for error types in the Indicators library.
 *
 * Last change $Date: 2023-03-16 17:36:45 +0100 (jeu. 16 mars 2023) $
 *
 * @author omaury
 * @author $Author: omaury $
 * @version $Revision: 644 $
 */
@RequiredArgsConstructor
public enum IndicatorsErrorCategory implements ErrorCategory {
    /**
     * While loading XML.
     */
    XML("IND01"),
    /**
     * For {@link ResourceManager}.
     */
    RESOURCES("IND02"),
    /**
     * While {@link Indicator#compute()}.
     */
    COMPUTATION("IND03");

    /**
     * Category code: prefix for {@link ErrorType#getFullCode()}.
     */
    @Getter
    private final String code;

    @Override
    public String getName() {
        return name();
    }

}
