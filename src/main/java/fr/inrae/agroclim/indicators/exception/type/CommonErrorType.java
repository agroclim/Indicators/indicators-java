package fr.inrae.agroclim.indicators.exception.type;

import java.util.StringJoiner;

import fr.inrae.agroclim.indicators.exception.ErrorType;

/**
 * Common methods to implements ErrorType.
 *
 * @author Olivier Maury
 */
public interface CommonErrorType extends ErrorType {
    /**
     * @return partial I18n key for messages.properties
     */
    private String getShortKey() {
        return getName().toLowerCase().replace("_", ".");
    }

    /**
     * @return parent error
     */
    CommonErrorType getParent();

    /**
     * @return error name (as {@link Enum#name()}).
     */
    String name();

    /**
     * @return Key for Resource/I18nResource.
     */
    @Override
    default String getI18nKey() {
        final String cat = getCategory().getName().toLowerCase();
        final StringJoiner sj = new StringJoiner(".", "error.", "");
        if (!getShortKey().startsWith(cat)) {
            sj.add(cat);
        }
        if (getParent() != null && !getShortKey().startsWith(getParent().getShortKey())) {
            sj.add(getParent().getShortKey());
        }
        sj.add(getShortKey());
        return sj.toString();
    }

    @Override
    default String getName() {
        return name();
    }
}
