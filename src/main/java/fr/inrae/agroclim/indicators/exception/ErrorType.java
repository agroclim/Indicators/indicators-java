package fr.inrae.agroclim.indicators.exception;

import java.io.Serializable;

/**
 * An error type with methods to get localized message.
 *
 * Last change $Date$
 *
 * @author omaury
 * @author $Author$
 * @version $Revision$
 */
public interface ErrorType extends Serializable {
    /**
     * @return category code
     */
    ErrorCategory getCategory();

    /**
     * @return concatenation of category code and sub code.
     */
    default String getFullCode() {
        return getCategory().getCode() + getSubCode();
    }

    /**
     * @return key used by I18n to get message.
     */
    String getI18nKey();

    /**
     * @return short name, formatted as enum name.
     */
    String getName();

    /**
     * @return sub code for the error.
     */
    String getSubCode();
}
