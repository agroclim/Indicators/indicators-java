package fr.inrae.agroclim.indicators.exception.type;

import fr.inrae.agroclim.indicators.exception.ErrorCategory;
import fr.inrae.agroclim.indicators.exception.IndicatorsErrorCategory;
import fr.inrae.agroclim.indicators.model.indicator.CompositeIndicator;
import fr.inrae.agroclim.indicators.model.indicator.Indicator;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * Keys from messages.properties used to warn about errors in {@link Indicator#compute()}.
 *
 * @author Olivier Maury
 */
@RequiredArgsConstructor
public enum ComputationErrorType implements CommonErrorType {
    /**
     * Definition of the indicator is not good.
     *
     * Parameter 1 : explaination.
     */
    WRONG_DEFINITION(null, "001"),
    /**
     * When input causes an exception.
     *
     * No parameter.
     */
    INPUT(null, "100"),
    /**
     * When an indicator in a {@link CompositeIndicator} fails to compute.
     *
     * Parameter 1 : indicator id.
     */
    COMPOSITE_COMPUTATION(INPUT, "101"),
    /**
     * Criteria should be NoCriteria or SimpleCriteria.
     *
     * Parameter 1 : indicator id.
     */
    CRITERIA_ISNT_NOCRITERIA_SIMPLECRITERIA(WRONG_DEFINITION, "002"),
    /**
     * Criteria should not be null.
     *
     * No parameter.
     */
    CRITERIA_NULL(WRONG_DEFINITION, "003"),
    /**
     * Daily data must not be null.
     *
     * No parameter.
     */
    DATA_NULL(INPUT, "113"),
    /**
     * Dividend indicator should never be null.
     *
     * No parameter.
     */
    QUOTIENT_DIVIDEND_NULL(WRONG_DEFINITION, "005"),
    /**
     * Computation of dividend failed.
     *
     * Parameter 1 : indicator id.
     */
    QUOTIENT_DIVIDEND_EXCEPTION(INPUT, "110"),
    /**
     * Computation of divisor failed.
     *
     * Parameter 1 : indicator id.
     */
    QUOTIENT_DIVISOR_EXCEPTION(INPUT, "111"),
    /**
     * Cannot compute quotient as result of divisor is zero.
     *
     * Parameter 1 : indicator id.
     */
    QUOTIENT_DIVISOR_ZERO(INPUT, "112"),
    /**
     * Divisor indicator should never be null.
     *
     * No parameter.
     */
    QUOTIENT_DIVISOR_NULL(WRONG_DEFINITION, "006"),
    /**
     * Execution of formula failed.
     *
     * parameter 1 : expression
     */
    FORMULA(null, "200"),
    /**
     * Agregation to apply must not be null.
     *
     * No parameter.
     */
    FORMULA_AGGREGATION_NULL(FORMULA, "211"),
    /**
     * Formula expression must not be null.
     *
     * No parameter.
     */
    FORMULA_EXPRESSION_NULL(FORMULA, "221"),
    /**
     * Formula expression must not be blank.
     *
     * No parameter.
     */
    FORMULA_EXPRESSION_BLANK(FORMULA, "222"),
    /**
     * Parsing error due to missing parenthesis.
     *
     * parameter 1 : expression
     */
    FORMULA_EXPRESSION_PARENTHESIS(FORMULA, "223"),
    /**
     * Parsing error.
     *
     * parameter 1 : expression
     */
    FORMULA_EXPRESSION_PARSING(FORMULA, "224"),
    /**
     * Unknown function.
     *
     * parameter 1 : expression
     * parameter 2 : function
     */
    FORMULA_FUNCTION_UNKNOWN(FORMULA, "231"),
    /**
     * Unknown variable.
     *
     * parameter 1 : expression
     * parameter 2 : variable
     */
    FORMULA_VARIABLE_UNDEFINED(FORMULA, "241"),
    /**
     * Threshold must not be null.
     *
     * No parameter.
     */
    THRESHOLD_NULL(WRONG_DEFINITION, "004"),
    /**
     * Variable name must not be null.
     */
    VARIABLE_NAME_NULL(WRONG_DEFINITION, "007"),
    /**
     * Value for the variable must not be null.
     *
     * parameter 1 : date
     * parameter 2 : variable name
     */
    VARIABLE_VALUE_NULL(INPUT, "114");

    /**
     * Parent refers to the resource part.
     */
    @Getter
    private final ComputationErrorType parent;

    /**
     * Subcode for the error.
     */
    @Getter
    private final String subCode;

    @Override
    public ErrorCategory getCategory() {
        return IndicatorsErrorCategory.COMPUTATION;
    }

}
