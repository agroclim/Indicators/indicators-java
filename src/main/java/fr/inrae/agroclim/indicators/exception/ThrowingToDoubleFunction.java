package fr.inrae.agroclim.indicators.exception;

import java.util.function.ToDoubleFunction;

/**
 * ToDoubleFunction to handle exceptions.
 *
 * Inspired by https://www.baeldung.com/java-lambda-exceptions.
 *
 * @author Olivier Maury
 * @param <T> the type of the input to the function
 * @param <E> the type of the thrown exception
 */
@FunctionalInterface
public interface ThrowingToDoubleFunction<T, E extends Exception> {

    /**
     * Applies this function to the given argument.
     *
     * @param value the function argument
     * @return the function result
     * @throws E exception
     */
    double applyAsDouble(T value) throws E;

    /**
     * @param <U> the type of the input to the function
     * @param throwingFunction the function
     * @return function which throws checked exception
     */
    static <U> ToDoubleFunction wrap(final ThrowingToDoubleFunction<U, Exception> throwingFunction) {
        return (ToDoubleFunction) (Object value) -> {
            try {
                return throwingFunction.applyAsDouble((U) value);
            } catch (final Exception ex) {
                throw new RuntimeException(ex);
            }
        };
    }
}
