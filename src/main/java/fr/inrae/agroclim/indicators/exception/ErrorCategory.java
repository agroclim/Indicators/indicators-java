package fr.inrae.agroclim.indicators.exception;

import fr.inrae.agroclim.indicators.resources.I18n;

/**
 * A category for error types.
 *
 * Last change $Date$
 *
 * @author omaury
 * @author $Author$
 * @version $Revision$
 */
public interface ErrorCategory {
    /**
     * @param i18n localized messages
     * @return category description for the category code in I18n
     */
    default String getCategory(final I18n i18n) {
        return i18n.get("error.category." + getName().toLowerCase());
    }

    /**
     * @return category code: prefix for {@link ErrorType#getFullCode()}.
     */
    String getCode();

    /**
     * @return short name, formatted as enum name.
     */
    String getName();

}
