/*
 * This file is part of Indicators.
 *
 * Indicators is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Indicators is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Indicators. If not, see <https://www.gnu.org/licenses/>.
 */
package fr.inrae.agroclim.indicators.exception;

import java.io.Serializable;
import java.util.Collection;
import java.util.Locale;

import org.json.JSONObject;

import fr.inrae.agroclim.indicators.resources.I18n;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

/**
 * Error messages for the user.
 *
 * Last change $Date$
 *
 * @author $Author$
 * @version $Revision$
 */
@EqualsAndHashCode
@RequiredArgsConstructor
@ToString
public final class ErrorMessage implements Serializable {

    /**
     * UUID for Serializable.
     */
    private static final long serialVersionUID = 6030595237342400004L;

    /**
     * Get JSON representation of ErrorMessage.
     *
     * @param bundleName Path of .property resource.
     * @param errorType  error type
     * @param arguments  Arguments for the message.
     * @return JSON representation
     */
    public static String toJSON(final String bundleName, final ErrorType errorType,
            final Collection<Serializable> arguments) {
        return new ErrorMessage(bundleName, errorType, arguments).toJSON();
    }

    /**
     * Path of .property resource.
     */
    @Getter
    private final String bundleName;

    /**
     * Error type.
     */
    @Getter
    private final ErrorType type;

    /**
     * Arguments for the message.
     */
    @Getter
    private final Collection<Serializable> arguments;

    /**
     * Get localized message using the Resources for bundleName and default
     * locale.
     *
     * @return localized message
     */
    public String getMessage() {
        return getMessage(Locale.getDefault());
    }

    /**
     * Get localized message using a Resources.
     *
     * @param resources resources from .properties file.
     * @return localized message
     */
    public String getMessage(final I18n resources) {
        if (arguments != null) {
            return resources.format(type.getI18nKey(), arguments.toArray());
        } else {
            return resources.get(type.getI18nKey());
        }
    }

    /**
     * Get localized message using the Resources for bundleName and the given locale.
     *
     * @param locale locale to localize the message
     * @return localized message
     */
    public String getMessage(final Locale locale) {
        return getMessage(new I18n(bundleName, locale));
    }

    /**
     * Serialize to JSON.
     *
     * @return JSON representation
     */
    public String toJSON() {
        final JSONObject json = new JSONObject();
        json.put("bundleName", bundleName);
        if (type.getCategory() != null) {
            final JSONObject cat = new JSONObject();
            cat.put("code", type.getCategory().getCode());
            cat.put("name", type.getCategory().getName());
            json.put("category", cat);
        }
        final JSONObject error = new JSONObject();
        error.put("code", type.getFullCode());
        error.put("name", type.getName());
        json.put("error", error);
        if (arguments != null) {
            json.put("arguments", arguments.stream().map(Object::toString).toList());
        }
        return json.toString(2);
    }
}
