package fr.inrae.agroclim.indicators.exception.type;

import fr.inrae.agroclim.indicators.exception.ErrorCategory;
import fr.inrae.agroclim.indicators.exception.IndicatorsErrorCategory;
import fr.inrae.agroclim.indicators.model.indicator.Indicator;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * Keys from messages.properties used to warn about errors in {@link Indicator#compute()}.
 *
 * @author Olivier Maury
 */
@RequiredArgsConstructor
public enum XmlErrorType implements CommonErrorType {
    /**
     * XML file not found.
     *
     * Parameter 0 : file path
     */
    FILE_NOT_FOUND(null, "001"),
    /**
     * Unable to load.
     *
     * No parameter.
     */
    UNABLE_TO_LOAD(null, "002"),
    /**
     * Unable to serialize.
     *
     * No parameter.
     */
    UNABLE_TO_SERIALIZE(null, "003");
    /**
     * Parent refers to the resource part.
     */
    @Getter
    private final XmlErrorType parent;

    /**
     * Subcode for the error.
     */
    @Getter
    private final String subCode;

    @Override
    public ErrorCategory getCategory() {
        return IndicatorsErrorCategory.XML;
    }
}
