/*
 * This file is part of Indicators.
 *
 * Indicators is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Indicators is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Indicators. If not, see <https://www.gnu.org/licenses/>.
 */
package fr.inrae.agroclim.indicators.model.indicator;

import java.util.List;
import java.util.Map;

import fr.inrae.agroclim.indicators.model.Knowledge;
import fr.inrae.agroclim.indicators.model.Parameter;
import fr.inrae.agroclim.indicators.util.Doublet;

/**
 * A simple indicator without parameters.
 *
 * Last change $Date$
 *
 * @author $Author$
 * @version $Revision$
 */
public abstract class SimpleIndicatorWithoutParameters extends SimpleIndicator {
    /**
     * UUID for Serializable.
     */
    private static final long serialVersionUID = 6030595237342422017L;

    @Override
    public final List<Doublet<Parameter, Number>> getParameterDefaults() {
        // no substitution
        return List.of();
    }

    @Override
    public final List<Parameter> getParameters() {
        // no substitution
        return List.of();
    }

    @Override
    public final Map<String, Double> getParametersValues() {
        // no substitution
        return Map.of();
    }

    @Override
    public final void setParametersFromKnowledge(final Knowledge knowledge) {
        // Nothing to do
    }

    @Override
    public final void setParametersValues(final Map<String, Double> values) {
        // Nothing to do
    }

    @Override
    public final String toStringTree(final String indent) {
        return toStringTreeBase(indent);
    }

    @Override
    public final void removeParameter(final Parameter param) {
        // Nothing to do
    }

}
