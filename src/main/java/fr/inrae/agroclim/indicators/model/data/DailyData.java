package fr.inrae.agroclim.indicators.model.data;

/**
 * Class with date time methods.
 *
 * Last changed : $Date$
 *
 * @author $Author$
 * @version $Revision$
 */
public abstract class DailyData extends HourlyData {

    /**
     * UUID for Serializable.
     */
    private static final long serialVersionUID = -6030595237342420001L;

    /**
     * Constructor.
     */
    protected DailyData() {
        super();
        setHour(0);
    }

    /**
     * Copy constructor.
     *
     * @param data instance to copy
     */
    protected DailyData(final DailyData data) {
        super(data);
        setHour(0);
    }
}
