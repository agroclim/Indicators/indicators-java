/*
 * This file is part of Indicators.
 *
 * Indicators is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Indicators is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Indicators. If not, see <https://www.gnu.org/licenses/>.
 */
package fr.inrae.agroclim.indicators.model.indicator;

import fr.inrae.agroclim.indicators.exception.IndicatorsException;
import fr.inrae.agroclim.indicators.model.data.DailyData;
import fr.inrae.agroclim.indicators.model.data.Resource;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlRootElement;

/**
 * Compute number of days for the criteria.
 *
 * Criteria to compute number of days.
 *
 * Last changed : $Date$
 *
 * @author jucufi
 * @author $Author$
 * @version $Revision$
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public final class NumberOfDays extends SimpleIndicatorWithCriteria implements Detailable {
    /**
     * UUID for Serializable.
     */
    private static final long serialVersionUID = 6030595237342422010L;

    /**
     * Constructor.
     */
    public NumberOfDays() {
        super();
    }

    @Override
    public NumberOfDays clone() throws CloneNotSupportedException {
        return (NumberOfDays) super.clone();
    }

    @Override
    public double computeSingleValue(final Resource<? extends DailyData> res) throws IndicatorsException {
        if (getCriteria() == null) {
            return res.getData().size();
        }
        int cpt = 0;
        boolean resultCriteria;
        for (final DailyData data : res.getData()) {
            resultCriteria = getCriteria().eval(data);
            if (resultCriteria) {
                cpt += 1;
            }
        }

        return cpt;
    }

}
