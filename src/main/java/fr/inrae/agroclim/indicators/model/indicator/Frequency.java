/*
 * This file is part of Indicators.
 *
 * Indicators is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Indicators is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Indicators. If not, see <https://www.gnu.org/licenses/>.
 */
package fr.inrae.agroclim.indicators.model.indicator;

import java.util.List;
import java.util.Map;
import java.util.Set;

import fr.inrae.agroclim.indicators.exception.IndicatorsException;
import fr.inrae.agroclim.indicators.model.Knowledge;
import fr.inrae.agroclim.indicators.model.Parameter;
import fr.inrae.agroclim.indicators.model.data.DailyData;
import fr.inrae.agroclim.indicators.model.data.Resource;
import fr.inrae.agroclim.indicators.model.data.Variable;
import fr.inrae.agroclim.indicators.util.Doublet;
import jakarta.xml.bind.annotation.XmlRootElement;
import lombok.Getter;
import lombok.Setter;

/**
 * % of number of days/period.
 *
 * @author jucufi
 */
@XmlRootElement
public final class Frequency extends SimpleIndicator implements Detailable {

    /**
     * UUID for Serializable.
     */
    private static final long serialVersionUID = 6030595237342422006L;

    /**
     * Number of days/period.
     */
    @Getter
    @Setter
    private NumberOfDays numberOfDays;

    /**
     * Constructor.
     */
    public Frequency() {
        super();
        numberOfDays = new NumberOfDays();
    }

    @Override
    public Frequency clone() throws CloneNotSupportedException {
        final Frequency clone = (Frequency) super.clone();
        if (numberOfDays != null) {
            clone.numberOfDays = numberOfDays.clone();
        }
        return clone;
    }

    @Override
    public double computeSingleValue(final Resource<? extends DailyData> data) throws IndicatorsException {
        final double cent = 100.;
        final int days = data.getData().size();
        return numberOfDays.compute(data) * cent / days;
    }

    @Override
    public List<Doublet<Parameter, Number>> getParameterDefaults() {
        return numberOfDays.getParameterDefaults();
    }

    @Override
    public List<Parameter> getParameters() {
        return numberOfDays.getParameters();
    }

    @Override
    public Map<String, Double> getParametersValues() {
        return numberOfDays.getParametersValues();
    }

    @Override
    public Set<Variable> getVariables() {
        return numberOfDays.getVariables();
    }

    @Override
    public boolean isComputable(final Resource<? extends DailyData> res) {
        return numberOfDays.isComputable(res);
    }

    @Override
    public void setParametersFromKnowledge(final Knowledge knowledge) {
        numberOfDays.setParametersFromKnowledge(knowledge);
    }

    @Override
    public void setParametersValues(final Map<String, Double> values) {
        numberOfDays.setParametersValues(values);
    }

    @Override
    public String toStringTree(final String indent) {
        final StringBuilder sb = new StringBuilder();
        sb.append(toStringTreeBase(indent));

        if (numberOfDays != null) {
            sb.append(indent).append("  numberOfDays:\n");
            sb.append(numberOfDays.toStringTree(indent + "  "))
            .append("\n");
        }
        return sb.toString();
    }

    @Override
    public void removeParameter(final Parameter param) {
        // Suppression du paramètre présent dans l'attribut (noeud XML) numberOfDays
        this.numberOfDays.removeParameter(param);
    }

}
