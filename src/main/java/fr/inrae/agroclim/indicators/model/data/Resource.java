/*
 * This file is part of Indicators.
 *
 * Indicators is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Indicators is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Indicators. If not, see <https://www.gnu.org/licenses/>.
 */
package fr.inrae.agroclim.indicators.model.data;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import lombok.Getter;

/**
 * Resource is a data source for climate, soil or phenology.
 * @param <T>
 *            data type
 */
public abstract class Resource<T extends Cloneable> extends DataLoadingListenerHandler implements Cloneable {
    /**
     * Default CSV separator.
     */
    public static final String DEFAULT_SEP = ";";

    /**
     * UUID for Serialisable.
     */
    private static final long serialVersionUID = 7164381207421561734L;

    /**
     * Daily data.
     */
    @Getter
    private final List<T> data;

    /**
     * Missing climatic variables, to check in aggregation indicators and
     * criteria.
     */
    @Getter
    private final List<String> missingVariables;

    /**
     * Computed list of years.
     */
    @Getter
    private final List<Integer> years;

    /**
     * Constructor.
     */
    protected Resource() {
        data = new ArrayList<>();
        years = new ArrayList<>();
        missingVariables = new ArrayList<>();
    }

    /**
     * @param year
     *            year of daily data
     */
    protected final void addYear(final Integer year) {
        if (!years.contains(year)) {
            years.add(year);
        }
    }

    @Override
    @SuppressWarnings("checkstyle:DesignForExtension")
    public Resource<T> clone() throws CloneNotSupportedException {
        @SuppressWarnings("unchecked")
        final Resource<T> clone = (Resource<T>) super.clone();
        clone.setMissingVariables(getMissingVariables());
        clone.getYears().addAll(getYears());
        return clone;
    }

    /**
     * @return true if the resource contains no daily data.
     */
    public final boolean isEmpty() {
        return data.isEmpty();
    }

    /**
     * @param variables
     *            Missing variables, to check in aggregation indicators and
     *            criteria.
     */
    public final void setMissingVariables(final Collection<String> variables) {
        missingVariables.clear();
        missingVariables.addAll(variables);
    }
}
