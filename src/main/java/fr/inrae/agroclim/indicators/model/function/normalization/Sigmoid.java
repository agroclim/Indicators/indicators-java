/*
 * This file is part of Indicators.
 *
 * Indicators is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Indicators is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Indicators. If not, see <https://www.gnu.org/licenses/>.
 */
package fr.inrae.agroclim.indicators.model.function.normalization;

import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlRootElement;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Normalize with 1 / (1 + exp((a - X) / b)).
 */
@XmlRootElement
@EqualsAndHashCode(
        callSuper = false,
        of = {"sigmoidA", "sigmoidB"}
        )
@ToString
public final class Sigmoid extends NormalizationFunction {
    /**
     * UUID for Serializable.
     */
    private static final long serialVersionUID = 4872847709393688043L;

    /**
     * A in 1 / (1 + exp((a - X) / b)).
     */
    @XmlAttribute
    @Getter
    @Setter
    private double sigmoidA;

    /**
     * B in 1 / (1 + exp((a - X) / b)).
     */
    @XmlAttribute
    @Getter
    @Setter
    private double sigmoidB;

    /**
     * Constructor.
     */
    public Sigmoid() {
    }

    /**
     * Constructor.
     *
     * @param a A in 1 / (1 + exp((a - X) / b)).
     * @param b B in 1 / (1 + exp((a - X) / b)).
     */
    public Sigmoid(final double a, final double b) {
        this();
        this.sigmoidA = a;
        this.sigmoidB = b;
    }

    /**
     * Constructor.
     *
     * @param s Sigmoid to clone
     */
    public Sigmoid(final Sigmoid s) {
        this(s.getSigmoidA(), s.getSigmoidB());
    }

    @Override
    public Sigmoid clone() {
        return new Sigmoid(this);
    }

    @Override
    public String getFormulaMathML() {
        return ""
                + "<mfrac>"
                + " <mn>1</mn>"
                + " <mrow>"
                + "  <mi>1</mi>"
                + "  <mo>+</mo>"
                + "  <msup>"
                + "   <mi>e</mi>"
                + "   <mfrac>"
                + "    <mrow>"
                + "     <mi>a</mi>"
                + "     <mo>-</mo>"
                + "     <mi>x</mi>"
                + "    </mrow>"
                + "    <mn>b</mn>"
                + "   </mfrac>"
                + "  </msup>"
                + " </mrow>"
                + "</mfrac>";
    }

    @Override
    public double normalize(final double value) {
        return 1d / (1d + Math.exp((sigmoidA - value) / sigmoidB));
    }
}
