package fr.inrae.agroclim.indicators.model.data;

import java.util.Set;

/**
 * Provides climatic/soil variables.
 *
 * Last changed : $Date$
 *
 * @author $Author$
 * @version $Revision$
 */
public interface HasProvidedVariables {

    /**
     * @return variables provided by the loader
     */
    Set<Variable> getProvidedVariables();
}
