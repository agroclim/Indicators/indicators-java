/*
 * Copyright (C) 2021 INRAE AgroClim
 *
 * This file is part of Indicators.
 *
 * Indicators is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Indicators is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Indicators. If not, see <http://www.gnu.org/licenses/>.
 */
package fr.inrae.agroclim.indicators.model.indicator;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import fr.inrae.agroclim.indicators.exception.IndicatorsException;
import fr.inrae.agroclim.indicators.model.Knowledge;
import fr.inrae.agroclim.indicators.model.Parameter;
import fr.inrae.agroclim.indicators.model.data.DailyData;
import fr.inrae.agroclim.indicators.model.data.Resource;
import fr.inrae.agroclim.indicators.model.data.Variable;
import fr.inrae.agroclim.indicators.util.Doublet;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlRootElement;
import jakarta.xml.bind.annotation.XmlType;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

/**
 * To inject a value into an indicator.
 *
 * Last changed : $Date$
 *
 * @author $Author$
 * @version $Revision$
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = {"parameterId", "parameterValue"})
@Log4j2
public final class InjectedParameter extends SimpleIndicator {

    /**
     * UUID for Serializable.
     */
    private static final long serialVersionUID = 6030595237342422008L;

    /**
     * Id of variety parameter or cell parameter to use.
     */
    @Setter
    @Getter
    private String parameterId;

    /**
     * Value of parameter for the variety or the cell.
     */
    @Setter
    @Getter
    private Double parameterValue;

    @Override
    public double computeSingleValue(final Resource<? extends DailyData> data) throws IndicatorsException {
        if (parameterValue == null) {
            return 0;
        }
        return parameterValue;
    }

    @Override
    public List<Doublet<Parameter, Number>> getParameterDefaults() {
        if (getParameters() == null) {
            return List.of();
        }
        final Optional<Parameter> found = getParameters().stream() //
                .filter(a -> parameterId.equals(a.getAttribute())) //
                .findFirst();
        if (found.isEmpty()) {
            return List.of();
        }
        return List.of(Doublet.of(found.get(), parameterValue));
    }

    @Override
    public List<Parameter> getParameters() {
        final Parameter param = new Parameter();
        param.setAttribute(parameterId);
        param.setId(parameterId);
        return Arrays.asList(param);
    }

    @Override
    public Map<String, Double> getParametersValues() {
        final Map<String, Double> params = new HashMap<>();
        params.put(parameterId, parameterValue);
        return params;
    }

    @Override
    public Set<Variable> getVariables() {
        return new HashSet<>();
    }

    @Override
    public boolean isComputable(final Resource<? extends DailyData> res) {
        // always good.
        return true;
    }

    @Override
    public void setParametersFromKnowledge(final Knowledge knowledge) {
        final InjectedParameter ind = (InjectedParameter) knowledge.getIndicator(getId());
        setParameterId(ind.getParameterId());
        parameterValue = ind.getValue();
    }

    @Override
    public void setParametersValues(final Map<String, Double> values) {
        if (values != null && values.containsKey(parameterId)) {
            parameterValue = values.get(parameterId);
        }
    }

    @Override
    public String toStringTree(final String indent) {
        final StringBuilder sb = new StringBuilder();
        sb.append(toStringTreeBase(indent));
        sb.append(indent).append("  parameterId: ").append(parameterId).append("\n");
        sb.append(indent).append("  parameterValue: ").append(parameterValue).append("\n");
        return sb.toString();
    }

    @Override
    public void removeParameter(final Parameter param) {
        if (this.parameterId.equals(param.getId())) {
            parameterId = null;
            parameterValue = null;
        }
    }
}
