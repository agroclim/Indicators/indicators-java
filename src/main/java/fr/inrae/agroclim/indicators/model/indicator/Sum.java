/*
 * This file is part of Indicators.
 *
 * Indicators is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Indicators is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Indicators. If not, see <https://www.gnu.org/licenses/>.
 */
package fr.inrae.agroclim.indicators.model.indicator;

import java.util.stream.DoubleStream;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlRootElement;

/**
 * Somme d'une variable définie dans un SimpleCriteria.
 *
 * @author jcufi
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public final class Sum extends AggregationIndicator {
    /**
     * UUID for Serialize.
     */
    private static final long serialVersionUID = 6030595237342422018L;

    @Override
    protected double aggregate(final DoubleStream s) {
        return s.sum();
    }

    @Override
    public Sum clone() throws CloneNotSupportedException {
        return (Sum) super.clone();
    }
}
