/*
 * This file is part of Indicators.
 *
 * Indicators is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Indicators is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Indicators. If not, see <https://www.gnu.org/licenses/>.
 */
package fr.inrae.agroclim.indicators.model.function.normalization;

import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlRootElement;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Normalize with A.exp(B/x).
 */
@XmlRootElement
@ToString
public final class Exponential extends NormalizationFunction {
    /**
     * UUID for Serializable.
     */
    private static final long serialVersionUID = 4872847709393688045L;

    /**
     * A in A.exp(B/x).
     */
    @XmlAttribute
    @Getter
    @Setter
    private double expA;

    /**
     * B in A.exp(B/x).
     */
    @XmlAttribute
    @Getter
    @Setter
    private double expB;

    /**
     * Constructor.
     */
    public Exponential() {
    }

    /**
     * Constructor.
     *
     * @param a A in A.exp(B/x).
     * @param b V in A.exp(B/x).
     */
    public Exponential(final double a, final double b) {
        this();
        this.expA = a;
        this.expB = b;
    }

    /**
     * Constructor.
     *
     * @param e exponential to clone
     */
    public Exponential(final Exponential e) {
        this(e.getExpA(), e.getExpB());
    }

    @Override
    public Exponential clone() {
        return new Exponential(this);
    }

    @Override
    public String getFormulaMathML() {
        return ""
                + "<mrow>"
                + " <mi>a</mi>"
                + " <mo>.</mo>"
                + " <msup>"
                + "  <mi>e</mi>"
                + "  <mrow>"
                + "   <mi>b</mi>"
                + "   <mo>/</mo>"
                + "   <mi>x</mi>"
                + "  </mrow>"
                + " </msup>"
                + "</mrow>";
    }

    @Override
    public double normalize(final double value) {
        return Math.max(0, Math.min(expA * Math.exp(expB / value), 1));
    }
}
