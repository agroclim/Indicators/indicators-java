/*
 * This file is part of Indicators.
 *
 * Indicators is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Indicators is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Indicators. If not, see <https://www.gnu.org/licenses/>.
 */
package fr.inrae.agroclim.indicators.model.indicator;

import java.util.HashSet;
import java.util.Set;

import fr.inrae.agroclim.indicators.exception.IndicatorsException;
import fr.inrae.agroclim.indicators.model.data.DailyData;
import fr.inrae.agroclim.indicators.model.data.Resource;
import fr.inrae.agroclim.indicators.model.data.Variable;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;
import lombok.Getter;

/**
 * Moyenne de différence de variables.
 *
 * @author jcufi
 */
@XmlType(propOrder = {"variable1", "variable2"})
public final class AverageOfDiff extends SimpleIndicatorWithoutParameters implements Detailable {

    /**
     * UUID for Serializable.
     */
    private static final long serialVersionUID = 6030595237342422002L;

    /**
     * Minuend in subtraction.
     */
    @XmlElement
    @Getter
    private Variable variable1;

    /**
     * Subtrahend in subtraction.
     */
    @XmlElement
    @Getter
    private Variable variable2;

    /**
     * Constructor.
     */
    public AverageOfDiff() {
        super();
    }

    @Override
    public AverageOfDiff clone() throws CloneNotSupportedException {
        final AverageOfDiff clone = (AverageOfDiff) super.clone();
        clone.variable1 = variable1;
        clone.variable2 = variable2;
        return clone;
    }

    @Override
    public double computeSingleValue(final Resource<? extends DailyData> res) throws IndicatorsException {
        double value = 0;
        for (final DailyData data : res.getData()) {
            value += data.getValue(variable1)
                    - data.getValue(variable2);
        }
        return value / res.getData().size();
    }

    @Override
    public Set<Variable> getVariables() {
        final Set<Variable> variables = new HashSet<>();
        variables.add(variable1);
        variables.add(variable2);
        return variables;
    }

    @Override
    public boolean isComputable(final Resource<? extends DailyData> res) {
        return !res.getMissingVariables().contains(variable1.getName())
                && !res.getMissingVariables().contains(variable2.getName());
    }

    /**
     * @param variable
     *            variable name for minuend
     */
    public void setVariable1(final String variable) {
        this.variable1 = Variable.getByName(variable);
    }

    /**
     * @param variable
     *            variable for minuend
     */
    public void setVariable1(final Variable variable) {
        this.variable1 = variable;
    }

    /**
     * @param variable
     *            variable name for subtrahend
     */
    public void setVariable2(final String variable) {
        this.variable2 = Variable.getByName(variable);
    }

    /**
     * @param variable
     *            variable for subtrahend
     */
    public void setVariable2(final Variable variable) {
        this.variable2 = variable;
    }
}
