/*
 * This file is part of Indicators.
 *
 * Indicators is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Indicators is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Indicators. If not, see <https://www.gnu.org/licenses/>.
 */
package fr.inrae.agroclim.indicators.model.function.aggregation;

import java.io.Serializable;
import java.util.Map;

import fr.inrae.agroclim.indicators.exception.IndicatorsException;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlAttribute;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

/**
 * Function to aggregate indicator values (eg.: JEXLFunction).
 *
 * @author jcufi
 */
@XmlAccessorType(XmlAccessType.FIELD)
@EqualsAndHashCode(
        callSuper = false,
        of = {"expression"}
        )
public abstract class AggregationFunction implements Serializable {
    /**
     * UUID for Serializable.
     */
    private static final long serialVersionUID = 3308126437081517193L;

    /**
     * Aggregation expression.
     */
    @XmlAttribute
    @Getter
    @Setter
    private String expression;

    /**
     * @param values
     *            values to use for aggregation
     * @return aggregated value
     * @throws IndicatorsException
     *             exception
     */
    public abstract double aggregate(Map<String, Double> values) throws IndicatorsException;

    /**
     * @return function is valid
     */
    public abstract boolean isValid();
}
