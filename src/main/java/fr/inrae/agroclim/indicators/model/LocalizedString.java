/*
 * This file is part of Indicators.
 *
 * Indicators is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Indicators is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Indicators. If not, see <https://www.gnu.org/licenses/>.
 */
package fr.inrae.agroclim.indicators.model;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlValue;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Optional localized string for indicator or parameter.
 *
 * Last change $Date$
 *
 * @author $Author$
 * @version $Revision$
 */
@XmlAccessorType(XmlAccessType.FIELD)
@EqualsAndHashCode(
        callSuper = false,
        of = {"lang", "value"}
        )
@AllArgsConstructor
@NoArgsConstructor
public final class LocalizedString implements Cloneable, Serializable {

    /**
     * UUID for Serializable.
     */
    private static final long serialVersionUID = 9205643160821888598L;

    /**
     * @param list list of localized strings
     * @param languageCode lang code
     * @return string for the lang or the first if not set
     */
    public static String getString(final List<LocalizedString> list,
            final String languageCode) {
        if (list == null || list.isEmpty()) {
            return null;
        }
        for (final LocalizedString obj : list) {
            if (Objects.equals(obj.getLang(), languageCode)) {
                return obj.getValue();
            }
        }
        return list.get(0).getValue();
    }

    /**
     * Language code ("en", "fr").
     */
    @XmlAttribute(name = "lang",
            namespace = javax.xml.XMLConstants.XML_NS_URI)
    @Getter
    @Setter
    private String lang;

    /**
     * Localized string.
     */
    @XmlValue
    @Getter
    @Setter
    private String value;

    @Override
    public LocalizedString clone() throws CloneNotSupportedException {
        final LocalizedString clone = (LocalizedString) super.clone();
        clone.lang = lang;
        clone.value = value;
        return clone;
    }
}
