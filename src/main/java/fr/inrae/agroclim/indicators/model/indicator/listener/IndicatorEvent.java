/*
 * This file is part of Indicators.
 *
 * Indicators is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Indicators is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Indicators. If not, see <https://www.gnu.org/licenses/>.
 */
package fr.inrae.agroclim.indicators.model.indicator.listener;

import java.util.Objects;

import fr.inrae.agroclim.indicators.model.Evaluation;
import fr.inrae.agroclim.indicators.model.indicator.CompositeIndicator;
import fr.inrae.agroclim.indicators.model.indicator.Indicator;
import lombok.Getter;

/**
 * Event when something changed for an indicator.
 *
 * Last changed : $Date$
 *
 * @author $Author$
 * @version $Revision$
 */
public final class IndicatorEvent {
    /**
     * Type of event.
     */
    public enum Type {
        /**
         * An indicator has been added to the evaluation.
         */
        ADD(Indicator.class),
        /**
         * An aggregation is missing for the indicator.
         */
        AGGREGATION_MISSING(CompositeIndicator.class),
        /**
         * Properties of an indicator has been changed.
         */
        CHANGE(Indicator.class),
        /**
         * There is not any climatic indicator for the phase.
         */
        CLIMATIC_MISSING(CompositeIndicator.class),
        /**
         * The evaluation failed computing.
         */
        COMPUTE_FAILURE(Evaluation.class),
        /**
         * The evaluation start computing.
         */
        COMPUTE_START(Evaluation.class),
        /**
         * The evaluation succeeded computing.
         */
        COMPUTE_SUCCESS(Evaluation.class),
        /**
         * The indicator is not computable.
         */
        NOT_COMPUTABLE(Indicator.class),
        /**
         * The evaluation does not contain any phase.
         */
        PHASE_MISSING(Evaluation.class),
        /**
         * An indicator has been removed from the evaluation.
         */
        REMOVE(Indicator.class),
        /**
         * Value of indicator was updated.
         */
        UPDATED_VALUE(Indicator.class);

        /**
         * Class of indicator, to prevent wrong arguments.
         */
        @Getter
        private final Class<? extends Indicator> clazz;

        /**
         * Constructor.
         *
         * @param value class of indicator
         */
        Type(final Class<? extends Indicator> value) {
            clazz = value;
        }

        /**
         * Build an IndicatorEvent.
         *
         * @param ind indicator source
         * @return event
         */
        public IndicatorEvent event(final Indicator ind) {
            return new IndicatorEvent(this, ind);
        }

    }

    /**
     * The IndicatorEvent.Type used to create this event.
     */
    @Getter
    private final Type associatedType;

    /**
     * The indicator source for this event.
     */
    @Getter
    private final Indicator source;

    /**
     * Constructor.
     *
     * @param type associated event type
     * @param indicator indicator source
     */
    private IndicatorEvent(final Type type, final Indicator indicator) {
        Objects.requireNonNull(type,
                "IndicatorEvent.Type must not be null!");
        Objects.requireNonNull(indicator,
                "Indicator indicator must not be null!");
        if (!type.getClazz().isAssignableFrom(indicator.getClass())) {
            throw new IllegalArgumentException("Associated class for "
                    + type.name() + " is " + type.getClazz().getCanonicalName()
                    + ", but provided = "
                    + indicator.getClass().getCanonicalName());
        }
        associatedType = type;
        source = indicator;
    }

}
