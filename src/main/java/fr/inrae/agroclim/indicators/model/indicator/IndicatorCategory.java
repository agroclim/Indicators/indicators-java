/*
 * This file is part of Indicators.
 *
 * Indicators is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Indicators is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Indicators. If not, see <https://www.gnu.org/licenses/>.
 */
package fr.inrae.agroclim.indicators.model.indicator;

import lombok.Getter;

/**
 * All categories of indicator.
 *
 * Last changed : $Date$
 *
 * @author $Author$
 * @version $Revision$
 */
public enum IndicatorCategory {
    /** . */
    EVALUATION(0, "evaluation"),
    /** . */
    PHENO_PHASES(1, "pheno"),
    /** . */
    CULTURAL_PRACTICES(2, "practices"),
    /** . */
    ECOPHYSIOLOGICAL_PROCESSES(3, "ecoprocesses"),
    /** . */
    CLIMATIC_EFFECTS(4, "climatic"),
    /** . */
    INDICATORS(5, "indicator");

    /**
     * @param tag
     *            category tag
     * @return category for the tag.
     */
    public static IndicatorCategory getByTag(final String tag) {
        for (IndicatorCategory category : values()) {
            if (category.tag.equals(tag)) {
                return category;
            }
        }
        return null;
    }

    /**
     * @param category
     *            tag of parent category
     * @return tag of child category
     */
    public static String getChildCategory(final String category) {
        IndicatorCategory cat = IndicatorCategory.valueOf(category);
        if (cat == null) {
            return null;
        }
        IndicatorCategory child = cat.getChildCategory();
        if (child == null) {
            return null;
        }
        return child.getTag();
    }

    /**
     * Order.
     *
     * Exists only to prevent side effects of member sorting during auto-format.
     */
    private final int order;

    /**
     * Tag.
     */
    @Getter
    private final String tag;

    /**
     * @param categoryOrder
     *            Order
     * @param categoryTag
     *            Tag
     */
    IndicatorCategory(final int categoryOrder, final String categoryTag) {
        this.order = categoryOrder;
        this.tag = categoryTag;
    }

    /**
     * @return next category by its order.
     */
    public IndicatorCategory getChildCategory() {
        final int childOrder = order + 1;
        for (IndicatorCategory category : values()) {
            if (category.order == childOrder) {
                return category;
            }
        }
        return null;
    }
}
