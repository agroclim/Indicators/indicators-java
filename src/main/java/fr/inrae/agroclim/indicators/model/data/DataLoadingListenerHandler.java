/*
 * This file is part of Indicators.
 *
 * Indicators is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Indicators is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Indicators. If not, see <https://www.gnu.org/licenses/>.
 */
package fr.inrae.agroclim.indicators.model.data;

import java.io.Serializable;

import javax.swing.event.EventListenerList;

import fr.inrae.agroclim.indicators.model.data.DataLoadingListener.DataFile;
import lombok.extern.log4j.Log4j2;

/**
 * Listener handler for data loading.
 *
 * This is a utility class that can be used by beans that support listeners for
 * data loading events. It manages a list of listeners and dispatches messages
 * to them. You can use an instance of this class as a member field of your bean
 * and delegate these types of work to it.
 *
 * Last changed : $Date$
 *
 * @author $Author$
 * @version $Revision$
 */
@Log4j2
public class DataLoadingListenerHandler implements HasDataLoadingListener,
Serializable {
    /**
     * UID for serialization.
     */
    private static final long serialVersionUID = -2141365352070901695L;

    /**
     * List for all listeners.
     */
    private final EventListenerList listeners;

    /**
     * Constructor.
     */
    public DataLoadingListenerHandler() {
        listeners = new EventListenerList();
    }

    /**
     * Constructor with a list.
     *
     * @param eventListenerList
     *            list for event listeners
     */
    public DataLoadingListenerHandler(final EventListenerList eventListenerList) {
        listeners = eventListenerList;
    }

    @Override
    public final void addDataLoadingListener(final DataLoadingListener l) {
        for (final DataLoadingListener listener : getDataLoadingListeners()) {
            if (listener.equals(l)) {
                return;
            }
        }
        listeners.add(DataLoadingListener.class, l);
    }

    @Override
    public final void addDataLoadingListeners(final DataLoadingListener[] ls) {
        for (final DataLoadingListener l : ls) {
            addDataLoadingListener(l);
        }
    }

    @Override
    public final void fireDataLoadingAddEvent(final Data data) {
        for (final DataLoadingListener listener : getDataLoadingListeners()) {
            listener.onDataLoadingAdd(data);
        }
    }

    @Override
    public final void fireDataLoadingEndEvent(final String text) {
        for (final DataLoadingListener listener : getDataLoadingListeners()) {
            listener.onDataLoadingEnd(text);
        }
    }

    @Override
    public final void fireDataLoadingStartEvent(final String text) {
        for (final DataLoadingListener listener : getDataLoadingListeners()) {
            listener.onDataLoadingStart(text);
        }
    }

    @Override
    public final void fireDataSetEvent(final DataFile type) {
        LOGGER.traceEntry();
        for (final DataLoadingListener listener : getDataLoadingListeners()) {
            listener.onFileSet(type);
        }
    }

    @Override
    public final DataLoadingListener[] getDataLoadingListeners() {
        return listeners.getListeners(DataLoadingListener.class);
    }
}
