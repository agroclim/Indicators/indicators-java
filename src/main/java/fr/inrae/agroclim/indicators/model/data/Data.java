/*
 * This file is part of Indicators.
 *
 * Indicators is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Indicators is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Indicators. If not, see <https://www.gnu.org/licenses/>.
 */
package fr.inrae.agroclim.indicators.model.data;

import java.util.List;

/**
 * Interface for all resources data.
 *
 * Last date : $Date$
 *
 * @author $Author$
 * @version $Revision$
 */
public interface Data {

    /**
     * Check if data is conform.
     *
     * @param line line number
     * @param path prefix for log
     */
    void check(int line, String path);

    /**
     * @return list of error messages after check
     */
    List<String> getErrors();

    /**
     * @return list of warning messages after check
     */
    List<String> getWarnings();
}
