/*
 * Copyright (C) 2021 INRAE AgroClim
 *
 * This file is part of Indicators.
 *
 * Indicators is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Indicators is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Indicators. If not, see <http://www.gnu.org/licenses/>.
 */
package fr.inrae.agroclim.indicators.model;

import java.util.Locale;

import fr.inrae.agroclim.indicators.util.NameableEnumUtils;
import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlType;

/**
 * Timescale of indicators.
 *
 * Last change $Date$
 *
 * @author $Author$
 * @version $Revision$
 */
@XmlType(name = "timescale")
@XmlEnum
public enum TimeScale implements Nameable {
    /**
     * Hourly.
     */
    HOURLY,
    /**
     * Daily.
     */
    DAILY;

    @Override
    public String getName() {
        return NameableEnumUtils.getName(this);
    }

    @Override
    public String getName(final Locale locale) {
        return NameableEnumUtils.getName(this, locale);
    }
}
