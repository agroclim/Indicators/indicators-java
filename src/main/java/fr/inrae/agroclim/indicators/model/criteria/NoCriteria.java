/*
 * Copyright (C) 2020 INRAE AgroClim
 *
 * This file is part of Indicators.
 *
 * Indicators is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Indicators is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Indicators. If not, see <http://www.gnu.org/licenses/>.
 */
package fr.inrae.agroclim.indicators.model.criteria;

import java.util.List;
import java.util.Map;

import fr.inrae.agroclim.indicators.exception.IndicatorsException;
import fr.inrae.agroclim.indicators.model.Parameter;
import fr.inrae.agroclim.indicators.model.data.DailyData;
import fr.inrae.agroclim.indicators.util.Doublet;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlType;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * Criteria to define variable to use in aggregation indicator (eg.: Sum).
 *
 * Last changed : $Date$
 *
 * @author omaury
 * @author $Author$
 * @version $Revision$
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType
@EqualsAndHashCode(callSuper = true)
@ToString
public final class NoCriteria extends VariableCriteria {
    /**
     * UUID for Serializable.
     */
    private static final long serialVersionUID = -4284278102762199669L;

    @Override
    public Criteria clone() {
        final NoCriteria clone = new NoCriteria();
        clone.setVariable(getVariable());
        return clone;
    }

    @Override
    public boolean eval(final DailyData data) throws IndicatorsException {
        return true;
    }

    @Override
    public List<Doublet<Parameter, Number>> getParameterDefaults() {
        // no substitution
        return List.of();
    }

    @Override
    public Map<String, Double> getParametersValues() {
        // no substitution
        return Map.of();
    }

    @Override
    public void removeParameter(final Parameter param) {
        // Do nothing for this type of criteria
    }

    @Override
    public void setParametersValues(final Map<String, Double> values) {
        // do nothing
    }

    @Override
    public String toStringTree(final String indent) {
        final StringBuilder sb = new StringBuilder();
        sb.append(indent).append("  class: ").append(getClass().getName())
        .append("\n");
        sb.append(indent).append("  variable: ").append(getVariable())
        .append("\n");
        return sb.toString();
    }

}
