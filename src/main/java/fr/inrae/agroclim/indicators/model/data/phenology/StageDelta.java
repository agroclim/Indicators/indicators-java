/*
 * This file is part of Indicators.
 *
 * Indicators is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Indicators is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Indicators. If not, see <https://www.gnu.org/licenses/>.
 */
package fr.inrae.agroclim.indicators.model.data.phenology;

import java.io.Serializable;
import java.util.Comparator;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlAttribute;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Number of days to add relative stages.
 *
 * Last changed : $Date$
 *
 * @author $Author$
 * @version $Revision$
 */
@XmlAccessorType(XmlAccessType.FIELD)
@ToString
@EqualsAndHashCode(of = {"stage", "days"})
public class StageDelta implements Comparable<StageDelta>, Serializable {
    /**
     * UUID for Serializable.
     */
    private static final long serialVersionUID = 1709651110835570005L;

    /**
     * Stage name.
     */
    @XmlAttribute
    @Getter
    @Setter
    private String stage;

    /**
     * Number of days to add or remove.
     */
    @XmlAttribute
    @Getter
    @Setter
    private Integer days;

    /**
     * Constructor.
     */
    public StageDelta() {

    }

    /**
     * Constructor.
     * @param stageName stage name
     * @param nbOfDays number of days to add or remove
     */
    public StageDelta(final String stageName, final Integer nbOfDays) {
        stage = stageName;
        days = nbOfDays;
    }

    @Override
    public final int compareTo(final StageDelta o) {
        final Comparator<StageDelta> comparator
        = Comparator.comparing(StageDelta::getStage,
                Comparator.nullsFirst(Comparator.naturalOrder()))
        .thenComparing(Comparator.comparingInt(StageDelta::getDays));
        return comparator.compare(this, o);
    }

}
