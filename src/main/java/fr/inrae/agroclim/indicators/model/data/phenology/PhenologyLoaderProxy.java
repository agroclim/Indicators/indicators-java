/*
 * This file is part of Indicators.
 *
 * Indicators is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Indicators is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Indicators. If not, see <https://www.gnu.org/licenses/>.
 */
package fr.inrae.agroclim.indicators.model.data.phenology;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import fr.inrae.agroclim.indicators.model.TimeScale;
import fr.inrae.agroclim.indicators.model.data.DataLoadingListenerHandler;
import fr.inrae.agroclim.indicators.model.data.ResourcesLoader;
import fr.inrae.agroclim.indicators.model.data.Variable;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlElementWrapper;
import jakarta.xml.bind.annotation.XmlTransient;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

/**
 * Proxy to load phenology data.
 *
 * Loader is chosen according to properties (XML tag).
 *
 * The proxy is initialized from evaluation XML file.
 *
 * Last changed : $Date$
 *
 * @author $Author$
 * @version $Revision$
 */
@XmlAccessorType(XmlAccessType.FIELD)
@EqualsAndHashCode(
        callSuper = false,
        of = {"annualStageBuilder", "fileLoader", "calculator", "deltas",
        "userHeader"}
        )
public final class PhenologyLoaderProxy extends DataLoadingListenerHandler
        implements ResourcesLoader<List<AnnualStageData>> {
    /**
     * UUID for Serializable.
     */
    private static final long serialVersionUID = 2980458089659125675L;

    /**
     * Builder of annual stages, using DOY.
     */
    @Getter
    @Setter
    @XmlTransient
    private AnnualStageBuilder annualStageBuilder;

    /**
     * CSV file.
     */
    @XmlElement(name = "file")
    private PhenologyFileLoader fileLoader;

    /**
     * Phenological calculator.
     */
    @XmlElement(name = "calculator")
    private PhenologyCalculator calculator;

    /**
     * Number of days to add relative stages.
     */
    @XmlElementWrapper(name = "deltas")
    @XmlElement(name = "delta")
    @Getter
    @Setter
    private List<StageDelta> deltas;

    /**
     * Headers of CSV file.
     */
    @XmlElement(name = "header")
    private String[] userHeader;

    /**
     * Constructor.
     */
    public PhenologyLoaderProxy() {
        super();
    }

    @Override
    public PhenologyLoaderProxy clone() {
        final PhenologyLoaderProxy clone = new PhenologyLoaderProxy();
        clone.fileLoader = fileLoader.clone();
        clone.userHeader = userHeader;
        return clone;
    }

    /**
     * @return Phenological calculator.
     */
    public PhenologyCalculator getCalculator() {
        return calculator;
    }

    @Override
    public Map<String, String> getConfigurationErrors() {
        if (getLoader() == null) {
            final Map<String, String> errors = new HashMap<>();
            errors.put("phenology", "error.evaluation.phenology.loader.missing");
            return errors;
        }
        return getLoader().getConfigurationErrors();
    }

    /**
     * @return CSV file
     */
    public PhenologyFileLoader getFile() {
        return fileLoader;
    }

    /**
     * @return loader/calculator for phenology data according to configuration.
     */
    private ResourcesLoader<List<AnnualStageData>> getLoader() {
        if (fileLoader != null) {
            return fileLoader;
        } else if (annualStageBuilder != null) {
            return annualStageBuilder;
        } else if (calculator != null) {
            return calculator;
        } else {
            return null;
        }
    }

    @Override
    public Collection<String> getMissingVariables() {
        throw new RuntimeException("Not implemented for phenology!");
    }

    /**
     * @return headers of CSV file
     */
    public String[] getUserHeader() {
        return userHeader;
    }

    @Override
    public Set<Variable> getVariables() {
        if (getLoader() == null) {
            return new HashSet<>();
        }
        return getLoader().getVariables();
    }

    @Override
    public List<AnnualStageData> load() {
        if (deltas != null && !deltas.isEmpty()) {
            final RelativeAnnualStageCalculator calc = new RelativeAnnualStageCalculator();
            calc.setDeltas(deltas);
            final List<AnnualStageData> annualStageDatas = getLoader().load();
            calc.setAnnualStageDatas(annualStageDatas);
            return calc.load();
        }
        return getLoader().load();
    }

    /**
     * @param value phenological calculator
     */
    public void setCalculator(final PhenologyCalculator value) {
        this.calculator = value;
    }

    /**
     * @param value
     *            CSV file loader
     */
    public void setFile(final PhenologyFileLoader value) {
        this.fileLoader = value;
    }

    @Override
    public void setTimeScale(final TimeScale timeScale) {
        // do nothing
    }
}
