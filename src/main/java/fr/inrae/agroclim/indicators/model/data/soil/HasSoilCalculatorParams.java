/*
 * This file is part of Indicators.
 *
 * Indicators is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Indicators is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Indicators. If not, see <https://www.gnu.org/licenses/>.
 */
package fr.inrae.agroclim.indicators.model.data.soil;

/**
 * Methods to set parameter on the SWB calculator.
 *
 * Last changed : $Date$
 *
 * @author $Author$
 * @version $Revision$
 */
public interface HasSoilCalculatorParams {

    /**
     * @param kcIni Coefficient cultural en situation non stressée durant la
     * période initiale
     */
    void setKcIni(Double kcIni);

    /**
     * @param kcLate Coefficient cultural en situation non stressée durant la
     * late season
     */
    void setKcLate(Double kcLate);

    /**
     * @param kcMid Coefficient cultural en situation non stressée durant la
     * middle season
     */
    void setKcMid(Double kcMid);

    /**
     * @param p Average fraction of TAW (pour obtenir RAW).
     */
    void setP(Double p);

    /**
     * @param soilDepth Soil depth (cm).
     */
    void setSoilDepth(Double soilDepth);

    /**
     * @param swcFc Teneur en eau du sol à la capacité au champ (% volumique).
     */
    void setSwcFc(Double swcFc);

    /**
     * @param swcMax Teneur en eau du sol maxi. (% volumique).
     */
    void setSwcMax(Double swcMax);

    /**
     * @param swcWp Teneur en eau du sol au point de flétrissement permanent (%
     * volumique).
     */
    void setSwcWp(Double swcWp);
}
