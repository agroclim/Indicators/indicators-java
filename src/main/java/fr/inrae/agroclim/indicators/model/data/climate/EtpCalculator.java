/*
 * This file is part of Indicators.
 *
 * Indicators is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Indicators is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Indicators. If not, see <https://www.gnu.org/licenses/>.
 */
package fr.inrae.agroclim.indicators.model.data.climate;

import java.io.Serializable;

import fr.inrae.agroclim.indicators.model.data.UseVariables;

/**
 * Calculator for ETP (aka ET0, ETR).
 *
 * Last changed : $Date$
 *
 * @author $Author$
 * @version $Revision$
 */
public interface EtpCalculator extends Cloneable, Serializable, UseVariables {
    /**
     * @return cloned instance
     */
    EtpCalculator clone();

    /**
     * @param data
     *            climatic daily data.
     * @return ET0 (mm)
     */
    Double compute(ClimaticDailyData data) throws IllegalArgumentException;
}
