/*
 * This file is part of Indicators.
 *
 * Indicators is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Indicators is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Indicators. If not, see <https://www.gnu.org/licenses/>.
 */
package fr.inrae.agroclim.indicators.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

/**
 * Parameter for indicator or indicator criteria.
 *
 * Last change $Date$
 *
 * @author $Author$
 * @version $Revision$
 */
@XmlAccessorType(XmlAccessType.FIELD)
@EqualsAndHashCode(
        callSuper = false,
        of = {"id", "attribute", "descriptions"}
        )
public final class Parameter implements Cloneable, Serializable {

    /**
     * UUID for Serializable.
     */
    private static final long serialVersionUID = 9205643160821888601L;

    /**
     * Attribute to change.
     */
    @XmlElement
    @Getter
    @Setter
    private String attribute;

    /**
     * Localized descriptions.
     */
    @XmlElement(name = "description")
    @Getter
    @Setter
    private List<LocalizedString> descriptions;

    /**
     * Parameter id.
     */
    @XmlElement
    @Getter
    @Setter
    private String id;

    /**
     * Constructor.
     */
    public Parameter() {
        // Do nothing
    }

    @Override
    public Parameter clone() throws CloneNotSupportedException {
        final Parameter clone = (Parameter) super.clone();
        clone.attribute = attribute;
        if (descriptions != null) {
            clone.descriptions = new ArrayList<>();
            for (final LocalizedString description : descriptions) {
                clone.descriptions.add(description.clone());
            }
        }
        clone.id = id;
        return clone;
    }

    /**
     * @param languageCode lang code
     * @return description for the lang
     */
    public String getDescription(final String languageCode) {
        return LocalizedString.getString(descriptions, languageCode);
    }

}
