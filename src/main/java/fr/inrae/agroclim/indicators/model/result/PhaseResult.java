/*
 * This file is part of Indicators.
 *
 * Indicators is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Indicators is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Indicators. If not, see <https://www.gnu.org/licenses/>.
 */
package fr.inrae.agroclim.indicators.model.result;

import java.util.ArrayList;
import java.util.List;

import fr.inrae.agroclim.indicators.model.AnnualPhase;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Normalized value of a phase for a year and values of composed indicators.
 *
 * Last changed : $Date$
 *
 * @author Olivier Maury
 * @author $Author$
 * @version $Revision$
 */
@ToString
public final class PhaseResult extends Result {

    /**
     * Phase details.
     */
    @Getter
    @Setter
    private AnnualPhase annualPhase;

    /**
     * Values of indicators.
     */
    @Getter
    private final List<IndicatorResult> indicatorResults = new ArrayList<>();

    /**
     * @return phase id with "+" and "-" replaced.
     */
    public String getEncodedPhaseId() {
        if (annualPhase == null) {
            return null;
        }
        String phaseId = annualPhase.getUid();
        return phaseId.replace("+", "p").replace("-", "m");
    }

    /**
     * @return phase id
     */
    public String getPhaseId() {
        if (annualPhase == null) {
            return null;
        }
        return annualPhase.getUid();
    }
}
