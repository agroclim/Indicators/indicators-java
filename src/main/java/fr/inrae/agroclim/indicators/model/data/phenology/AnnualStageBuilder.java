/*
 * This file is part of Indicators.
 *
 * Indicators is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Indicators is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Indicators. If not, see <https://www.gnu.org/licenses/>.
 */
package fr.inrae.agroclim.indicators.model.data.phenology;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import fr.inrae.agroclim.indicators.model.TimeScale;
import fr.inrae.agroclim.indicators.model.data.DataLoadingListenerHandler;
import fr.inrae.agroclim.indicators.model.data.ResourcesLoader;
import fr.inrae.agroclim.indicators.model.data.Variable;
import lombok.EqualsAndHashCode;
import lombok.Setter;

/**
 * Builder to provide agroclimatic stages from DOY.
 *
 * Last changed : $Date$
 *
 * @author $Author$
 * @version $Revision$
 */
@EqualsAndHashCode(
        callSuper = false,
        of = {"doys", "years"}
        )
public final class AnnualStageBuilder extends DataLoadingListenerHandler
implements ResourcesLoader<List<AnnualStageData>> {
    /**
     * UID for serialization.
     */
    private static final long serialVersionUID = 9205643160821888602L;

    /**
     * List of years on which build stages.
     */
    @Setter
    private List<Integer> years;

    /**
     * Day of year for each stage to build.
     */
    @Setter
    private Map<String, Integer> doys;

    @Override
    public ResourcesLoader<List<AnnualStageData>> clone()
            throws CloneNotSupportedException {
        final AnnualStageBuilder clone = new AnnualStageBuilder();
        if (years != null) {
            clone.years = new ArrayList<>();
            clone.years.addAll(years);
        }
        if (doys != null) {
            clone.doys = new HashMap<>();
            clone.doys.putAll(doys);
        }
        return clone;
    }

    @Override
    public Map<String, String> getConfigurationErrors() {
        final Map<String, String> errors = new HashMap<>();
        if (years == null) {
            errors.put("phenology.years",
                    "error.evaluation.phenology.years.missing");
        } else if (years.isEmpty()) {
            errors.put("phenology.years",
                    "error.evaluation.phenology.years.empty");
        }
        if (doys == null) {
            errors.put("phenology.doys",
                    "error.evaluation.phenology.doys.missing");
        } else if (doys.isEmpty()) {
            errors.put("phenology.doys",
                    "error.evaluation.phenology.doys.empty");
        }
        if (errors.isEmpty()) {
            return null;
        }
        return errors;
    }

    @Override
    public Collection<String> getMissingVariables() {
        throw new RuntimeException("Not implemented for phenology!");
    }

    @Override
    public Set<Variable> getVariables() {
        return new HashSet<>();
    }

    @Override
    public List<AnnualStageData> load() {
        final List<AnnualStageData> data = new ArrayList<>();
        Collections.sort(years);
        final List<String> stageNames = new ArrayList<>(doys.keySet());
        Collections.sort(stageNames);
        years.forEach(year -> {
            final AnnualStageData annualStageData = new AnnualStageData();
            annualStageData.setYear(year);
            stageNames.forEach(name -> {
                final int doy = doys.get(name);
                annualStageData.add(name, doy);
            });
            data.add(annualStageData);
        });
        return data;
    }

    @Override
    public void setTimeScale(final TimeScale timeScale) {
        // do nothing
    }

}
