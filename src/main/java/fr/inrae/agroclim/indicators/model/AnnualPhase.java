/*
 * This file is part of Indicators.
 *
 * Indicators is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Indicators is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Indicators. If not, see <https://www.gnu.org/licenses/>.
 */
package fr.inrae.agroclim.indicators.model;

import java.util.Date;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Phenological phase for a year.
 *
 * Last changed : $Date$
 *
 * @author Olivier Maury
 * @author $Author$
 * @version $Revision$
 */
@ToString
public class AnnualPhase {

    /**
     * String ID (eg.: s0s1).
     */
    @Getter
    @Setter
    private String uid;

    /**
     * First day of phase.
     */
    @Getter
    @Setter
    private Date start;

    /**
     * Name of start stage (eg.: s0).
     */
    @Getter
    @Setter
    private String startStage;

    /**
     * Last day of phase.
     */
    @Getter
    @Setter
    private Date end;

    /**
     * Name of end stage (eg.: s1).
     */
    @Getter
    @Setter
    private String endStage;

    /**
     * Year of harvest.
     */
    @Getter
    @Setter
    private int harvestYear;
}
