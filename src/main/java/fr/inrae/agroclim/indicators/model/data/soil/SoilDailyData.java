/*
 * This file is part of Indicators.
 *
 * Indicators is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Indicators is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Indicators. If not, see <https://www.gnu.org/licenses/>.
 */
package fr.inrae.agroclim.indicators.model.data.soil;

import java.util.List;

import fr.inrae.agroclim.indicators.model.data.DailyData;
import fr.inrae.agroclim.indicators.model.data.Variable;

/**
 * Daily data for soil.
 *
 * Last changed : $Date$
 *
 * @author $Author$
 * @version $Revision$
 */
public final class SoilDailyData extends DailyData {
    /**
     * UUID for Serializable.
     */
    private static final long serialVersionUID = -3018258536670137539L;

    @Override
    public void check(final int line, final String path) {
        throw new RuntimeException(
                "SoilDailyData.check() is not implemented!");
    }

    @Override
    public List<String> getErrors() {
        throw new RuntimeException(
                "SoilDailyData.getErrors() is not implemented!");
    }

    /**
     * @return Teneur en eau du sol en % volumique
     */
    public Double getSwc() {
        return getValue(Variable.SOILWATERCONTENT);
    }

    @Override
    public Double getValue(final Variable variable) {
        return super.getRawValue(variable);
    }

    @Override
    public List<String> getWarnings() {
        throw new RuntimeException(
                "SoilDailyData.getWarnings() is not implemented!");
    }

    /**
     * @return the waterReserve
     */
    public Double getWaterReserve() {
        return getValue(Variable.WATER_RESERVE);
    }

    /**
     * @param value
     *            teneur en eau du sol en % volumique
     */
    public void setSwc(final Double value) {
        setValue(Variable.SOILWATERCONTENT, value);
    }

    /**
     * @param value
     *            the waterReserve to set
     */
    public void setWaterReserve(final Double value) {
        setValue(Variable.WATER_RESERVE, value);
    }

    @Override
    public String toString() {
        return "SoilDailyData{" + "year=" + getYear() + ", month=" + getMonth()
                + ", day=" + getDay() + ", swc=" + getSwc() + ", waterReserve="
                + getWaterReserve() + '}';
    }
}
