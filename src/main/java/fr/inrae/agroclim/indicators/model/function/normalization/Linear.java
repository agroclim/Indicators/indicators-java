/*
 * This file is part of Indicators.
 *
 * Indicators is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Indicators is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Indicators. If not, see <https://www.gnu.org/licenses/>.
 */
package fr.inrae.agroclim.indicators.model.function.normalization;

import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlRootElement;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Normalize with A.x + B.
 *
 * @author Olivier Maury
 */
@XmlRootElement
@EqualsAndHashCode(callSuper = false, of = {"linearA", "linearB"})
@ToString
public final class Linear extends NormalizationFunction {

    /**
     * UUID for Serializable.
     */
    private static final long serialVersionUID = 4872847709393688046L;

    /**
     * A in A.x + B.
     */
    @XmlAttribute
    @Getter
    @Setter
    private double linearA;

    /**
     * B in A.x + B.
     */
    @XmlAttribute
    @Getter
    @Setter
    private double linearB;

    /**
     * Constructor with no args for Serializable.
     */
    public Linear() {
    }

    /**
     * Constructor coeff.
     *
     * @param a A in A.x + B.
     * @param b B in A.x + B.
     */
    public Linear(final double a, final double b) {
        linearA = a;
        linearB = b;
    }

    @Override
    public Linear clone() {
        final Linear clone = new Linear();
        clone.linearA = linearA;
        clone.linearB = linearB;
        return clone;
    }

    @Override
    public String getFormulaMathML() {
        return ""
                + "<mrow>"
                + " <mrow>"
                + "  <mn>a</mn>"
                + "  <mo>.</mo>"
                + "  <mi>x</mi>"
                + " </mrow>"
                + " <mo>+</mo>"
                + " <mi>b</mi>"
                + "</mrow>";
    }

    @Override
    public double normalize(final double value) {
        return linearA * value + linearB;
    }
}
