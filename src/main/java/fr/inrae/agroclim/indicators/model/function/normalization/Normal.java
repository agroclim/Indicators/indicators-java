/*
 * This file is part of Indicators.
 *
 * Indicators is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Indicators is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Indicators. If not, see <https://www.gnu.org/licenses/>.
 */
package fr.inrae.agroclim.indicators.model.function.normalization;

import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlRootElement;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Normalize with 1/(exp(A.(x - C)² + B.(x - C)) + 1).
 *
 * @author jcufi
 */
@XmlRootElement
@ToString
public final class Normal extends NormalizationFunction {
    /**
     * UUID for Serializable.
     */
    private static final long serialVersionUID = 4872847709393688044L;

    /**
     * A in 1/(exp(A.(x - C)² + B.(x - C)) + 1).
     */
    @XmlAttribute
    @Getter
    @Setter
    private double normalA;

    /**
     * B in 1/(exp(A.(x - C)² + B.(x - C)) + 1).
     */
    @XmlAttribute
    @Getter
    @Setter
    private double normalB;

    /**
     * C in 1/(exp(A.(x - C)² + B.(x - C)) + 1).
     */
    @XmlAttribute
    @Getter
    @Setter
    private double normalC;

    /**
     * Constructor.
     */
    public Normal() {
    }

    /**
     * Constructor.
     *
     * @param a 1/(exp(A.(x - C)² + B.(x - C)) + 1)
     * @param b 1/(exp(A.(x - C)² + B.(x - C)) + 1)
     * @param c 1/(exp(A.(x - C)² + B.(x - C)) + 1)
     */
    public Normal(final double a, final double b, final double c) {
        this();
        this.normalA = a;
        this.normalB = b;
        this.normalC = c;
    }

    /**
     * Constructor.
     *
     * @param n normal to clone
     */
    public Normal(final Normal n) {
        this(n.normalA, n.normalB, n.normalC);
    }

    @Override
    public Normal clone() {
        return new Normal(this);
    }

    @Override
    public String getFormulaMathML() {
        return "<mfrac>"
                + " <mn>1</mn>"
                + " <mrow>"
                + "  <mi>1</mi>"
                + "  <mo>+</mo>"
                + "  <msup>"
                + "   <mi>e</mi>"
                + "   <mrow>"
                + "    <mi>a</mi>"
                + "    <mo>.</mo>"
                + "    <mrow>"
                + "     <msup>"
                + "      <mfenced>"
                + "       <mrow>"
                + "        <mi>x</mi>"
                + "        <mo>-</mo>"
                + "        <mi>c</mi>"
                + "       </mrow>"
                + "      </mfenced>"
                + "      <mn>2</mn>"
                + "     </msup>"
                + "    </mrow>"
                + "    <mo>+</mo>"
                + "    <mi>b</mi>"
                + "    <mo>.</mo>"
                + "    <mfenced>"
                + "     <mrow>"
                + "      <mi>x</mi>"
                + "      <mo>-</mo>"
                + "      <mi>c</mi>"
                + "     </mrow>"
                + "    </mfenced>"
                + "   </mrow>"
                + "  </msup>"
                + " </mrow>"
                + "</mfrac>";
    }

    @Override
    public double normalize(final double value) {
        final double denom = normalA * Math.pow(value - normalC, 2) + normalB
                * (value - normalC);
        return 1 / (1 + Math.exp(denom));
    }
}
