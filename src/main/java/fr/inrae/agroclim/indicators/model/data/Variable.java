/*
 * This file is part of Indicators.
 *
 * Indicators is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Indicators is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Indicators. If not, see <https://www.gnu.org/licenses/>.
 */
package fr.inrae.agroclim.indicators.model.data;

import java.util.HashSet;
import java.util.Set;

import fr.inrae.agroclim.indicators.model.TimeScale;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlEnumValue;
import lombok.Getter;
import lombok.NonNull;

/**
 * Climatic or soil variables usable to compute indicators.
 *
 * Last changed : $Date$
 *
 * @author $Author$
 * @version $Revision$
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlEnum
public enum Variable {
    /**
     * Instantaneous hourly air temperature [°C].
     */
    @XmlEnumValue("th")
    TH(Type.CLIMATIC, TimeScale.HOURLY),
    /**
     * Minimal air temperature [°C].
     */
    @XmlEnumValue("tmin")
    TMIN(Type.CLIMATIC, TimeScale.DAILY),
    /**
     * Maximal air temperature [°C].
     */
    @XmlEnumValue("tmax")
    TMAX(Type.CLIMATIC, TimeScale.DAILY),
    /**
     * Average air temperature [°C].
     */
    @XmlEnumValue("tmean")
    TMEAN(Type.CLIMATIC, TimeScale.DAILY),
    /**
     * Global radiation [W/m²].
     */
    @XmlEnumValue("radiation")
    RADIATION(Type.CLIMATIC, TimeScale.DAILY),
    /**
     * Rain precipitation during the period [mm].
     */
    @XmlEnumValue("rain")
    RAIN(Type.CLIMATIC, TimeScale.DAILY, TimeScale.HOURLY),
    /**
     * Wind speed [m/s].
     */
    @XmlEnumValue("wind")
    WIND(Type.CLIMATIC, TimeScale.DAILY, TimeScale.HOURLY),
    /**
     * Evapotranspiration [mm/d].
     */
    @XmlEnumValue("etp")
    ETP(Type.CLIMATIC, TimeScale.DAILY),
    /**
     * Relative humidity [%].
     *
     * Range: 0-100.
     */
    @XmlEnumValue("rh")
    RH(Type.CLIMATIC, TimeScale.DAILY, TimeScale.HOURLY),
    /**
     * Soil water reserve [mm].
     */
    @XmlEnumValue("waterreserve")
    WATER_RESERVE(Type.SOIL, TimeScale.DAILY),
    /**
     * Soil water content [% mass].
     */
    @XmlEnumValue("soilwatercontent")
    SOILWATERCONTENT(Type.SOIL, TimeScale.DAILY);

    /**
     * Type of variable.
     */
    public enum Type {
        /**
         * Climatic variable.
         */
        CLIMATIC,
        /**
         * Soil variable.
         */
        SOIL;
    }

    /**
     * @param variableName name of variable to find.
     * @return found variable or null
     */
    public static Variable getByName(final String variableName) {
        if (variableName == null) {
            throw new NullPointerException("Variable name is null!");
        }
        return valueOf(variableName.toUpperCase());
    }

    /**
     * @param timescale time scale of variable
     * @param type type of variable
     * @return filtered set of variable according to TimeScale and Type.
     */
    public static Set<Variable> getByTimeScaleAndType(@NonNull final TimeScale timescale, final Type type) {
        final Set<Variable> variables = new HashSet<>();
        for (final Variable variable : values()) {
            if (variable.getType().equals(type)) {
                for (final TimeScale ts : variable.getTimescales()) {
                    if (ts.equals(timescale)) {
                        variables.add(variable);
                    }
                }
            }
        }
        return variables;
    }

    /**
     * @param type type of variable
     * @return filtered set of variable according to Type.
     */
    public static Set<Variable> getByType(final Type type) {
        final Set<Variable> variables = new HashSet<>();
        for (final Variable var : values()) {
            if (var.getType().equals(type)) {
                variables.add(var);
            }
        }
        return variables;
    }

    /**
     * Related time scales.
     */
    @Getter
    private final TimeScale[] timescales;

    /**
     * Variable type.
     */
    @Getter
    private final Type type;

    /**
     * Constructor.
     *
     * @param variableType variable type
     * @param variableTimescales related time scales
     */
    Variable(final Type variableType, final TimeScale... variableTimescales) {
        type = variableType;
        if (variableTimescales == null) {
            timescales = new TimeScale[]{TimeScale.DAILY};
        } else {
            timescales = variableTimescales;
        }
    }

    /**
     * @return variable name
     */
    public String getName() {
        return name().toLowerCase();
    }
}
