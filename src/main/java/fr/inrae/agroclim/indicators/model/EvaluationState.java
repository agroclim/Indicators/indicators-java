/*
 * This file is part of Indicators.
 *
 * Indicators is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Indicators is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Indicators. If not, see <https://www.gnu.org/licenses/>.
 */
package fr.inrae.agroclim.indicators.model;

/**
 * Evaluation states.
 *
 * Last change $Date$
 *
 * @author $Author$
 * @version $Revision$
 */
public enum EvaluationState {
    /**
     * Evaluation is just created.
     */
    NEW {
        @Override
        public void onValidate(final Evaluation evaluation) {
            if (!isValid(evaluation)) {
                evaluation.setState(INVALID);
            }
        }
    },
    /**
     * Evaluation as invalid parameters.
     */
    INVALID {
        @Override
        public void onValidate(final Evaluation evaluation) {
            if (isValid(evaluation)) {
                evaluation.setState(VALID);
            }
        }
    },
    /**
     * Evaluation as valid parameters.
     */
    VALID {
        @Override
        public void onValidate(final Evaluation evaluation) {
            if (!isValid(evaluation)) {
                evaluation.setState(INVALID);
            }
        }
    };

    /**
     * Check evaluation validation.
     *
     * @param evaluation
     *            evaluation to check
     * @return evaluation is valid: computation can be done.
     */
    private static boolean isValid(final Evaluation evaluation) {
        return !evaluation.isOnErrorOrIncomplete(true);
    }

    /**
     * Validate evaluation.
     *
     * @param evaluation
     *            to validate.
     */
    public abstract void onValidate(Evaluation evaluation);
}
