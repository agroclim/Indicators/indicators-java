/*
 * This file is part of Indicators.
 *
 * Indicators is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Indicators is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Indicators. If not, see <https://www.gnu.org/licenses/>.
 */
package fr.inrae.agroclim.indicators.model.indicator;

import java.util.stream.DoubleStream;

import fr.inrae.agroclim.indicators.model.criteria.SimpleCriteria;

/**
 * Maximum value defined in a {@link SimpleCriteria}.
 *
 * Last changed : $Date$
 *
 * @author omaury
 */
public final class Max extends AggregationIndicator {
    /**
     * UUID for Serialize.
     */
    private static final long serialVersionUID = 6030595237342422022L;

    @Override
    protected double aggregate(final DoubleStream s) {
        return s.max().getAsDouble();
    }

    @Override
    public Max clone() throws CloneNotSupportedException {
        return (Max) super.clone();
    }
}
