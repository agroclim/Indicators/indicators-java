/*
 * This file is part of Indicators.
 *
 * Indicators is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Indicators is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Indicators. If not, see <https://www.gnu.org/licenses/>.
 */
package fr.inrae.agroclim.indicators.model.data.soil;

import fr.inrae.agroclim.indicators.model.data.Resource;

/**
 * Storage of soil daily data.
 *
 * Last change $Date$
 *
 * @author $Author$
 * @version $Revision$
 */
public final class SoilResource extends Resource<SoilDailyData> {
    /**
     * UUID for Serializable.
     */
    private static final long serialVersionUID = -8086612477523799365L;

    @Override
    public SoilResource clone() throws CloneNotSupportedException {
        return (SoilResource) super.clone();
    }
}
