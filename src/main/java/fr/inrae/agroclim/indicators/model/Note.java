package fr.inrae.agroclim.indicators.model;

import java.io.Serializable;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlID;
import lombok.EqualsAndHashCode;
import lombok.Getter;

/**
 * Indicator note.
 * Last change $Date: 2022-06-16 10:52:23 +0200 (jeu., 16 juin 2022) $
 *
 * @author jdecome
 * @author $Author: omaury $
 * @version $Revision: 588 $
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@EqualsAndHashCode(
        callSuper = false,
        of = {"id"}
        )
public final class Note implements Cloneable, Serializable {

    /**
     * UUID for Serializable.
     */
    private static final long serialVersionUID = 5432240898259571239L;

    /**
     * Id of note.
     */
    @XmlElement
    @Getter
    @XmlID
    private String id;

    /**
     * Description of note.
     */
    @XmlElement
    @Getter
    private String description;

    @Override
    public Note clone() {
        final Note clone = new Note();
        clone.id = this.id;
        clone.description = this.description;
        return clone;
    }
}
