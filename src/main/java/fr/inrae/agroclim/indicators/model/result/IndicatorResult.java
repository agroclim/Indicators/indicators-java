/*
 * This file is part of Indicators.
 *
 * Indicators is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Indicators is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Indicators. If not, see <https://www.gnu.org/licenses/>.
 */
package fr.inrae.agroclim.indicators.model.result;

import java.util.ArrayList;
import java.util.List;

import fr.inrae.agroclim.indicators.model.indicator.IndicatorCategory;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Indicator value after
 * {@code Indicator.compute(Resource<? extends DailyData>)}.
 *
 * Last changed : $Date$
 *
 * @author Olivier Maury
 * @author $Author$
 * @version $Revision$
 */
@ToString
public class IndicatorResult extends Result {

    /**
     * Category of indicator (phase, process, effect, indicator).
     */
    @Getter
    @Setter
    private IndicatorCategory indicatorCategory;

    /**
     * Indicator ID.
     */
    @Getter
    @Setter
    private String indicatorId;

    /**
     * Value of composed indicators.
     */
    @Getter
    private final List<IndicatorResult> indicatorResults = new ArrayList<>();

    /**
     * Raw value of indicator.
     */
    @Getter
    @Setter
    private Double rawValue;
}
