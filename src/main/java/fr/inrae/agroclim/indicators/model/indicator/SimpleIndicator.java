/*
 * This file is part of Indicators.
 *
 * Indicators is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Indicators is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Indicators. If not, see <https://www.gnu.org/licenses/>.
 */
package fr.inrae.agroclim.indicators.model.indicator;

import fr.inrae.agroclim.indicators.exception.IndicatorsException;
import fr.inrae.agroclim.indicators.model.EvaluationType;
import fr.inrae.agroclim.indicators.model.data.DailyData;
import fr.inrae.agroclim.indicators.model.data.Resource;
import fr.inrae.agroclim.indicators.model.function.normalization.NormalizationFunction;
import fr.inrae.agroclim.indicators.model.indicator.listener.IndicatorEvent;
import fr.inrae.agroclim.indicators.model.indicator.listener.IndicatorListener;
import jakarta.xml.bind.annotation.XmlSeeAlso;
import lombok.EqualsAndHashCode;
import lombok.extern.log4j.Log4j2;

/**
 * Simple indicator has fixed definition.
 *
 * @author jucufi
 */
@XmlSeeAlso({Average.class, AverageOfDiff.class, DiffOfSum.class,
    Frequency.class, InjectedParameter.class, Max.class, Min.class, NumberOfDays.class, PhaseLength.class,
    PotentialSowingDaysFrequency.class, Sum.class, Tamm.class})
@EqualsAndHashCode(
        callSuper = true
        )
@Log4j2
public abstract class SimpleIndicator extends Indicator {

    /**
     * UUID for Serializable.
     */
    private static final long serialVersionUID = 6030595237342422015L;

    /**
     * Constructor.
     */
    protected SimpleIndicator() {
        super();
        setIndicatorCategory(IndicatorCategory.INDICATORS);
    }

    /**
     * Constructor with function.
     * @param n
     *            function to normalize value
     */
    protected SimpleIndicator(final NormalizationFunction n) {
        this();
        if (n != null) {
            setNormalizationFunction(n.clone());
        }
    }

    /**
     * Compute and normalize the indicator value.
     * @param data
     *            climatic resource used to compute indicator.
     * @return normalized result
     */
    @Override
    public final Double compute(final Resource<? extends DailyData> data) throws IndicatorsException {
        double value = computeSingleValue(data);
        setNotNormalizedValue(value);
        if (getNormalizationFunction() != null
                && getType() != EvaluationType.WITHOUT_AGGREGATION) {
            value = getNormalizationFunction().normalize(value);
            setValue(value);
        }
        return value;
    }

    /**
     * Compute value from daily data.
     *
     * @param data
     *            resource with daily data
     * @return value
     * @throws IndicatorsException
     *             exception
     */
    public abstract double computeSingleValue(Resource<? extends DailyData> data) throws IndicatorsException;

    @Override
    public final void fireValueUpdated() {
        for (final IndicatorListener listener : getIndicatorListeners()) {
            listener.onIndicatorEvent(
                    IndicatorEvent.Type.UPDATED_VALUE.event(this));
        }
    }

    @Override
    public final EvaluationType getType() {
        if (getParent() != null) {
            return getParent().getType();
        }
        return null;
    }

}
