/*
 * This file is part of Indicators.
 *
 * Indicators is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Indicators is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Indicators. If not, see <https://www.gnu.org/licenses/>.
 */
package fr.inrae.agroclim.indicators.model.data.phenology;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import lombok.Getter;
import lombok.Setter;

/**
 * Calculator to add relative stages.
 *
 * Last changed : $Date$
 *
 * @author $Author$
 * @version $Revision$
 */
public final class RelativeAnnualStageCalculator {

    /**
     * Relative number of days for each stage to build.
     */
    @Setter
    private List<StageDelta> deltas;

    /**
     * Stages to handle.
     */
    @Getter
    @Setter
    private List<AnnualStageData> annualStageDatas;

    /**
     * Compute new stage and return all stages.
     *
     * @return all stages
     */
    public List<AnnualStageData> load() {
        Map<Integer, AnnualStageData> annuallyData = new HashMap<>();
        annualStageDatas.forEach(annualStageData -> {
            int year = annualStageData.getYear();
            if (!annuallyData.containsKey(year)) {
                annuallyData.put(year, annualStageData);
            } else {
                throw new RuntimeException("Year " + year + " already set!");
            }
        });
        annualStageDatas.forEach(annualStageData -> {
            int year = annualStageData.getYear();
            List<Stage> stages = new ArrayList<>(annualStageData.getStages());

            stages.forEach(stage -> {
                final String name = stage.getName();
                deltas.forEach(stageDelta -> {
                    Integer delta = stageDelta.getDays();
                    if (delta == null || delta == 0) {
                        return;
                    }
                    String stageName = stageDelta.getStage();
                    if (!Objects.equals(name, stageName)) {
                        return;
                    }
                    int doy = stage.getValue() + delta;
                    if (doy <= 0) {
                        doy = 0;
                    }
                    if (delta > 0) {
                        stageName = stage.getName() + "+" + delta;
                    } else if (delta < 0) {
                        stageName = stage.getName() + delta;
                    }
                    annuallyData.get(year).add(stageName, doy);
                });
            });
        });
        List<AnnualStageData> data = new ArrayList<>();
        data.addAll(annuallyData.values());
        return data;
    }

}
