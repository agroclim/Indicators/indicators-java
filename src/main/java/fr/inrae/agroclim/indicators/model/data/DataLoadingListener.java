/*
 * This file is part of Indicators.
 *
 * Indicators is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Indicators is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Indicators. If not, see <https://www.gnu.org/licenses/>.
 */
package fr.inrae.agroclim.indicators.model.data;

import java.util.EventListener;

/**
 * Listener of data loading.
 *
 * Last changed : $Date$
 *
 * @author $Author$
 * @author Olivier Maury
 * @version $Revision$
 */
public interface DataLoadingListener extends EventListener {
    /**
     * Type of file.
     */
    enum DataFile {
        /**
         * Climatic file.
         */
        CLIMATIC,
        /**
         * Phenological file.
         */
        PHENOLOGICAL
    }

    /**
     * Fired when new data is added.
     *
     * @param data added data
     */
    void onDataLoadingAdd(Data data);

    /**
     * Fired when data loading ends.
     *
     * @param text
     *            description
     */
    void onDataLoadingEnd(String text);

    /**
     * Fired when data loading starts.
     *
     * @param text
     *            description
     */
    void onDataLoadingStart(String text);

    /**
     * Fired when new file is set.
     *
     * @param type type of data file
     */
    void onFileSet(DataFile type);
}
