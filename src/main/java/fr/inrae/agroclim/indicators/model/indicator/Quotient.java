/*
 * This file is part of Indicators.
 *
 * Indicators is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Indicators is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Indicators. If not, see <https://www.gnu.org/licenses/>.
 */
package fr.inrae.agroclim.indicators.model.indicator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import fr.inrae.agroclim.indicators.exception.IndicatorsException;
import fr.inrae.agroclim.indicators.exception.type.ComputationErrorType;
import fr.inrae.agroclim.indicators.model.Knowledge;
import fr.inrae.agroclim.indicators.model.Parameter;
import fr.inrae.agroclim.indicators.model.data.DailyData;
import fr.inrae.agroclim.indicators.model.data.Resource;
import fr.inrae.agroclim.indicators.model.data.Variable;
import fr.inrae.agroclim.indicators.model.indicator.listener.IndicatorEvent;
import fr.inrae.agroclim.indicators.model.indicator.listener.IndicatorListener;
import fr.inrae.agroclim.indicators.util.Doublet;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlRootElement;
import jakarta.xml.bind.annotation.XmlType;
import lombok.Getter;
import lombok.Setter;

/**
 * Computing quotient of indicators (dividend/divisor).
 *
 * Last changed : $Date$
 *
 * @author $Author$
 * @version $Revision$
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = {"dividend", "divisor"})
public final class Quotient extends SimpleIndicator implements Detailable, IndicatorListener {

    /**
     * UUID for Serializable.
     */
    private static final long serialVersionUID = 6030595237342422014L;

    /**
     * Dividend in quotient or numerator in fraction.
     */
    @Getter
    @Setter
    private SimpleIndicator dividend;

    /**
     * Divisor or denominator in fraction.
     */
    @Getter
    @Setter
    private SimpleIndicator divisor;

    /**
     * Constructor.
     */
    public Quotient() {
        super();
    }

    @Override
    public Quotient clone() throws CloneNotSupportedException {
        final Quotient clone = (Quotient) super.clone();
        if (dividend != null) {
            clone.dividend = (SimpleIndicator) dividend.clone();
        }
        if (divisor != null) {
            clone.divisor = (SimpleIndicator) divisor.clone();
        }
        return clone;
    }

    @Override
    public double computeSingleValue(final Resource<? extends DailyData> data) throws IndicatorsException {
        if (dividend == null) {
            throw new IndicatorsException(ComputationErrorType.QUOTIENT_DIVIDEND_NULL);
        }
        if (divisor == null) {
            throw new IndicatorsException(ComputationErrorType.QUOTIENT_DIVISOR_NULL);
        }
        final double dividendValue;
        try {
            dividendValue = dividend.compute(data);
        } catch (final IndicatorsException e) {
            throw new IndicatorsException(ComputationErrorType.QUOTIENT_DIVIDEND_EXCEPTION, e, dividend.getId());
        }
        final double divisorValue;
        try {
            divisorValue = divisor.compute(data);
        } catch (final IndicatorsException e) {
            throw new IndicatorsException(ComputationErrorType.QUOTIENT_DIVISOR_EXCEPTION, e, divisor.getId());
        }
        if (divisorValue == 0.0) {
            throw new IndicatorsException(ComputationErrorType.QUOTIENT_DIVISOR_ZERO, divisor.getId());
        }
        return dividendValue / divisorValue;
    }

    @Override
    public List<Doublet<Parameter, Number>> getParameterDefaults() {
        final List<Doublet<Parameter, Number>> val = new ArrayList<>();
        if (dividend != null && dividend.getParameterDefaults() != null) {
            val.addAll(dividend.getParameterDefaults());
        }
        if (divisor != null && divisor.getParameterDefaults() != null) {
            val.addAll(divisor.getParameterDefaults());
        }
        return val;
    }

    @Override
    public List<Parameter> getParameters() {
        final List<Parameter> params = new ArrayList<>();
        if (dividend != null && dividend.getParameters() != null) {
            params.addAll(dividend.getParameters());
        }
        if (divisor != null && divisor.getParameters() != null) {
            params.addAll(divisor.getParameters());
        }
        return params;
    }

    @Override
    public Map<String, Double> getParametersValues() {
        final Map<String, Double> val = new HashMap<>();
        if (dividend != null && dividend.getParametersValues() != null) {
            val.putAll(dividend.getParametersValues());
        }
        if (divisor != null && divisor.getParametersValues() != null) {
            val.putAll(divisor.getParametersValues());
        }
        return val;
    }

    @Override
    public Set<Variable> getVariables() {
        final Set<Variable> variables = new HashSet<>();
        if (dividend != null) {
            variables.addAll(dividend.getVariables());
        }
        if (divisor != null) {
            variables.addAll(divisor.getVariables());
        }
        return variables;
    }

    @Override
    public boolean isComputable(final Resource<? extends DailyData> data) {
        if (dividend == null || divisor == null) {
            return false;
        }
        return dividend.isComputable(data) && divisor.isComputable(data);
    }

    @Override
    public void onIndicatorEvent(final IndicatorEvent event) {
        if (event.getAssociatedType() != IndicatorEvent.Type.CHANGE) {
            return;
        }
        final IndicatorEvent e = IndicatorEvent.Type.CHANGE.event(this);
        fireIndicatorEvent(e);
    }

    @Override
    public void setParametersFromKnowledge(final Knowledge knowledge) {
        if (dividend != null) {
            dividend.setParametersFromKnowledge(knowledge);
        }
        if (divisor != null) {
            divisor.setParametersFromKnowledge(knowledge);
        }
    }

    @Override
    public void setParametersValues(final Map<String, Double> values) {
        if (dividend != null) {
            dividend.setParametersValues(values);
        }
        if (divisor != null) {
            divisor.setParametersValues(values);
        }
    }

    @Override
    public String toStringTree(final String indent) {
        final StringBuilder sb = new StringBuilder();
        sb.append(toStringTreeBase(indent));

        if (dividend != null) {
            sb.append(indent).append("  dividend:\n");
            sb.append(dividend.toStringTree(indent + "  "))
            .append("\n");
        }
        if (divisor != null) {
            sb.append(indent).append("  divisor:\n");
            sb.append(divisor.toStringTree(indent + "  "))
            .append("\n");
        }

        return sb.toString();
    }

    @Override
    public void removeParameter(final Parameter param) {
        if (dividend != null) {
            this.dividend.removeParameter(param);
        }
        if (divisor != null) {
            this.divisor.removeParameter(param);
        }
    }
}
