/*
 * This file is part of Indicators.
 *
 * Indicators is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Indicators is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Indicators. If not, see <https://www.gnu.org/licenses/>.
 */
package fr.inrae.agroclim.indicators.model;

import java.util.List;
import java.util.Map;

import fr.inrae.agroclim.indicators.util.Doublet;

/**
 * Indicator or indicator criteria can have user defined parameters.
 *
 * Last change $Date$
 *
 * @author $Author$
 * @version $Revision$
 */
public interface HasParameters {
    /**
     * @return parameter for substituted attribute ⮕ value of attribute from the evaluation. Must not be null.
     */
    List<Doublet<Parameter, Number>> getParameterDefaults();

    /**
     * @return parameters (id and attribute) for the indicator or indicator
     * criteria.
     */
    List<Parameter> getParameters();

    /**
     * @return id of parameter ⮕ value of parameter
     */
    Map<String, Double> getParametersValues();

    /**
     * @param values id of parameter ⮕ value of parameter
     */
    void setParametersValues(Map<String, Double> values);
    /**
     * Delete parameter (for the indicator or criteria).
     * @param param parameter to delete
     */
    void removeParameter(Parameter param);
}
