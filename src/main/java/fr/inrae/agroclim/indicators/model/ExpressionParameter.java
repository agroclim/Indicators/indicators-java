/*
 * Copyright (C) 2021 INRAE AgroClim
 *
 * This file is part of Indicators.
 *
 * Indicators is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Indicators is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Indicators. If not, see <http://www.gnu.org/licenses/>.
 */
package fr.inrae.agroclim.indicators.model;

import java.io.Serializable;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlAttribute;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Parameter for JEXLFormula.
 *
 * Last change $Date$
 *
 * @author $Author$
 * @version $Revision$
 */
@EqualsAndHashCode(
        callSuper = false,
        of = {"name", "value"}
        )
@AllArgsConstructor
@NoArgsConstructor
@XmlAccessorType(XmlAccessType.FIELD)
public class ExpressionParameter implements Cloneable, Serializable {

    /**
     * UUID for Serializable.
     */
    private static final long serialVersionUID = 9205643160821888603L;

    /**
     * Variable name in formula.
     */
    @Getter
    @Setter
    @XmlAttribute(required = true)
    private String name;

    /**
     * Variable value in formula.
     */
    @Getter
    @Setter
    @XmlAttribute(required = true)
    private Double value;

    @Override
    public final ExpressionParameter clone() throws CloneNotSupportedException {
        final ExpressionParameter clone = (ExpressionParameter) super.clone();
        clone.name = name;
        clone.value = value;
        return clone;
    }
}
