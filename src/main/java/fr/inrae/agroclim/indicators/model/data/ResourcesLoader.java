/*
 * This file is part of Indicators.
 *
 * Indicators is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Indicators is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Indicators. If not, see <https://www.gnu.org/licenses/>.
 */
package fr.inrae.agroclim.indicators.model.data;

import java.util.Collection;
import java.util.Map;

import fr.inrae.agroclim.indicators.model.TimeScale;

/**
 * Common interface to retrieve resource, implemented by proxy or specialized
 * loader.
 *
 * Last changed : $Date$
 *
 * @author $Author$
 * @version $Revision$
 * @param <T>
 */
public interface ResourcesLoader<T> extends Cloneable, HasDataLoadingListener,
UseVariables {
    /**
     * @return cloned instance
     * @throws java.lang.CloneNotSupportedException common exception for cloning
     */
    ResourcesLoader<T> clone() throws CloneNotSupportedException;

    /**
     * @return null or configuration errors (missing key) or errors on
     *         configuration values
     */
    Map<String, String> getConfigurationErrors();

    /**
     * @return Missing variables, to check in aggregation indicators and
     *         criteria.
     */
    Collection<String> getMissingVariables();

    /**
     * @return data for resources. For a list, index is the day.
     */
    T load();

    /**
     * @param timeScale related time scales
     */
    void setTimeScale(TimeScale timeScale);
}
