/*
 * This file is part of Indicators.
 *
 * Indicators is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Indicators is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Indicators. If not, see <https://www.gnu.org/licenses/>.
 */
package fr.inrae.agroclim.indicators.model.indicator;

import java.util.HashSet;
import java.util.Set;

import fr.inrae.agroclim.indicators.exception.IndicatorsException;
import fr.inrae.agroclim.indicators.model.data.DailyData;
import fr.inrae.agroclim.indicators.model.data.Resource;
import fr.inrae.agroclim.indicators.model.data.Variable;

/**
 * Get number of days of phase.
 *
 * @author jcufi
 */
public final class PhaseLength extends SimpleIndicatorWithoutParameters
implements Detailable {
    /**
     * UUID for Serializable.
     */
    private static final long serialVersionUID = 6030595237342422012L;

    /**
     * Constructor.
     */
    public PhaseLength() {
        super();
    }

    @Override
    public PhaseLength clone() throws CloneNotSupportedException {
        return (PhaseLength) super.clone();
    }

    @Override
    public double computeSingleValue(final Resource<? extends DailyData> data) throws IndicatorsException {
        return data.getData().size();
    }

    @Override
    public Set<Variable> getVariables() {
        final Set<Variable> variables = new HashSet<>();
        // Some climatic data is needed to get data size.
        variables.add(Variable.TMEAN);
        return variables;
    }

    @Override
    public boolean isComputable(final Resource<? extends DailyData> data) {
        return data != null && !data.isEmpty();
    }
}
