/*
 * Copyright (C) 2020 INRAE AgroClim
 *
 * This file is part of Indicators.
 *
 * Indicators is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Indicators is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Indicators. If not, see <http://www.gnu.org/licenses/>.
 */
package fr.inrae.agroclim.indicators.model.criteria;

import java.util.Objects;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlEnum;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * Logical operator for the simple criteria and comparison criteria.
 *
 * Last changed : $Date$
 *
 * @author $Author$
 * @version $Revision$
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlEnum
@RequiredArgsConstructor
public enum RelationalOperator {
    /**
     * Equal to.
     */
    EQ(false, "=") {
        @Override
        boolean eval(final Double left, final Double right) {
            return Objects.equals(left, right);
        }
    },
    /**
     * Not equal to.
     */
    NE(false, "!=") {
        @Override
        boolean eval(final Double left, final Double right) {
            return !Objects.equals(left, right);
        }
    },
    /**
     * Greater than.
     */
    GT(false, ">") {
        @Override
        boolean eval(final Double left, final Double right) {
            return left != null && (right == null || left > right);
        }
    },
    /**
     * Less than.
     */
    LT(true, "<") {
        @Override
        boolean eval(final Double left, final Double right) {
            return left == null || right != null && left < right;
        }
    },
    /**
     * Greater than or equal to.
     */
    GE(false, ">=") {
        @Override
        boolean eval(final Double left, final Double right) {
            return Objects.equals(left, right) || GT.eval(left, right);
        }
    },
    /**
     * Less than or equal to.
     */
    LE(true, "<=") {
        @Override
        boolean eval(final Double left, final Double right) {
            return Objects.equals(left, right) || LT.eval(left, right);
        }
    };

    /**
     * If the operator is inferior.
     */
    @Getter
    private final boolean isInferior;

    /**
     * String representation.
     */
    @Getter
    private final String repr;

    /**
     * Evaluate the comparison between left value and right value using the
     * operator.
     *
     * @param left left value
     * @param right right value
     * @return evaluation
     */
    abstract boolean eval(Double left, Double right);
}
