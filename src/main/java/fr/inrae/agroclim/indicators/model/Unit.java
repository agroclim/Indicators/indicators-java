package fr.inrae.agroclim.indicators.model;

import java.io.Serializable;
import java.util.List;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlID;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

/**
 * Unit of an indicator result in case of usage with climatic variables at standard unit.
 *
 * Last change $Date$
 *
 * @author omaury
 * @author $Author$
 * @version $Revision$
 */
@XmlAccessorType(XmlAccessType.FIELD)
@EqualsAndHashCode(
        callSuper = false,
        of = {"id", "iri", "labels", "symbols"}
        )
public final class Unit implements Cloneable, Serializable {
    /**
     * UUID for Serializable.
     */
    private static final long serialVersionUID = 5432240898259571240L;

    /**
     * Id of unit.
     */
    @XmlElement
    @Getter
    @XmlID
    private String id;

    /**
     * Term IRI.
     */
    @XmlElement
    @Getter
    private String iri;

    /**
     * Localized labels.
     */
    @XmlElement(name = "label")
    @Getter
    @Setter
    private List<LocalizedString> labels;

    /**
     * Localized symbols.
     */
    @XmlElement(name = "symbol")
    @Getter
    @Setter
    private List<LocalizedString> symbols;
}
