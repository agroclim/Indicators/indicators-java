/*
 * This file is part of Indicators.
 *
 * Indicators is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Indicators is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Indicators. If not, see <https://www.gnu.org/licenses/>.
 */
package fr.inrae.agroclim.indicators.model.data;

import fr.inrae.agroclim.indicators.model.data.DataLoadingListener.DataFile;

/**
 * Listener for data loading.
 *
 * Last changed : $Date$
 *
 * @author $Author$
 * @version $Revision$
 */
public interface HasDataLoadingListener {
    /**
     * @param listener
     *            listener to add
     */
    void addDataLoadingListener(DataLoadingListener listener);

    /**
     * @param listeners
     *            listeners to add
     */
    void addDataLoadingListeners(DataLoadingListener[] listeners);

    /**
     * Notify observer that a new data is added.
     *
     * @param data
     *            new added data
     */
    void fireDataLoadingAddEvent(Data data);

    /**
     * Notify observer that data loading ends.
     *
     * @param text
     *            description
     */
    void fireDataLoadingEndEvent(String text);

    /**
     * Notify observer that data loading starts.
     *
     * @param text
     *            description
     */
    void fireDataLoadingStartEvent(String text);

    /**
     * Notify observer that data file was set.
     *
     * @param type data type
     */
    void fireDataSetEvent(DataFile type);

    /**
     * @return listeners for data loading
     */
    DataLoadingListener[] getDataLoadingListeners();
}
