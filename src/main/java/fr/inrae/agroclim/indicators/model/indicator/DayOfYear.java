/*
 * This file is part of Indicators.
 *
 * Indicators is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Indicators is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Indicators. If not, see <https://www.gnu.org/licenses/>.
 */
package fr.inrae.agroclim.indicators.model.indicator;

import fr.inrae.agroclim.indicators.exception.IndicatorsException;
import fr.inrae.agroclim.indicators.exception.type.ComputationErrorType;
import fr.inrae.agroclim.indicators.model.data.DailyData;
import fr.inrae.agroclim.indicators.model.data.Resource;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlRootElement;
import lombok.Getter;
import lombok.Setter;

/**
 * Getting first or last day (returned as DOY) when criteria is fulfilled.
 *
 * Criteria to compute indicator.
 *
 * Last changed : $Date$
 *
 * @author $Author$
 * @version $Revision$
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public final class DayOfYear extends SimpleIndicatorWithCriteria implements Detailable {
    /**
     * Hours in a day.
     */
    private static final double HOURS_IN_DAY = 24.;

    /**
     * UUID for Serializable.
     */
    private static final long serialVersionUID = 6030595237342422004L;

    /**
     * Return first day of year (true) or last (false).
     */
    @Getter
    @Setter
    private Boolean first;

    /**
     * Constructor.
     */
    public DayOfYear() {
        super();
    }

    @Override
    public DayOfYear clone() throws CloneNotSupportedException {
        final DayOfYear clone = (DayOfYear) super.clone();
        clone.first = first;
        return clone;
    }

    @Override
    public double computeSingleValue(final Resource<? extends DailyData> data) throws IndicatorsException {
        if (first == null) {
            throw new IndicatorsException(ComputationErrorType.WRONG_DEFINITION, "first must not be null");
        }
        double res = 0;
        boolean resultCriteria;
        for (final DailyData ddata : data.getData()) {
            resultCriteria = getCriteria().eval(ddata);
            if (resultCriteria) {
                res = ddata.getDayOfYear() + ddata.getHour() / HOURS_IN_DAY;
                if (Boolean.TRUE.equals(first)) {
                    break;
                }
            }
        }

        return res;
    }

}
