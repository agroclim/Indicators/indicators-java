/*
 * This file is part of Indicators.
 *
 * Indicators is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Indicators is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Indicators. If not, see <https://www.gnu.org/licenses/>.
 */
/**
 * Phenological data handling and modelling.
 *
 * Yearly crop:
 * - linear,
 *   - water balance: 4 stages
 *   - phenology: 6 stages
 * - curve,
 *   - water balance: 4 stages
 *   - phenology: 6 stages
 * Grapevine:
 * - curve_grapevine_sw,
 *   - water balance: 4 stages
 * - curve_grapevine,
 *   - phenology: 5 stages
 */
package fr.inrae.agroclim.indicators.model.data.phenology;
