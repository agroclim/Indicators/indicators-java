/*
 * This file is part of Indicators.
 *
 * Indicators is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Indicators is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Indicators. If not, see <https://www.gnu.org/licenses/>.
 */
package fr.inrae.agroclim.indicators.model.indicator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import fr.inrae.agroclim.indicators.exception.IndicatorsException;
import fr.inrae.agroclim.indicators.model.Knowledge;
import fr.inrae.agroclim.indicators.model.Parameter;
import fr.inrae.agroclim.indicators.model.data.DailyData;
import fr.inrae.agroclim.indicators.model.data.Resource;
import fr.inrae.agroclim.indicators.model.data.Variable;
import fr.inrae.agroclim.indicators.model.indicator.listener.IndicatorEvent;
import fr.inrae.agroclim.indicators.model.indicator.listener.IndicatorListener;
import fr.inrae.agroclim.indicators.util.Doublet;
import jakarta.xml.bind.annotation.XmlRootElement;
import jakarta.xml.bind.annotation.XmlType;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

/**
 * Substraction of 2 values from Sum.
 *
 * @author jcufi
 */
@XmlRootElement
@XmlType(propOrder = {"sumVariable1", "sumVariable2"})
@EqualsAndHashCode(
        callSuper = false,
        of = {"sumVariable1", "sumVariable2"}
        )
public final class DiffOfSum extends SimpleIndicator implements Detailable, IndicatorListener {

    /**
     * UUID for Serializable.
     */
    private static final long serialVersionUID = 6030595237342422005L;

    /**
     * Indicator which sums variable for minuend.
     */
    @Getter
    @Setter
    private Sum sumVariable1;

    /**
     * Indicator which sums variable for subtrahend.
     */
    @Getter
    @Setter
    private Sum sumVariable2;

    /**
     * Constructor.
     */
    public DiffOfSum() {
        super();
    }

    @Override
    public DiffOfSum clone() throws CloneNotSupportedException {
        final DiffOfSum clone = (DiffOfSum) super.clone();
        if (sumVariable1 != null) {
            clone.sumVariable1 = sumVariable1.clone();
        }
        if (sumVariable2 != null) {
            clone.sumVariable2 = sumVariable2.clone();
        }
        return clone;
    }

    @Override
    public double computeSingleValue(final Resource<? extends DailyData> res) throws IndicatorsException {
        final double sum1 = getSumVariable1().compute(res);
        final double sum2 = getSumVariable2().compute(res);
        return sum1 - sum2;
    }

    @Override
    public List<Doublet<Parameter, Number>> getParameterDefaults() {
        final List<Doublet<Parameter, Number>> val = new ArrayList<>();
        if (sumVariable1 != null) {
            val.addAll(sumVariable1.getParameterDefaults());
        }
        if (sumVariable2 != null) {
            val.addAll(sumVariable2.getParameterDefaults());
        }
        return val;
    }

    @Override
    public List<Parameter> getParameters() {
        final List<Parameter> params = new ArrayList<>();
        if (sumVariable1 != null) {
            params.addAll(sumVariable1.getParameters());
        }
        if (sumVariable2 != null) {
            params.addAll(sumVariable2.getParameters());
        }
        return params;
    }

    @Override
    public Map<String, Double> getParametersValues() {
        final Map<String, Double> val = new HashMap<>();
        if (sumVariable1 != null) {
            val.putAll(sumVariable1.getParametersValues());
        }
        if (sumVariable2 != null) {
            val.putAll(sumVariable2.getParametersValues());
        }
        return val;
    }

    @Override
    public Set<Variable> getVariables() {
        final Set<Variable> variables = new HashSet<>();
        if (sumVariable1 != null) {
            variables.addAll(sumVariable1.getVariables());
        }
        if (sumVariable2 != null) {
            variables.addAll(sumVariable2.getVariables());
        }
        return variables;
    }

    @Override
    public boolean isComputable(final Resource<? extends DailyData> res) {
        return sumVariable1.isComputable(res) && sumVariable2.isComputable(res);
    }

    @Override
    public void onIndicatorEvent(final IndicatorEvent event) {
        if (event.getAssociatedType() != IndicatorEvent.Type.CHANGE) {
            return;
        }
        final IndicatorEvent e = IndicatorEvent.Type.CHANGE.event(this);
        fireIndicatorEvent(e);
    }

    @Override
    public void removeParameter(final Parameter param) {
        if (sumVariable1 != null) {
            sumVariable1.removeParameter(param);
        }
        if (sumVariable2 != null) {
            sumVariable2.removeParameter(param);
        }
    }

    @Override
    public void setParametersFromKnowledge(final Knowledge knowledge) {
        if (sumVariable1 == null || sumVariable2 == null) {
            return;
        }
        final DiffOfSum indicator = (DiffOfSum) knowledge.getIndicator(getId());
        if (indicator == null) {
            return;
        }
        if (indicator.getSumVariable1() != null) {
            sumVariable1.setParameters(
                    indicator.getSumVariable1().getParameters());
        }
        if (indicator.getSumVariable2() != null) {
            sumVariable2.setParameters(
                    indicator.getSumVariable1().getParameters());
        }
    }

    @Override
    public void setParametersValues(final Map<String, Double> values) {
        if (sumVariable1 != null) {
            sumVariable1.setParametersValues(values);
        }
        if (sumVariable2 != null) {
            sumVariable2.setParametersValues(values);
        }
    }

    @Override
    public String toStringTree(final String indent) {
        final StringBuilder sb = new StringBuilder();
        sb.append(toStringTreeBase(indent));

        if (sumVariable1 != null) {
            sb.append(indent).append("  sumVariable1:\n");
            sb.append(sumVariable1.toStringTree(indent + "  "))
            .append("\n");
        }
        if (sumVariable2 != null) {
            sb.append(indent).append("  sumVariable2:\n");
            sb.append(sumVariable2.toStringTree(indent + "  "))
            .append("\n");
        }
        return sb.toString();
    }

}
