/*
 * Copyright (C) 2021 INRAE AgroClim
 *
 * This file is part of Indicators.
 *
 * Indicators is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Indicators is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Indicators. If not, see <http://www.gnu.org/licenses/>.
 */
package fr.inrae.agroclim.indicators.model.function.normalization;

import java.io.Serializable;

import jakarta.xml.bind.annotation.XmlRootElement;
import jakarta.xml.bind.annotation.XmlTransient;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Linear function on the related interval.
 *
 * Last change $Date$
 *
 * @author $Author$
 * @version $Revision$
 */
@XmlRootElement
@EqualsAndHashCode(of = {"min", "max", "linear"})
@ToString
public final class MultiLinearInterval implements Cloneable, Serializable {
    /**
     * UUID for Serializable.
     */
    private static final long serialVersionUID = 4872847709393688050L;

    /**
     * Minimum value of the interval, included.
     */
    @Getter
    @Setter
    private Double min;
    /**
     * Maximum value of the interval, excluded.
     */
    @Getter
    @Setter
    private Double max;
    /**
     * Linear function to apply.
     */
    @Getter
    @Setter
    private Linear linear = new Linear();

    @Override
    public MultiLinearInterval clone() {
        final MultiLinearInterval clone = new MultiLinearInterval();
        clone.min = this.min;
        clone.max = this.max;
        if (this.linear != null) {
            clone.linear = linear.clone();
        }
        return clone;
    }

    /**
     * @return A in A.x + B.
     */
    @XmlTransient
    public double getLinearA() {
        return linear.getLinearA();
    }

    /**
     * @return B in A.x + B.
     */
    @XmlTransient
    public double getLinearB() {
        return linear.getLinearB();
    }

    /**
     * Check if the value is in the interval.
     *
     * @param value value to check
     * @return true if the value is in the interval
     */
    public boolean matches(final double value) {
        if (min == null) {
            if (max != null) {
                return value < max;
            }
            return true;
        }
        if (max == null) {
            return min <= value;
        }
        return min <= value && value < max;
    }

    /**
     * @param linearA A in A.x + B.
     */
    public void setLinearA(final double linearA) {
        linear.setLinearA(linearA);
    }

    /**
     * @param linearB B in A.x + B.
     */
    public void setLinearB(final double linearB) {
        linear.setLinearB(linearB);
    }

}
