/*
 * This file is part of Indicators.
 *
 * Indicators is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Indicators is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Indicators. If not, see <https://www.gnu.org/licenses/>.
 */
package fr.inrae.agroclim.indicators.model.function.normalization;

import java.io.Serializable;
import java.util.Locale;

import fr.inrae.agroclim.indicators.model.Nameable;
import fr.inrae.agroclim.indicators.resources.I18n;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import lombok.ToString;

/**
 * Abstract for all normalization function.
 *
 * Nameable to display name in Combobox.
 *
 * @author jucufi
 */
@XmlAccessorType(XmlAccessType.FIELD)
@ToString
public abstract class NormalizationFunction implements Cloneable, Nameable, Serializable {

    /**
     * UUID for Serializable.
     */
    private static final long serialVersionUID = 4872847709393688040L;

    /**
     * Constructor with no args for Serializable.
     */
    protected NormalizationFunction() {
    }

    @Override
    public abstract NormalizationFunction clone();

    /**
     * @return the mathematic formula of normalisation with MathML syntax.
     */
    public abstract String getFormulaMathML();

    @Override
    public final String getName() {
        return getName(Locale.getDefault());
    }

    @Override
    public final String getName(final Locale locale) {
        final I18n res = new I18n("fr.inrae.agroclim.indicators.resources.messages", Locale.getDefault());
        return res.get("normalization." + getClass().getSimpleName());
    }

    /**
     * @param value value to normalize
     * @return normalized value between 0 and 1
     */
    public abstract double normalize(double value);
}
