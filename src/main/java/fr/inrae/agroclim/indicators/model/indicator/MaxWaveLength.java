/*
 * This file is part of Indicators.
 *
 * Indicators is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Indicators is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Indicators. If not, see <https://www.gnu.org/licenses/>.
 */
package fr.inrae.agroclim.indicators.model.indicator;

import fr.inrae.agroclim.indicators.exception.IndicatorsException;
import fr.inrae.agroclim.indicators.exception.type.ComputationErrorType;
import fr.inrae.agroclim.indicators.model.data.DailyData;
import fr.inrae.agroclim.indicators.model.data.Resource;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlRootElement;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

/**
 * Compute the maximum duration of a wave (nb days or hours) matching the criteria.
 *
 * Last changed : $Date$
 *
 * @author $Author$
 * @version $Revision$
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@EqualsAndHashCode(callSuper = true, of = {"threshold"})
public final class MaxWaveLength extends SimpleIndicatorWithCriteria implements Detailable {
    /**
     * UUID for Serializable.
     */
    private static final long serialVersionUID = 6030595237342422009L;

    /**
     * Number of step (day or hour) per wave.
     */
    @Getter
    @Setter
    private Integer threshold;

    /**
     * Constructor.
     */
    public MaxWaveLength() {
        super();
    }

    @Override
    public MaxWaveLength clone() throws CloneNotSupportedException {
        final MaxWaveLength clone = (MaxWaveLength) super.clone();
        clone.threshold = threshold;
        return clone;
    }

    @Override
    public double computeSingleValue(final Resource<? extends DailyData> climatic) throws IndicatorsException {
        if (getCriteria() == null) {
            throw new IndicatorsException(ComputationErrorType.CRITERIA_NULL);
        }
        if (threshold == null) {
            throw new IndicatorsException(ComputationErrorType.THRESHOLD_NULL);
        }
        int waveLength = 0;
        int max = 0;
        boolean resultCriteria;

        for (final DailyData data : climatic.getData()) {
            resultCriteria = getCriteria().eval(data);
            if (resultCriteria) {
                waveLength += 1;
                if (waveLength >= threshold && max < waveLength) {
                    max = waveLength;
                }
            } else {
                waveLength = 0;
            }
        }
        return max;

    }
}
