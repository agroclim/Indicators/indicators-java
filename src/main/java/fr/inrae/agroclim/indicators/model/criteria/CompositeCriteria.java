/*
 * This file is part of Indicators.
 *
 * Indicators is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Indicators is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Indicators. If not, see <https://www.gnu.org/licenses/>.
 */
package fr.inrae.agroclim.indicators.model.criteria;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import fr.inrae.agroclim.indicators.exception.IndicatorsException;
import fr.inrae.agroclim.indicators.exception.type.ComputationErrorType;
import fr.inrae.agroclim.indicators.model.Parameter;
import fr.inrae.agroclim.indicators.model.data.DailyData;
import fr.inrae.agroclim.indicators.model.data.Resource;
import fr.inrae.agroclim.indicators.model.data.Variable;
import fr.inrae.agroclim.indicators.model.data.climate.ClimaticDailyData;
import fr.inrae.agroclim.indicators.util.Doublet;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;

/**
 * Contains multiple criteria.
 *
 * Last changed : $Date$
 *
 * @author $Author$
 * @version $Revision$
 */
@EqualsAndHashCode(
        callSuper = true,
        of = {"criteria", "logicalOperator"}
        )
public final class CompositeCriteria extends Criteria {
    /**
     * UUID for Serialize.
     */
    private static final long serialVersionUID = 8096714573528536936L;

    /**
     * Criteria the composite criteria contains.
     */
    @Getter
    @Setter
    private List<Criteria> criteria;

    /**
     * The operator linking the criteria, AND by default.
     */
    @Setter
    @NonNull
    private LogicalOperator logicalOperator = LogicalOperator.AND;

    /**
     * Constructor.
     */
    public CompositeCriteria() {
        super();
    }

    @Override
    public CompositeCriteria clone() {
        CompositeCriteria clone = new CompositeCriteria();
        clone.criteria = new ArrayList<>();
        criteria.forEach(clone.criteria::add);
        clone.logicalOperator = logicalOperator;
        return clone;
    }

    @Override
    public boolean eval(final DailyData dailyData) throws IndicatorsException {
        if (logicalOperator == null) {
            throw new IndicatorsException(ComputationErrorType.WRONG_DEFINITION, "The logical operator must be set!");
        }
        switch (logicalOperator) {
        case AND:
            for (final Criteria aCriteria : criteria) {
                if (!aCriteria.eval(dailyData)) {
                    return false;
                }
            }
            return true;
        case OR:
            for (final Criteria aCriteria : criteria) {
                if (aCriteria.eval(dailyData)) {
                    return true;
                }
            }
            return false;
        case XOR:
            int nbTrue = 0;
            for (final Criteria aCriteria : criteria) {
                if (aCriteria.eval(dailyData)) {
                    nbTrue++;
                }
            }
            return nbTrue == 1;
        default:
            throw new RuntimeException("The logical operator is not handled: "
                    + logicalOperator.name());
        }
    }

    /**
     * @param data
     *            climatic data
     * @return formula
     * @throws IndicatorsException
     *             exception while getting data
     */
    public String getFormula(final ClimaticDailyData data) throws IndicatorsException {
        StringBuilder sb = new StringBuilder();
        boolean first = true;
        for (final Criteria aCriteria : criteria) {
            sb.append("(");
            if (aCriteria instanceof SimpleCriteria crit) {
                sb.append(crit.getFormula(data));
            } else if (aCriteria instanceof CompositeCriteria crit) {
                sb.append(crit.getFormula(data));
            }
            sb.append(")");
            if (first) {
                sb.append(" && ");
                first = false;
            }
        }
        return sb.toString();
    }

    /**
     * @return Logical operator to connect criteria.
     */
    public LogicalOperator getLogicalOperator() {
        return logicalOperator;
    }

    @Override
    public List<Doublet<Parameter, Number>> getParameterDefaults() {
        final List<Doublet<Parameter, Number>> val = new ArrayList<>();
        criteria.forEach(i -> val.addAll(i.getParameterDefaults()));
        return val;
    }

    @Override
    public List<Parameter> getParameters() {
        List<Parameter> parameters = new ArrayList<>();
        criteria.stream()
            .filter(crit -> crit.getParameters() != null)
            .forEach(crit ->
            crit.getParameters().forEach(param -> {
                if (!parameters.contains(param)) {
                    parameters.add(param);
                }
            })
        );
        return parameters;
    }

    @Override
    public Map<String, Double> getParametersValues() {
        final Map<String, Double> val = new HashMap<>();
        criteria.forEach(crit -> val.putAll(crit.getParametersValues()));
        return val;
    }

    @Override
    public Set<Variable> getVariables() {
        Set<Variable> variables = new HashSet<>();
        if (criteria == null) {
            return variables;
        }
        criteria.forEach(crit -> variables.addAll(crit.getVariables()));
        return variables;
    }

    @Override
    public boolean isComputable(final Resource<? extends DailyData> res) {
        for (final Criteria aCriteria : criteria) {
            if (!aCriteria.isComputable(res)) {
                return false;
            }
        }
        return true;
    }

    @Override
    public void setParametersValues(final Map<String, Double> values) {
        criteria.forEach(crit -> crit.setParametersValues(values));
    }

    @Override
    public String toStringTree(final String indent) {
        StringBuilder sb = new StringBuilder();
        sb.append(indent).append("  class: ").append(getClass().getName())
        .append("\n");
        sb.append(indent).append("  logicalOperator: ");
        if (logicalOperator != null) {
            sb.append(logicalOperator.name());
        }
        sb.append("\n");
        sb.append(indent).append("  criteria:\n");
        criteria.forEach(crit -> sb.append(crit.toStringTree(indent + "  ")));
        return sb.toString();
    }

    @Override
    public void removeParameter(final Parameter param) {
        for (final Criteria c : this.criteria) {
            if (c.getParameters().contains(param)) {
                c.getParameters().remove(param);
            }
        }
    }

}
