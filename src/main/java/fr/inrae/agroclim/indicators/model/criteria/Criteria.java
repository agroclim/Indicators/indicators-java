/*
 * This file is part of Indicators.
 *
 * Indicators is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Indicators is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Indicators. If not, see <https://www.gnu.org/licenses/>.
 */
package fr.inrae.agroclim.indicators.model.criteria;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.Serializable;
import java.util.List;

import fr.inrae.agroclim.indicators.exception.IndicatorsException;
import fr.inrae.agroclim.indicators.model.Computable;
import fr.inrae.agroclim.indicators.model.HasParameters;
import fr.inrae.agroclim.indicators.model.Parameter;
import fr.inrae.agroclim.indicators.model.criteria.visitor.CriteriaVisitable;
import fr.inrae.agroclim.indicators.model.criteria.visitor.CriteriaVisitor;
import fr.inrae.agroclim.indicators.model.data.DailyData;
import fr.inrae.agroclim.indicators.model.data.UseVariables;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlElementWrapper;
import jakarta.xml.bind.annotation.XmlRootElement;
import jakarta.xml.bind.annotation.XmlTransient;
import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

/**
 * Ancestor of all criteria.
 *
 * Last changed : $Date$
 *
 * @author jcufi
 * @author $Author$
 * @version $Revision$
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement
@EqualsAndHashCode(
        callSuper = false,
        of = {"parameters"}
        )
public abstract class Criteria
        implements Cloneable, CriteriaVisitable, Computable, HasParameters, Serializable, UseVariables {
    /**
     * UUID for Serializable.
     */
    private static final long serialVersionUID = -4284278102762199668L;

    /**
     * Parameters for indicator criteria.
     */
    @XmlElementWrapper(name = "parameters")
    @XmlElement(name = "parameter")
    @Getter
    @Setter
    private List<Parameter> parameters;

    /**
     * Handling PropertyChangeListeners.
     */
    @XmlTransient
    @Getter(AccessLevel.PROTECTED)
    private final PropertyChangeSupport propertySupport;

    /**
     * Constructor.
     */
    protected Criteria() {
        this.propertySupport = new PropertyChangeSupport(this);
    }

    @Override
    public final void accept(final CriteriaVisitor v) {
        v.visit(this);
    }

    /**
     * @param listener listener
     */
    public final void addPropertyChangeListener(
            final PropertyChangeListener listener) {
        if (propertySupport.getPropertyChangeListeners() != null) {
            for (final PropertyChangeListener l
                    : propertySupport.getPropertyChangeListeners()) {
                if (listener.equals(l)) {
                    return;
                }
            }
        }
        propertySupport.addPropertyChangeListener(listener);
    }

    @Override
    public abstract Criteria clone();

    /**
     * @param data
     *            data to compare
     * @return comparison result
     * @throws IndicatorsException
     *             exception during evaluation
     */
    public abstract boolean eval(DailyData data) throws IndicatorsException;

    /**
     * @param indent
     *            indentation string
     * @return Structured string representation.
     */
    public abstract String toStringTree(String indent);

}
