/*
 * This file is part of Indicators.
 *
 * Indicators is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Indicators is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Indicators. If not, see <https://www.gnu.org/licenses/>.
 */
package fr.inrae.agroclim.indicators.model.data.phenology;

import java.io.Serializable;
import java.util.Objects;

/**
 * Phenological stage (name, DOY).
 */
public final class Stage implements Cloneable, Serializable {
    /**
     * UUID for Serializable.
     */
    private static final long serialVersionUID = 1709651110835570004L;
    /**
     * 1.
     */
    public static final int ONE = 1;
    /**
     * 2.
     */
    public static final int TWO = 2;
    /**
     * 3.
     */
    public static final int THREE = 3;
    /**
     * 4.
     */
    public static final int FOUR = 4;
    /**
     * 5.
     */
    public static final int FIVE = 5;

    /**
     * Stage name.
     */
    private String name;

    /**
     * DOY.
     *
     * For winter crop, first stage inferior to 365, last stage superior to 365.
     */
    private int value;

    /**
     * Constructor.
     */
    public Stage() {
        super();
    }

    /**
     * Constructor.
     *
     * @param n
     *            stage name
     * @param v
     *            doy
     */
    public Stage(final String n, final int v) {
        super();
        this.name = n;
        this.value = v;
    }

    @Override
    public Stage clone() {
        return new Stage(name, value);
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Stage other = (Stage) obj;
        if (this.value != other.value) {
            return false;
        }
        return Objects.equals(this.name, other.name);
    }

    /**
     * @return stage name
     */
    public String getName() {
        return name;
    }

    /**
     * @return DOY
     */
    public int getValue() {
        return value;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + Objects.hashCode(name);
        result = prime * result + value;
        return result;
    }

    /**
     * @param n
     *            stage name
     */
    public void setName(final String n) {
        this.name = n;
    }

    /**
     * @param v
     *            DOY
     */
    public void setValue(final int v) {
        this.value = v;
    }

    @Override
    public String toString() {
        return "Stage [name=" + name + ", value=" + value + "]";
    }
}
