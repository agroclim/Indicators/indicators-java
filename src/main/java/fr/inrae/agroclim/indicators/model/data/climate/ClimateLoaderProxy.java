/*
 * This file is part of Indicators.
 *
 * Indicators is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Indicators is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Indicators. If not, see <https://www.gnu.org/licenses/>.
 */
package fr.inrae.agroclim.indicators.model.data.climate;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import fr.inrae.agroclim.indicators.model.TimeScale;
import fr.inrae.agroclim.indicators.model.data.DataLoadingListenerHandler;
import fr.inrae.agroclim.indicators.model.data.Variable;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlTransient;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

/**
 * Proxy to load climate data.
 *
 * Loader is chosen according to properties (XML tag).
 *
 * The proxy is initialized from evaluation XML file.
 *
 * Last changed : $Date$
 *
 * @author $Author$
 * @version $Revision$
 */
@XmlAccessorType(XmlAccessType.FIELD)
@EqualsAndHashCode(
        callSuper = false,
        of = {"etpCalculator", "file", "loader", "windAt10"}
        )
public final class ClimateLoaderProxy extends DataLoadingListenerHandler implements ClimateLoader {
    /**
     * UUID for Serializable.
     */
    private static final long serialVersionUID = 455956144995603377L;

    /**
     * Calculator to compute ETP from climatic daily data.
     */
    @XmlTransient
    private EtpCalculator etpCalculator;

    /**
     * CSV file.
     */
    @Getter
    @Setter
    @XmlElement(name = "file")
    private ClimateFileLoader file;

    /**
     * Generic loader.
     */
    @Setter
    @XmlTransient
    private ClimateLoader loader;

    /**
     * Related time scales.
     */
    @Setter
    @XmlTransient
    private TimeScale timeScale = TimeScale.DAILY;

    /**
     * Wind speed is measure at 10m. If not set, true.
     */
    @XmlElement(name = "windspeedat10m")
    private Boolean windAt10;

    @Override
    public ClimateLoaderProxy clone() throws CloneNotSupportedException {
        final ClimateLoaderProxy clone = new ClimateLoaderProxy();
        if (etpCalculator != null) {
            clone.etpCalculator = etpCalculator.clone();
        }
        if (file != null) {
            clone.file = file.clone();
        }
        if (loader != null) {
            clone.loader = loader.clone();
        }
        clone.windAt10 = windAt10;
        return clone;
    }

    @Override
    public Map<String, String> getConfigurationErrors() {
        if (getLoader() == null) {
            final Map<String, String> errors = new HashMap<>();
            errors.put("climate", "error.evaluation.climate.loader.missing");
            return errors;
        }
        return getLoader().getConfigurationErrors();
    }

    /**
     * @return Calculator to compute ETP from climatic daily data.
     */
    public EtpCalculator getEtpCalculator() {
        if (etpCalculator == null) {
            etpCalculator = new EtpPenmanMonteithFAO(windAt10 == null
                    || windAt10);
        }
        return etpCalculator;
    }

    /**
     * @return file/database loader according to configuration.
     */
    private ClimateLoader getLoader() {
        if (file != null) {
            file.addDataLoadingListeners(getDataLoadingListeners());
            file.setEtpCalculator(getEtpCalculator());
            file.setTimeScale(timeScale);
            return file;
        } else if (loader != null) {
            loader.addDataLoadingListeners(getDataLoadingListeners());
            return loader;
        }
        return null;
    }

    /**
     * @return Missing climatic variables, to check in aggregation indicators.
     */
    @Override
    public Collection<String> getMissingVariables() {
        if (getLoader() == null) {
            return new HashSet<>();
        }
        return getLoader().getMissingVariables();
    }

    /**
     * @return variables provided by the loader
     */
    @Override
    public Set<Variable> getProvidedVariables() {
        if (getLoader() == null) {
            return new HashSet<>();
        }
        return getLoader().getProvidedVariables();
    }

    @Override
    public Set<Variable> getVariables() {
        if (getLoader() == null) {
            return new HashSet<>();
        }
        return getLoader().getVariables();
    }

    @Override
    public List<ClimaticDailyData> load() {
        if (getLoader() == null) {
            return new ArrayList<>();
        }
        getLoader().setTimeScale(timeScale);
        return getLoader().load();
    }
}
