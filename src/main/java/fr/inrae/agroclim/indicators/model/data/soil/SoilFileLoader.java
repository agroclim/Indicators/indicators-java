/*
 * This file is part of Indicators.
 *
 * Indicators is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Indicators is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Indicators. If not, see <https://www.gnu.org/licenses/>.
 */
package fr.inrae.agroclim.indicators.model.data.soil;

import java.io.File;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import fr.inrae.agroclim.indicators.model.TimeScale;
import fr.inrae.agroclim.indicators.model.data.FileLoader;
import fr.inrae.agroclim.indicators.model.data.Resource;
import fr.inrae.agroclim.indicators.model.data.Variable;
import lombok.EqualsAndHashCode;

/**
 * Load soil data from file.
 *
 * Last changed : $Date$
 *
 * @author $Author$
 * @version $Revision$
 */
@EqualsAndHashCode(callSuper = true, of = {"file", "headers", "separator"})
public final class SoilFileLoader extends FileLoader implements SoilLoader {
    /**
     * UID for serialization.
     */
    private static final long serialVersionUID = -2430155206967092814L;
    /**
     * Allowed headers for the CSV file.
     */
    private static final List<String> ALLOWED_HEADERS = Arrays.asList("year",
            "month", "day", "swc", "waterReserve");
    /**
     * CSV file.
     */
    private final File file;

    /**
     * Headers of CSV file.
     */
    private final String[] headers;

    /**
     * CSV separator.
     */
    private String separator = Resource.DEFAULT_SEP;

    /**
     * Constructor.
     *
     * @param csvFile
     *            CSV file
     * @param csvHeaders
     *            CSV headers
     * @param csvSeparator
     *            CSV separator
     */
    public SoilFileLoader(final File csvFile, final String[] csvHeaders,
            final String csvSeparator) {
        this.file = csvFile;
        this.headers = csvHeaders;
        this.separator = csvSeparator;
    }

    @Override
    public SoilFileLoader clone() {
        return new SoilFileLoader(file, headers, separator);
    }

    @Override
    public Map<String, String> getConfigurationErrors() {
        final Map<String, String> errors = new HashMap<>();
        if (file == null) {
            errors.put("soil.file", "error.evaluation.soil.file.missing");
        } else if (!file.exists()) {
            errors.put("soil.file", "error.evaluation.soil.file.doesnotexist");
        } else if (file.length() == 0) {
            errors.put("soil.file", "error.evaluation.soil.file.empty");
        }
        if (separator == null) {
            errors.put("soil.separator",
                    "error.evaluation.soil.separator.missing");
        } else if (separator.isEmpty()) {
            errors.put("soil.separator",
                    "error.evaluation.soil.separator.empty");
        }
        if (headers == null) {
            errors.put("soil.header", "error.evaluation.soil.header.missing");
        }
        if (errors.isEmpty()) {
            return null;
        }
        return errors;
    }

    @Override
    public Collection<String> getMissingVariables() {
        throw new RuntimeException("Not implemented for soil!");
    }

    @Override
    public Set<Variable> getProvidedVariables() {
        return super.getProvidedVariables(headers);
    }

    @Override
    public Set<Variable> getVariables() {
        return new HashSet<>();
    }

    @Override
    public List<SoilDailyData> load() {
        return load(file, separator, headers, ALLOWED_HEADERS,
                SoilDailyData.class);
    }

    @Override
    public void setTimeScale(final TimeScale timeScale) {
        // do nothing
    }
}
