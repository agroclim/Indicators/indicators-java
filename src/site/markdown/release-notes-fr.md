---
title: Notes de version
description: Modifications de la bibliothèque d'indicateurs.
keywords: "version"
date: 2024-09-19
---

# [v2.1.1](https://forgemia.inra.fr/agroclim/Indicators/indicators-java/-/releases/v2.1.1) − 18 octobre 2024

- Modifier le seuil de l'indicateur `excraidays` à `RAIN > 20`.

# [v2.1.0](https://forgemia.inra.fr/agroclim/Indicators/indicators-java/-/releases/v2.1.0) − 19 septembre 2024

- Ajouter la méthode `Evaluation.computeEachDate()`.
- Renvoyer les résultats de calcul par la méthode `Evaluation.compute()` et ne plus les stocker dans `Evaluation`.
- Supprimer `Evaluation#getResults()`.
- Supprimer `Evaluation#getComputedPhases()`.
- Supprimer `CompositeIndicator#toAggregate(boolean)`.
- Passer le nombre de jours de la phase S4 - S5 pour la vigne à 35.

# [v2.0.2](https://forgemia.inra.fr/agroclim/Indicators/indicators-java/-/releases/v2.0.2) − 11 juillet 2024

- Passer à Jakarta XML Binding.

# [v2.0.1](https://forgemia.inra.fr/agroclim/Indicators/indicators-java/-/releases/v2.0.1) − 5 avril 2024

- Ajouter le modèle phénologique Richardson.
- Renommer `CompositeIndicator.toAggregate(boolean)`.
- Corriger l'id et la référence d'indicateurs THI.
- Dépréciation `CompositeIndicator#toAggregate(boolean)`.

# [v2.0.0](https://forgemia.inra.fr/agroclim/Indicators/indicators-java/-/releases/v2.0.0) − 18 janvier 2024

- Gestion des données climatiques manquantes.
- Changement de gestion des exceptions et affichage de codes d'erreurs.
- Passage sous GitLab.
- Mise en ligne de la documentation.
