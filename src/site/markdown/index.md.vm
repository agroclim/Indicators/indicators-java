#set($h1 = '#')
#set($h2 = '##')
#set($h3 = '###')
#set($h4 = '####')
<head>
    <title>Documentation technique de ${project.name}</title>
</head>

$h1 Documentation technique de ${project.name}

Bibliothèque d’indicateurs, utilisable autant en mode éco-climatique (avec modèle phénologique intégré) qu’en mode agro-climatique.
Cette bibliothèque est utilisée dans le logiciel de bureau [GETARI](https://agroclim.inrae.fr/getari/) et la chaîne de traitement SEASON intégrée dans [SICLIMA](https://agroclim.inrae.fr/siclima/).

$h2 Liens du projet :

- [Gestion de projet](project-info.html) : les informations sur la gestion de version, la gestion des tâches et bogues et le serveur d’intégration.
- [Dépendances](dependencies.html).
- [Licence du projet](license.html) : reprend la balise `<licenses>` du fichier `pom.xml`.

$h2 Organisation du code

Le code source est organisé ainsi :

- `pom.xml` : fichier Maven
- `src/main/` : code source
- `src/test/` : code pour les tests
- `src/site/` : documentation, voir ci-dessous

$h3 Documentation

La documentation est écrite dans le dossier `/src/site/markdown/`.
L’arborescence du site est définie dans `/src/site/site.xml` et `/src/site/site_en.xml`.
Les formats plein texte sont préférés pour pouvoir gérer les versions.
Ce document est écrit dans le format Markdown.

Les diagrammes UML sont créés dans le format [PlantUML](http://plantuml.com/)
(extension `.puml` dans `/src/site/resources/images/`).
Voici les préfixes pour les diagrammes :

- `act-` : diagrammes d’activité
- `cas-` : diagrammes de cas d’utilisation
- `cls-` : diagrammes de classe
- `cmp-` : diagrammes de composants
- `seq-` : diagrammes de séquence

La documentation technique est générée par Maven.
Les fichiers Markdown listant les indicateurs et les codes d’erreur sont générés par la classe `GenerateMarkdown`.

```
mvn exec:java -DskipTests -Dexec.mainClass=fr.inrae.agroclim.indicators.GenerateMarkdown -Dexec.args='${basedir}/src/site/markdown -'
mvn site
```

Jetty peut être utilisé pour le rendu en temps réel,
c’est-à-dire qu’il suffit de rafraîchir le navigateur pour afficher
les changements : utiliser `mvn site:run` et visiter le lien,
généralement http://localhost:8080/.

Au besoin, regénérer les diagrammes UML avec la commande
`mvn com.github.jeluard:plantuml-maven-plugin:generate`.
