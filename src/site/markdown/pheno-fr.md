---
title: Modèles phénologiques
description: Détails des modèles phénologiques implémentés dans la bibliothèque
keywords: "phénologie", "modèle"
date: 2024-03-25
---

La bibliothèque d'indicateurs comporte plusieurs modèles phénologiques.
Ils sont implémentés dans le paquet `fr.inrae.agroclim.indicators.model.data.phenology`.

# Modèle curvilinéaire

Code : `curve`

# Modèle curvilinéaire pour la vigne

Code : `curve_grapevine`

C'est une déclinaison du modèle curvilinéaire pour la vigne.

# Modèle curvilinéaire vigne bilan hydrique

Code : `curve_grapevine_sw`

C'est une déclinaison du modèle curvilinéaire pour la vigne, dans le cadre du calcul du bilan hydrique.

# Modèle Linéaire


Code : `linear`

# Modèle Richardson

Code : `richardson`

Ce modèle est décrit dans « Theau, J. P., & Zerourou, A. (2008). Herb’âge, une méthode de calcul des sommes de températures pour la gestion des prairies. Les Cahiers d’Orphée, 103-114. ».
Il a été implémenté pour le ticket #9.

Ce modèle calcule les somme des températures moyennes supérieures à 0°C avec écrêtage à 18°C à partir du premier février.

Il fonctionne sur 1 an.

*s0* est le début de la saison, c'est-à-dire le paramètre `sowingDate` (date de semis).

Il fournit 3 stades :

- s1 : épi 10 centimètres
- s2 : épiaison
- s3 : floraison

Les paramètres variétaux nécessaires sont repris dans le fichier de test `pheno_richardson_3_stages-gramineesa.ini`.
