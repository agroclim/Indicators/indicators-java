<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<!DOCTYPE xml>
<!--
This file is part of Indicators

Indicators is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Indicators is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Indicators. If not, see <https://www.gnu.org/licenses/>.
-->
<evaluationSettings>
    <climate>
        <file path="../model/data/climate/climat_sample.txt">
            <separator>	</separator>
            <header>year</header>
            <header>month</header>
            <header>day</header>
            <header>tmin</header>
            <header>tmax</header>
            <header>radiation</header>
            <header>rain</header>
            <header></header>
            <header>wind</header>
            <header></header>
            <header></header>
            <header></header>
            <header>etp</header>
        </file>
    </climate>
    <phenology>
        <file path="../model/data/phenology/pheno_sample.csv">
            <separator>;</separator>
            <header>year</header>
            <header>s0</header>
            <header>s1</header>
            <header>s2</header>
            <header>s3</header>
            <header>s4</header>
        </file>
    </phenology>
    <name>test</name>
    <compositeIndicator>
        <name xml:lang="en">test</name>
        <id>root-evaluation</id>
        <tag>climatic</tag>
        <aggregationFunction xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="jexlFunction" expression="(s0s1+s2s3+s0s2)/3"/>
        <indicator xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="compositeIndicator">
            <name xml:lang="en">s1</name>
            <id>s0s1</id>
            <tag>pheno-s0-s1</tag>
            <category>pheno</category>
            <color>#664DB3</color>
            <indicator xsi:type="compositeIndicator">
                <name xml:lang="en">s0</name>
                <id>pheno_s0</id>
                <category>pheno</category>
                <color>#664DB3</color>
            </indicator>
            <indicator xsi:type="compositeIndicator">
                <name xml:lang="en">Crop mortality</name>
                <name xml:lang="fr">Mortalité culturale</name>
                <id>mort</id>
                <category>ecoprocesses</category>
                <color>#664DB3</color>
                <indicator xsi:type="compositeIndicator">
                    <name xml:lang="en">Water damage</name>
                    <name xml:lang="fr">Dégâts d'eau</name>
                    <id>watdam</id>
                    <category>climatic</category>
                    <color>#664DB3</color>
                    <indicator xsi:type="numberOfDays">
                        <description xml:lang="en">Number of heavy rainy days (&gt; {heavyRain} mm).</description>
                        <description xml:lang="fr">Nombre de jours de fortes pluies (&gt; {heavyRain} mm).</description>
                        <name xml:lang="en">Heavy rain days</name>
                        <name xml:lang="fr">Jours de fortes pluies</name>
                        <id>hraidays</id>
                        <category>indicator</category>
                        <color>#664DB3</color>
                        <normalizationFunction xsi:type="exponential" expA="0.1" expB="4.0"/>
                        <criteria xsi:type="simpleCriteria">
                            <parameters>
<parameter>
    <attribute>threshold</attribute>
    <id>heavyRain</id>
</parameter>
                            </parameters>
                            <variable>rain</variable>
                            <inferiorToThreshold>false</inferiorToThreshold>
                            <strict>true</strict>
                            <threshold>35.0</threshold>
                            <noThreshold>false</noThreshold>
                        </criteria>
                    </indicator>
                </indicator>
            </indicator>
        </indicator>
        <indicator xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="compositeIndicator">
            <name xml:lang="en">s3</name>
            <id>s2s3</id>
            <tag>pheno-s2-s3</tag>
            <category>pheno</category>
            <color>#5F9EA0</color>
            <indicator xsi:type="compositeIndicator">
                <name xml:lang="en">s2</name>
                <id>pheno_s2</id>
                <category>pheno</category>
                <color>#5F9EA0</color>
            </indicator>
            <indicator xsi:type="compositeIndicator">
                <name xml:lang="en">Yield quality</name>
                <name xml:lang="fr">Qualité des récoltes</name>
                <id>qual</id>
                <category>ecoprocesses</category>
                <color>#5F9EA0</color>
                <indicator xsi:type="compositeIndicator">
                    <name xml:lang="en">Radiation deficit</name>
                    <name xml:lang="fr">Déficit de rayonnement</name>
                    <id>radd</id>
                    <category>climatic</category>
                    <color>#5F9EA0</color>
                    <indicator xsi:type="numberOfDays">
                        <description xml:lang="en">Number of days with radiation stress (radiation &lt; 200.0 w/m²).</description>
                        <description xml:lang="fr">Nombre de jours de stress radiatif (rayonnement &lt; 200,0 w/m²).</description>
                        <name xml:lang="en">Radiative stress days</name>
                        <name xml:lang="fr">Jours à faible rayonnement</name>
                        <id>rsdays</id>
                        <category>indicator</category>
                        <color>#5F9EA0</color>
                        <normalizationFunction xsi:type="sigmoid" sigmoidA="20.0" sigmoidB="-4.0"/>
                        <criteria xsi:type="simpleCriteria">
                            <variable>radiation</variable>
                            <inferiorToThreshold>true</inferiorToThreshold>
                            <strict>true</strict>
                            <threshold>200.0</threshold>
                            <noThreshold>false</noThreshold>
                        </criteria>
                    </indicator>
                </indicator>
            </indicator>
        </indicator>
        <indicator xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="compositeIndicator">
            <name xml:lang="en">s2</name>
            <id>s0s2</id>
            <tag>pheno-s0-s2</tag>
            <category>ecoprocesses</category>
            <color>#E6B3CC</color>
            <indicator xsi:type="compositeIndicator">
                <name xml:lang="en">s0</name>
                <id>pheno_s0</id>
                <category>pheno</category>
                <color>#E6B3CC</color>
            </indicator>
            <indicator xsi:type="compositeIndicator">
                <name xml:lang="en">Crop interventions other than sowing</name>
                <name xml:lang="fr">Interventions culturales sauf semis</name>
                <id>intervention</id>
                <category>practices</category>
                <color>#E6B3CC</color>
                <indicator xsi:type="compositeIndicator">
                    <name xml:lang="en">Wind conditions</name>
                    <name xml:lang="fr">Conditions de vent</name>
                    <id>wind</id>
                    <category>climatic</category>
                    <color>#E6B3CC</color>
                    <indicator xsi:type="numberOfDays">
                        <description xml:lang="en">Number of days with high wind (wind speed &gt; 5.28 m/s).</description>
                        <description xml:lang="fr">Nombre de jours de grand vent (vitesse de vent &gt; 5,28 m/s).</description>
                        <name xml:lang="en">High wind days</name>
                        <name xml:lang="fr">Jours de grand vent</name>
                        <id>hwinddays</id>
                        <category>indicator</category>
                        <color>#E6B3CC</color>
                        <normalizationFunction xsi:type="sigmoid" sigmoidA="30.0" sigmoidB="-5.0"/>
                        <criteria xsi:type="simpleCriteria">
                            <variable>wind</variable>
                            <inferiorToThreshold>false</inferiorToThreshold>
                            <strict>true</strict>
                            <threshold>5.28</threshold>
                            <noThreshold>false</noThreshold>
                        </criteria>
                    </indicator>
                </indicator>
            </indicator>
        </indicator>
    </compositeIndicator>
</evaluationSettings>
