/*
 * This file is part of Indicators.
 *
 * Indicators is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Indicators is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Indicators. If not, see <https://www.gnu.org/licenses/>.
 */
package fr.inrae.agroclim.indicators.util;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;

import com.fasterxml.jackson.databind.MappingIterator;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;

import fr.inrae.agroclim.indicators.model.Evaluation;
import fr.inrae.agroclim.indicators.model.data.DataTestHelper;
import fr.inrae.agroclim.indicators.model.data.phenology.AnnualStageData;
import fr.inrae.agroclim.indicators.model.data.phenology.Stage;
import fr.inrae.agroclim.indicators.model.data.phenology.StageDelta;
import lombok.extern.log4j.Log4j2;

/**
 * Test StageUtils.
 */
@Log4j2
public final class StageUtilsTest extends DataTestHelper {

    @Test
    public void checkGood() {
        Evaluation eval = getEvaluation(getEvaluationTestFile());
        eval.initializeResources();
        assertNotNull(eval);
        Map<String, List<String>> actual = StageUtils.check(eval);
        assertTrue(actual.isEmpty());
    }

    @Test
    public void checkWrong() {
        File file = getFile("xml/test-evaluation-wrong-stages.xml");
        Evaluation eval = getEvaluation(file);
        eval.initializeResources();
        assertNotNull(eval);
        Map<String, List<String>> actual = StageUtils.check(eval);
        assertFalse(actual.isEmpty());
        assertEquals(1, actual.size());
        assertTrue(actual.containsKey("s1"));
        assertEquals(1, actual.get("s1").size());
        assertEquals("s0s1", actual.get("s1").get(0));
    }

    /**
     * Test StageUtils.getDeltasFromStages().
     */
    @Test
    public void getDeltasFromStages() {
        final List<String> stages = Arrays.asList("s0p1", "s6m11");
        final List<StageDelta> expected = new ArrayList<>();
        expected.add(new StageDelta("s0", 1));
        expected.add(new StageDelta("s6", -11));
        final List<StageDelta> actual = StageUtils.getDeltasFromStages(stages);
        assertEquals(expected, actual);
    }

    @Test
    public void repr() {
        final Map<String, StageDelta> tests = new HashMap<>();
        tests.put("s0m1", new StageDelta("s0", -1));
        tests.put("s0p1", new StageDelta("s0", 1));
        tests.put("s10", new StageDelta("s10", 0));
        tests.forEach((expected, delta) -> {
            assertEquals(expected, StageUtils.repr(delta));
        });
    }

    @Test
    public void sanitizeStagesForSoil() throws FileNotFoundException, IOException {
        LOGGER.traceEntry();
        final File file = getFile("util/stages.csv");
        final List<AnnualStageData> data = new ArrayList<>();
        CsvMapper mapper = new CsvMapper();
        CsvSchema schema = CsvSchema.emptySchema().withHeader();
        try (MappingIterator<Map<String, String>> it = mapper.readerFor(Map.class)
        .with(schema)
                .readValues(file)) {
            while (it.hasNext()) {
                Map<String, String> map = it.next();
                if (map == null) {
                    continue;
                }
                final Integer year = Integer.parseInt(map.get("year"));
                AnnualStageData annual = new AnnualStageData(year);
                data.add(annual);
                for (final Map.Entry<String, String> entry : map.entrySet()) {
                    if (entry.getKey().equals("year")) {
                        continue;
                    }
                    if (entry.getValue() != null && !entry.getValue().isEmpty()) {
                        annual.add(entry.getKey(), Integer.parseInt(entry.getValue()));
                    }
                }
            }

        }
        assertFalse(data.isEmpty());
        final int sowingDate = data.get(0).getStageValue("s0");
        List<AnnualStageData> actual;
        actual = StageUtils.sanitizeStagesForSoil(data, sowingDate);
        assertFalse(actual.isEmpty());
        assertEquals(data.size(), actual.size());
        actual.forEach((each)
                -> assertEquals(
                        "Year=" + each.getYear() + ", " + each,
                        Stage.FOUR,
                        each.getStages().size())
                );
    }
}
