/*
 * This file is part of Indicators.
 *
 * Indicators is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Indicators is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Indicators. If not, see <https://www.gnu.org/licenses/>.
 */
package fr.inrae.agroclim.indicators.util;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import org.junit.Test;

/**
 * Test DateUtils.
 *
 * Last changed : $Date$
 *
 * @author $Author$
 * @version $Revision$
 */
public final class DateUtilsTest {

    /**
     * Test asLocalDate(date).
     */
    @Test
    public void asLocalDate() {
        final Integer year = 1959;
        final Integer doy = 274;
        Date date = DateUtils.getDate(year, doy);
        LocalDate localDate = DateUtils.asLocalDate(date);
        assertEquals(doy.intValue(), localDate.getDayOfYear());
        assertEquals(year.intValue(), localDate.getYear());
    }

    /**
     * Test getDate(year, doy).
     */
    @Test
    public void getDate() {
        final Integer year = 1959;
        final Integer doy = 274;
        Date found = DateUtils.getDate(year, doy);
        Date expected;
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
            expected = sdf.parse(year + "-10-01 00:00:00");
        } catch (ParseException ex) {
            throw new RuntimeException("This should never occur!", ex);
        }
        assertEquals(expected, found);

        Integer foundYear = DateUtils.getYear(found);
        assertEquals(year, foundYear);

        Integer foundDoy = DateUtils.getDoy(found);
        assertEquals(doy, foundDoy);
    }

    /**
     * Test midnight handling.
     */
    @Test
    public void midnight() {
         final Integer year = 1959;
        final Integer month = 1;
        final Integer day = 1;
        final Integer doy = 1;
        final Integer hour = 0;
        final Date expectedDate = Date.from(LocalDateTime.of(year, Month.of(month), day, hour, 0)
                .atZone(ZoneId.of("UTC")).toInstant());
        final Date date = DateUtils.getDate(year, doy);
        final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        final String expected = "1959-01-01 00:00:00";
        final String actual = sdf.format(date);
        assertEquals(expected, actual);
        assertEquals(year, DateUtils.getYear(date));
        assertEquals(month, DateUtils.getMonth(date));
        assertEquals(day, DateUtils.getDom(date));
        assertEquals(expectedDate, date);

        Calendar cal = DateUtils.getCalendar();
        cal.set(Calendar.YEAR, year);
        cal.set(Calendar.DAY_OF_YEAR, doy);
        cal.set(Calendar.HOUR_OF_DAY, hour);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        final Date date2 = cal.getTime();
        assertEquals(date, date2);
    }

    /**
     * Test getDate(year, doy).
     */
    @Test
    public void getDate2() {
        final Integer year = 1959;
        final Integer doy = 1;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        String actual = sdf.format(DateUtils.getDate(year, doy));
        String expected = year + "-01-01 00:00:00";
        assertEquals(expected, actual);
    }

    @Test
    public void getDomGetDoy() throws ParseException {
        final Integer year = 1959;
        final Integer month = 10;
        final Integer dom = 11;
        final Integer doy = 284;
        final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        final Date date = sdf.parse(year + "-" + month + "-" + dom);
        Integer actual;
        actual = DateUtils.getDom(date);
        assertEquals(dom, actual);
        actual = DateUtils.getDoy(date);
        assertEquals(doy, actual);
    }

    /**
     * Test getYear().
     */
    @Test
    public void getYear() {
        final Integer expected = 2017;
        Date date;
        Date date2;
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
            date = sdf.parse(expected + "-12-31 00:00:00");
            date2 = sdf.parse(expected + "-01-01 00:00:00");
        } catch (ParseException ex) {
            throw new RuntimeException("This should never occur!", ex);
        }
        Integer actual = DateUtils.getYear(date);
        assertEquals(expected, actual);
        actual = DateUtils.getYear(date2);
        assertEquals(expected, actual);
    }

    /**
     * Test isLeap().
     */
    @Test
    public void leapYears() {
        boolean found;
        String message = "%d is a leap year.";

        final int[] years = { 2000, 1904, 2004 };
        for (int year : years) {
            found = DateUtils.isLeap(year);
            assertTrue(String.format(message, year), found);
        }
    }

    /**
     * Test isLeap().
     */
    @Test
    public void notLeapYears() {
        boolean found;
        String message = "%d is not a leap year.";

        final int[] years = { 1900, 1903, 2014 };
        for (int year : years) {
            found = DateUtils.isLeap(year);
            assertFalse(String.format(message, year), found);
        }
    }
}
