package fr.inrae.agroclim.indicators.util;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * Test {@link DeepCopyHelper}.
 */
public final class DeepCopyHelperTest {

    @Data
    @AllArgsConstructor
    public static class TestClass {

        private String id;
        private TestClass parent;

        public TestClass(TestClass instance) {
            if (instance.parent != null) {
                this.parent = new TestClass(instance.parent);
            }
            this.id = instance.id;
        }
    }

    @Test
    public void deepCopyList() {
        List<TestClass> list;
        List<TestClass> copied;
        TestClass item1 = new TestClass("Item 1", null);
        TestClass item2 = new TestClass("Item 2", item1);

        list = new ArrayList<>();
        copied = DeepCopyHelper.deepCopy(list, TestClass.class);
        assertEquals(list, copied);

        list.add(item1);
        copied = DeepCopyHelper.deepCopy(list, TestClass.class);
        assertEquals(list, copied);

        list.add(item2);
        copied = DeepCopyHelper.deepCopy(list, TestClass.class);
        assertEquals(list, copied);

        item1.setId("Changed");
        assertNotEquals(list, copied);
    }

    @Test
    public void deepCopyMap() {
        Map<String, TestClass> map;
        Map<String, TestClass> copied;
        TestClass item1 = new TestClass("Item 1", null);
        TestClass item2 = new TestClass("Item 2", item1);

        map = new HashMap<>();
        copied = DeepCopyHelper.deepCopy(map, String.class, TestClass.class);
        assertEquals(map, copied);

        map.put("item 1", item1);
        copied = DeepCopyHelper.deepCopy(map, String.class, TestClass.class);
        assertEquals(map, copied);

        map.put("item 2", item2);
        copied = DeepCopyHelper.deepCopy(map, String.class, TestClass.class);
        assertEquals(map, copied);

        item1.setId("Changed");
        assertNotEquals(map, copied);
    }
}
