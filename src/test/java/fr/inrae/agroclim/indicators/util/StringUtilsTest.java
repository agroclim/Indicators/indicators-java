/*
 * This file is part of Indicators.
 *
 * Indicators is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Indicators is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Indicators. If not, see <https://www.gnu.org/licenses/>.
 */
package fr.inrae.agroclim.indicators.util;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Test;

/**
 * Test StringUtils.
 *
 * Last changed : $Date$
 *
 * @author $Author$
 * @version $Revision$
 */
public final class StringUtilsTest {

    /**
     * Example.
     */
    private enum WeekDays {
        /**
         * Days.
         */
        MON, TUE, WED, THU, FRI, SAT, SUN
    }

    /**
     * Test {@link StringUtils#isBlank(String)}.
     */
    @Test
    public void isBlank() {
        final String[] areBlank = new String[]{"", " ", "   ", null};
        for (final String value : areBlank) {
            assertTrue(StringUtils.isBlank(value));
        }
        final String[] areNotBlank = new String[]{"0", "-", " - ", "- ", " -"};
        for (final String value : areNotBlank) {
            assertFalse(StringUtils.isBlank(value));
        }
    }

    /**
     * Test {@link StringUtils#isNumeric(String)}.
     */
    @Test
    public void isNumeric() {
        final String[] areNumeric = new String[]{"1", "0", "84", "1995"};
        for (final String value : areNumeric) {
            assertTrue(StringUtils.isNumeric(value));
        }
        final String[] areNotNumeric = new String[]{"", " ", "a1", "0b", "84.",
        "voilà"};
        for (final String value : areNotNumeric) {
            assertFalse(StringUtils.isNumeric(value));
        }
    }

    /**
     * Test {@link StringUtils#join(String[], String)}.
     */
    @Test
    public void joinArray() {
        final String[] array = {"a", "1", "b", "2"};
        String actual = StringUtils.join(array, ", ");
        String expected = "a, 1, b, 2";
        assertEquals(expected, actual);

        final String[] oneElem = {"a"};
        expected = "a";
        actual = StringUtils.join(oneElem, ", ");
        assertEquals(expected, actual);
    }

    /**
     * Test {@link StringUtils#join(String[], String)}.
     */
    @Test
    public void joinArrayNull() {
        final String[] strings = null;
        final String expected = "";
        final String actual = StringUtils.join(strings, ",");
        assertEquals(expected, actual);
    }

    /**
     * Test joinEnumNames().
     */
    @Test
    public void joinEnumNames() {
        final String actual = StringUtils.joinEnumNames(WeekDays.class, ", ");
        final String expected = "MON, TUE, WED, THU, FRI, SAT, SUN";
        assertEquals(expected, actual);
    }

    /**
     * Test {@link StringUtils#join(List<String>, String)}.
     */
    @Test
    public void joinList() {
        final List<Object> list = new ArrayList<>();
        list.add("a");
        list.add(1);
        list.add("b");
        list.add(2);
        String actual = StringUtils.join(list, ", ");
        String expected = "a, 1, b, 2";
        assertEquals(expected, actual);

        final List<String> oneElem = Arrays.asList("a");
        expected = "a";
        actual = StringUtils.join(oneElem, ", ");
        assertEquals(expected, actual);
    }

    /**
     * Test {@link StringUtils#join(List<String>, String)}.
     */
    @Test
    public void joinListNull() {
        final List<Object> list = null;
        final String expected = "";
        final String actual = StringUtils.join(list, ",");
        assertEquals(expected, actual);
    }

    /**
     * Test {@link StringUtils#removeTrailingNull(String[])}.
     */
    @Test
    public void removeTrailingNull() {
        final String[] values = new String[]{"null", "a", "b", null, "d", null};
        final String[] expected = new String[]{"null", "a", "b", null, "d"};
        final String[] actual = StringUtils.removeTrailingNull(values);
        assertEquals(Arrays.asList(expected), Arrays.asList(actual));
    }
}
