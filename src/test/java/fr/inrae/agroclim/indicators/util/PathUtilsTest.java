/*
 * Copyright (C) 2021 INRAE AgroClim
 *
 * This file is part of Indicators.
 *
 * Indicators is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Indicators is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Indicators. If not, see <http://www.gnu.org/licenses/>.
 */
package fr.inrae.agroclim.indicators.util;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import com.fasterxml.jackson.databind.MappingIterator;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;

import fr.inrae.agroclim.indicators.model.data.DataTestHelper;

/**
 * Test PathUtils.
 *
 * Last changed : $Date$
 *
 * @author $Author$
 * @version $Revision$
 */
@RunWith(Parameterized.class)
public class PathUtilsTest extends DataTestHelper {

    /**
     * @return combinaisons of baseDirectory, path and expectation
     * @throws java.io.FileNotFoundException
     */
    @Parameterized.Parameters(name = "{index}: {0} {1} {2}")
    public static List<Object[]> data() throws FileNotFoundException, IOException {
        final List<Object[]> data = new ArrayList<>();
        final String filePath;
        if (System.getProperty("os.name").startsWith("Windows")) {
            filePath = "util/paths_windows.csv";
        } else {
            filePath = "util/paths_unix.csv";
        }
        final File file = getFile(filePath);
        CsvMapper mapper = new CsvMapper();
        CsvSchema schema = CsvSchema.emptySchema().withHeader();
        try (MappingIterator<Map<String, String>> it = mapper.readerFor(Map.class).with(schema).readValues(file)) {
            while (it.hasNext()) {
                Map<String, String> map = it.next();
                if (map == null) {
                    continue;
                }
                String[] values = {map.get("basePath"), map.get("absolute"), map.get("relative")};
                data.add(values);
            }
        }
        return data;
    }
    private final String absolutePath;
    private final String basePath;
    private final String relativePath;

    public PathUtilsTest(final String b, final String p, final String e) {
        basePath = b;
        absolutePath = p;
        relativePath = e;
    }

    @Test
    public void resolve() {
        final String actual = PathUtils.resolve(basePath, relativePath);
        assertEquals(absolutePath, actual);
    }

    @Test
    public void relativize() {
        final String actual = PathUtils.relativize(basePath, absolutePath);
        assertEquals(relativePath, actual);
    }
}
