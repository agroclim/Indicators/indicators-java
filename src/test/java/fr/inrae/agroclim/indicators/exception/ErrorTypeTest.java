package fr.inrae.agroclim.indicators.exception;

import static org.junit.Assert.assertTrue;

import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import fr.inrae.agroclim.indicators.exception.type.ComputationErrorType;
import fr.inrae.agroclim.indicators.exception.type.ResourceErrorType;
import fr.inrae.agroclim.indicators.exception.type.XmlErrorType;
import fr.inrae.agroclim.indicators.util.ErrorTypeUtils;

/**
 * Ensure all implementations of {@link ErrorType} are well defined.
 *
 * @author Olivier Maury
 */
@RunWith(Parameterized.class)
public class ErrorTypeTest {
    /**
     * Where translations are.
     */
    private static final String BUNDLE_NAME = "fr.inrae.agroclim.indicators.resources.messages";

    /**
     * @return classes to test.
     */
    @Parameterized.Parameters
    public static List<Class<? extends Enum<?>>> data() {
        return List.of(ComputationErrorType.class, ResourceErrorType.class, XmlErrorType.class);
    }

    /**
     * Class to test.
     */
    private final Class<? extends Enum<?>> clazz;

    /**
     * Constructor.
     *
     * @param c parametrized argument
     */
    public ErrorTypeTest(final Class<? extends Enum<?>> c) {
        this.clazz = c;
    }

    @Test
    public void i18n() {
        final Map<Locale, Map<Class<?>, List<String>>> missing = ErrorTypeUtils.getMissingTranslations(clazz,
                BUNDLE_NAME, List.of(Locale.ENGLISH, Locale.FRENCH));
        assertTrue(missing.toString(), missing.isEmpty());
    }

    @Test
    public void subCodeAreUnique() {
        final Set<String> duplicates = ErrorTypeUtils.getDuplicatedCodes(clazz);
        assertTrue(duplicates.isEmpty());
    }
}
