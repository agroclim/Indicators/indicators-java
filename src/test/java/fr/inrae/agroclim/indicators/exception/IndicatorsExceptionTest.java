package fr.inrae.agroclim.indicators.exception;

import static org.junit.Assert.assertEquals;

import java.util.Locale;

import org.junit.Test;

import fr.inrae.agroclim.indicators.exception.type.ComputationErrorType;

/**
 * Test translations and formatting of exception message.
 *
 * @author Olivier Maury
 */
public class IndicatorsExceptionTest {

    @Test
    public void getLocalizedMessage() {
        Locale.setDefault(Locale.ENGLISH);
        var instance = new IndicatorsException(ComputationErrorType.FORMULA, "m * c * c");
        var actual = instance.getLocalizedMessage();
        var expected = "Error while executing expression \"m * c * c\".";
        assertEquals(expected, actual);
    }
}
