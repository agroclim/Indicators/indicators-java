package fr.inrae.agroclim.indicators.exception;

import static org.junit.Assert.assertFalse;

import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import fr.inrae.agroclim.indicators.resources.I18n;

/**
 * Ensure all translations of {@link IndicatorsErrorCategory} are well defined.
 *
 * @author Olivier Maury
 */
@RunWith(Parameterized.class)
public class IndicatorsErrorCategoryTest {

    /**
     * @return classes to test.
     */
    @Parameterized.Parameters
    public static List<IndicatorsErrorCategory> data() {
        return Arrays.asList(IndicatorsErrorCategory.values());
    }

    /**
     * Category to test.
     */
    private final IndicatorsErrorCategory category;

    /**
     * Constructor.
     *
     * @param cat category to test
     */
    public IndicatorsErrorCategoryTest(final IndicatorsErrorCategory cat) {
        this.category = cat;
    }

    private void missingTranslation(Locale locale) {
        final String bundleName = "fr.inrae.agroclim.indicators.resources.messages";
        final I18n i18n = new I18n(bundleName, locale);
        final String tr = category.getCategory(i18n);
        final boolean actual = tr.startsWith("!") && tr.endsWith("!");
        final String msg = tr + " is not translated in " + locale;
        assertFalse(msg, actual);
    }

    @Test
    public void missingTranslationEnglish() {
        missingTranslation(Locale.ENGLISH);
    }

    @Test
    public void missingTranslationFrench() {
        missingTranslation(Locale.FRENCH);
    }
}
