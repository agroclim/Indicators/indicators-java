/*
 * This file is part of Indicators.
 *
 * Indicators is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Indicators is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Indicators. If not, see <https://www.gnu.org/licenses/>.
 */
package fr.inrae.agroclim.indicators.exception;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Collection;
import java.util.Locale;

import org.junit.Test;

import fr.inrae.agroclim.indicators.resources.I18n;
import fr.inrae.agroclim.indicators.resources.Resources;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * Test for ErrorMessage.
 *
 * Last change $Date$
 *
 * @author $Author$
 * @version $Revision$
 */
public class ErrorMessageTest {

    /**
     * Keys from messages.properties used to warn about errors.
     */
    @RequiredArgsConstructor
    public enum ErrorI18nKey implements ErrorType {
        KEY("error.day.null");

        @Getter
        private final String i18nKey;

        @Override
        public ErrorCategory getCategory() {
            return new ErrorCategory() {
                @Override
                public String getCode() {
                    return "TEST00";
                }

                @Override
                public String getName() {
                    return "CATEGORY";
                }
            };
        }

        @Override
        public String getName() {
            return name();
        }

        @Override
        public String getSubCode() {
            return null;
        }

    }

    /**
     * Bundle name for tested messages.
     */
    private static final String BUNDLE_NAME = "fr.inrae.agroclim.indicators.resources.messages";
    /**
     * The locale for tested messages.
     */
    private static final Locale LOCALE = Locale.FRENCH;
    /**
     * Resources from .properties file.
     */
    private static final I18n RESOURCES = new I18n(BUNDLE_NAME, LOCALE);

    /**
     * Test getMessage.
     */
    @Test
    public void getMessageWithArguments() {
        final ErrorI18nKey key = ErrorI18nKey.KEY;
        final String category = "climate";
        final Integer line = 11;
        final I18n resources = new I18n(BUNDLE_NAME, LOCALE);
        final Collection<Serializable> arguments = Arrays.asList(category, line);
        final ErrorMessage error = new ErrorMessage(BUNDLE_NAME, key, arguments);
        String found = error.getMessage(resources);
        String expected = RESOURCES.format(key.getI18nKey(), category, line);
        assertEquals(expected, found);
        final Resources defaultResources = new Resources(BUNDLE_NAME, Locale.getDefault());
        expected = defaultResources.format(key.getI18nKey(), category, line);
        found = error.getMessage();
        assertEquals(expected, found);

        assertNotNull(error.toJSON());
        assertFalse(error.toJSON().isBlank());
    }

    /**
     * Test getMessage.
     */
    @Test
    public void getMessageWithoutArguments() {
        final String bundleName = "fr.inrae.agroclim.indicators.resources.messages";
        final ErrorI18nKey key = ErrorI18nKey.KEY;
        final I18n resources = new I18n(bundleName, LOCALE);
        final ErrorMessage error = new ErrorMessage(bundleName, key, null);
        String found = error.getMessage(resources);
        String expected = RESOURCES.get(key.getI18nKey());
        assertEquals(expected, found);
        final Resources defaultResources = new Resources(BUNDLE_NAME, Locale.getDefault());
        expected = defaultResources.getString(key.getI18nKey());
        found = error.getMessage();
        assertEquals(expected, found);
    }
}
