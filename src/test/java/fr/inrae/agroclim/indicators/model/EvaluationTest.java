/*
 * This file is part of Indicators.
 *
 * Indicators is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Indicators is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Indicators. If not, see <https://www.gnu.org/licenses/>.
 */
package fr.inrae.agroclim.indicators.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.junit.Before;
import org.junit.Test;

import fr.inrae.agroclim.indicators.exception.IndicatorsException;
import fr.inrae.agroclim.indicators.model.data.DataTestHelper;
import fr.inrae.agroclim.indicators.model.data.ResourceManager;
import fr.inrae.agroclim.indicators.model.data.Variable;
import fr.inrae.agroclim.indicators.model.indicator.CompositeIndicator;
import fr.inrae.agroclim.indicators.model.indicator.Indicator;
import fr.inrae.agroclim.indicators.model.result.EvaluationResult;
import fr.inrae.agroclim.indicators.model.result.IndicatorResult;
import fr.inrae.agroclim.indicators.model.result.PhaseResult;
import lombok.NonNull;
import lombok.extern.log4j.Log4j2;

/**
 * Test for Evaluation.
 *
 * Last change $Date$
 *
 * @author $Author$
 * @version $Revision$
 */
@Log4j2
public final class EvaluationTest extends DataTestHelper {

    /**
     * Evaluation from good XML file.
     */
    private Evaluation evaluation;

    /**
     * Evaluation from good XML file and with a climatic file contains NA data.
     */
    private Evaluation evaluationWithNoData = null;

    /**
     * Set up Evaluation to test.
     */
    @Before
    public void beforeTest() {
        final File xmlFile = getEvaluationTestFile();
        evaluation = getEvaluation(xmlFile);
    }

    @Test
    public void cloneTest() {
        Evaluation e = new Evaluation();
        e.setValue(1.d);
        e.setName("fr", "eval");
        Evaluation e2 = e.clone();
        assertEquals(e.getValue(), e2.getValue());
        assertEquals(e.getName(), e2.getName());
        e2.setValue(2.d);
        e2.setName("fr", "eval2");
        assertNotEquals(e.getValue(), e2.getValue());
        assertNotEquals(e.getNames(), e2.getNames());
        assertNotEquals(e.getName(), e2.getName());

        e2.setSettings(new EvaluationSettings());
        e2.getSettings().setName("eval");
        Evaluation e3 = e2.clone();
        assertNotNull(e3.getSettings());
    }

    /**
     * Check if evaluation is isComputable.
     */
    @Test
    public void computable() {
        assertTrue("Evaluation must not be null!", evaluation != null);
        final boolean found = evaluation.isComputable();
        assertTrue("evaluation must be computable!", found);
    }

    /**
     * Check if evaluation computes.
     */
    @Test
    public void compute() {
        assertTrue("Evaluation must not be null!", evaluation != null);
        String error = null;
        try {
            evaluation.initializeResources();
            evaluation.compute();
        } catch (final IndicatorsException e) {
            error = e.getClass() + " : " + e.getLocalizedMessage();
        }
        assertNull(error, error);
    }

    @Test
    public void computableWithNoData() {
    	final File xmlFile = getEvaluationWithNoDataTestFile();
    	assertNotNull("xml file is null", xmlFile);
    	this.evaluationWithNoData = getEvaluation(xmlFile);
    	assertTrue("Evaluation with no data object must not be null!", this.evaluationWithNoData != null);
    	String error = null;
    	evaluationWithNoData.initializeResources();
        try {
        	evaluationWithNoData.compute();
        } catch (final IndicatorsException e) {
            error = e.getClass() + " : " + e.getLocalizedMessage();
        }
        assertNull(error, error);

    }

    /**
     * Test containsClimaticIndicator().
     */
    @Test
    public void containsClimaticIndicator() {
        assertTrue("Evaluation must not be null!", evaluation != null);
        final boolean found = evaluation.containsClimaticIndicator();
        assertTrue("evaluation do contains climatic indicators!", found);
    }

    /**
     * Ensure category tag is well read.
     */
    @Test
    public void getCategory() {
        final List<String> colors = evaluation.getIndicators().stream().map((ind) -> ind.getCategory()).collect(Collectors.toList());
        assertEquals(evaluation.getIndicators().size(), colors.size());
        assertTrue(colors.toString() + " must contain pheno", colors.contains("pheno"));
    }

    /**
     * Ensure color tag is well read.
     */
    @Test
    public void getColor() {
        final List<String> colors = evaluation.getIndicators().stream().map((ind) -> ind.getColor()).collect(Collectors.toList());
        assertEquals(evaluation.getIndicators().size(), colors.size());
        assertTrue(colors.toString() + " must contain #5F9EA0", colors.contains("#5F9EA0"));
    }

    /**
     * Check that getComputedPhases() return X phase * Y years.
     */
    @Test
    public void getComputedPhases() {
        assertTrue("Evaluation must not be null!", evaluation != null);
        String error = null;
        try {
            evaluation.initializeResources();
            final Map<Integer, EvaluationResult> results = evaluation.compute();
            final List<AnnualPhase> phases = results.values().stream() //
                    .flatMap(r -> r.getPhaseResults().stream()) //
                    .map(r -> r.getAnnualPhase()) //
                    .toList();
            final int nbOfPhases = evaluation.getIndicators().size();
            final int nbOfYears = evaluation.getClimaticResource().getYears().size();
            assertEquals(nbOfPhases * nbOfYears, phases.size());
        } catch (final IndicatorsException e) {
            error = e.getClass() + " : " + e.getLocalizedMessage();
        }
        assertNull(error, error);
    }

    /**
     * Check that getIndicators() return all indicators defined in XML.
     */
    @Test
    public void getIndicators() {
        assertTrue("Evaluation must not be null!", evaluation != null);
        final List<Indicator> phases = evaluation.getIndicators();
        assertEquals("Only 1 phase is defined in XML.", phases.size(), 1);
        assertEquals(phases.get(0).getName(), "s1");
    }

    /**
     * Ensure name tag is well read.
     */
    @Test
    public void getName() {
        final List<String> colors = evaluation.getIndicators().stream().map((ind) -> ind.getName()).collect(Collectors.toList());
        assertEquals(evaluation.getIndicators().size(), colors.size());
        assertTrue(colors.toString() + " must contain s1", colors.contains("s1"));
    }

    /**
     * Ensure provided variables match headers.
     */
    @Test
    public void getProvidedVariables() {
        final Set<Variable> actual = evaluation.getSettings().getClimateLoader().getProvidedVariables();
        final Set<Variable> expected = new HashSet<>();
        expected.addAll(Arrays.asList(Variable.TMIN, Variable.TMAX, Variable.TMEAN, Variable.RADIATION, Variable.RAIN,
                Variable.RH, Variable.WIND, Variable.ETP));
        assertEquals(expected, actual);
    }

    /**
     * Check that results are unique.
     */
    @Test
    public void getResultsUnique() {
        final List<String> duplicates = new ArrayList<>();
        final Set<String> repr = new HashSet<>();
        final String fmt = "%s | %4d | %s-%s | %f | %f";
        try {
            evaluation.initializeResources();
            final Map<Integer, EvaluationResult> results = evaluation.compute();
            assertNotNull("Results must not be null!", results);
            results.entrySet().forEach((entryER) -> {
                final Integer year = entryER.getKey();
                entryER.getValue().getPhaseResults().forEach((phaseResult) -> {
                    final String phaseId = phaseResult.getPhaseId();
                    phaseResult.getIndicatorResults().forEach((process) -> {
                        process.getIndicatorResults().stream().map((ind) -> String.format(fmt,
                                phaseId, year, ind.getIndicatorId(),
                                ind.getIndicatorCategory(),
                                ind.getRawValue(), ind.getNormalizedValue())).forEachOrdered((res) -> {
                                    if (repr.contains(res)) {
                                        duplicates.add(res);
                                    } else {
                                        repr.add(res);
                                    }
                                });
                    });
                });
            });
        } catch (final IndicatorsException e) {
            throw new RuntimeException("This should never occur.", e);
        }
        assertTrue(duplicates.toString(), duplicates.isEmpty());
    }

    private Double getResultValue(@NonNull final String phaseId,
            @NonNull final String indicatorId, final int year,
            @NonNull final Map<Integer, EvaluationResult> results) {
        for (final Map.Entry<Integer, EvaluationResult> entry : results.entrySet()) {
            if (entry.getKey() != year) {
                continue;
            }
            for (final PhaseResult phaseResult : entry.getValue().getPhaseResults()) {
                if (!phaseId.equals(phaseResult.getPhaseId())) {
                    continue;
                }
                if (indicatorId.equals(phaseResult.getPhaseId())) {
                    return phaseResult.getNormalizedValue();
                }
                for (final IndicatorResult processResult : phaseResult.getIndicatorResults()) {
                    if (indicatorId.equals(processResult.getIndicatorId())) {
                        return processResult.getNormalizedValue();
                    }
                    for (final IndicatorResult indResult : processResult.getIndicatorResults()) {
                        if (indicatorId.equals(indResult.getIndicatorId())) {
                            return indResult.getNormalizedValue();
                        }
                    }
                }
            }
        }
        return null;
    }

    /**
     * Ensure tag tag is well read.
     */
    @Test
    public void getTag() {
        final List<String> colors = evaluation.getIndicators().stream().map((ind) -> ((CompositeIndicator) ind).getTag()).collect(Collectors.toList());
        assertEquals(evaluation.getIndicators().size(), colors.size());
        assertTrue(colors.toString() + " must contain pheno-s0-s1", colors.contains("pheno-s0-s1"));
    }

    /**
     * Check that getVariables() return all variables used in XML.
     */
    @Test
    public void getVariables() {
        assertTrue("Evaluation must not be null!", evaluation != null);
        final Set<Variable> variables = evaluation.getVariables();
        final Set<Variable> expected = new HashSet<>();
        expected.add(Variable.ETP);
        expected.add(Variable.RAIN);
        expected.add(Variable.TMAX);
        assertEquals(expected, variables);
    }

    /**
     * Check that resources are well loaded in initializeResources().
     */
    @Test
    public void initializeResources() {
        assertTrue("Evaluation must not be null!", evaluation != null);
        final ResourceManager mgr = evaluation.getResourceManager();
        assertTrue("ClimaticResource must be empty before initialization!", mgr
                .getClimaticResource().isEmpty());
        assertTrue("PhenologicalResource must be empty before initialization!",
                mgr.getPhenologicalResource().isEmpty());
        evaluation.initializeResources();
        assertFalse("ClimaticResource must NOT be empty after initialization!",
                mgr.getClimaticResource().isEmpty());
        assertFalse(
                "PhenologicalResource NOT must be empty after initialization!",
                mgr.getPhenologicalResource().isEmpty());
    }

    /**
     * Check that aggregation function is well written.
     */
    @Test
    public void isAggregationValid() {
        LOGGER.trace("initial:");
        assertTrue("initial evaluation must be valid", evaluation.isAggregationValid());
        String exp;

        LOGGER.trace("2 phases:");
        final CompositeIndicator phase2 = evaluation.getPhases().get(0).clone();
        phase2.setId("s2s3");
        evaluation.add(phase2);

        assertEquals(2, evaluation.getPhases().size());

        assertFalse("must be invalid with 2 phases", evaluation.isAggregationValid());

        exp = evaluation.getPhases().get(0).getId() + " + " + phase2.getId();
        LOGGER.trace("{}:", exp);
        evaluation.getAggregationFunction().setExpression(exp);
        assertTrue(evaluation.getAggregationFunction().isValid());
        assertTrue("must be valid with " + exp,
                evaluation.isAggregationValid());

        exp = "portnawak";
        LOGGER.trace("{}:", exp);
        evaluation.getAggregationFunction().setExpression(exp);
        assertTrue(evaluation.getAggregationFunction().isValid());
        assertFalse("must be invalid with " + exp, evaluation.isAggregationValid());
    }

    /**
     * The test evaluation is good to be launched.
     */
    @Test
    public void isOnErrorOrIncomplete() {
        assertTrue("Evaluation must not be null!", evaluation != null);
        boolean found = evaluation.isOnErrorOrIncomplete(false);
        assertFalse("evaluation must not have errors!", found);
        found = evaluation.isOnErrorOrIncomplete(true);
        assertFalse("evaluation must not have errors!", found);
    }

    /**
     * Test Serialization of Evaluation.
     * @throws IOException while creating temporary file
     */
    @Test
    public void serialize() throws IOException {
        ObjectOutputStream oos = null;
        String error = null;
        final File file = Files.createTempFile("evaluation", "ser").toFile();
        file.deleteOnExit();
        try {
            final FileOutputStream fichier = new FileOutputStream(file);
            oos = new ObjectOutputStream(fichier);
            oos.writeObject(evaluation);
            oos.flush();
        } catch (final java.io.IOException e) {
            LOGGER.error(e);
            LOGGER.catching(e);
            error = e.getLocalizedMessage();
        } finally {
            try {
                if (oos != null) {
                    oos.flush();
                    oos.close();
                }
            } catch (final IOException ex) {
                LOGGER.error(ex);
            }
        }
        assertNull(error);
    }

    /**
     * Test if setParameters() works.
     */
    @Test
    public void setParameters() {
        assertTrue("Evaluation must not be null!", evaluation != null);
        String error = null;
        Map<Integer, EvaluationResult> results;
        try {
            evaluation.initializeResources();
            results = evaluation.compute();
        } catch (final IndicatorsException e) {
            error = e.getClass() + " : " + e.getLocalizedMessage();
            fail(error);
            return;
        }

        final Double initValue = getResultValue("s0s1", "heat", 2015, results);
        assertNotNull("value of climatic effect \"heat\" must not be null", initValue);

        evaluation.setParametersFromKnowledge();
        final Map<String, Double> parameters = new HashMap<>();
        // change threshold from 35 to 25°C.
        parameters.put("Theat", 25.);
        evaluation.setParametersValues(parameters);
        try {
            results = evaluation.compute();
        } catch (IndicatorsException e) {
            fail(error);
            return;
        }

        final Double newValue = getResultValue("s0s1", "heat", 2015, results);
        assertNotEquals("init = " + initValue + ", new = " + newValue, initValue, newValue);
    }

    /**
     * Test isAggregationMissing().
     */
    @Test
    public void isAggregationMissing() {
        assertTrue("Evaluation must not be null!", evaluation != null);
        boolean found = evaluation.isAggregationMissing(false);
        assertFalse("evaluation do not need to be aggregated!", found);
        found = evaluation.isAggregationMissing(true);
        assertFalse("evaluation do not need to be aggregated!", found);
    }

    /**
     * EvaluationSettings.type.
     */
    @Test
    public void type() {
        assertEquals(EvaluationType.WITH_AGGREGATION,
                evaluation.getSettings().getType());
    }
}
