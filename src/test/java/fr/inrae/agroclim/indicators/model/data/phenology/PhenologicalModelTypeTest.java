/*
 * This file is part of Indicators.
 *
 * Indicators is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Indicators is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Indicators. If not, see <https://www.gnu.org/licenses/>.
 */
package fr.inrae.agroclim.indicators.model.data.phenology;

import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.Locale;

import org.junit.Test;

/**
 * Test model of Phenology model type.
 *
 * Last changed : $Date$
 *
 * @author $Author$
 * @version $Revision$
 */
public class PhenologicalModelTypeTest {
    /**
     * All types must have localized names for en and fr.
     */
    @Test
    public void getName() {
        for (final PhenologicalModelType type : PhenologicalModelType.values()) {
            Arrays.asList(Locale.ENGLISH, Locale.FRENCH).forEach(locale -> {
                final String msg = String.format("PhenologicalModelType %s does not "
                        + "have localized name for %s", type.name(), locale);
                assertTrue(msg, !type.getName(locale).contains("!"));
            });
        }
    }
}
