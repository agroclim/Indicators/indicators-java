/*
 * This file is part of Indicators.
 *
 * Indicators is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Indicators is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Indicators. If not, see <https://www.gnu.org/licenses/>.
 */
package fr.inrae.agroclim.indicators.model.data.climate;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import fr.inrae.agroclim.indicators.model.data.DataTestHelper;

/**
 * Test Climate resource class.
 *
 * Last changed : $Date$
 *
 * @author $Author$
 * @version $Revision$
 */
public final class ClimaticResourceTest extends DataTestHelper {

    /**
     * Test ClimaticResource.getClimaticDataByPhaseAndYear().
     */
    @Test
    public void getClimaticDataByPhaseAndYear() {
        ClimaticResource res = new ClimaticResource();
        assertTrue(res.isEmpty());
        res.setData(getClimatic2015Data());
        assertFalse(res.isEmpty());
        assertFalse(res.getYears().isEmpty());
        int year = res.getYears().get(0);
        final int start = 120;
        final int end = 140;
        final ClimaticResource filtered = res.getClimaticDataByPhaseAndYear(start, end, year);
        assertFalse(filtered.isEmpty());
        assertFalse(filtered.getYears().isEmpty());
        final int nbOfDays = filtered.getData().size();
        assertEquals(end - start, nbOfDays);
        assertEquals(start, filtered.getData().get(0).getDayOfYear());
        assertEquals(end - 1, filtered.getData().get(nbOfDays - 1).getDayOfYear());
    }
}
