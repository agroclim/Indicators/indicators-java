/*
 * Copyright (C) 2020 INRAE AgroClim
 *
 * This file is part of Indicators.
 *
 * Indicators is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Indicators is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Indicators. If not, see <http://www.gnu.org/licenses/>.
 */
package fr.inrae.agroclim.indicators.model.data.phenology;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import fr.inrae.agroclim.indicators.model.data.DataTestHelper;
import lombok.Data;
import lombok.RequiredArgsConstructor;

/**
 * Test file loader for Phenology data.
 *
 * Last changed : $Date$
 *
 * @author $Author$
 * @version $Revision$
 */
@RunWith(Parameterized.class)
@RequiredArgsConstructor
public class PhenologyFileLoaderTest extends DataTestHelper {

    /**
     * Relative path of CSV file with headers.
     */
    @Data
    @RequiredArgsConstructor
    public static class TestData {
        private final String relativePath;
        private final String[] headers;

    }

    /**
     * @return relative path of pheno_sample*csv with headers.
     */
    @Parameterized.Parameters(name = "{index}: {0}")
    public static Collection<TestData> data() {
        return Arrays.asList(
                new TestData("model/data/phenology/pheno_sample.csv",
                        new String[]{"year", "s0", "s1", "s2", "s3", "s4"}),
                new TestData("model/data/phenology/pheno_sample_ignored_first_column.csv",
                        new String[]{"", "year", "s0", "s1", "s2", "s3", "s4"}),
                new TestData("model/data/phenology/pheno_sample_plus_s5.csv",
                        new String[]{"year", "s0", "s1", "s2", "s3", "s4"}));
    }

    /**
     * Relative path of CSV file with headers.
     */
    private final TestData testData;

    /**
     * Test loading with good file.
     */
    @Test
    public void load() {
        final String[] headers = testData.getHeaders();
        final String csvSeparator = ";";
        final String csvFile = getFile(testData.getRelativePath()).getAbsolutePath();
        assertNotNull(csvFile);
        final PhenologyFileLoader loader = new PhenologyFileLoader(csvFile, headers, csvSeparator);
        final List<AnnualStageData> data = loader.load();
        assertEquals(36, data.size());
        final AnnualStageData aData = data.get(data.size() - 2);
        assertEquals(Integer.valueOf(2014), aData.getYear());
        assertEquals(Integer.valueOf(180), aData.getStageValue("s3"));
    }
}
