/*
 * Copyright (C) 2020 INRAE AgroClim
 *
 * This file is part of Indicators.
 *
 * Indicators is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Indicators is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Indicators. If not, see <http://www.gnu.org/licenses/>.
 */
package fr.inrae.agroclim.indicators.model.indicator;

import static junit.framework.TestCase.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import fr.inrae.agroclim.indicators.exception.IndicatorsException;
import fr.inrae.agroclim.indicators.model.data.DataTestHelper;
import fr.inrae.agroclim.indicators.model.data.Variable;
import fr.inrae.agroclim.indicators.model.data.climate.ClimaticResource;

/**
 * Test {@link AverageOfDiff} indicator.
 *
 * Last changed : $Date$
 *
 * @author $Author$
 * @version $Revision$
 */
public class AverageOfDiffTest extends DataTestHelper {

    /**
     * climate-2015.csv.
     */
    private final ClimaticResource climaticData;

    /**
     * Indicator to test.
     */
    private final AverageOfDiff indicator;

    /**
     * Constructor.
     */
    public AverageOfDiffTest() {
        climaticData = new ClimaticResource();
        climaticData.setData(getClimatic2015Data());
        indicator = new AverageOfDiff();
        indicator.setVariable1(Variable.TMAX);
        indicator.setVariable2(Variable.TMIN);
    }

    @Test
    public void computeSingleValue() {
        final double expected = climaticData.getData().stream()
                .map(d -> d.getTmax() - d.getTmin())
                .reduce((v1, v2) -> v1 + v2)
                .get() / climaticData.getData().size();
        double result = -1;
        String error = null;
        try {
            result = indicator.computeSingleValue(climaticData);
        } catch (final IndicatorsException ex) {
            error = ex.getClass().getCanonicalName() + " : " + ex.getMessage();
        }
        assertNull(error, error );
        assertEquals(expected, result, 0.0);
    }

    @Test
    public void isComputable() {
        assertTrue(indicator.isComputable());
        assertTrue(indicator.isComputable(climaticData));
    }
}
