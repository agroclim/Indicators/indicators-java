/*
 * Copyright (C) 2020 INRAE AgroClim
 *
 * This file is part of Indicators.
 *
 * Indicators is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Indicators is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Indicators. If not, see <http://www.gnu.org/licenses/>.
 */
package fr.inrae.agroclim.indicators.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.File;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

import fr.inrae.agroclim.indicators.exception.IndicatorsException;
import fr.inrae.agroclim.indicators.model.data.DataTestHelper;
import fr.inrae.agroclim.indicators.model.result.EvaluationResult;

/**
 * Commons methods to test data proxy and loaders.
 *
 * Last changed : $Date$
 *
 * @author $Author$
 * @version $Revision$
 */
public class EvalutationCustomHeadersTest extends DataTestHelper {

    /**
     * Evaluation from good XML file.
     */
    private Evaluation evaluation;

    /**
     * Set up Evaluation to test.
     */
    @Before
    public void beforeTest() {
        final File xmlFile = getFile("xml/test-evaluation-custom-headers.xml");
        evaluation = getEvaluation(xmlFile);
    }

    /**
     * Check if evaluation computes.
     */
    @Test
    public void compute() {
        assertTrue("Evaluation must not be null!", evaluation != null);
        final Map<Integer, EvaluationResult> results;
        try {
            evaluation.initializeResources();
            results = evaluation.compute();
        } catch (final IndicatorsException e) {
            final String error = e.getClass() + " : " + e.getLocalizedMessage();
            fail(error);
            return;
        }
        assertNotNull("Results must not be null!", results);
        assertEquals("One year en climate file", 1, results.keySet().size());
        results.values().stream().flatMap(v -> v.getPhaseResults().stream()).forEach(phaseResult -> {
            final String phaseId = phaseResult.getPhaseId();
            phaseResult.getIndicatorResults().forEach(process -> {
                long nbOfNulls = process.getIndicatorResults().stream()
                        .map(ind -> ind.getRawValue()).filter(v -> v == null).count();
                assertTrue("Null value as raw value of indicator in phase " + phaseId, nbOfNulls == 0);
            });
        });
    }
}
