/*
 * Copyright (C) 2020 INRAE AgroClim
 *
 * This file is part of Indicators.
 *
 * Indicators is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Indicators is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Indicators. If not, see <http://www.gnu.org/licenses/>.
 */
package fr.inrae.agroclim.indicators.model.criteria;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import lombok.RequiredArgsConstructor;
import lombok.ToString;

/**
 * Test relational operator for comparison.
 *
 * Last changed : $Date$
 *
 * @author $Author$
 * @version $Revision$
 */
@RequiredArgsConstructor
@RunWith(Parameterized.class)
public class RelationalOperatorTest {
    /**
     * Test data.
     */
    @RequiredArgsConstructor
    @ToString
    public static class Param {
        private final Double leftValue;
        private final Double rightValue;
        private final RelationalOperator operator;
        private final boolean expected;
    }
    /**
     * @return combinaisons of values, operator and expectations
     */
    @Parameterized.Parameters
    public static List<Param> data() {
        return Arrays.asList(
                // with null
                //
                new Param(0., null, RelationalOperator.EQ, false),
                //
                new Param(null, null, RelationalOperator.EQ, true),
                //
                new Param(0., null, RelationalOperator.GE, true),
                //
                new Param(null, 0., RelationalOperator.GE, false),
                //
                new Param(null, null, RelationalOperator.GE, true),
                //
                new Param(0., null, RelationalOperator.GT, true),
                //
                new Param(null, null, RelationalOperator.GT, false),
                //
                new Param(null, 0., RelationalOperator.GT, false),
                //
                new Param(null, 0., RelationalOperator.LE, true),
                //
                new Param(0., null, RelationalOperator.LE, false),
                //
                new Param(null, null, RelationalOperator.LE, true),
                //
                new Param(null, 0., RelationalOperator.LT, true),
                //
                new Param(null, null, RelationalOperator.LT, true),
                //
                new Param(0., null, RelationalOperator.LT, false),
                // with values
                //
                new Param(0., 1., RelationalOperator.EQ, false),
                //
                new Param(0., 0., RelationalOperator.EQ, true),
                //
                new Param(0., 1., RelationalOperator.GE, false),
                //
                new Param(1., 0., RelationalOperator.GE, true),
                //
                new Param(1., 1., RelationalOperator.GE, true),
                //
                new Param(0., 1., RelationalOperator.GT, false),
                //
                new Param(1., 1., RelationalOperator.GT, false),
                //
                new Param(1., 0., RelationalOperator.GT, true),
                //
                new Param(1., 0., RelationalOperator.LE, false),
                //
                new Param(0., 1., RelationalOperator.LE, true),
                //
                new Param(1., 1., RelationalOperator.LE, true),
                //
                new Param(1., 0., RelationalOperator.LT, false),
                //
                new Param(1., 1., RelationalOperator.LT, false),
                //
                new Param(0., 1., RelationalOperator.LT, true)
                );
    }

    /**
     * Test data.
     */
    private final Param param;

    @Test
    public void eval() {
        final boolean actual = param.operator.eval(param.leftValue, param.rightValue);
        final boolean expected = param.expected;
        final String msg = String.format("%s %s %s returned %b", param.leftValue, param.operator.name(), param.rightValue, actual);
        assertEquals(msg, expected, actual);
    }
}
