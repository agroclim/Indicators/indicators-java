/*
 * This file is part of Indicators.
 *
 * Indicators is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Indicators is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Indicators. If not, see <https://www.gnu.org/licenses/>.
 */
package fr.inrae.agroclim.indicators.model;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import fr.inrae.agroclim.indicators.exception.IndicatorsException;

/**
 * Test for EvaluationSettings.
 *
 * Last change $Date$
 *
 * @author $Author$
 * @version $Revision$
 */
public final class EvaluationSettingsTest {

    @Test
    public void initializeKnowledge() throws IndicatorsException {
        EvaluationSettings settings = new EvaluationSettings();
        settings.initializeKnowledge();
        assertTrue("Knowledge should not be null!",
                settings.getKnowledge() != null);
    }
}
