/*
 * Copyright (C) 2021 INRAE AgroClim
 *
 * This file is part of Indicators.
 *
 * Indicators is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Indicators is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Indicators. If not, see <http://www.gnu.org/licenses/>.
 */
package fr.inrae.agroclim.indicators.model.indicator;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import com.fasterxml.jackson.databind.MappingIterator;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;

import fr.inrae.agroclim.indicators.exception.IndicatorsException;
import fr.inrae.agroclim.indicators.model.data.DataTestHelper;
import fr.inrae.agroclim.indicators.model.data.climate.ClimaticDailyData;
import fr.inrae.agroclim.indicators.model.data.climate.ClimaticResource;
import lombok.Data;

/**
 * Test Formula.
 *
 * Last changed : $Date$
 *
 * @author $Author$
 * @version $Revision$
 */
@RunWith(Parameterized.class)
public class FormulaTest extends DataTestHelper {

    /**
     * thi.csv.
     */
    @Data
    public static class TestData {

        /**
         * T°C.
         */
        private Double th;
        /**
         * RH %.
         */
        private Double rh;
        /**
         * Expected THI.
         */
        private Double thi;
    }
    /**
     * @return thi.csv.
     */
    @Parameterized.Parameters(name = "{index}: {0}")
    public static Collection<TestData> data() {
        final File file = getFile("model/indicators/thi.csv");
        final CsvMapper mapper = new CsvMapper();
        final CsvSchema schema = CsvSchema.emptySchema().withHeader();
        try (MappingIterator<TestData> it = mapper.readerFor(TestData.class)
                .with(schema)
                .readValues(file)) {
            return it.readAll();
        } catch (final IOException ex) {
            throw new RuntimeException(ex);
        }
    }
    /**
     * Climatic resource, container of data.
     */
    private final ClimaticResource climaticResource = new ClimaticResource();
    /**
     * Row of thi.csv.
     */
    private final TestData data;
    /**
     * THI formula.
     */
    private final Formula thiFormula = new Formula();

    /**
     * Formula with parameters.
     */
    private final Formula paramFormula = new Formula();

    /**
     * Constructor.
     *
     * @param row Row of thi.csv
     */
    public FormulaTest(final TestData row) {
        this.data = row;
        this.thiFormula.setAggregation(Formula.Aggregation.AVERAGE);
        this.thiFormula.setExpression("1.8 * TH - (1 - RH/100) * (TH - 14.3) + 32");
        this.paramFormula.setAggregation(Formula.Aggregation.AVERAGE);
        this.paramFormula.setExpression("TH + paramA + paramB");
    }

    @Test
    public void computeSingleValue() throws IndicatorsException {
        assertNotNull(this.data);
        assertNotNull(this.data.getRh());
        assertNotNull(this.data.getTh());
        assertNotNull(this.data.getThi());
        final Double expected = this.data.getThi();
        final Double actual = this.thiFormula.computeSingleValue(fromTestData(this.data));
        assertEquals(expected, actual, 0.01);
    }

    @Test
    public void computeWithParameters() throws IndicatorsException {
        assertNotNull(this.data);
        assertNotNull(this.data.getTh());
        final double paramA = 10.;
        final double paramB = 0.5;
        final Map<String, Double> params = new HashMap<>();
        params.put("paramA", paramA);
        params.put("paramB", paramB);
        this.paramFormula.setParametersValues(params);
        final Double expected = this.data.getTh() + paramA + paramB;
        final Double actual = this.paramFormula.computeSingleValue(fromTestData(this.data));
        assertEquals(expected, actual, 0.01);
    }

    private ClimaticResource fromTestData(final TestData data) {
        final ClimaticDailyData dailyData = new ClimaticDailyData();
        dailyData.setTh(data.getTh());
        dailyData.setRh(data.getRh());
        climaticResource.setData(Arrays.asList(dailyData));
        return climaticResource;
    }
}
