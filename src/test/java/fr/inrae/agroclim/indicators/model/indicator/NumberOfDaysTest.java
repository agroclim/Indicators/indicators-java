/*
 * This file is part of Indicators.
 *
 * Indicators is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Indicators is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Indicators. If not, see <https://www.gnu.org/licenses/>.
 */
package fr.inrae.agroclim.indicators.model.indicator;

import static junit.framework.TestCase.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import fr.inrae.agroclim.indicators.exception.IndicatorsException;
import fr.inrae.agroclim.indicators.model.criteria.ComparisonCriteria;
import fr.inrae.agroclim.indicators.model.criteria.Criteria;
import fr.inrae.agroclim.indicators.model.criteria.RelationalOperator;
import fr.inrae.agroclim.indicators.model.criteria.SimpleCriteria;
import fr.inrae.agroclim.indicators.model.data.DataTestHelper;
import fr.inrae.agroclim.indicators.model.data.Variable;
import fr.inrae.agroclim.indicators.model.data.climate.ClimaticResource;

/**
 * Test "number of days" indicator.
 *
 * Last changed : $Date: 2016-12-20 17:30:03 +0100 (mar., 20 déc. 2016) $
 *
 * @author $Author: omaury $
 * @version $Revision: 71 $
 */
public final class NumberOfDaysTest extends DataTestHelper {

    /**
     * climate-2015.csv.
     */
    private final ClimaticResource climaticData;

    /**
     * tmin < 0°C.
     */
    private final Criteria frost;

    /**
     * Constructor.
     */
    public NumberOfDaysTest() {
        frost = new SimpleCriteria("tmin", RelationalOperator.LT, 0);
        climaticData = new ClimaticResource();
        climaticData.setData(getClimatic2015Data());
    }

    /**
     * Test of compute method, of class NumberOfDays.
     */
    @Test
    public void testCompute() {
        final NumberOfDays instance = new NumberOfDays();
        instance.setCriteria(frost);

        final double expected = 15.0;
        double result = -1;
        String error = null;
        try {
            result = instance.compute(climaticData);
        } catch (final IndicatorsException ex) {
            error = ex.getClass().getCanonicalName() + " : " + ex.getMessage();
        }
        assertTrue(error, error == null);
        assertEquals(expected, result, 0.0);
    }

    /**
     * Number of frosting days in 2015.
     */
    @Test
    public void testComputeSingleValue() {
        final NumberOfDays instance = new NumberOfDays();
        instance.setCriteria(frost);

        final double expected = 15.0;
        double result = -1;
        String error = null;
        try {
            result = instance.computeSingleValue(climaticData);
        } catch (final IndicatorsException ex) {
            error = ex.getClass().getCanonicalName() + " : " + ex.getMessage();
        }
        assertTrue(error, error == null);
        assertEquals(expected, result, 0.0);
    }

    @Test
    public void testRainGreaterThanEtp() {
        final ComparisonCriteria criteria = new ComparisonCriteria();
        criteria.setLeftVariable(Variable.ETP);
        criteria.setRelationalOperator(RelationalOperator.GT);
        criteria.setRightVariable(Variable.RAIN);
        final NumberOfDays instance = new NumberOfDays();
        instance.setCriteria(criteria);
        // computed with LibreOffice =SI(M2>J2;1;0)
        final double expected = 312.;
        double result = -1;
        String error = null;
        try {
            result = instance.computeSingleValue(climaticData);
        } catch (final IndicatorsException ex) {
            error = ex.getClass().getCanonicalName() + " : " + ex.getMessage();
        }
        assertTrue(error, error == null);
        assertEquals(expected, result, 0.0);
    }

    /**
     * Without criteria defined: count all days.
     */
    @Test
    public void withoutCriteria() {
        final NumberOfDays instance = new NumberOfDays();

        final double expected = climaticData.getData().size();
        double result = -1;
        String error = null;
        try {
            result = instance.computeSingleValue(climaticData);
        } catch (final IndicatorsException ex) {
            error = ex.getClass().getCanonicalName() + " : " + ex.getMessage();
        }
        assertTrue(error, error == null);
        assertEquals(expected, result, 0.0);
    }
}
