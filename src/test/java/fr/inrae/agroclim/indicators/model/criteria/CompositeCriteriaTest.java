/*
 * This file is part of Indicators.
 *
 * Indicators is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Indicators is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Indicators. If not, see <https://www.gnu.org/licenses/>.
 */
package fr.inrae.agroclim.indicators.model.criteria;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import fr.inrae.agroclim.indicators.exception.IndicatorsException;
import fr.inrae.agroclim.indicators.model.data.climate.ClimaticDailyData;

/**
 * Test Composite criteria.
 *
 * Last changed : $Date$
 *
 * @author $Author$
 * @version $Revision$
 */
public final class CompositeCriteriaTest {
    /**
     * tmean = 0.
     */
    private final ClimaticDailyData data0;

    /**
     * tmean = 5.
     */
    private final ClimaticDailyData data5;

    /**
     * tmean = 10.
     */
    private final ClimaticDailyData data10;

    /**
     * Elemental criteria for composite.
     */
    private final SimpleCriteria inf10 = new SimpleCriteria("tmean", RelationalOperator.LT, 10);
    /**
     * Elemental criteria for composite.
     */
    private final SimpleCriteria sup0 = new SimpleCriteria("tmean", RelationalOperator.GT, 0);

    /**
     * Composite criteria to test.
     */
    private final CompositeCriteria criteria;

    /**
     * Constructor.
     */
    public CompositeCriteriaTest() {
        criteria = new CompositeCriteria();
        final List<Criteria> all = new ArrayList<>();
        all.add(inf10);
        all.add(sup0);
        criteria.setCriteria(all);
        criteria.setLogicalOperator(LogicalOperator.AND);
        data0 = new ClimaticDailyData();
        data0.setTmean(0.);
        data5 = new ClimaticDailyData();
        final double kvin = 5.;
        data5.setTmean(kvin);
        data10 = new ClimaticDailyData();
        final double dek = 10.;
        data10.setTmean(dek);
    }

    /**
     * Check sup0 AND inf10 criteria.
     *
     * @throws IndicatorsException
     *             exception while getting value.
     */
    @Test
    public void and() throws IndicatorsException {
        criteria.setLogicalOperator(LogicalOperator.AND);
        assertFalse(criteria.getFormula(data0) + " must be false",
                criteria.eval(data0));
        assertTrue(criteria.getFormula(data5) + "  must be true",
                criteria.eval(data5));
        assertFalse(criteria.getFormula(data10) + "  must be false",
                criteria.eval(data10));
    }

    /**
     * Check inf10 criteria.
     *
     * @throws IndicatorsException
     *             exception while getting value.
     */
    @Test
    public void inf10() throws IndicatorsException {
        assertTrue(inf10.getFormula(data0) + " must be true",
                inf10.eval(data0));
        assertTrue(inf10.getFormula(data5) + " must be true",
                inf10.eval(data5));
        assertFalse(inf10.getFormula(data10) + " must be false",
                inf10.eval(data10));
    }

    /**
     * Check sup0 OR inf10 criteria.
     *
     * @throws IndicatorsException
     *             exception while getting value.
     */
    @Test
    public void or() throws IndicatorsException {
        criteria.setLogicalOperator(LogicalOperator.OR);
        assertTrue(criteria.getFormula(data0) + " must be true",
                criteria.eval(data0));
        assertTrue(criteria.getFormula(data5) + "  must be true",
                criteria.eval(data5));
        assertTrue(criteria.getFormula(data10) + "  must be true",
                criteria.eval(data10));
    }

    /**
     * Check sup0 criteria.
     *
     * @throws IndicatorsException
     *             exception while getting value.
     */
    @Test
    public void sup0() throws IndicatorsException {
        assertFalse(
                sup0.getFormula(data0) + " must be false. " + sup0.toString(),
                sup0.eval(data0));
        assertTrue(sup0.getFormula(data5) + " must be true", sup0.eval(data5));
        assertTrue(sup0.getFormula(data10) + " must be true",
                sup0.eval(data10));
    }

    /**
     * Check sup0 XOR inf10 criteria.
     *
     * @throws IndicatorsException
     *             exception while getting value.
     */
    @Test
    public void xor() throws IndicatorsException {
        criteria.setLogicalOperator(LogicalOperator.XOR);
        assertTrue(criteria.getFormula(data0) + " must be true",
                criteria.eval(data0));
        assertFalse(criteria.getFormula(data5) + "  must be false",
                criteria.eval(data5));
        assertTrue(criteria.getFormula(data10) + "  must be true",
                criteria.eval(data10));
    }
}
