/*
 * This file is part of Indicators.
 *
 * Indicators is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Indicators is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Indicators. If not, see <https://www.gnu.org/licenses/>.
 */
package fr.inrae.agroclim.indicators.model.data;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import fr.inrae.agroclim.indicators.model.data.climate.ClimaticDailyData;

/**
 * Test DailyData.
 */
public final class DailyDataTest {

    /**
     * Test of getValue method, of class DailyData.
     */
    @Test
    public void getValue() {
        final Double etp = 1.5648d;
        final Double tmin = 0.6d;
        ClimaticDailyData data = new ClimaticDailyData();
        data.setMonth(1);
        data.setEtp(etp);
        data.setTmin(tmin);
        Object result;
        result = data.getValue(Variable.ETP);
        assertEquals(etp, result);

        result = data.getValue(Variable.TMIN);
        assertEquals(tmin, result);
    }
}
