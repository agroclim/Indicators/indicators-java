/*
 * Copyright (C) 2020 INRAE AgroClim
 *
 * This file is part of Indicators.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.inrae.agroclim.indicators.model.indicator.listener;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import fr.inrae.agroclim.indicators.exception.IndicatorsException;
import fr.inrae.agroclim.indicators.model.Knowledge;
import fr.inrae.agroclim.indicators.model.indicator.CompositeIndicator;
import fr.inrae.agroclim.indicators.model.indicator.Indicator;

/**
 * Test of CompositeIndicator.
 *
 * @author Olivier Maury
 */
public class CompositeIndicatorTest {

    /**
     * Flag to check if aggregation listener is fired when adding an indicator.
     */
    private boolean listenerFired = false;

    @Test
    public void addThenRemove() throws IndicatorsException {
        CompositeIndicator c = new CompositeIndicator();
        c.addFunctionListener((final CompositeIndicator i) -> {
            listenerFired = true;
        });
        Knowledge k = Knowledge.load();

        String indicatorId1 = "numbheatwav";
        assertFalse(indicatorId1 + "should not be present in empty CompositeIndicator!", c.isPresent(indicatorId1));
        Indicator ind1 = k.getIndicator(indicatorId1);
        c.add(ind1);
        assertFalse(listenerFired);
        assertTrue(indicatorId1 + "should present after adding!", c.isPresent(indicatorId1));

        String indicatorId2 = "cold";
        assertFalse(indicatorId2 + "should not be present in empty CompositeIndicator!", c.isPresent(indicatorId2));
        Indicator ind2 = k.getIndicator(indicatorId2);
        c.add(ind2);
        assertTrue(listenerFired);
        assertTrue(indicatorId2 + "should present after adding!", c.isPresent(indicatorId2));

        listenerFired = false;
        c.remove(ind2);
        assertTrue(listenerFired);
        assertFalse(indicatorId2 + "should not be present after deletion!", c.isPresent(indicatorId2));

        listenerFired = false;
        c.remove(ind1);
        assertFalse(listenerFired);
        assertFalse(indicatorId2 + "should not be present after deletion!", c.isPresent(indicatorId2));
    }

    @Test
    public void equals() {
        final CompositeIndicator pheno = new CompositeIndicator();
        pheno.setName("en", "s0");
        pheno.setId("pheno_s0");
        pheno.setCategory("pheno");

        final CompositeIndicator mort = new CompositeIndicator();
        pheno.setName("en", "Crop mortality");
        pheno.setName("fr", "Mortalité culturale");
        pheno.setId("mort");
        pheno.setCategory("ecoprocesses");
        pheno.setColor("#5F9EA0");

        assertFalse(pheno.equals(mort));
    }
}
