/*
 * Copyright (C) 2020 INRAE AgroClim
 *
 * This file is part of Indicators.
 *
 * Indicators is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Indicators is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Indicators. If not, see <http://www.gnu.org/licenses/>.
 */
package fr.inrae.agroclim.indicators.model.data;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import fr.inrae.agroclim.indicators.util.PathUtilsTest;

/**
 * Test FileLoader.
 *
 * Last changed : $Date$
 *
 * @author $Author$
 * @version $Revision$
 */
@RunWith(Parameterized.class)
public class FileLoaderTest {

    /**
     * @return combinaisons of baseDirectory, path and expectation
     * @throws java.io.IOException while reading CSV
     */
    @Parameterized.Parameters(name = "{index}: {0} {1} {2}")
    public static List<Object[]> data() throws IOException {
        return PathUtilsTest.data();
    }
    /**
     * Loader to test.
     */
    private final FileLoader loader;
    /**
     * path.
     */
    private final Path path;
    /**
     * Expected path.
     */
    private final String expected;

    public FileLoaderTest(final String b, final String p, final String e) {
        expected = e;
        loader = new FileLoader();
        loader.setBaseDirectory(Paths.get(b));
        path = Paths.get(p);
    }

    @Test
    public void absolutize() {
        final String actual = loader.absolutize(expected);
        assertEquals(path.toString(), actual);
    }

    @Test
    public void relativize() {
        final String actual = loader.relativize(path);
        assertEquals(expected, actual);
    }

    /**
     * Changing directory.
     */
    @Test
    public void setBaseDirectory() {
        loader.setPath(expected);
        final String tmpdir = System.getProperty("java.io.tmpdir");
        final String absPath = loader.getFile().getAbsolutePath();
        loader.setBaseDirectory(Paths.get(tmpdir));
        String actual = loader.getFile().getAbsolutePath();
        assertEquals(absPath, actual);
        // only assert that relative paths have been changed
        if (!expected.startsWith("/") && !expected.startsWith("D:")) {
            actual = loader.getPath();
            assertNotEquals(expected, actual);
        }
    }
}
