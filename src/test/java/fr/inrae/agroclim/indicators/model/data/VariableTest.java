/*
 * Copyright (C) 2021 INRAE AgroClim
 *
 * This file is part of Indicators.
 *
 * Indicators is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Indicators is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Indicators. If not, see <http://www.gnu.org/licenses/>.
 */
package fr.inrae.agroclim.indicators.model.data;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Set;

import org.junit.Test;

import fr.inrae.agroclim.indicators.model.TimeScale;

/**
 * Test the Variable methods.
 *
 * Last changed : $Date$
 *
 * @author $Author$
 * @version $Revision$
 */
public class VariableTest {

    @Test
    public void getByTimeScaleAndTypeDailyClimatic() {
        assertEquals(1, Variable.TMIN.getTimescales().length);
        assertEquals(TimeScale.DAILY, Variable.TMIN.getTimescales()[0]);
        Set<Variable> actual = Variable.getByTimeScaleAndType(TimeScale.DAILY, Variable.Type.CLIMATIC);
        assertTrue(actual.contains(Variable.TMIN));
    }

    @Test
    public void getTimeScale() {
        for (final Variable var : Variable.values()) {
            assertTrue(var.getTimescales().length > 0);
        }
    }

}
