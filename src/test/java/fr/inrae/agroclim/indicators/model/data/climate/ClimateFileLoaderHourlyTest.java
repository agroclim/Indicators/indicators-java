package fr.inrae.agroclim.indicators.model.data.climate;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import org.junit.Test;

import fr.inrae.agroclim.indicators.model.TimeScale;
import fr.inrae.agroclim.indicators.model.data.DataTestHelper;
import fr.inrae.agroclim.indicators.util.DateUtils;

/**
 * Test file loader for Climate hourly data.
 *
 * Last changed : $Date$
 *
 * @author $Author$
 * @version $Revision$
 */
public class ClimateFileLoaderHourlyTest extends DataTestHelper {
    /**
     * Test with a good file.
     */
    @Test
    public void load() {
        final String filename = "climate-hourly-2015.csv";
        final String path = getFile("model/data/climate/" + filename).toString();
        final String[] headers = {"year", "month", "day", "hour", "th", "rain", "wind", "rh"};
        final ClimateFileLoader loader = new ClimateFileLoader(path, headers, ";");
        loader.setTimeScale(TimeScale.HOURLY);
        final Map<String, String> errors = loader.getConfigurationErrors();
        assertNull(errors);

        final List<ClimaticDailyData> data = loader.load();
        assertNotNull("climate data must not be null!", data);
        final int nbOfData = 8760;
        assertEquals(filename + " has only 8760 lines.", nbOfData,
                data.size());
        final Double expected = 3.4;
        ClimaticDailyData ddata = data.get(0);
        final int year = 2015;
        final int midnight = 0;
        final Date firstDate = DateUtils.getDate(year, 1);
        assertEquals(firstDate, ddata.getDate());
        assertNotNull("Hourly must be not null in file « " + filename + " »", ddata.getHour());
        assertEquals(ddata.toString(), midnight, ddata.getHour().intValue());
        assertEquals(ddata.toString(), expected, ddata.getTh());
        ddata = data.get(nbOfData - 1);
        final Double expected2 = 97.;
        assertEquals(ddata.toString(), expected2, ddata.getRh());
        final int expectedHour2 = 23;
        assertEquals(ddata.toString(), expectedHour2, ddata.getHour().intValue());
    }

    /**
     * Test with a good file with 1-24 hours.
     */
    @Test
    public void load1to24hours() {
        final String filename = "climate-hourly-2015-1_24.csv";
        final String path = getFile("model/data/climate/" + filename).toString();
        final String[] headers = {"year", "month", "day", "hour", "th", "rain", "wind", "rh"};
        final ClimateFileLoader loader = new ClimateFileLoader(path, headers, ";");
        loader.setMidnight(24);
        loader.setTimeScale(TimeScale.HOURLY);
        final Map<String, String> errors = loader.getConfigurationErrors();
        assertNull(errors);

        final List<ClimaticDailyData> data = loader.load();
        assertNotNull("climate data must not be null!", data);
        final int nbOfData = 8760;
        assertEquals(filename + " has only 8760 lines.", nbOfData, data.size());
        final Double expected = 3.4;
        ClimaticDailyData ddata = data.get(0);
        final int year = 2015;
        final int midnight = 0;
        assertNotNull("Hourly must be not null in file « " + filename + " »", ddata.getHour());
        assertEquals(ddata.toString(), midnight, ddata.getHour().intValue());
        final Date firstDate = DateUtils.getDate(year, 1);
        final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        assertEquals(sdf.format(firstDate), sdf.format(ddata.getDate()));
        assertEquals(firstDate, ddata.getDate());
        assertEquals(ddata.toString(), expected, ddata.getTh());
        ddata = data.get(nbOfData - 1);
        final Double expected2 = 97.;
        assertEquals(ddata.toString(), expected2, ddata.getRh());
        assertEquals(ddata.toString(), DateUtils.NB_OF_HOURS_IN_DAY - 1, ddata.getHour().intValue());
    }
}
