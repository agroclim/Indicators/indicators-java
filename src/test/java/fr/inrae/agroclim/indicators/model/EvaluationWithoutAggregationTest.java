/*
 * Copyright (C) 2020 INRAE AgroClim
 *
 * This file is part of Indicators.
 *
 * Indicators is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Indicators is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Indicators. If not, see <http://www.gnu.org/licenses/>.
 */
package fr.inrae.agroclim.indicators.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import fr.inrae.agroclim.indicators.exception.IndicatorsException;
import fr.inrae.agroclim.indicators.model.data.DataTestHelper;
import fr.inrae.agroclim.indicators.model.indicator.CompositeIndicator;
import fr.inrae.agroclim.indicators.model.indicator.Indicator;
import fr.inrae.agroclim.indicators.xml.XMLUtil;

/**
 * Test for Evaluation.
 *
 * Last change $Date$
 *
 * @author $Author$
 * @version $Revision$
 */
public class EvaluationWithoutAggregationTest extends DataTestHelper {

    /**
     * Evaluation from good XML file.
     */
    private Evaluation evaluation;

    /**
     * Set up Evaluation to test.
     */
    @Before
    public void beforeTest() {
        final File xmlFile = getFile(
                "xml/test-evaluation-without-aggregation.xml");
        evaluation = getEvaluation(xmlFile);
    }

    /**
     * Check if evaluation is isComputable.
     */
    @Test
    public void computable() {
        assertNotNull("Evaluation must not be null!", evaluation);
        boolean found = evaluation.isComputable();
        assertTrue("evaluation must be computable!", found);
    }

    /**
     * Check if evaluation computes.
     * @throws IndicatorsException while computing
     */
    @Test(expected = Test.None.class)
    public void compute() throws IndicatorsException {
    	assertNotNull("Evaluation must not be null!", evaluation);
        evaluation.initializeResources();
        evaluation.compute();
    }

    /**
     * The test evaluation is good to be launched.
     */
    @Test
    public void isOnErrorOrIncomplete() {
    	assertNotNull("Evaluation must not be null!", evaluation);
        boolean found = evaluation.isOnErrorOrIncomplete(false);
        assertFalse("evaluation must not have errors!", found);
        found = evaluation.isOnErrorOrIncomplete(true);
        assertFalse("evaluation must not have errors!", found);
    }
    /**
     * Test save() a file.
     * @throws IndicatorsException while serializing or loading
     * @throws java.io.IOException while creating tmp file or writing
     */
    @Test(expected = Test.None.class)
    public void save() throws IndicatorsException, IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        XMLUtil.serialize(evaluation.getSettings(), baos,
                EvaluationSettings.CLASSES_FOR_JAXB);
        Path path = Files.createTempFile("evaluation-without-aggregation", ".gri");
        try (OutputStream outputStream = new FileOutputStream(path.toFile())) {
            baos.writeTo(outputStream);
        }
        EvaluationSettings saved;
        saved = (EvaluationSettings) XMLUtil.load(path.toFile(),
                EvaluationSettings.CLASSES_FOR_JAXB);
        assertEquals(evaluation.getType(), saved.getType());
        path.toFile().delete();

        // twice, changing type
        evaluation.setType(EvaluationType.WITH_AGGREGATION);
        baos = new ByteArrayOutputStream();
        XMLUtil.serialize(evaluation.getSettings(), baos,
                EvaluationSettings.CLASSES_FOR_JAXB);
        try (OutputStream outputStream = new FileOutputStream(path.toFile())) {
            baos.writeTo(outputStream);
        }
        saved = (EvaluationSettings) XMLUtil.load(path.toFile(),
                EvaluationSettings.CLASSES_FOR_JAXB);
        assertEquals(EvaluationType.WITH_AGGREGATION, evaluation.getType());
        assertEquals(evaluation.getType(), saved.getType());
        path.toFile().delete();
    }

    @Test
    public void setType() {
        evaluation.setType(EvaluationType.WITH_AGGREGATION);
        assertEquals(EvaluationType.WITH_AGGREGATION, evaluation.getType());
        assertEquals(EvaluationType.WITH_AGGREGATION, evaluation.getSettings().getType());
    }

    /**
     * Test isAggregationMissing().
     */
    @Test
    public void isAggregationMissing() {
    	assertNotNull("Evaluation must not be null!", evaluation);
        boolean found = evaluation.isAggregationMissing(false);
        assertFalse("evaluation do not need to be aggregated!", found);
        found = evaluation.isAggregationMissing(true);
        assertFalse("evaluation do not need to be aggregated!", found);
    }

    /**
     * EvaluationSettings.type.
     */
    @Test
    public void type() {
        assertEquals(EvaluationType.WITHOUT_AGGREGATION, evaluation.getType());
        assertEquals(EvaluationType.WITHOUT_AGGREGATION, evaluation.getSettings().getType());
        List<Indicator> indicators = evaluation.getIndicators();
        while (!indicators.isEmpty()) {
            Indicator ind = indicators.get(0);
            indicators.remove(0);
            assertEquals("Evaluation type of " + ind.getId(), EvaluationType.WITHOUT_AGGREGATION, ind.getType());
            if (ind instanceof CompositeIndicator compositeIndicator) {
                indicators.addAll(compositeIndicator.getIndicators());
            }
        }
    }
}
