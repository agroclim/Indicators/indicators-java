/*
 * Copyright (C) 2020 INRAE AgroClim
 *
 * This file is part of Indicators.
 *
 * Indicators is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Indicators is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Indicators. If not, see <http://www.gnu.org/licenses/>.
 */
package fr.inrae.agroclim.indicators.model.indicator;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.File;
import java.io.IOException;
import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import com.fasterxml.jackson.databind.MappingIterator;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;

import fr.inrae.agroclim.indicators.model.data.DataTestHelper;
import fr.inrae.agroclim.indicators.model.data.climate.ClimaticDailyData;
import lombok.Data;

/**
 *
 * @author omaury
 */
@RunWith(Parameterized.class)
public class TammFormulaTest extends DataTestHelper {

    /**
     * monilia.csv.
     */
    @Data
    public static class TestData {
        /**
         * Date.
         */
        private String date;
        /**
         * Rain.
         */
        private Double rain;
        /**
         * T°C mean.
         */
        private Double tmean;
        /**
         * % stage D.
         */
        private Double pcStageD;
        /**
         * % stage E.
         */
        private Double pcStageE;
        /**
         * % stage G.
         */
        private Double pcStageG;
        /**
         * Risk on flower.
         */
        private Double flowerRisk;
        /**
         * 50% risk.
         */
        private Double pc50Risk;
        /**
         * Weighted risk.
         */
        private Double weightedRisk;

        public TestData() {
        }
    }

    /**
     * Row of monilia.csv.
     */
    private final TestData data;

    /**
     * Constructor.
     * @param row Row of monilia.csv
     */
    public TammFormulaTest(final TestData row) {
        this.data = row;
    }

    /**
     * @return monilia.csv.
     */
    @Parameterized.Parameters(name = "{index}: {0}")
    public static Collection<TestData> data() {
        final File file = getFile("model/indicators/monilia.csv");
        final CsvMapper mapper = new CsvMapper();
        final CsvSchema schema = CsvSchema.emptySchema().withHeader();
        try (MappingIterator<TestData> it = mapper.readerFor(TestData.class)
                .with(schema)
                .readValues(file)) {
            return it.readAll();
        } catch (final IOException ex) {
            throw new RuntimeException(ex);
        }
    }

    @Test
    public void computeDailyValue() {
        assertNotNull(data);
        final Tamm indicator = new Tamm();
        final double actual = indicator.computeDailyValue(fromTestData(data));
        double expected = data.getFlowerRisk();
        if (expected < 0.5) {
            expected = 0;
        }
        assertEquals(expected, actual, 0.01);
    }

    private static ClimaticDailyData fromTestData(final TestData data) {
        final ClimaticDailyData dailyData = new ClimaticDailyData();
        dailyData.setTmean(data.getTmean());
        dailyData.setRain(data.getRain());
        return dailyData;
    }
}
