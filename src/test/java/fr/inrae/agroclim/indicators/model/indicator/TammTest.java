/*
 * Copyright (C) 2020 INRAE AgroClim
 *
 * This file is part of Indicators.
 *
 * Indicators is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Indicators is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Indicators. If not, see <http://www.gnu.org/licenses/>.
 */
package fr.inrae.agroclim.indicators.model.indicator;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import fr.inrae.agroclim.indicators.model.data.climate.ClimaticResource;

/**
 * Test.
 *
 * Last changed : $Date$
 *
 * @author $Author$
 * @version $Revision$
 */
public class TammTest {

	/**
	 * Ensure Worksheet EXP() returns same as Math.exp().
	 */
	@Test
	public void exp() {
		assertEquals(2.71828182845904, Math.exp(1.), 0.00000000000001);
		assertEquals(22026.4657948067, Math.exp(10.), 0.0000000001);
	}

	@Test
	public void isComputable() {
		final Tamm indicator = new Tamm();
		final ClimaticResource res = new ClimaticResource();
		assertTrue(indicator.isComputable(res));
	}

}
