/*
 * Copyright (C) 2022 INRAE AgroClim
 *
 * This file is part of Indicators.
 *
 * Indicators is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Indicators is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Indicators. If not, see <http://www.gnu.org/licenses/>.
 */
package fr.inrae.agroclim.indicators.model.indicator;

import static junit.framework.TestCase.assertEquals;

import org.junit.Test;

import fr.inrae.agroclim.indicators.exception.IndicatorsException;
import fr.inrae.agroclim.indicators.model.criteria.RelationalOperator;
import fr.inrae.agroclim.indicators.model.criteria.SimpleCriteria;
import fr.inrae.agroclim.indicators.model.data.DataTestHelper;
import fr.inrae.agroclim.indicators.model.data.Variable;
import fr.inrae.agroclim.indicators.model.data.climate.ClimaticResource;

/**
 * Tests for #9718.
 */
public class ColdsumtminTest extends DataTestHelper {
    /**
     * climate-2015.csv.
     */
    private final ClimaticResource climaticData;

    /**
     * Precision.
     */
    private static final double EPSILON = 0.001d;

    /**
     * Constructor.
     */
    public ColdsumtminTest() {
        climaticData = new ClimaticResource();
        climaticData.setData(getClimatic2015Data());
    }

    @Test
    public void test() throws IndicatorsException {
        final Sum indicator = new Sum();
        final SimpleCriteria criteria = new SimpleCriteria();
        criteria.setVariable(Variable.TMIN);
        criteria.setOperator(RelationalOperator.LT);
        criteria.setThreshold(50.);
        indicator.setCriteria(criteria);

        ClimaticResource data = climaticData.getClimaticDataByPhaseAndYear(90, 91, 2015);

        double expected = data.getData().get(0).getTmin();
        double actual = indicator.compute(data);
        assertEquals(expected, actual, EPSILON);
    }
}
