/*
 * Copyright (C) 2021 INRAE AgroClim
 *
 * This file is part of Indicators.
 *
 * Indicators is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Indicators is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Indicators. If not, see <http://www.gnu.org/licenses/>.
 */
package fr.inrae.agroclim.indicators.model;

import static org.junit.Assert.assertEquals;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.junit.Test;

import fr.inrae.agroclim.indicators.exception.IndicatorsException;
import fr.inrae.agroclim.indicators.model.data.Variable;

/**
 * Test JEXLFormula.
 *
 * Last changed : $Date$
 *
 * @author $Author$
 * @version $Revision$
 */
public class JEXLFormulaTest {

    @Test
    public void getVariables() {
        final JEXLFormula formula = new JEXLFormula();
        formula.setExpression("1.8 * TH - (1 - RH/100) * (TH - 14.3) + 32");
        final Set<String> expected = new HashSet<>();
        expected.add("TH");
        expected.add("RH");
        final Set<String> actual = formula.getExpressionVariables();
        assertEquals(expected, actual);

        final Set<Variable> expectedVariables = new HashSet<>();
        expectedVariables.add(Variable.TH);
        expectedVariables.add(Variable.RH);
        final Set<Variable> actualVariables = formula.getDataVariables();
        assertEquals(expectedVariables, actualVariables);
    }

    @Test
    public void useFormulaCriteria() throws IndicatorsException {
        final JEXLFormula formula = new JEXLFormula();
        formula.setExpression("formulaCriteria:between(2, 1, 3)");
        var actual = formula.evaluate(new HashMap<>(), Boolean.class);
        assertEquals(Boolean.TRUE, actual);
    }

    @Test
    public void useMathMethod() throws IndicatorsException {
        final JEXLFormula formula = new JEXLFormula();
        formula.setExpression("math:min(2, 1, 3)");
        var actual = formula.evaluate(new HashMap<>(), Double.class);
        Double expected = 1.0d;
        assertEquals(expected, actual);

        // Lara's formula
        formula.setExpression("100*(1 - math:exp(-1 * math:exp(34.036 * (math:log(TMAX)- math:log(49.8323)))))");
        actual = formula.evaluate(Map.of("TMAX", 50.0d), Double.class);
        final Double expected2 = 67.40928984767142;
        assertEquals(expected2, actual);
    }
}
