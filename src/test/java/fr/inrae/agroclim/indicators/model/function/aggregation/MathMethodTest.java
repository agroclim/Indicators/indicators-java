package fr.inrae.agroclim.indicators.model.function.aggregation;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertNull;

import java.util.Locale;

import org.junit.Test;

import fr.inrae.agroclim.indicators.model.JEXLFormula;
import fr.inrae.agroclim.indicators.resources.Messages;

/**
 * Test of Aggregation methods for {@link JEXLFormula}.
 *
 * Last changed : $Date: 2021-05-27 14:07:41 +0200 (jeu. 27 mai 2021) $
 *
 * @author $Author: omaury $
 * @version $Revision: 535 $
 */
public class MathMethodTest {
    /**
     * Delta for equality tests.
     */
    public static final double DELTA = 0.01;
    /**
     * Some values to play with.
     */
    private static final Double[] VALUES = {-1., 0., 1., 2.1};

    @Test
    public void avg() {
        final Double actual = MathMethod.avg(VALUES);
        assertEquals(2.1 / 4., actual, DELTA);
    }

    @Test
    public void avgNaN() {
        final Double actual = MathMethod.avg(Float.NaN, Float.NaN);
        assertEquals(Float.NaN, actual, DELTA);
    }

    @Test
    public void avgNull() {
        assertNull(MathMethod.avg(null, null));
    }

    /**
     * Check that all methods have translated help.
     */
    @Test
    public void i18n() {
        for (final Locale locale : new Locale[] {Locale.ENGLISH, Locale.FRENCH}) {
            Locale.setDefault(locale);
            for (final String name : MathMethod.getMethodNamesWithParameters().keySet()) {
                final String key = "MathMethod." + name + ".description";
                final String actual = Messages.get(key);
                final String missing = "!" + key + "!";
                final String msg = "Missing translation in " + locale.getDisplayLanguage() + " for " + key;
                assertNotEquals(msg, missing, actual);
            }
        }
    }

    @Test
    public void max() {
        final Double actual = MathMethod.max(VALUES);
        assertEquals(2.1, actual, DELTA);
    }

    @Test
    public void maxNaN() {
        assertNull(MathMethod.max(Float.NaN, Float.NaN));
        assertEquals(1.0, MathMethod.max(1.0, Float.NaN), DELTA);
    }

    @Test
    public void maxPositiveZero() {
        final Double negZero = -0.;
        final Double posZero = +0.;
        final Double actual = MathMethod.max(negZero, posZero);
        assertEquals(posZero, actual, DELTA);
        assertNotSame(negZero, actual);
    }

    @Test
    public void min() {
        final Double actual = MathMethod.min(VALUES);
        assertEquals(-1., actual, DELTA);
    }

    @Test
    public void minNaN() {
        assertNull(MathMethod.min(Float.NaN, Float.NaN));
        assertEquals(1.0, MathMethod.min(1.0, Float.NaN), DELTA);
    }

    @Test
    public void minNegativeZero() {
        final Double negZero = -0.;
        final Double posZero = +0.;
        final Double actual = MathMethod.min(negZero, posZero);
        assertEquals(negZero, actual, DELTA);
        assertNotSame(posZero, actual);
    }
}
