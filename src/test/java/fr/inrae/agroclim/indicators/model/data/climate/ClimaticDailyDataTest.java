/*
 * Copyright (C) 2021 INRAE AgroClim
 *
 * This file is part of Indicators.
 *
 * Indicators is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Indicators is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Indicators. If not, see <http://www.gnu.org/licenses/>.
 */
package fr.inrae.agroclim.indicators.model.data.climate;

import static org.junit.Assert.assertEquals;

import java.time.LocalDateTime;
import java.time.Month;
import java.time.ZoneId;
import java.util.Date;

import org.junit.Test;

/**
 * Test file loader for Climate hourly data.
 *
 * Last changed : $Date$
 *
 * @author $Author$
 * @version $Revision$
 */
public class ClimaticDailyDataTest {

    @Test
    public void getDate() {
        final int year = 2015;
        final int month = 1;
        final int day = 1;
        final int hour = 0;
        ClimaticDailyData data = new ClimaticDailyData();
        data.setYear(year);
        data.setMonth(month);
        data.setDay(day);
        data.setHour(hour);
        Date actual = data.getDate();
        final Date expected = Date.from(LocalDateTime.of(year, Month.of(month), day, hour, 0)
                .atZone(ZoneId.of("UTC")).toInstant());
        assertEquals(expected, actual);
    }
}
