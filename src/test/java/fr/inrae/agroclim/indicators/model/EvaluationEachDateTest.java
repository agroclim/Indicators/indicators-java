package fr.inrae.agroclim.indicators.model;

import fr.inrae.agroclim.indicators.exception.IndicatorsException;
import fr.inrae.agroclim.indicators.model.data.DataTestHelper;
import fr.inrae.agroclim.indicators.model.indicator.CompositeIndicator;
import fr.inrae.agroclim.indicators.model.indicator.Indicator;
import fr.inrae.agroclim.indicators.model.result.EvaluationResult;
import fr.inrae.agroclim.indicators.model.result.IndicatorResult;
import fr.inrae.agroclim.indicators.model.result.PhaseResult;
import fr.inrae.agroclim.indicators.util.DateUtils;
import java.io.File;
import java.time.LocalDate;
import java.time.Month;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * Test {@link Evaluation#computeEachDate()}.
 *
 * @author Olivier Maury
 */
public class EvaluationEachDateTest extends DataTestHelper {

    /**
     * The only year in climatic data.
     */
    private static final Integer YEAR = 2015;

    /**
     * Evaluation from good XML file.
     */
    private static Evaluation evaluation;

    /**
     * Computation results.
     */
    private static Map<LocalDate, Map<Integer, EvaluationResult>> results;

    /**
     * DOY of Stage 0.
     */
    private static final int S0 = 120;

    /**
     * DOY of Stage 0 plus 1 day.
     */
    private static final int S0_PLUS_1 = S0 + 1;

    /**
     * DOY of Stage 1.
     */
    private static final int S1 = 140;

    /**
     * Set up Evaluation to test.
     *
     * Check if evaluation computes.
     */
    @BeforeClass
    public static void beforeTest() {
        final File xmlFile = getFile("xml/evaluation-phalen.gri");
        evaluation = getEvaluation(xmlFile);
        assertTrue("Evaluation must not be null!", evaluation != null);
        try {
            evaluation.initializeResources();
            results = evaluation.computeEachDate();
        } catch (final IndicatorsException e) {
            final String error = e.getClass() + " : " + e.getLocalizedMessage();
            fail(error);
        }
    }

    /**
     * Ensure only 2015 has data.
     */
    @Test
    public void years() {
        assertNotNull(results);

        assertEquals(evaluation.getClimaticResource().getData().size(), results.size());
        final List<Integer> years = results.values().stream().flatMap(m -> m.keySet().stream()).distinct().toList();
        assertNotNull(years);
        assertEquals(1, years.size());
        assertEquals(YEAR, years.get(0));
    }

    /**
     * Ensure only 1 indicator is computed.
     */
    @Test
    public void indicators() {
        // s0s1 : phalen
        final List<Indicator> indicators = new ArrayList<>();
        final List<Indicator> allIndicators = new ArrayList<>();
        allIndicators.addAll(evaluation.getIndicators());
        Indicator indicator = allIndicators.remove(0);
        while (indicator != null) {
            if (indicator instanceof CompositeIndicator c) {
                allIndicators.addAll(c.getIndicators());
            } else {
                indicators.add(indicator);
            }
            if (!allIndicators.isEmpty()) {
                indicator = allIndicators.remove(0);
            } else {
                indicator = null;
            }
        }
        final int nbOfIndicators = indicators.size();
        assertEquals(1, nbOfIndicators);
    }

    /**
     * Ensure no data is returned out of s0s1.
     */
    @Test
    public void noDataOutOfPhase() {
        // no data on 1st january 2015 as no phase
        final Optional<LocalDate> firstDateOptional = results.keySet().stream().findFirst();
        assertTrue(firstDateOptional.isPresent());
        final LocalDate firstDate = firstDateOptional.get();
        final LocalDate expectedFirstDate = LocalDate.of(YEAR, Month.JANUARY, 1);
        assertEquals(expectedFirstDate, firstDate);
        assertFalse(results.get(firstDate).containsKey(YEAR));
    }

    /**
     * Ensure data are return each date of s0s1.
     */
    @Test
    public void dataInPhase() {
        for (int doy = S0_PLUS_1; doy < S1; doy++) {
            final LocalDate date = DateUtils.asLocalDate(DateUtils.getDate(YEAR, doy));
            final List<PhaseResult> phaseResults = results.get(date).get(YEAR).getPhaseResults();
            assertFalse(phaseResults.isEmpty());
            assertEquals(1, phaseResults.size());
            final List<IndicatorResult> processResults = phaseResults.get(0).getIndicatorResults();
            assertNotNull("On " + date + "/" + doy + ", process results must not be null!", processResults);
            assertEquals(1, processResults.size());
            assertEquals("growth", processResults.get(0).getIndicatorId());

            // practices > growth > phalength > phalen
            final List<IndicatorResult> practicesResults = processResults.get(0).getIndicatorResults();
            assertEquals("phalength", practicesResults.get(0).getIndicatorId());
            List<IndicatorResult> indicatorResults = practicesResults.get(0).getIndicatorResults();
            assertEquals("phalen", indicatorResults.get(0).getIndicatorId());
            Double value = indicatorResults.get(0).getRawValue();
            assertNotNull("On " + date + "/" + doy + ", phalen must not be null!", value);
        }
    }

    /**
     * Check computed values for each date.
     */
    @Test
    public void checkData() {
        for (int doy = S0_PLUS_1; doy < S1; doy++) {
            final LocalDate date = DateUtils.asLocalDate(DateUtils.getDate(YEAR, doy));
            final List<PhaseResult> phaseResults = results.get(date).get(YEAR).getPhaseResults();
            final List<IndicatorResult> processResults = phaseResults.get(0).getIndicatorResults();
            final List<IndicatorResult> practicesResults = processResults.get(0).getIndicatorResults();
            final List<IndicatorResult> indicatorResults = practicesResults.get(0).getIndicatorResults();
            final Double value = indicatorResults.get(0).getRawValue();
            assertNotNull("On " + date + "/" + doy + ", phalen must not be null!", value);
            final Double expected = Double.valueOf(doy - S0);
            assertEquals("On " + date + "/" + doy + ", phalen must equal " + expected, expected, value);
        }
    }
}
