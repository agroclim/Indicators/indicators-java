/*
 * This file is part of Indicators.
 *
 * Indicators is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Indicators is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Indicators. If not, see <https://www.gnu.org/licenses/>.
 */
package fr.inrae.agroclim.indicators.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNull;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Test;

/**
 * Test LocalizedString.
 *
 * Last change $Date$
 *
 * @author $Author$
 * @version $Revision$
 */
public final class LocalizedStringTest {

    /**
     * Clone test.
     *
     * @throws CloneNotSupportedException should not be thrown
     */
    @Test
    public void cloneTest() throws CloneNotSupportedException {
        LocalizedString s = new LocalizedString("fr", "s");
        LocalizedString s2 = s.clone();
        assertEquals(s.getLang(), s2.getLang());
        assertEquals(s.getValue(), s2.getValue());
        s2.setValue("s2");
        assertNotEquals(s.getValue(), s2.getValue());
    }

    /**
     * getString(null).
     */
    @Test
    public void getStringNull() {
        List<LocalizedString> list = null;
        String languageCode = "en";
        String actual = LocalizedString.getString(list, languageCode);
        assertNull(actual);
    }
    /**
     * getString() with null locale.
     */
    @Test
    public void getStringNullLocale() {
        String expected = "value";
        List<LocalizedString> list = Arrays.asList(new LocalizedString(null, expected));
        String languageCode = null;
        String actual = LocalizedString.getString(list, languageCode);
        assertEquals(expected, actual);
    }
    /**
     * getString() with empty list.
     */
    @Test
    public void getStringEmpty() {
        List<LocalizedString> list = new ArrayList<>();
        String languageCode = "en";
        String actual = LocalizedString.getString(list, languageCode);
        assertNull(actual);
    }

    /**
     * getString() with empty list.
     */
    @Test
    public void getStringUnknownLocale() {
        List<LocalizedString> list = new ArrayList<>();
        list.add(new LocalizedString("en", "Hello"));
        list.add(new LocalizedString("fr", "Salut"));
        String languageCode = "eo";
        String actual = LocalizedString.getString(list, languageCode);
        assertEquals("Hello", actual);
    }
}
