/*
 * This file is part of Indicators.
 *
 * Indicators is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Indicators is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Indicators. If not, see <https://www.gnu.org/licenses/>.
 */
package fr.inrae.agroclim.indicators.model.indicator;

import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import fr.inrae.agroclim.indicators.exception.IndicatorsException;
import fr.inrae.agroclim.indicators.model.Knowledge;
import lombok.extern.log4j.Log4j2;

/**
 * Test that all indicators defined in knowledge.xml have all implementations.
 *
 * Last changed : $Date$
 *
 * @author $Author$
 * @version $Revision$
 */
@Log4j2
@RunWith(Parameterized.class)
public class ImplementationsTest {

    /**
     * Indicator to test.
     */
    private final Indicator indicator;

    /**
     * @return all indicators
     */
    @Parameterized.Parameters
    public static List<Indicator> data() {
        List<Indicator> indicators = new ArrayList<>();
        try {
            Knowledge knowledge = Knowledge.load();
            for (final Indicator indicator : knowledge.getIndicators()) {
                if (!(indicator instanceof CompositeIndicator)) {
                    continue;
                }
                ((CompositeIndicator) indicator).getIndicators()
                        .forEach((ind) -> {
                    indicators.add(ind);
                });
            }
        } catch (final IndicatorsException ex) {
            LOGGER.fatal("Loading knowledge should not fail!", ex);
        }
        return indicators;
    }

    /**
     * Constructor.
     *
     * @param ind indicator to test
     */
    public ImplementationsTest(final Indicator ind) {
        indicator = ind;
    }

    /**
     * Test that indicator defined in knowledge.xml implements  {@link Cloneable}.
     */
    @Test
    public void isCloneable() {
        String msg = String.format(
                "Indicator '%s' of class '%s' must be cloneable.",
                indicator.getName(),
                indicator.getClass().getName()
        );
        assertTrue(msg, indicator instanceof Cloneable);
        String msgClone = null;
        try {
            indicator.clone();
        } catch (CloneNotSupportedException ex) {
            msgClone = String.format(
                    "Call of clone() on indicator '%s' of class "
                    + "'%s' must success.",
                    indicator.getName(),
                    indicator.getClass().getName()
            );
        }
        assertNull(msgClone);
    }

    /**
     * Test that indicator defined in knowledge.xml implements {@link Detailable}.
     */
    @Test
    public void isDetailable() {
        String msg = String.format(
                "Indicator '%s' of class '%s' must be detailable for Getari.",
                indicator.getName(),
                indicator.getClass().getName()
        );
        assertTrue(msg, indicator instanceof Detailable);
    }
}
