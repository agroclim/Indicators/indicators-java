/*
 * Copyright (C) 2021 INRAE AgroClim
 *
 * This file is part of Indicators.
 *
 * Indicators is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Indicators is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Indicators. If not, see <http://www.gnu.org/licenses/>.
 */
package fr.inrae.agroclim.indicators.model.criteria;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.junit.Test;

import fr.inrae.agroclim.indicators.exception.IndicatorsException;
import fr.inrae.agroclim.indicators.model.EvaluationSettings;
import fr.inrae.agroclim.indicators.model.ExpressionParameter;
import fr.inrae.agroclim.indicators.model.data.Variable;
import fr.inrae.agroclim.indicators.model.data.climate.ClimaticDailyData;
import fr.inrae.agroclim.indicators.xml.XMLUtil;

/**
 * Test Formula criteria.
 *
 * Last changed : $Date$
 *
 * @author $Author$
 * @version $Revision$
 */
public class FormulaCriteriaTest {

    private final ClimaticDailyData data = new ClimaticDailyData();

    public FormulaCriteriaTest() {
        data.setTmean(15D);
    }

    @Test
    public void between() {
        boolean actual;
        actual = FormulaCriteria.between(null, 2D, 3D);
        assertFalse(actual);
        actual = FormulaCriteria.between(1D, 2D, 3D);
        assertFalse(actual);
        actual = FormulaCriteria.between(2D, 1D, 3D);
        assertTrue(actual);
        actual = FormulaCriteria.between(2, 1, 3);
        assertTrue(actual);
        actual = FormulaCriteria.between(2D, null, 3D);
        assertTrue(actual);
        actual = FormulaCriteria.between(2D, 1D, null);
        assertTrue(actual);
    }

    @Test
    public void formulaFunction() throws IndicatorsException {
        final FormulaCriteria crit = new FormulaCriteria();
        crit.setExpression("formulaCriteria:between(TMEAN, 10, 20)");
        assertTrue(crit.eval(data));
    }

    @Test
    public void formulaWithParameter() throws IndicatorsException {
        final FormulaCriteria crit = new FormulaCriteria();
        crit.setExpression("formulaCriteria:between(TMEAN, min, max)");
        final List<ExpressionParameter> expressionParameters = new ArrayList<>();
        expressionParameters.add(new ExpressionParameter("min", 10d));
        expressionParameters.add(new ExpressionParameter("max", 20d));
        crit.setExpressionParameters(expressionParameters);
        assertTrue(crit.eval(data));
        Set<Variable> variables = crit.getVariables();
        assertNotNull(variables);
        assertTrue(variables.contains(Variable.TMEAN));

        final ByteArrayOutputStream stream = new ByteArrayOutputStream();
        XMLUtil.serialize(crit, stream, EvaluationSettings.CLASSES_FOR_JAXB);
        final String xml = new String(stream.toByteArray());
        assertEquals("""
                     <?xml version="1.0" encoding="UTF-8" standalone="yes"?>
                     <formulaCriteria>
                         <expression>formulaCriteria:between(TMEAN, min, max)</expression>
                         <expressionParameter name="min" value="10.0"/>
                         <expressionParameter name="max" value="20.0"/>
                     </formulaCriteria>
                     """, xml);

        final FormulaCriteria clone = crit.clone();
        assertEquals(crit, clone);
        variables = clone.getVariables();
        assertNotNull(variables);
        assertTrue(variables.contains(Variable.TMEAN));

        // test deserialization
        final InputStream inputStream = new ByteArrayInputStream(xml.getBytes());
        final FormulaCriteria actual = (FormulaCriteria) XMLUtil.loadResource(inputStream, EvaluationSettings.CLASSES_FOR_JAXB);
        assertNotNull(actual);
        assertEquals(crit, actual);
        variables = actual.getVariables();
        assertNotNull(variables);
        assertTrue(variables.contains(Variable.TMEAN));
    }

    @Test(expected = IndicatorsException.class)
    public void wrongFunction() throws IndicatorsException {
        final FormulaCriteria crit = new FormulaCriteria();
        crit.setExpression("that:notExists(TMEAN, 10, 20)");
        crit.eval(data);
    }

}
