/*
 * This file is part of Indicators.
 *
 * Indicators is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Indicators is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Indicators. If not, see <https://www.gnu.org/licenses/>.
 */
package fr.inrae.agroclim.indicators.model.indicator;

import static junit.framework.TestCase.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import fr.inrae.agroclim.indicators.exception.IndicatorsException;
import fr.inrae.agroclim.indicators.model.criteria.Criteria;
import fr.inrae.agroclim.indicators.model.criteria.RelationalOperator;
import fr.inrae.agroclim.indicators.model.criteria.SimpleCriteria;
import fr.inrae.agroclim.indicators.model.data.DataTestHelper;
import fr.inrae.agroclim.indicators.model.data.climate.ClimaticResource;

/**
 * Test Quotient indicator.
 *
 * Last changed : $Date$
 *
 * @author $Author$
 * @version $Revision$
 */
public final class QuotientTest extends DataTestHelper {
    /**
     * climate-2015.csv.
     */
    private final ClimaticResource climaticData;

    /**
     * DayOfYear indicator.
     */
    private final Quotient indicator;

    /**
     * Constructor.
     */
    public QuotientTest() {
        /**
         * tmin < 0°C.
         */
        final Criteria frost = new SimpleCriteria("tmin", RelationalOperator.LT, 0);
        final NumberOfDays daysOfFrost = new NumberOfDays();
        daysOfFrost.setCriteria(frost);
        final NumberOfDays count = new NumberOfDays();
        climaticData = new ClimaticResource();
        climaticData.setData(getClimatic2015Data());
        indicator = new Quotient();
        indicator.setDividend(daysOfFrost);
        indicator.setDivisor(count);
    }

    /**
     * % day of frost in 2015.
     */
    @Test
    public void firstDayOfFrost() {

        final double expected = 15. / 365.0;
        double result = -1;
        String error = null;
        try {
            result = indicator.computeSingleValue(climaticData);
        } catch (IndicatorsException ex) {
            error = ex.getClass().getCanonicalName() + " : " + ex.getMessage();
        }
        assertTrue(error, error == null);
        assertEquals(expected, result, 0.0);
    }

    @Test
    public void injectedParameter() {
        InjectedParameter dividend = new InjectedParameter();
        dividend.setParameterValue(10d);
        InjectedParameter divisor = new InjectedParameter();
        divisor.setParameterValue(1d);
        Quotient ind = new Quotient();
        ind.setDividend(dividend);
        ind.setDivisor(divisor);
        double result = -1;
        String error = null;
        try {
            result = ind.computeSingleValue(climaticData);
        } catch (IndicatorsException ex) {
            error = ex.getClass().getCanonicalName() + " : " + ex.getMessage();
        }
        double expected = 10d;
        assertTrue(error, error == null);
        assertEquals(expected, result, 0.0);
    }
}
