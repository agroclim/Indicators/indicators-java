/*
 * Copyright (C) 2020 INRAE AgroClim
 *
 * This file is part of Indicators.
 *
 * Indicators is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Indicators is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Indicators. If not, see <http://www.gnu.org/licenses/>.
 */
package fr.inrae.agroclim.indicators.model.indicator.listener;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import fr.inrae.agroclim.indicators.exception.IndicatorsException;
import fr.inrae.agroclim.indicators.model.Knowledge;
import fr.inrae.agroclim.indicators.model.criteria.SimpleCriteria;
import fr.inrae.agroclim.indicators.model.indicator.Indicator;
import fr.inrae.agroclim.indicators.model.indicator.NumberOfDays;

/**
 *
 *
 * Last change $Date$
 *
 * @author Olivier Maury
 * @author $Author$
 * @version $Revision$
 */
public class PropertyChangeListenerTest {

    /**
     * Flag to check if aggregation listener is fired when changing property of
     * a criteria.
     */
    private boolean listenerFired;
    /**
     * Type of event fired when changing property of a criteria.
     */
    private IndicatorEvent.Type eventType;

    @Test
    public void cdaystmin() throws IndicatorsException {
        listenerFired = false;
        Knowledge k = Knowledge.load();

        String indicatorId1 = "cdaystmin";
        Indicator ind1 = k.getIndicator(indicatorId1);
        assertNotNull(ind1);
        assertTrue(ind1 instanceof NumberOfDays);

        NumberOfDays cdaystmin = (NumberOfDays) ind1;
        assertTrue(cdaystmin.getCriteria() instanceof SimpleCriteria);
        SimpleCriteria criteria = (SimpleCriteria) cdaystmin.getCriteria();
        IndicatorListener listener = (final IndicatorEvent event) -> {
            listenerFired = true;
            eventType = event.getAssociatedType();
        };

        cdaystmin.addIndicatorListener(listener);
        assertFalse("Listener must not be fired yet", listenerFired);
        criteria.setThreshold(criteria.getThreshold() + 1);
        assertTrue("Listener must be fired", listenerFired);
        assertEquals(IndicatorEvent.Type.CHANGE, eventType);
    }
}
