/*
 * This file is part of Indicators.
 *
 * Indicators is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Indicators is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Indicators. If not, see <https://www.gnu.org/licenses/>.
 */
package fr.inrae.agroclim.indicators.model.data.phenology;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.junit.Assert;
import org.junit.Test;

import com.fasterxml.jackson.databind.MappingIterator;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvParser;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;

import lombok.extern.log4j.Log4j2;


/**
 * Test static methods of Phenology calculator.
 *
 * Last changed : $Date$
 *
 * @author $Author$
 * @version $Revision$
 */
@Log4j2
public final class PhenologyCalculatorMethodsTest {
    /**
     * Wrapper to read CSV files in test.
     */
    private class CsvReader {

        /**
         * Row iterator.
         */
        private final MappingIterator<Map<String, String>> reader;

        /**
         * Constructor.
         *
         * @param filePath CSV file path
         * @throws IOException exception (file not found, reading error, ...)
         */
        CsvReader(final String filePath) throws IOException {
            File csvFile = new File(filePath);
            CsvMapper mapper = new CsvMapper();
            CsvSchema schema = CsvSchema.emptySchema()
                    .withColumnSeparator(';')
                    .withHeader();
            reader = mapper.readerFor(Map.class)
                    .with(schema)
                    .with(CsvParser.Feature.SKIP_EMPTY_LINES)
                    .readValues(csvFile);
        }

        /**
         * Close reader.
         *
         * @throws IOException IO exception while closing
         */
        public void close() throws IOException {
            reader.close();
        }

        /**
         * @return rows from CSV
         */
        public List<Map<String, Number>> read() {
            List<Map<String, Number>> results = new ArrayList<>();
            while (reader.hasNext()) {
                Map<String, String> row = reader.next();
                if (row == null) {
                    break;
                }
                Map<String, Number> data = new HashMap<>();
                row.forEach((k, v) -> {
                    try {
                        if (k.equals("i")) {
                            data.put(k, Integer.parseInt(v));
                        } else {
                            data.put(k, Double.parseDouble(v));
                        }
                    } catch (NumberFormatException e) {
                        LOGGER.catching(e);
                    }
                });
                results.add(data);
            }
            return results;
        }
    }

    /**
     * Directory for CSV files.
     */
    private static final String DIR = "src/test/resources/fr/inrae/agroclim/"
            + "indicators/model/data/phenology/";

    /**
     * Delta estimation.
     */
    private final double delta = 0.00001;

    /**
     * Relative delta estimation.
     */
    private final double relativeDelta = 0.0005;

    @Test
    public void cumjviOneValue() {
        final double optiVernAmp = 10;
        final double optiVernTemp = 6.5;
        final int startDate = 1;
        final double initialCumjvi = 0;
        final double firstTmean = 20.95;
        final List<Double> tmeans = Arrays.asList(firstTmean);
        double[] actual = PhenologyCalculator.cumjvi(optiVernAmp,
                optiVernTemp, tmeans, startDate, initialCumjvi);
        assertEquals(1, actual.length);
        assertTrue(0. == actual[0]);
    }

    /**
     * Compare with results from R.
     *
     * @throws IOException error while reading CSV
     */
    @Test
    public void cumjvi() throws IOException {
        File iniFile = new File(DIR + "cumjvi.ini");
        FileInputStream fis = new FileInputStream(iniFile);
        Properties props = new Properties();
        props.load(fis);
        double optiVernAmp
                = Double.parseDouble(props.getProperty("optiVernAmp"));
        double optiVernTemp
                = Double.parseDouble(props.getProperty("optiVernTemp"));
        int startDate = Integer.parseInt(props.getProperty("startDate"));
        double initialCumjvi = Double.parseDouble(
                props.getProperty("initialCumjvi"));
        String filePath = DIR + "cumjvi.csv";
        final double firstTmean = 20.95;
        CsvReader reader = new CsvReader(filePath);
        List<Double> tmeans = new ArrayList<>();
        List<Double> expected = new ArrayList<>();
        for (final Map<String, Number> row : reader.read()) {
            int i = (Integer) row.get("i");
            double tmean = (Double) row.get("tmean");
            tmeans.add(tmean);
            if (i >= startDate) {
                double result = (Double) row.get("result");
                expected.add(result);
            }
            if (i == 1) {
                assertTrue(firstTmean == tmean);
            }
        }
        reader.close();
        double[] res = PhenologyCalculator.cumjvi(optiVernAmp, optiVernTemp,
                tmeans, startDate, initialCumjvi);
        List<Double> actual = new ArrayList<>();
        for (double result : res) {
            actual.add(result);
        }
        assertEquals(expected.size(), actual.size());
        for (int i = 0; i < expected.size(); i++) {
            double diff = Math.abs(expected.get(i) - actual.get(i));
            double d = relativeDelta + Math.abs(expected.get(i));
            if (diff > d) {
                assertTrue(
                        String.format("i=%d expected=%.2f actual=%.2f "
                                + "diff=%f>%f",
                                i + startDate, expected.get(i),
                                actual.get(i), diff, d),
                        false);
            }
        }
    }

    /**
     * Test setNbOfStages() / getNbOfStages().
     */
    @Test
    public void nbOfStages() {
        PhenologyCalculator calc = new PhenologyCalculator();
        for (int i = 0; i <= Stage.FOUR; i++) {
            calc.setNbOfStages(i);
            assertEquals(i, calc.getNbOfStages());
        }
    }

	/**
	 * Compare with results from R (get by photp(48.8534100, 1)).
	 * @throws IOException
	 *             exception while reading CSV file
	 */
	@Test
	public void photoperiod() throws IOException {
		String filePath = DIR + "photoperiod.csv";
		CsvReader reader = new CsvReader(filePath);
            for (final Map<String, Number> row : reader.read()) {
			double latitude = (Double) row.get("latitude");
                int day = row.get("day").intValue();
			Double expected = (Double) row.get("result");
			final Double actual = PhenologyCalculator
					.photoperiod(day, latitude);
			Assert.assertEquals(expected, actual, delta);
		}
		reader.close();

	}

	/**
	 * Compare with results from R (get by RFPI(...)).
	 * @throws IOException
	 *             error while reading CSV
	 */
	@Test
	public void rfpi() throws IOException {
		/*-
		 * zlat=48.3; jday=1; P_phobase=2; P_phosat=6; P_sensiphot=0.9;
		 * PARAM_PHENO=c(1,2,3,4,5,6,P_sensiphot,P_phosat,P_phobase)
		 * RFPI(jday, zlat, PARAM_PHENO)
		 */
		String filePath = DIR + "rfpi.csv";
		CsvReader reader = new CsvReader(filePath);
            for (final Map<String, Number> row : reader.read()) {
			double latitude = (Double) row.get("latitude");
                int day = row.get("day").intValue();
			double sensitivity = (Double) row.get("sensitivity");
			double base = (Double) row.get("base");
			double saturating = (Double) row.get("saturating");
			Double expected = (Double) row.get("result");
			Double actual = PhenologyCalculator.rfpi(day, latitude,
					sensitivity, saturating, base);
			Assert.assertEquals(expected, actual, delta);
		}
		reader.close();
    }

    /**
     * Compare with results from R.
     *
     * @throws IOException error while reading CSV
     */
    @Test
    public void rfvi() throws IOException {
        String filePath = DIR + "rfvi.csv";
        CsvReader reader = new CsvReader(filePath);
        for (final Map<String, Number> row : reader.read()) {
            int nbOfVernalizationDays
                    = row.get("nbOfVernalizationDays").intValue();
            int minNbOfVernalizationDays
                    = row.get("minNbOfVernalizationDays").intValue();
            double cumjvi = (Double) row.get("cumjvi");
            Double expected = (Double) row.get("result");
            Double actual = PhenologyCalculator.rfvi(nbOfVernalizationDays,
                    minNbOfVernalizationDays, cumjvi);
            Assert.assertEquals(expected, actual, delta);
        }
        reader.close();
    }
}
