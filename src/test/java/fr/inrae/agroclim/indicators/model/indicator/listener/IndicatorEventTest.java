/*
 * This file is part of Indicators.
 *
 * Indicators is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Indicators is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Indicators. If not, see <https://www.gnu.org/licenses/>.
 */
package fr.inrae.agroclim.indicators.model.indicator.listener;

import static org.junit.Assert.assertNotNull;

import org.junit.Test;

import fr.inrae.agroclim.indicators.model.Evaluation;
import fr.inrae.agroclim.indicators.model.indicator.PotentialSowingDaysFrequency;

/**
 * Test IndicatorEvent.
 *
 * Last changed : $Date$
 *
 * @author $Author$
 * @version $Revision$
 */
public class IndicatorEventTest {

    /**
     * No exception when well used.
     */
    @Test(expected = Test.None.class)
    public void shouldNotRaiseException() {
        IndicatorEvent e;
        e = IndicatorEvent.Type.ADD.event(
                new PotentialSowingDaysFrequency()
        );
        assertNotNull(e);
        e = IndicatorEvent.Type.PHASE_MISSING.event(
                new Evaluation()
        );
        assertNotNull(e);
    }

    /**
     * Inconsistency between type and indicator.
     */
    @Test(expected = IllegalArgumentException.class)
    public void shouldRaiseException() {
        IndicatorEvent e;
        e = IndicatorEvent.Type.PHASE_MISSING.event(
                new PotentialSowingDaysFrequency()
        );
        assertNotNull(e);
    }

    /**
     * Null indicator.
     */
    @Test(expected = NullPointerException.class)
    public void shouldRaiseNullPointerException() {
        IndicatorEvent e;
        e = IndicatorEvent.Type.PHASE_MISSING.event(null);
        assertNotNull(e);
    }
}
