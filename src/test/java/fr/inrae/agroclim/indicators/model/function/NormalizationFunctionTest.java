/*
 * This file is part of Indicators.
 *
 * Indicators is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Indicators is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Indicators. If not, see <https://www.gnu.org/licenses/>.
 */
package fr.inrae.agroclim.indicators.model.function;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import fr.inrae.agroclim.indicators.model.function.normalization.Exponential;
import fr.inrae.agroclim.indicators.model.function.normalization.Linear;
import fr.inrae.agroclim.indicators.model.function.normalization.MultiLinear;
import fr.inrae.agroclim.indicators.model.function.normalization.MultiLinearInterval;
import fr.inrae.agroclim.indicators.model.function.normalization.Normal;
import fr.inrae.agroclim.indicators.model.function.normalization.Sigmoid;

/**
 *
 * @author jucufi
 */
public class NormalizationFunctionTest {
    /**
     * Epsilon.
     */
    private static final double E = 0.00001;

    @Test
    public void exponential() {
        Exponential n = new Exponential(0.1d, 5.5d);
        assertEquals(0.1096d, n.normalize(60d), E);
    }

    @Test
    public void linear() {
        Linear n = new Linear(0.03d, 0.2d);
        assertEquals(0.5d, n.normalize(10d), E);
    }

    @Test
    public void multilinear() {
        MultiLinear n = new MultiLinear();
        assertEquals(0, n.normalize(10d), E);

        Linear linear = new Linear(0.03d, 0.2d);
        MultiLinearInterval interval = new MultiLinearInterval();
        interval.setLinear(linear);
        n.getIntervals().add(interval);
        assertEquals(0.5d, n.normalize(10d), E);

        interval.setMax(9d);
        assertEquals(0, n.normalize(10d), E);
    }

    @Test
    public void normal() {
        Normal n = new Normal(1d, 20d, 30d);
        assertEquals(0.5d, n.normalize(10d), E);
    }

    @Test
    public void sigmoid() {
        Sigmoid n = new Sigmoid(20d, -4d);
        assertEquals(0.5d, n.normalize(20d), E);
    }

    @Test
    public void sigmoid6225() {
        Sigmoid n = new Sigmoid(5d, 0d);
        assertEquals(Double.NaN, n.normalize(5d), E);
    }
}
