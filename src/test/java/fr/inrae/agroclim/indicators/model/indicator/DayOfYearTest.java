/*
 * This file is part of Indicators.
 *
 * Indicators is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Indicators is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Indicators. If not, see <https://www.gnu.org/licenses/>.
 */
package fr.inrae.agroclim.indicators.model.indicator;

import static junit.framework.TestCase.assertEquals;
import static org.junit.Assert.assertNull;

import org.junit.Test;

import fr.inrae.agroclim.indicators.exception.IndicatorsException;
import fr.inrae.agroclim.indicators.model.criteria.Criteria;
import fr.inrae.agroclim.indicators.model.criteria.RelationalOperator;
import fr.inrae.agroclim.indicators.model.criteria.SimpleCriteria;
import fr.inrae.agroclim.indicators.model.data.DataTestHelper;
import fr.inrae.agroclim.indicators.model.data.climate.ClimaticResource;

/**
 * Test "day of year" indicator.
 *
 * Last changed : $Date$
 *
 * @author $Author$
 * @version $Revision$
 */
public final class DayOfYearTest extends DataTestHelper {
    /**
     * climate-2015.csv.
     */
    private final ClimaticResource climaticData;

    /**
     * tmin < 0°C.
     */
    private final Criteria frost;

    /**
     * DayOfYear indicator.
     */
    private final DayOfYear indicator;

    /**
     * Constructor.
     */
    public DayOfYearTest() {
        frost = new SimpleCriteria("tmin", RelationalOperator.LT, 0);
        climaticData = new ClimaticResource();
        climaticData.setData(getClimatic2015Data());
        indicator = new DayOfYear();
        indicator.setCriteria(frost);
    }

    /**
     * First day of frost in 2015.
     */
    @Test
    public void firstDayOfFrost() {
        indicator.setFirst(true);

        final double expected = 2.0;
        double result = -1;
        String error = null;
        try {
            result = indicator.computeSingleValue(climaticData);
        } catch (final IndicatorsException ex) {
            error = ex.getClass().getCanonicalName() + " : " + ex.getMessage();
        }
        assertNull(error, error);
        assertEquals(expected, result, 0.0);
    }

    /**
     * Last day of frost in 2015.
     */
    @Test
    public void lastDayOfFrost() {
        indicator.setFirst(false);

        // 12-12-2015
        final double expected = 346.0;
        double result = -1;
        String error = null;
        try {
            result = indicator.computeSingleValue(climaticData);
        } catch (final IndicatorsException ex) {
            error = ex.getClass().getCanonicalName() + " : " + ex.getMessage();
        }
        assertNull(error, error);
        assertEquals(expected, result, 0.0);
    }

}
