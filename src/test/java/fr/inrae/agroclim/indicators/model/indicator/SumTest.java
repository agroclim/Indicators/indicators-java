package fr.inrae.agroclim.indicators.model.indicator;

import static junit.framework.TestCase.assertEquals;
import static org.junit.Assert.assertNull;

import org.junit.Test;

import fr.inrae.agroclim.indicators.exception.IndicatorsException;
import fr.inrae.agroclim.indicators.model.criteria.NoCriteria;
import fr.inrae.agroclim.indicators.model.criteria.SimpleCriteria;
import fr.inrae.agroclim.indicators.model.data.DataTestHelper;
import fr.inrae.agroclim.indicators.model.data.Variable;
import fr.inrae.agroclim.indicators.model.data.climate.ClimaticResource;

/**
 * Test "Sum" indicator.
 *
 * Last changed : $Date$
 *
 * @author $Author$
 * @version $Revision$
 */
public class SumTest extends DataTestHelper {
    /**
     * climate-2015.csv.
     */
    private final ClimaticResource climaticData;

    /**
     * Constructor.
     */
    public SumTest() {
        climaticData = new ClimaticResource();
        climaticData.setData(getClimatic2015Data());
    }

    /**
     * With NoCriteria.
     */
    @Test
    public void noCriteria() {
        final Sum indicator = new Sum();
        final NoCriteria criteria = new NoCriteria();
        criteria.setVariable(Variable.RAIN);
        indicator.setCriteria(criteria);

        final double expected = climaticData.getData().stream()
                .map(data -> data.getRain())
                .reduce((v1, v2) -> v1 + v2)
                .get();
        double result = -1;
        String error = null;
        try {
            result = indicator.computeSingleValue(climaticData);
        } catch (final IndicatorsException ex) {
            error = ex.getClass().getCanonicalName() + " : " + ex.getMessage();
        }
        assertNull(error, error);
        assertEquals(expected, result, 0.0);
    }

    /**
     * With noThreshold set to true.
     */
    @Test
    public void noThreshold() {
        final Sum indicator = new Sum();
        final SimpleCriteria criteria = new SimpleCriteria();
        criteria.setVariable(Variable.RAIN);
        criteria.setNoThreshold(true);
        indicator.setCriteria(criteria);

        final double expected = climaticData.getData().stream()
                .map(data -> data.getRain())
                .reduce((v1, v2) -> v1 + v2)
                .get();
        double result = -1;
        String error = null;
        try {
            result = indicator.computeSingleValue(climaticData);
        } catch (final IndicatorsException ex) {
            error = ex.getClass().getCanonicalName() + " : " + ex.getMessage();
        }
        assertNull(error, error);
        assertEquals(expected, result, 0.0);
    }
}
