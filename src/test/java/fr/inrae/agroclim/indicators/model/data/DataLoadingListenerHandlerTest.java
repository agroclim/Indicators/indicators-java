/*
 * Copyright (C) 2021 INRAE AgroClim
 *
 * This file is part of Indicators.
 *
 * Indicators is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Indicators is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Indicators. If not, see <http://www.gnu.org/licenses/>.
 */
package fr.inrae.agroclim.indicators.model.data;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

/**
 * Test handling of listeners.
 *
 * Last changed : $Date$
 *
 * @author $Author$
 * @version $Revision$
 */
public class DataLoadingListenerHandlerTest {

    /**
     * Ensure no duplication of listeners.
     */
    @Test
    public void addDataLoadingListener() {
        final DataLoadingListenerHandler handler = new DataLoadingListenerHandler();
        final DataLoadingListener l = new DataLoadingListener() {
            @Override
            public void onDataLoadingAdd(final Data data) {
                // do nothing
            }

            @Override
            public void onDataLoadingEnd(final String text) {
                // do nothing
            }

            @Override
            public void onDataLoadingStart(final String text) {
                // do nothing
            }

            @Override
            public void onFileSet(final DataLoadingListener.DataFile type) {
                // do nothing
            }
        };
        assertEquals(0, handler.getDataLoadingListeners().length);
        handler.addDataLoadingListener(l);
        assertEquals(1, handler.getDataLoadingListeners().length);
        handler.addDataLoadingListener(l);
        assertEquals(1, handler.getDataLoadingListeners().length);

        final DataLoadingListener l2 = new DataLoadingListener() {
            @Override
            public void onDataLoadingAdd(final Data data) {
                // do nothing
            }

            @Override
            public void onDataLoadingEnd(final String text) {
                // do nothing
            }

            @Override
            public void onDataLoadingStart(final String text) {
                // do nothing
            }

            @Override
            public void onFileSet(final DataLoadingListener.DataFile type) {
                // do nothing
            }
        };
        final DataLoadingListener[] ls2 = {l, l2, l};
        handler.addDataLoadingListeners(ls2);
        assertEquals(2, handler.getDataLoadingListeners().length);
    }
}
