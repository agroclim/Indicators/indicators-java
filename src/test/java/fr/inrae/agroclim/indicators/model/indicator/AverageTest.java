/*
 * Copyright (C) 2020 INRAE AgroClim
 *
 * This file is part of Indicators.
 *
 * Indicators is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Indicators is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Indicators. If not, see <http://www.gnu.org/licenses/>.
 */
package fr.inrae.agroclim.indicators.model.indicator;

import static junit.framework.TestCase.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.nio.file.Files;
import java.nio.file.Path;

import org.junit.Test;

import fr.inrae.agroclim.indicators.exception.IndicatorsException;
import fr.inrae.agroclim.indicators.model.Knowledge;
import fr.inrae.agroclim.indicators.model.TimeScale;
import fr.inrae.agroclim.indicators.model.criteria.NoCriteria;
import fr.inrae.agroclim.indicators.model.data.DataTestHelper;
import fr.inrae.agroclim.indicators.model.data.Variable;
import fr.inrae.agroclim.indicators.model.data.climate.ClimaticDailyData;
import fr.inrae.agroclim.indicators.model.data.climate.ClimaticResource;

/**
 * Test {@link Average} indicator.
 *
 * Last changed : $Date$
 *
 * @author $Author$
 * @version $Revision$
 */
public class AverageTest extends DataTestHelper {
    /**
     * climate-2015.csv.
     */
    private final ClimaticResource climaticData;

    /**
     * Indicator to test.
     */
    private final Average indicator;

    /**
     * Constructor.
     */
    public AverageTest() {
        climaticData = new ClimaticResource();
        climaticData.setData(getClimatic2015Data());
        indicator = new Average();
        indicator.setVariable(Variable.TMIN);
    }

    @Test
    public void computeSingleValue() {
        final double expected = climaticData.getData().stream()
                .mapToDouble(ClimaticDailyData::getTmin) //
                .average().getAsDouble();
        double result = -1;
        String error = null;
        try {
            result = indicator.computeSingleValue(climaticData);
        } catch (IndicatorsException ex) {
            error = ex.getClass().getCanonicalName() + " : " + ex.getMessage();
        }
        assertEquals(null, error);
        assertEquals(expected, result, E);
    }

    @Test
    public void isComputable() {
        assertTrue(indicator.isComputable());
        assertTrue(indicator.isComputable(climaticData));
    }

    @Test
    public void mint() throws IndicatorsException, CloneNotSupportedException {
        final Knowledge knowledge = Knowledge.load(TimeScale.DAILY);
        final String indicatorId = "mint";
        final IndicatorCategory expectedCat = IndicatorCategory.INDICATORS;
        final Indicator ind = knowledge.getIndicator(indicatorId);
        assertNotNull("An indicator with id \"" + indicatorId + "\" must be found!", ind);
        assertEquals(Average.class, ind.getClass());
        assertEquals(expectedCat, ind.getIndicatorCategory());
        assertEquals(expectedCat.getTag(), ind.getCategory());
        final Average avg = (Average) ind;
        assertEquals(Variable.TMIN, avg.getVariable());
        assertEquals(1, avg.getVariables().size());
        assertEquals(avg.getVariable(), avg.getVariables().iterator().next());
        assertNotNull(avg.getCriteria());
        assertEquals(avg.getVariable(), ((NoCriteria) avg.getCriteria()).getVariable());
        final double expected = indicator.computeSingleValue(climaticData);
        final double result = avg.computeSingleValue(climaticData);
        final Average clone = avg.clone();
        assertEquals(avg.getVariable(), clone.getVariable());
        assertEquals(avg.getVariable(), ((NoCriteria) clone.getCriteria()).getVariable());
        assertEquals(expected, result, E);
    }

    @Test
    public void serialization() throws IOException, ClassNotFoundException {
        final Path path = Files.createTempFile("indicator-average", ".ser");
        try (FileOutputStream fout = new FileOutputStream(path.toFile());
                ObjectOutputStream out = new ObjectOutputStream(fout);) {
            out.writeObject(indicator);
            out.flush();
        }
        assertTrue(path.toFile().length() > 0);

        try (FileInputStream fin = new FileInputStream(path.toFile());
                ObjectInputStream ois = new ObjectInputStream(fin);) {
            final Average deserialized = (Average) ois.readObject();
            assertNotNull(deserialized);
            assertEquals(indicator, deserialized);
            assertEquals(indicator.getVariables(), deserialized.getVariables());
            assertEquals(indicator.getCriteria().getVariables(), deserialized.getCriteria().getVariables());
        }
    }
}
