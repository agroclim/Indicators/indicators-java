/*
 * This file is part of Indicators.
 *
 * Indicators is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Indicators is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Indicators. If not, see <https://www.gnu.org/licenses/>.
 */
package fr.inrae.agroclim.indicators.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import fr.inrae.agroclim.indicators.exception.IndicatorsException;
import fr.inrae.agroclim.indicators.model.data.Variable;
import fr.inrae.agroclim.indicators.model.indicator.CompositeIndicator;
import fr.inrae.agroclim.indicators.model.indicator.Indicator;
import fr.inrae.agroclim.indicators.model.indicator.IndicatorCategory;
import fr.inrae.agroclim.indicators.model.indicator.NumberOfWaves;
import fr.inrae.agroclim.indicators.model.indicator.Quotient;

/**
 * Test the class vs the XML file.
 *
 * Last change $Date$
 *
 * @author $Author$
 * @version $Revision$
 */
@RunWith(Parameterized.class)
public final class KnowledgeTest {
    /**
     * @return combinaisons of values, operator and expectations
     */
    @Parameterized.Parameters
    public static List<TimeScale> data() {
        return Arrays.asList(TimeScale.values());
    }

    /**
     * Knowledge loaded from XML.
     */
    private Knowledge knowledge;

    /**
     * Constructor.
     * @param timeScale timescale for the knowledge XML file.
     */
    public KnowledgeTest(final TimeScale timeScale) {
        try {
            knowledge = Knowledge.load(timeScale);
        } catch (final IndicatorsException ex) {
            fail("Loading XML file from Knowledge.load() should work!");
        }
    }

    /**
     * @param listName composite indicator name
     * @param names list of names
     * @param duplicatedNames list of duplicated names
     * @param ids list of ids
     * @param duplicatedIds list of duplicated ids
     * @param list composite indicator list
     */
    private void assertNoDuplicates(final String listName,
            final List<CompositeIndicator> list) {

        final List<String> names = new ArrayList<>();
        final List<String> duplicatedNames = new ArrayList<>();
        final List<String> ids = new ArrayList<>();
        final List<String> duplicatedIds = new ArrayList<>();
        for (final CompositeIndicator i : list) {
            final List<Indicator> indicators = i.getIndicators();
            for (final Indicator indicator : indicators) {
                final String name = indicator.getName();
                if (names.contains(name)) {
                    duplicatedNames.add(name);
                } else {
                    names.add(name);
                }
                final String id = indicator.getId();
                if (ids.contains(id)) {
                    duplicatedIds.add(id);
                } else {
                    ids.add(id);
                }
            }
        }
        assertTrue("From " + listName
                + ", there should not be any duplicated names! "
                + duplicatedNames.toString(), duplicatedNames.isEmpty());
        assertTrue(
                "From " + listName
                + ", there should not be any duplicated id! "
                + duplicatedIds.toString(), duplicatedIds.isEmpty());
    }

    /**
     * LocalizedString must end with a point.
     */
    @Test
    public void descriptionEndsWithPoint() {
        knowledge
        .getIndicators()
        .forEach(
                (comp) -> {
                    comp.getIndicators()
                    .forEach(
                            (ind) -> {
                                for (final LocalizedString description : ind
                                        .getDescriptions()) {
                                    final String lang = description
                                            .getLang();
                                    final String value = description
                                            .getValue();
                                    assertTrue(
                                            "Description must not be null for indicator "
                                                    + ind.getId()
                                                    + ", lang="
                                                    + lang,
                                                    value != null);
                                    assertTrue(
                                            "Description must end with a point for indicator "
                                                    + ind.getId()
                                                    + ", lang="
                                                    + lang,
                                                    value.endsWith("."));
                                }
                            });
                });
    }

    /**
     * English description must be set for all indicators.
     */
    @Test
    public void descriptionInEnglish() {
        knowledge.getIndicators().forEach(
                (comp) -> {
                    comp.getIndicators().forEach(
                            (ind) -> {
                                boolean inEnglish = false;
                                for (final LocalizedString description : ind
                                        .getDescriptions()) {
                                    if ("en".equals(description.getLang())) {
                                        if (description.getValue() == null
                                                || ".".equals(description
                                                        .getValue())) {
                                            continue;
                                        }
                                        inEnglish = true;
                                        break;
                                    }
                                }
                                assertTrue("English description must be set to "
                                        + ind.getId(), inEnglish);
                            });
                });
    }

    /**
     * French description must be set for all indicators.
     */
    @Test
    public void descriptionInFrench() {
        knowledge.getIndicators().forEach(
                (comp) -> {
                    comp.getIndicators().forEach(
                            (ind) -> {
                                boolean inFrench = false;
                                for (final LocalizedString description : ind
                                        .getDescriptions()) {
                                    if ("fr".equals(description.getLang())) {
                                        if (description.getValue() == null
                                                || ".".equals(description
                                                        .getValue())) {
                                            continue;
                                        }
                                        inFrench = true;
                                        break;
                                    }
                                }
                                assertTrue("French description must be set to "
                                        + ind.getId(), inFrench);
                            });
                });
    }

    /**
     * Check that getIndicator() works.
     */
    @Test
    public void getIndicator() {
        String indicatorId = "wetfreq";
        Indicator indicator = knowledge.getIndicator(indicatorId);
        assertNotNull("An indicator with id \"" + indicatorId
                + "\" must be found!", indicator);
        indicatorId = "cold";
        indicator = knowledge.getIndicator(indicatorId);
        assertNotNull("An indicator with id \"" + indicatorId
                + "\" must be found!", indicator);
        indicatorId = "heat";
        indicator = knowledge.getIndicator(indicatorId);
        assertNotNull("An indicator with id \"" + indicatorId
                + "\" must be found!", indicator);
    }

    @Test
    public void getIndicatorCategory() {
        final String indicatorId = "mort";
        IndicatorCategory expected;
        expected = IndicatorCategory.ECOPHYSIOLOGICAL_PROCESSES;
        final Indicator indicator = knowledge.getIndicator(indicatorId);
        assertEquals(expected, indicator.getIndicatorCategory());
        assertEquals(expected.getTag(), indicator.getCategory());
    }

    /**
     * Check that getNextIndicator() works.
     */
    @Test
    public void getNextIndicators() {
        String indicatorId = "growth";
        CompositeIndicator indicator = (CompositeIndicator) knowledge.getIndicator(indicatorId);
        assertNotNull("An indicator with id \"" + indicatorId
                + "\" must be found!", indicator);
        List<? extends Indicator> next = knowledge.getNextIndicators(indicator);
        assertFalse(indicatorId + " do have child indicators.", next.isEmpty());
        assertTrue(indicatorId + " do have many child indicators, found "
                + next.size(), next.size() > 3);

        indicatorId = "cold";
        indicator = (CompositeIndicator) knowledge.getIndicator(indicatorId);
        next = knowledge.getNextIndicators(indicator);
        assertFalse(indicatorId + " do have child indicators.", next.isEmpty());
        assertTrue(indicatorId + " do have many child indicators, found "
                + next.size(), next.size() >= 10);

        indicatorId = "mort";
        indicator = (CompositeIndicator) knowledge.getIndicator(indicatorId);
        next = knowledge.getNextIndicators(indicator);
        assertFalse(indicatorId + " do have child indicators.", next.isEmpty());
        assertTrue(indicatorId + " do have many child indicators, found "
                + next.size(), next.size() > 1);
    }

    /**
     * Check that getNextIndicator() works for practices.
     */
    @Test
    public void getNextIndicatorsPractices() {
        String indicatorId = "s0s2";
        CompositeIndicator indicator = new CompositeIndicator();
        indicator.setId(indicatorId);
        indicator.setIndicatorCategory(IndicatorCategory.CULTURAL_PRACTICES);
        indicator.setTag("pheno-s0-s2");
        List<? extends Indicator> next = knowledge.getNextIndicators(indicator);
        assertFalse(indicatorId + " do have child indicators.", next.isEmpty());
        final int expected = 2;
        assertEquals(indicatorId + " do have " + expected + " child indicators",
                expected, next.size());

        indicator.setIndicatorCategory(IndicatorCategory.ECOPHYSIOLOGICAL_PROCESSES);
        next = knowledge.getNextIndicators(indicator);
        assertFalse(indicatorId + " do have child indicators.", next.isEmpty());

        indicatorId = "sow";
        indicator = (CompositeIndicator) knowledge.getIndicator(indicatorId);
        next = knowledge.getNextIndicators(indicator);
        assertFalse(indicatorId + " do have child indicators.", next.isEmpty());
        assertTrue(indicatorId + " do have many child indicators, found "
                + next.size(), next.size() > 3);

    }

    /**
     * Check that getNextIndicator() works for ecophysiologicalProcesses.
     */
    @Test
    public void getNextIndicatorsProcesses() {
        final String indicatorId = "ecophysiologicalProcesses";
        final CompositeIndicator indicator = new CompositeIndicator();
        indicator.setId(indicatorId);
        indicator.setIndicatorCategory(IndicatorCategory.PHENO_PHASES);
        indicator.setTag(IndicatorCategory.ECOPHYSIOLOGICAL_PROCESSES.getTag());
        final List<? extends Indicator> next = knowledge.getNextIndicators(indicator);
        assertFalse(indicatorId + " do have child indicators.", next.isEmpty());
        final int expected = 3;
        assertEquals(indicatorId + " do have " + expected + " child indicators",
                expected, next.size());
    }

    private Collection<Indicator> getSimpleIndicators() {
        final List<Indicator> list = new ArrayList<>();
        knowledge.getIndicators().stream().map((comp) -> comp.getIndicators())
        .forEach((indicators) -> list.addAll(indicators));
        return list;
    }

    /**
     * Ensure that measurement unit is set for all indicators.
     */
    @Test
    public void measurementUnit() {
        var format = "Unit must be set for %d %s indicators: \"%s\".";
        List<String> missing = new ArrayList<>();
        knowledge.getIndicators().forEach(
                (comp) -> {
                    comp.getIndicators().forEach(
                            (ind) -> {
                                if (ind.getUnit() == null) {
                                    missing.add(ind.getId());
                                }
                });
            });
        var msg = String.format(format, missing.size(), knowledge.getTimescale(), String.join(", ", missing));
        assertTrue(msg, missing.isEmpty());
    }

    /**
     * Assert that indicators has at least one variable and variables are not
     * null.
     */
    @Test
    public void noNullVariables() {
        getSimpleIndicators().forEach((ind) -> {
            assertNotNull(ind);
            final String id = ind.getId();
            final Set<Variable> vars = ind.getVariables();
            assertNotNull(id + ".variables must not be a null set.", vars);
            assertTrue(id + ".variables must contain at least one variable.", !vars.isEmpty());
            // with unmodifiable set, contains(null) throws NullPointerException
            final boolean contains = vars.stream().anyMatch(Objects::isNull);
            assertFalse(id + ".variables must not contains null. variables=" + vars, contains);
        });

    }

    /**
     * Ensure that quotient indicators have all dividend and divisor.
     */
    @Test
    public void notNullQuotientDividendAndDivisor() {
        final List<Quotient> indicators = getSimpleIndicators().stream()
                .filter((ind) -> (ind instanceof Quotient))
                .map((ind) -> (Quotient) ind)
                .filter((ind) -> ind.getDividend() == null || ind.getDivisor() == null)
                .collect(Collectors.toList());
        assertTrue(indicators.isEmpty());
    }

    /**
     * LocalizedString must end with a point.
     */
    @Test
    public void parameterDescriptionEndsWithPoint() {
        knowledge.getParameters().forEach((param) -> {
            for (final LocalizedString description : param.getDescriptions()) {
                final String lang = description.getLang();
                final String value = description.getValue();
                assertTrue(
                        "Description must not be null for parameter "
                                + param.getId() + ", lang=" + lang,
                                value != null);
                assertTrue(
                        "Description must end with a point for parameter "
                                + param.getId() + ", lang=" + lang,
                                value.endsWith("."));
            }
        });
    }

    /**
     * Assert that all ids of indicator parameters and criteria parameters are
     * defined.
     */
    @Test
    public void parameterIdsAreDefined() {
        assertNotNull("Knowledge parameters must not be null!", knowledge.getParameters());
        assertFalse("Knowledge parameters must not be empty!", knowledge.getParameters().isEmpty());
        final Set<String> ids = new HashSet<>();
        knowledge.getParameters().forEach((param) -> {
            ids.add(param.getId());
        });
        final List<String> errors = new ArrayList<>();
        knowledge.getIndicators().forEach((comp) -> {
            comp.getIndicators().forEach((ind) -> {
                assertNotNull("Strange, an indicator is null in " + comp.getId(), ind);
                if (ind.getParameters() == null) {
                    errors.add(ind.getClass().getSimpleName() + ".getParameters() must not be null in " + ind.getId());
                } else {
                    ind.getParameters().forEach((param) -> {
                        if (!ids.contains(param.getId())) {
                            errors.add(param.getId() + " in " + ind.getId());
                        }
                    });
                }
            });
        });
        assertTrue("All parameter ids must be defined: "
                + String.join(". ", errors), errors.isEmpty());
    }

    /**
     * Assert that there is not any duplicated parameter id.
     */
    @Test
    public void parameterIdsAreUnique() {
        assertTrue("Knowledge parameters must not be null!",
                knowledge.getParameters() != null);
        assertTrue("Knowledge parameters must not be empty!",
                !knowledge.getParameters().isEmpty());
        final Set<String> ids = new HashSet<>();
        final Set<String> duplicates = new HashSet<>();
        for (final Parameter param : knowledge.getParameters()) {
            if (ids.contains(param.getId())) {
                duplicates.add(param.getId());
            }
            ids.add(param.getId());
        }
        assertTrue("Parameter ids must be unique! " + duplicates.toString(),
                duplicates.isEmpty());
    }

    /**
     * English description must be set for all parameters.
     */
    @Test
    public void parameterInEnglish() {
        knowledge.getParameters().forEach((param) -> {
            boolean inEnglish = false;
            for (final LocalizedString description : param.getDescriptions()) {
                if ("en".equals(description.getLang())) {
                    if (description.getValue() == null
                            || ".".equals(description.getValue())) {
                        continue;
                    }
                    inEnglish = true;
                    break;
                }
            }
            assertTrue("English description must be set to " + param.getId(),
                    inEnglish);
        });
    }

    /**
     * French description must be set for all parameters.
     */
    @Test
    public void parameterInFrench() {
        knowledge.getParameters().forEach((param) -> {
            boolean inFrench = false;
            for (final LocalizedString description : param.getDescriptions()) {
                if ("fr".equals(description.getLang())) {
                    if (description.getValue() == null
                            || ".".equals(description.getValue())) {
                        continue;
                    }
                    inFrench = true;
                    break;
                }
            }
            assertTrue("French description must be set to " + param.getId(),
                    inFrench);
        });
    }

    /**
     * Test that setI18n update well.
     */
    @Test
    public void setI18n() {
        final String indicatorId = "dryfreq";
        final Indicator indicator = knowledge.getIndicator(indicatorId);
        assertNotNull("An indicator with id \"" + indicatorId
                + "\" must be found!", indicator);
        final String languageCode = "fr";
        final Indicator test = new NumberOfWaves();
        test.setId(indicatorId);
        knowledge.setI18n(test);
        assertEquals(indicator.getName(), test.getName());
        assertEquals(indicator.getDescription(languageCode),
                test.getDescription(languageCode));
    }

    /**
     * Assert that IDs of indicators are unique.
     */
    @Test
    public void uniqueIndicatorsIdAndNames() {
        assertNotNull("Loading XML file from Knowledge.load() should work!", knowledge);
        assertNoDuplicates("climatic effects", knowledge.getIndicators());
        assertNoDuplicates("cultural practices",
                knowledge.getCulturalPractices());
        assertNoDuplicates("ecophysiological processes",
                knowledge.getEcophysiologicalProcesses());
        final List<CompositeIndicator> allIndicators = new ArrayList<>();
        allIndicators.addAll(knowledge.getIndicators());
        allIndicators.addAll(knowledge.getCulturalPractices());
        allIndicators.addAll(knowledge.getEcophysiologicalProcesses());
        assertNoDuplicates("all", allIndicators);
    }
}
