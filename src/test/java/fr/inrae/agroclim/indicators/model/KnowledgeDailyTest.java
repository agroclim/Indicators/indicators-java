/*
 * This file is part of Indicators.
 *
 * Indicators is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Indicators is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Indicators. If not, see <https://www.gnu.org/licenses/>.
 */
package fr.inrae.agroclim.indicators.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.junit.BeforeClass;
import org.junit.Test;

import fr.inrae.agroclim.indicators.exception.IndicatorsException;
import fr.inrae.agroclim.indicators.model.data.Variable;
import fr.inrae.agroclim.indicators.model.indicator.CompositeIndicator;
import fr.inrae.agroclim.indicators.model.indicator.Indicator;
import fr.inrae.agroclim.indicators.model.indicator.Min;

/**
 * Test the class vs the XML file.
 *
 * Last change $Date$
 *
 * @author $Author$
 * @version $Revision$
 */
public final class KnowledgeDailyTest {

    /**
     * Knowledge loaded from XML.
     */
    private static Knowledge knowledge;

    @BeforeClass
    public static void loadXml() {
        try {
            knowledge = Knowledge.load(TimeScale.DAILY);
        } catch (final IndicatorsException ex) {
            fail("Loading XML file from Knowledge.load() should work!");
        }
    }

    /**
     * Test cloning indicators.
     * @throws CloneNotSupportedException should never be thrown
     */
    @Test
    public void cloneTest() throws CloneNotSupportedException {
        assertTrue("Loading XML file from Knowledge.load() should work!", knowledge != null);
        for (final CompositeIndicator comp : knowledge.getIndicators()) {
            for (final Indicator ind : comp.getIndicators()) {
                ind.clone();
            }
        }
    }

    /**
     * Test description localisation.
     */
    @Test
    public void description() {
        assertTrue("Loading XML file from Knowledge.load() should work!", knowledge != null);
        Indicator ranget = null;
        for (final CompositeIndicator comp : knowledge.getIndicators()) {
            for (final Indicator ind : comp.getIndicators()) {
                if ("ranget".equals(ind.getId())) {
                    ranget = ind;
                }
            }
        }
        assertNotNull("ranget should exist in climatic effects!", ranget);
        assertTrue(ranget.getDescriptions() != null);
        assertEquals(2, ranget.getDescriptions().size());
        assertEquals("There is not any Esperanto description", "Average Thermal amplitude for each period.",
                ranget.getDescription("eo"));
        assertEquals("There should be a description in English.", "Average Thermal amplitude for each period.",
                ranget.getDescription("en"));
        assertEquals("There should be a description in French.",
                "Amplitude thermique journalière moyenne pour chaque phase de développement.",
                ranget.getDescription("fr"));
    }

    /**
     * Check "mintmin" indicator.
     */
    @Test
    public void getMintmin() {
        final String indicatorId = "mintmin";
        final Indicator indicator = knowledge.getIndicator(indicatorId);
        assertNotNull("The daily indicator must be found: " + indicatorId, indicator);
        assertEquals(indicatorId + " must be Min", Min.class, indicator.getClass());
        final Min min = (Min) indicator;
        assertNotNull(indicatorId + " must have NonNull Criteria", min.getCriteria());
        assertTrue(indicatorId + " must have no parameters", min.getParameters().isEmpty());
        assertEquals(indicatorId + " must have 1 variable", 1, min.getVariables().size());
        assertEquals(indicatorId + " must have variable as TMIN", Variable.TMIN, min.getVariables().iterator().next());
    }

    /**
     * Check that getNextIndicator() works.
     */
    @Test
    public void getNextIndicators() {
        String indicatorId = "growth";
        CompositeIndicator indicator;
        indicator = (CompositeIndicator) knowledge.getIndicator(indicatorId);
        assertNotNull("An indicator with id \"" + indicatorId + "\" must be found!", indicator);
        List<? extends Indicator> next = knowledge.getNextIndicators(indicator);
        assertFalse(indicatorId + " do have child indicators.", next.isEmpty());
        assertTrue(indicatorId + " do have many child indicators, found " + next.size(), next.size() > 5);

        indicatorId = "cold";
        indicator = (CompositeIndicator) knowledge.getIndicator(indicatorId);
        next = knowledge.getNextIndicators(indicator);
        assertFalse(indicatorId + " do have child indicators.", next.isEmpty());
        assertTrue(indicatorId + " do have many child indicators, found " + next.size(), next.size() > 10);

        indicatorId = "mort";
        indicator = (CompositeIndicator) knowledge.getIndicator(indicatorId);
        next = knowledge.getNextIndicators(indicator);
        assertFalse(indicatorId + " do have child indicators.", next.isEmpty());
        assertTrue(indicatorId + " do have many child indicators, found " + next.size(), next.size() > 1);
    }

    /**
     * verndays must return tMinVern + tMaxVern.
     */
    @Test
    public void getParametersNumberOfDays() {
        final String indicatorId = "verndays";
        final Indicator indicator = knowledge.getIndicator(indicatorId);
        assertNotNull("An indicator with id \"" + indicatorId + "\" must be found!", indicator);
        final List<String> expected = Arrays.asList("tMinVern", "tMaxVern");
        final List<String> actual = indicator.getParameters().stream().map((p) -> p.getId())
                .collect(Collectors.toList());
        assertEquals(expected, actual);
    }

    /**
     * verndays must return tMinVern + tMaxVern.
     *
     * @throws java.lang.CloneNotSupportedException
     *             should never occur
     */
    @Test
    public void getParametersValuesNumberOfDays() throws CloneNotSupportedException {
        final String indicatorId = "vernfreq";
        final Indicator indicator = knowledge.getIndicator(indicatorId).clone();
        assertNotNull("An indicator with id \"" + indicatorId + "\" must be found!", indicator);
        Map<String, Double> expected = new HashMap<>();
        expected.put("tMinVern", 5.);
        expected.put("tMaxVern", 15.);
        Map<String, Double> actual = indicator.getParametersValues();
        assertEquals(expected, actual);

        expected = new HashMap<>();
        expected.put("tMinVern", 1.);
        expected.put("tMaxVern", 11.);
        indicator.setParametersValues(expected);
        actual = indicator.getParametersValues();
        assertEquals(expected, actual);
    }

    /**
     * verndays description where tMinVern and tMaxVern are replaced.
     *
     * @throws java.lang.CloneNotSupportedException
     *             should never occur
     */
    @Test
    public void getPrettyDescription() throws CloneNotSupportedException {
        final String indicatorId = "verndays";
        final Indicator indicator = knowledge.getIndicator(indicatorId).clone();
        assertNotNull("An indicator with id \"" + indicatorId + "\" must be found!", indicator);
        String expected = "Nombre de jours pour lesquels 0 °C < tmoy < 10 °C.";
        String actual = indicator.getPrettyDescription("fr");
        assertEquals(expected, actual);

        final Map<String, Double> values = new HashMap<>();
        values.put("tMinVern", 0.1);
        values.put("tMaxVern", 10.1);
        indicator.setParametersValues(values);
        expected = "Nombre de jours pour lesquels 0,1 °C < tmoy < 10,1 °C.";
        actual = indicator.getPrettyDescription("fr");
        assertEquals(expected, actual);

        expected = "Number of days when 0.1 °C < tmean < 10.1 °C.";
        actual = indicator.getPrettyDescription("en");
        assertEquals(expected, actual);
    }

    /**
     * hsdays description where tMinVern and tMaxVern are replaced.
     *
     * @throws java.lang.CloneNotSupportedException
     *             should never occur
     */
    @Test
    public void getPrettyDescriptionHsdays() throws CloneNotSupportedException {
        final String indicatorId = "hsdays";
        final Indicator indicator = knowledge.getIndicator(indicatorId).clone();
        assertNotNull("An indicator with id \"" + indicatorId + "\" must be found!", indicator);
        String expected = "Nombre de jours de stress thermique (tmax > 35 °C).";
        String actual = indicator.getPrettyDescription("fr");
        assertEquals(expected, actual);

        final Map<String, Double> values = new HashMap<>();
        values.put("Theat", 10.1);
        indicator.setParametersValues(values);
        expected = "Nombre de jours de stress thermique (tmax > 10,1 °C).";
        actual = indicator.getPrettyDescription("fr");
        assertEquals(expected, actual);

        expected = "Number of heat stress days (tmax > 10.1 °C).";
        actual = indicator.getPrettyDescription("en");
        assertEquals(expected, actual);
    }

    /**
     * photothermal quotient must return radiation + tmean.
     */
    @Test
    public void getVariablesQuotient() {
        final String indicatorId = "photothermalquotient";
        final Indicator indicator = knowledge.getIndicator(indicatorId);
        assertNotNull("An indicator with id \"" + indicatorId + "\" must be found!", indicator);
        final Set<Variable> expected = new HashSet<>();
        expected.add(Variable.RADIATION);
        expected.add(Variable.TMEAN);
        final Set<Variable> actual = indicator.getVariables();
        assertEquals(expected, actual);
    }

    /**
     * rain must return rain.
     */
    @Test
    public void getVariablesSum() {
        final String indicatorId = "rainsum";
        final Indicator indicator = knowledge.getIndicator(indicatorId);
        assertNotNull("An indicator with id \"" + indicatorId + "\" must be found!", indicator);
        final Set<Variable> expected = new HashSet<>();
        expected.add(Variable.RAIN);
        final Set<Variable> actual = indicator.getVariables();
        assertEquals(expected, actual);
    }

    /**
     * 2 parameters are set for indicator verndays.
     */
    @Test
    public void parametersForVerndays() {
        assertTrue("Loading XML file from Knowledge.load() should work!", knowledge != null);
        Indicator verndays = null;
        for (final CompositeIndicator comp : knowledge.getIndicators()) {
            for (final Indicator ind : comp.getIndicators()) {
                if ("verndays".equals(ind.getId())) {
                    verndays = ind;
                }
            }
        }
        assertTrue("ranget should exist in climatic effects!", verndays != null);
    }

}
