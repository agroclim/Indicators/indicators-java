/*
 * This file is part of Indicators.
 *
 * Indicators is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Indicators is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Indicators. If not, see <https://www.gnu.org/licenses/>.
 */
package fr.inrae.agroclim.indicators.model.data.phenology;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.util.List;

import org.junit.Test;

import fr.inrae.agroclim.indicators.exception.IndicatorsException;
import fr.inrae.agroclim.indicators.model.EvaluationSettings;
import fr.inrae.agroclim.indicators.model.data.DataTestHelper;
import fr.inrae.agroclim.indicators.xml.XMLUtil;

/**
 * Test Phenology loader class.
 * *
 * Last changed : $Date$
 *
 * @author $Author$
 * @version $Revision$
 */
public final class PhenologyLoaderTest extends DataTestHelper {
    /**
     * Test loading test-evaluation.xml.
     */
    @Test
    public void load() {
        String error = null;
        File xmlFile = getEvaluationTestFile();
        try {
            EvaluationSettings e = (EvaluationSettings) XMLUtil.load(
                    xmlFile, EvaluationSettings.CLASSES_FOR_JAXB);
            assertTrue("evaluationSettings must not be null", e != null);
            e.setFilePath(xmlFile.getAbsolutePath());
            assertTrue("evaluationSettings.phenology must not be null",
                    e.getPhenologyLoader() != null);
            assertTrue("evaluationSettings.phenology.file must not be null", e
                    .getPhenologyLoader().getFile() != null);
            assertNotNull("evaluationSettings.phenology.file.path must not be "
                    + "null", e.getPhenologyLoader().getFile().getPath());
            List<AnnualStageData> data = e.getPhenologyLoader().load();
            assertTrue("loaded data must not be null", data != null);
            assertTrue("loaded data must not be empty", !data.isEmpty());
        } catch (final IndicatorsException ex) {
            error = ex.getMessage() + " " + ex.getCause().getMessage();
        }
        assertTrue(error, error == null);
    }
}
