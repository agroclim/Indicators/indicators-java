package fr.inrae.agroclim.indicators.model.indicator;

import static junit.framework.TestCase.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import fr.inrae.agroclim.indicators.exception.IndicatorsException;
import fr.inrae.agroclim.indicators.model.criteria.Criteria;
import fr.inrae.agroclim.indicators.model.criteria.RelationalOperator;
import fr.inrae.agroclim.indicators.model.criteria.SimpleCriteria;
import fr.inrae.agroclim.indicators.model.data.DataTestHelper;
import fr.inrae.agroclim.indicators.model.data.climate.ClimaticResource;

/**
 * Test "maximum wave duration" indicator.
 *
 * Last changed : $Date$
 *
 * @author $Author$
 * @version $Revision$
 */
public class MaxWaveLengthTest extends DataTestHelper {
    /**
     * climate-2015.csv.
     */
    private final ClimaticResource climaticData;


    /**
     * Constructor.
     */
    public MaxWaveLengthTest() {
        climaticData = new ClimaticResource();
        climaticData.setData(getClimatic2015Data());
    }

    /**
     * Test of compute method.
     */
    @Test
    public void computeSingleValueFullYear() {
        // tmax > -20°C => full year.
        final Criteria criteria = new SimpleCriteria("tmax", RelationalOperator.GT, -20);
        final MaxWaveLength instance = new MaxWaveLength();
        instance.setCriteria(criteria);
        final int nbDays = 7;
        instance.setThreshold(nbDays);

        final double expected = climaticData.getData().size();
        double result = -1;
        String error = null;
        try {
            result = instance.computeSingleValue(climaticData);
        } catch (final IndicatorsException ex) {
            error = ex.getClass().getCanonicalName() + " : " + ex.getMessage();
        }
        assertTrue(error, error == null);
        assertEquals(expected, result, 0.0);
    }

    /**
     * Test of compute method.
     */
    @Test
    public void computeSingleValueSomeDays() {
        // tmax > 20°C => some days.
        final Criteria criteria = new SimpleCriteria("tmax", RelationalOperator.GT, 20);
        final MaxWaveLength instance = new MaxWaveLength();
        instance.setCriteria(criteria);
        final int nbDays = 0;
        instance.setThreshold(nbDays);

        final double expected = 124.;
        double result = -1;
        String error = null;
        try {
            result = instance.computeSingleValue(climaticData);
        } catch (final IndicatorsException ex) {
            error = ex.getClass().getCanonicalName() + " : " + ex.getMessage();
        }
        assertTrue(error, error == null);
        assertEquals(expected, result, 0.0);
    }

    /**
     * Without criteria: raise exception.
     * @throws IndicatorsException expected exception while compute
     */
    @Test(expected = IndicatorsException.class)
    public void withoutCriteria() throws IndicatorsException {
        final MaxWaveLength instance = new MaxWaveLength();
        instance.computeSingleValue(climaticData);
    }
}
