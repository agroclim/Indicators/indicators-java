/*
 * This file is part of Indicators.
 *
 * Indicators is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Indicators is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Indicators. If not, see <https://www.gnu.org/licenses/>.
 */
package fr.inrae.agroclim.indicators.model.data.climate;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;

import fr.inrae.agroclim.indicators.model.data.DataTestHelper;



/**
 * Test ETP calculator from ClimaticDailyData.
 *
 * @author Olivier Maury
 */
public final class EtpPenmanMonteithFAOTest extends DataTestHelper {

    /**
     * Calculator for wind at 10m.
     */
    private final EtpPenmanMonteithFAO calculatorWind10m = new EtpPenmanMonteithFAO(
            true);
    /**
     * Calculator for wind at 2m.
     */
    private final EtpPenmanMonteithFAO calculatorWind2m = new EtpPenmanMonteithFAO(
            false);

    /**
     * Data from CLIMATIK.
     */
    @Test
    public void avignon() {
        /**
         * Climatic daily data used to compute data.
         */
        List<ClimaticDailyData> climaticDailyData = getClimatic2015Data();

        for (ClimaticDailyData data : climaticDailyData) {

        	// #7012: ETP FAO method expects radiation in MJ/m2 (W/m2  -> MJ/m2)
        	double radiation = data.getRadiation() * 0.0864;
        	data.setRadiation(radiation);

            Double expected = data.getEtp();
            data.setEtp(null);
            Double actual = calculatorWind2m.compute(data);
            assertEquals("Wrong for day " + data.getDay(), expected, actual,
                    0.000001);
            data.setEtpCalculator(calculatorWind2m);
            Double actual2 = data.getEtp();
            assertEquals(
                    "Computed ETP from calculator or from data must be the same!",
                    actual, actual2);
        }
    }

    /**
     * Compare computed values from R code.
     */
    @Test
    public void compareWithRResults() {
        ClimaticDailyData data = new ClimaticDailyData();
        data.setTmean(10.4);

        // #7012: radiation retrieved from climatic db, and expected by ETP FAO = MJ/m2
        // 23.73 W/m2
        data.setRadiation(23.73 * 60 * 60 * 24 /1000 / 1000);

        data.setRh(88.);
        data.setWind(1.);
        Double expected, actual;
        // 2 m
        expected = 0.4964845;
        actual = calculatorWind2m.compute(data);
        assertEquals("Compute at 2m", expected, actual, 0.0000001);
        // 10 m
        expected = 0.4588926;
        actual = calculatorWind10m.compute(data);
        assertEquals("Compute at 10m", expected, actual, 0.0000001);
    }

    @Test
    public void missingRadiation() {
        ClimaticDailyData data = new ClimaticDailyData();
        data.setTmean(10.4);
        data.setRadiation(null);
        data.setRh(88.);
        data.setWind(1.);
        boolean exception = false;
        try {
            calculatorWind2m.compute(data);
        } catch (IllegalArgumentException e) {
            assertEquals("error.radiation.null", e.getMessage());
            exception = true;
        }
        assertTrue(exception);
    }

    @Test
    public void missingRh() {
        ClimaticDailyData data = new ClimaticDailyData();
        data.setTmean(10.4);
        data.setRadiation(23.73);
        data.setRh(null);
        data.setWind(1.);
        boolean exception = false;
        try {
            calculatorWind2m.compute(data);
        } catch (IllegalArgumentException e) {
            assertEquals("error.rh.null", e.getMessage());
            exception = true;
        }
        assertTrue(exception);
    }

    @Test
    public void missingWind() {
        ClimaticDailyData data = new ClimaticDailyData();
        data.setTmean(10.4);
        data.setRadiation(23.73);
        data.setRh(88.);
        data.setWind(null);
        boolean exception = false;
        try {
            calculatorWind2m.compute(data);
        } catch (IllegalArgumentException e) {
            assertEquals("error.wind.null", e.getMessage());
            exception = true;
        }
        assertTrue(exception);
    }
}
