/*
 * Copyright (C) 2020 INRAE AgroClim
 *
 * This file is part of Indicators.
 *
 * Indicators is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Indicators is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Indicators. If not, see <http://www.gnu.org/licenses/>.
 */

package fr.inrae.agroclim.indicators.model;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.io.File;

import org.junit.Before;
import org.junit.Test;

import fr.inrae.agroclim.indicators.exception.IndicatorsException;
import fr.inrae.agroclim.indicators.model.data.DataTestHelper;
import lombok.extern.log4j.Log4j2;

/**
 * Test that evaluation with cultural practices works.
 *
 * Last change $Date$
 *
 * @author Olivier Maury
 * @author $Author$
 * @version $Revision$
 */
@Log4j2
public class CulturalPracticesTest extends DataTestHelper {

    /**
     * Evaluation from XML file.
     */
    private Evaluation evaluation;

    /**
     * Set up Evaluation to test.
     */
    @Before
    public void beforeTest() {
        final File xmlFile = getFile("xml/cultural-practices.gri");
        evaluation = getEvaluation(xmlFile);
    }

    /**
     * Check if evaluation is isComputable.
     */
    @Test
    public void computable() {
        assertNotNull("Evaluation must not be null!", evaluation);
        boolean found = evaluation.isComputable();
        assertTrue("evaluation must be computable!", found);
    }

    @Test
    public void compute() {
    	assertNotNull("Evaluation must not be null!", evaluation);
        String error = null;
        try {
            evaluation.initializeResources();
            long start = System.currentTimeMillis();
            evaluation.compute();
            LOGGER.info("Evaluation.compute() last {} seconds", (System.currentTimeMillis() - start) / 1000.);
        } catch (final IndicatorsException e) {
            LOGGER.catching(e);
            error = e.getClass() + " : " + e.getLocalizedMessage();
        }
        assertNull(error, error);
    }
}
