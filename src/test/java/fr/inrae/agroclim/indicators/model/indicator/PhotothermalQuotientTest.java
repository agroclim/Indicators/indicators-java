/*
 * Copyright (C) 2021 INRAE AgroClim
 *
 * This file is part of Indicators.
 *
 * Indicators is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Indicators is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Indicators. If not, see <http://www.gnu.org/licenses/>.
 */
package fr.inrae.agroclim.indicators.model.indicator;

import static junit.framework.TestCase.assertEquals;
import static org.junit.Assert.assertNotEquals;

import org.junit.Test;

import fr.inrae.agroclim.indicators.exception.IndicatorsException;
import fr.inrae.agroclim.indicators.model.criteria.RelationalOperator;
import fr.inrae.agroclim.indicators.model.criteria.SimpleCriteria;
import fr.inrae.agroclim.indicators.model.data.DataTestHelper;
import fr.inrae.agroclim.indicators.model.data.Variable;
import fr.inrae.agroclim.indicators.model.data.climate.ClimaticDailyData;
import fr.inrae.agroclim.indicators.model.data.climate.ClimaticResource;
import fr.inrae.agroclim.indicators.model.function.normalization.Sigmoid;

/**
 * Test photothermalquotient, #8258.
 *
 * Last changed : $Date$
 *
 * @author $Author$
 * @version $Revision$
 */
public class PhotothermalQuotientTest extends DataTestHelper {
    /**
     * climate-2015.csv.
     */
    private final ClimaticResource climaticData;

    /**
     * photothermalquotient indicator.
     */
    private final Quotient indicator;

    /**
     * Constructor.
     */
    public PhotothermalQuotientTest() {
        climaticData = new ClimaticResource();
        climaticData.setData(getClimatic2015Data());

        indicator = new Quotient();
        final Sum dividend = new Sum();
        final SimpleCriteria dividendCriteria = new SimpleCriteria();
        dividend.setCriteria(dividendCriteria);
        dividendCriteria.setOperator(RelationalOperator.GT);
        dividendCriteria.setThreshold(0);
        dividendCriteria.setVariable(Variable.RADIATION);
        indicator.setDividend(dividend);
        final Sum divisor = new Sum();
        final SimpleCriteria divisorCriteria = new SimpleCriteria();
        divisor.setCriteria(divisorCriteria);
        divisorCriteria.setOperator(RelationalOperator.GT);
        divisorCriteria.setThreshold(0);
        divisorCriteria.setVariable(Variable.TMEAN);
        indicator.setDivisor(divisor);
    }

    @Test
    public void computeSingleValue() throws IndicatorsException {
        final Double radiationSum = climaticData.getData().stream().map(ClimaticDailyData::getRadiation)
                .reduce(0d, Double::sum);
        final Double tmeanSum = climaticData.getData().stream().map(ClimaticDailyData::getTmean)
                .reduce(0d, Double::sum);
        final double expected = radiationSum / tmeanSum;
        assertNotEquals(1d, expected, 0.01);
        final double actual = indicator.computeSingleValue(climaticData);
        assertEquals(expected, actual, E);
    }

    @Test
    public void withNormalization() throws IndicatorsException {
        indicator.getDividend().setNormalizationFunction(new Sigmoid());
        indicator.getDivisor().setNormalizationFunction(new Sigmoid());
        final double actual = indicator.computeSingleValue(climaticData);
        assertEquals(1.0d, actual, 0.01);
    }
}
