/*
 * This file is part of Indicators.
 *
 * Indicators is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Indicators is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Indicators. If not, see <https://www.gnu.org/licenses/>.
 */
package fr.inrae.agroclim.indicators.model.data;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.stream.Collectors;

import fr.inrae.agroclim.indicators.exception.IndicatorsException;
import fr.inrae.agroclim.indicators.model.Evaluation;
import fr.inrae.agroclim.indicators.model.EvaluationSettings;
import fr.inrae.agroclim.indicators.model.data.climate.ClimateFileLoader;
import fr.inrae.agroclim.indicators.model.data.climate.ClimaticDailyData;
import fr.inrae.agroclim.indicators.model.data.climate.EtpCalculator;
import fr.inrae.agroclim.indicators.model.data.climate.EtpPenmanMonteithFAO;
import fr.inrae.agroclim.indicators.model.data.phenology.AnnualStageData;
import fr.inrae.agroclim.indicators.model.data.phenology.PhenologicalModelType;
import fr.inrae.agroclim.indicators.model.data.phenology.PhenologyCalculator;
import fr.inrae.agroclim.indicators.model.data.phenology.PhenologyFileLoader;
import fr.inrae.agroclim.indicators.model.data.phenology.Stage;
import fr.inrae.agroclim.indicators.model.data.soil.HasSoilCalculatorParams;
import fr.inrae.agroclim.indicators.xml.XMLUtil;
import lombok.extern.log4j.Log4j2;

/**
 * Commons methods to test data proxy and loaders.
 *
 * Last changed : $Date$
 *
 * @author $Author$
 * @version $Revision$
 */
@Log4j2
public abstract class DataTestHelper {

    /**
     * climate-1951.csv.
     */
    protected static final String CLIMATE_1951 = "climate-1951.csv";
    /**
     * Directory for data files for {@link PhenologicalCalculator}.
     */
    private static final String PHENO_DIR = "src/test/resources/fr/inrae/agroclim/indicators/model/data/phenology/";
    /**
     * Epsilon.
     */
    protected static final double E = 0.00001;
    /**
     * File for evaluation-test.xml.
     */
    private static volatile File evaluationTestFile;
    /**
     * File for evaluationwithnodata-test.xml.
     */
    private static volatile File evaluationWithNoDataFile;
    /**
     * File for dataWithNa.csv
     */
    private static volatile File climateWithNAFile;
    /**
     * File for phenoForNoData.csv
     */
    private static volatile File phenoSampleForNoData;

    /**
     * File for climate-2015.csv.
     */
    private static volatile File climate2015File;

    /**
     * File for climate-1951.csv.
     */
    private static volatile File climate1951File;

    /**
     * Data from climate-2015.csv.
     */
    private static volatile List<ClimaticDailyData> climaticDailyData;
    /**
     * File for pheno_sample.csv.
     */
    private static volatile File phenoSample;

    /**
     * ETP Penman Monteith formula at 2m.
     */
    protected static EtpCalculator etpCalculator = new EtpPenmanMonteithFAO(false);

    /**
     * @return File for climate-1951.csv
     */
    protected static File getClimate1951File() {
        if (climate1951File == null) {
            climate1951File = getFile("model/data/climate/" + CLIMATE_1951);
        }
        return climate1951File;
    }

    /**
     * @return Headers for climate-1950.csv
     */
    protected static String[] getClimate1951Headers() {
        final String[] headers = { "year", "month", "day", "tmin", "tmax",
                "tmean", "radiation", "rain", "rh", "wind", "etp" };
        return headers;
    }

    /**
     * @return Column separator for climate-1951.csv
     */
    protected static String getClimate1951Separator() {
        return ",";
    }

    /**
     * @return File for climate-2015.csv
     */
    protected static File getClimate2015File() {
        if (climate2015File == null) {
            climate2015File = getFile(getClimate2015Path());
        }
        return climate2015File;
    }

    /**
     * @return Headers for climate-2015.csv
     */
    protected static String[] getClimate2015Headers() {
        final String[] headers = { "NUM_POSTE", "year", "month", "day", "tmin",
                "tmax", "tmean", "radiation_j.cm.2", "radiation", "rain", "rh",
                "wind", "etp" };
        return headers;
    }

    /**
     * @return relative path for climate-2015.csv
     */
    protected static String getClimate2015Path() {
        return "model/data/climate/climate-2015.csv";
    }

    /**
     * @return Column separator for climate-2015.csv
     */
    protected static String getClimate2015Separator() {
        return "\t";
    }
    protected static File getClimateWithNAFile() {
        if (climateWithNAFile == null) {
            climateWithNAFile = getFile("model/data/climate/climate-dataWithNa.csv");
        }
        return climateWithNAFile;
    }
    /**
     * @return Data from climate-2015.csv.
     */
    protected static List<ClimaticDailyData> getClimatic2015Data() {
        if (climaticDailyData == null) {
            climaticDailyData = getClimaticData(
                    "climate-2015.csv",
                    getClimate2015Separator(),
                    getClimate2015Headers());
        }
        return Collections.unmodifiableList(climaticDailyData);
    }
    /**
     *
     * @param fileName name of data file
     * @param separator CSV separator
     * @param headers user defined headers
     * @return data from file.
     */
    protected static List<ClimaticDailyData> getClimaticData(
            final String fileName, final String separator,
            final String[] headers) {
        final File climate = getFile("model/data/climate/" + fileName);
        final ClimateFileLoader fileLoader = new ClimateFileLoader();
        fileLoader.setFile(climate);
        fileLoader.setSeparator(separator);
        fileLoader.setHeaders(headers);
        fileLoader.setEtpCalculator(etpCalculator);
        final List<ClimaticDailyData> data = fileLoader.load();
        return Collections.unmodifiableList(data);
    }

    /**
     * Get double value from properties.
     *
     * @param ini properties
     * @param key key
     * @return value
     */
    protected static double getDouble(final Properties ini, final String key) {
        final String val = ini.getProperty(key);
        if (val == null) {
            throw new RuntimeException("No key " + key
                    + " was found in ini file!");
        }
        return Double.parseDouble(val);
    }

    /**
     * @return relative path for empty.csv
     */
    protected static String getEmptyPath() {
        return "model/data/empty.csv";
    }

    /**
     * Load Evaluation from file and check configuration.
     *
     * @param file XML file
     * @return Evaluation
     */
    protected static Evaluation getEvaluation(final File file) {
        return getEvaluation(file, true);
    }

    /**
     * Load Evaluation from file and check configuration.
     *
     * @param file XML file
     * @param checkConfig if configuration must be checked
     * @return Evaluation
     */
    protected static Evaluation getEvaluation(final File file, final boolean checkConfig) {
        final EvaluationSettings settings;
        try {
            settings = (EvaluationSettings) XMLUtil.load(file, EvaluationSettings.CLASSES_FOR_JAXB);
            settings.initializeKnowledge();
            settings.setFilePath(file.getAbsolutePath());
        } catch (final IndicatorsException ex) {
            LOGGER.error(ex.getLocalizedMessage());
            return null;
        }
        if (checkConfig) {
            final Map<String, String> errors = settings.getConfigurationErrors();
            if (errors != null && !errors.isEmpty()) {
                LOGGER.error(errors.toString());
                return null;
            }
        }
        final Evaluation eval = new Evaluation(settings.getEvaluation());
        eval.setSettings(settings);
        eval.initializeParent();
        return eval;
    }

    /**
     * @return File for evaluation-test.xml
     */
    protected static File getEvaluationTestFile() {
        if (evaluationTestFile == null) {
            evaluationTestFile = getFile("xml/test-evaluation.xml");
        }
        return evaluationTestFile;
    }

    /**
     * @return File for evaluationwithnodata-test.xml
     */
    protected static File getEvaluationWithNoDataTestFile() {
        if (evaluationWithNoDataFile == null) {
            evaluationWithNoDataFile = getFile("xml/evaluationwithnodata-test.xml");
        }
        return evaluationWithNoDataFile;
    }
    /**
     * @param path relative path
     * @return File for path
     */
    protected static File getFile(final String path) {
        final URL resource = DataTestHelper.class.getResource(
                "/fr/inrae/agroclim/indicators/" + path);
        if (resource == null) {
            throw new RuntimeException(path + " not found!");
        }
        return new File(resource.getPath());
    }

    /**
     * Get integer value from properties.
     *
     * @param ini properties
     * @param key key
     * @return value
     */
    protected static int getInt(final Properties ini, final String key) {
        final String val = ini.getProperty(key);
        if (val == null) {
            throw new RuntimeException("No key " + key
                    + " was found in ini file!");
        }
        return Integer.parseInt(val);
    }

    /**
     * @return 2015 data from pheno_sample.csv
     */
    protected static List<AnnualStageData> getPheno2015Data() {
        return getPhenoSampleData().stream().filter(s -> s.getYear() == 2015).collect(Collectors.toList());
    }

    protected static PhenologyCalculator getPhenologyCalculator(final Properties ini) {
        final PhenologyCalculator calc = new PhenologyCalculator();
        final String modelType = ini.getProperty("model");
        final int nbOfStages = getInt(ini, "nbOfStages");
        calc.setCrop(ini.getProperty("crop"));
        calc.setModel(PhenologicalModelType.valueOf(modelType));
        calc.setNbOfStages(nbOfStages);
        switch (calc.getModel()) {
        case linear:
            calc.setBaseTemperature(getDouble(ini, "baseTemperature"));
            break;
        case curve:
            calc.setMaxTemperature(getDouble(ini, "maxTemperature"));
            calc.setMinTemperature(getDouble(ini, "minTemperature"));
            calc.setOptimalTemperature(getDouble(ini, "optimalTemperature"));
            break;
        case curve_grapevine:
        case curve_grapevine_sw:
            calc.setChuineA(getDouble(ini, "chuineA"));
            calc.setChuineB(getDouble(ini, "chuineB"));
            calc.setChuineC(getDouble(ini, "chuineC"));
            calc.setMaxTemperature(getDouble(ini, "maxTemperature"));
            calc.setMaxTemperatureDeb(getDouble(ini, "maxTemperatureDeb"));
            calc.setMaxTemperatureVer(getDouble(ini, "maxTemperatureVer"));
            calc.setMinTemperature(getDouble(ini, "minTemperature"));
            calc.setMinTemperatureDeb(getDouble(ini, "minTemperatureDeb"));
            calc.setMinTemperatureVer(getDouble(ini, "minTemperatureVer"));
            calc.setOptimalTemperature(getDouble(ini, "optimalTemperature"));
            calc.setOptimalTemperatureDeb(getDouble(ini, "optimalTemperatureDeb"));
            calc.setOptimalTemperatureVer(getDouble(ini, "optimalTemperatureVer"));
            calc.setS3ToS4(35);
            break;
        case richardson:
            calc.setBaseTemperature(getDouble(ini, "baseTemperature"));
            calc.setMaxTemperature(getDouble(ini, "maxTemperature"));
            break;
        default:
            final String msg = "Model type " + calc.getModel() + " not handled!";
            LOGGER.error(msg);
            throw new RuntimeException(msg);
        }
        calc.setNbOfYears(getInt(ini, "nbOfYears"));
        calc.setPhotoperiodToCompute("1".equals(ini.getProperty("isPhotoperiodToCompute")));
        if (calc.isPhotoperiodToCompute()) {
            calc.setPhotoperiodBase(getDouble(ini, "photoperiodBase"));
            calc.setPhotoperiodSaturating(getDouble(ini, "photoperiodSaturating"));
            calc.setPhotoperiodSensitivity(getDouble(ini, "photoperiodSensitivity"));
            calc.setSiteLatitude(getDouble(ini, "siteLatitude"));
        }
        calc.setVernalizationToCompute("1".equals(ini.get("isVernalizationToCompute")));
        if (calc.isVernalizationToCompute()) {
            calc.setMinNbOfVernalizationDays(getInt(ini, "minNbOfVernalizationDays"));
            calc.setNbOfVernalizationDays(getInt(ini, "nbOfVernalizationDays"));
            calc.setOptimalVernalizationAmplitude(getDouble(ini, "optimalVernalizationAmplitude"));
            calc.setOptimalVernalizationTemperature(getDouble(ini, "optimalVernalizationTemperature"));
        }
        calc.setPhase1Sum(getDouble(ini, "phase1Sum"));
        calc.setPhase2Sum(getDouble(ini, "phase2Sum"));
        calc.setPhase3Sum(getDouble(ini, "phase3Sum"));
        if (nbOfStages > Stage.THREE) {
            calc.setPhase4Sum(getDouble(ini, "phase4Sum"));
        }
        if (nbOfStages > Stage.FOUR) {
            calc.setPhase5Sum(getDouble(ini, "phase5Sum"));
        }
        if (nbOfStages > Stage.FIVE) {
            calc.setPhase6Sum(getDouble(ini, "phase6Sum"));
        }
        calc.setSowingDate(getInt(ini, "sowingDate"));
        calc.setVariety(ini.getProperty("variety"));
        return calc;
    }

    /**
     * @param baseName base name of file (without .ini)
     * @param phenologyCalculator the related calculator
     * @param startYear first year of period
     * @param endYear last year of period
     * @param etpCalculator calculator to use
     * @return list of climatic daily data
     */
    protected static List<ClimaticDailyData> getPhenologyClimaticDailyData(final String baseName,
            final PhenologyCalculator phenologyCalculator, final int startYear, final int endYear,
            final EtpCalculator etpCalculator) {
        final String csvFilePath = String.format("%s%s-meteo.csv", PHENO_DIR, baseName);
        final File climate = new File(csvFilePath);
        final String separator = getClimate1951Separator();
        final String[] headers = getClimate1951Headers();
        final ClimateFileLoader fileLoader = new ClimateFileLoader();
        fileLoader.setFile(climate);
        fileLoader.setSeparator(separator);
        fileLoader.setHeaders(headers);
        fileLoader.setEtpCalculator(etpCalculator);
        final List<ClimaticDailyData> all = fileLoader.load();
        final List<ClimaticDailyData> data = new ArrayList<>();
        LOGGER.trace("{}-{} ({})", startYear, endYear, endYear + phenologyCalculator.getNbOfYears() - 1);
        for (final ClimaticDailyData ddata : all) {
            if (ddata.getYear() >= startYear && ddata.getYear() <= endYear + phenologyCalculator.getNbOfYears() - 1) {
                data.add(ddata);
            }
        }
        return data;
    }

    /**
     * Load a properties file related to phenology.
     *
     * @param baseName base name of file (without .ini)
     * @return loaded properties
     * @throws FileNotFoundException should not occur
     * @throws IOException  should not occur
     */
    protected static Properties getPhenologyProperties(final String baseName)
            throws FileNotFoundException, IOException {
        final String iniFilePath = String.format("%s%s.ini", PHENO_DIR, baseName);
        final File iniF = new File(iniFilePath);
        if (!iniF.exists()) {
            throw new RuntimeException("The file " + iniFilePath + " does not exist");
        }
        final FileInputStream inStream = new FileInputStream(iniF);
        final Properties ini = new Properties();
        LOGGER.trace("Loading {}", iniFilePath);
        ini.load(inStream);
        return ini;
    }

    /**
     * @return data for pheno_sample.csv
     */
    protected static List<AnnualStageData> getPhenoSampleData() {
        final String[] headers = {"year", "s0", "s1", "s2", "s3", "s4"};
        final String csvSeparator = ";";
        final String csvFile = getPhenoSampleFile().getAbsolutePath();
        final PhenologyFileLoader loader = new PhenologyFileLoader(csvFile, headers, csvSeparator);
        return loader.load();
    }

    /**
     * @return File for pheno_sample.csv
     */
    public static File getPhenoSampleFile() {
        if (phenoSample == null) {
            phenoSample = getFile("model/data/phenology/pheno_sample.csv");
        }
        return phenoSample;
    }

    /**
     * @return relative path for Soil-1951.csv
     */
    protected static String getSoil1951Path() {
        return "model/data/soil/soil-1951.csv";
    }

    /**
     * @return Headers for soil-ok.csv
     */
    protected static String[] getSoilHeaders() {
        final String[] headers = { "year", "month", "day", "swc",
        "waterReserve" };
        return headers;
    }

    /**
     * @param errors
     *            errors from config.
     * @return formatted message with errors
     */
    protected static String helperErrors(final Map<String, String> errors) {
        if (errors == null) {
            return null;
        }
        final StringBuilder sb = new StringBuilder();
        for (final String key : errors.keySet()) {
            sb.append(key).append(": ").append(errors.get(key)).append("\n");
        }
        return sb.toString();
    }

    /**
     * File for phenoForNoData.csv
     */
    protected static File phenoSampleForNoData() {
        if (phenoSampleForNoData == null) {
            final String csvPath = "model/data/phenology/phenoForNoData.csv";
            phenoSampleForNoData = getFile(csvPath);
        }
        return phenoSampleForNoData;
    }

    /**
     * Define Kc, soilDepth, SWC.
     *
     * @param calc
     *            calculator
     */
    protected static void setSoilCalculatorParams(
            final HasSoilCalculatorParams calc) {
        // Cf. param_indecoclim_ble.csv
        calc.setKcIni(0.4);
        calc.setKcLate(0.3);
        calc.setKcMid(1.15);
        calc.setP(0.55);
        // Cf. param_soil_SWB.csv
        calc.setSoilDepth(140.);
        calc.setSwcFc(24.14);
        calc.setSwcMax(26.);
        calc.setSwcWp(8.);
    }

}
