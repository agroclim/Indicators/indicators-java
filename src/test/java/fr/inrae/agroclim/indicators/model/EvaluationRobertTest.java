/*
 * Copyright (C) 2022 INRAE AgroClim
 *
 * This file is part of Indicators.
 *
 * Indicators is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Indicators is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Indicators. If not, see <http://www.gnu.org/licenses/>.
 */
package fr.inrae.agroclim.indicators.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.File;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

import fr.inrae.agroclim.indicators.exception.IndicatorsException;
import fr.inrae.agroclim.indicators.model.data.DataTestHelper;
import fr.inrae.agroclim.indicators.model.data.climate.ClimaticResource;
import fr.inrae.agroclim.indicators.model.result.EvaluationResult;
import fr.inrae.agroclim.indicators.model.result.IndicatorResult;
import fr.inrae.agroclim.indicators.model.result.PhaseResult;

/**
 * Tests for #9718.
 */
public class EvaluationRobertTest extends DataTestHelper {

    private static double getResult(final Map<Integer, EvaluationResult> results, final int year) {
        final List<PhaseResult> phaseResults = results.get(year).getPhaseResults();
        assertNotNull(phaseResults);
        assertEquals(1, phaseResults.size());
        final PhaseResult phaseResult = phaseResults.get(0);
        assertNotNull(phaseResult);
        final List<IndicatorResult> ecoprocesses = phaseResult.getIndicatorResults();
        assertNotNull(ecoprocesses);
        assertEquals(1, ecoprocesses.size());
        final IndicatorResult ecoprocess = ecoprocesses.get(0);
        assertNotNull(ecoprocess);
        assertEquals("mort", ecoprocess.getIndicatorId());
        final IndicatorResult practices = ecoprocesses.get(0);
        final IndicatorResult climatik = practices.getIndicatorResults().get(0);
        final IndicatorResult indResult = climatik.getIndicatorResults().get(0);
        assertEquals("coldsumtmin", indResult.getIndicatorId());
        double actual = indResult.getRawValue();
        return actual;
    }

    /**
     * Evaluation to test.
     */
    private Evaluation evaluation;

    @Before
    public void beforeTest() {
        final File xmlFile = getFile("xml/evaluation-robert.gri");
        evaluation = getEvaluation(xmlFile);
    }

    /**
     * Check if evaluation computes.
     */
    @Test
    public void compute() {
        assertTrue("Evaluation must not be null!", evaluation != null);
        final Map<Integer, EvaluationResult> results;
        try {
            evaluation.initializeResources();
            results = evaluation.compute();
        } catch (final IndicatorsException e) {
            final String error = e.getClass() + " : " + e.getLocalizedMessage();
            fail(error);
            return;
        }

        assertNotNull(results);
        assertEquals(2, results.keySet().size());
        assertTrue(results.containsKey(1991));
        assertTrue(results.containsKey(1992));

        for (final int year : Arrays.asList(1991, 1992)) {
            ClimaticResource data = evaluation.getResourceManager() //
                    .getClimaticResource().getClimaticDataByPhaseAndYear(90, 91, year);
            double expected = data.getData().get(0).getTmin();

            double actual = getResult(results, year);
            assertEquals(expected, actual, 0.0001);
        }
    }

}
