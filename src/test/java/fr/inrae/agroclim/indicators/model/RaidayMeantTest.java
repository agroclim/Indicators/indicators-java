/*
 * This file is part of Indicators.
 *
 * Indicators is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Indicators is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Indicators. If not, see <https://www.gnu.org/licenses/>.
 */
package fr.inrae.agroclim.indicators.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.junit.Before;
import org.junit.Test;

import fr.inrae.agroclim.indicators.exception.IndicatorsException;
import fr.inrae.agroclim.indicators.model.criteria.RelationalOperator;
import fr.inrae.agroclim.indicators.model.criteria.SimpleCriteria;
import fr.inrae.agroclim.indicators.model.data.DataTestHelper;
import fr.inrae.agroclim.indicators.model.data.Variable;
import fr.inrae.agroclim.indicators.model.data.climate.ClimateFileLoader;
import fr.inrae.agroclim.indicators.model.data.climate.ClimaticDailyData;
import fr.inrae.agroclim.indicators.model.data.climate.EtpPenmanMonteithFAO;
import fr.inrae.agroclim.indicators.model.data.phenology.AnnualStageData;
import fr.inrae.agroclim.indicators.model.data.phenology.PhenologyCalculator;
import fr.inrae.agroclim.indicators.model.data.phenology.PhenologyCalculatorTest;
import fr.inrae.agroclim.indicators.model.data.phenology.PhenologyFileLoader;
import fr.inrae.agroclim.indicators.model.indicator.CompositeIndicator;
import fr.inrae.agroclim.indicators.model.indicator.NumberOfDays;
import fr.inrae.agroclim.indicators.model.result.EvaluationResult;
import fr.inrae.agroclim.indicators.model.result.IndicatorResult;
import fr.inrae.agroclim.indicators.model.result.PhaseResult;
import fr.inrae.agroclim.indicators.util.DateUtils;
import fr.inrae.agroclim.indicators.xml.XMLUtil;
import lombok.NonNull;
import lombok.extern.log4j.Log4j2;

/**
 * Test that raiday and meant computes well.
 */
@Log4j2
public class RaidayMeantTest extends DataTestHelper {

    private static final String DIR = "src/test/resources/fr/inrae/agroclim/indicators/model/data/";

    private static final SimpleDateFormat SDF;
    static {
        SDF = new SimpleDateFormat("yyyy-MM-dd");
        SDF.setTimeZone(TimeZone.getTimeZone("UTC"));
    }

    private static Map<String, Map<Integer, Map<String, Double>>> hardCodedComputation() {
        final List<String> phases = Arrays.asList("s0s1", "s1s2", "s2s3", "s3s4");
        // phaseId -> year -> indicatorId : rawValue
        final Map<String, Map<Integer, Map<String, Double>>> results = new HashMap<>();
        phases.forEach((phaseId) -> {
            results.put(phaseId, new HashMap<>());
            IntStream.rangeClosed(1959, 2017).forEach((year) -> {
                results.get(phaseId).put(year, new HashMap<>());
            });
        });
        final List<ClimaticDailyData> climate = loadClimate();
        final List<AnnualStageData> pheno = loadPheno();
        pheno.forEach((asData) -> {
            final int year = asData.getYear();
            phases.forEach((phaseId) -> {
                final String start = phaseId.substring(0, 2);
                final String end = phaseId.substring(2, 4);
                // due to before() and after()
                final Date startDate = DateUtils.getDate(year - 1, asData.getStageValue(start));
                final Date endDate = DateUtils.getDate(year - 1, asData.getStageValue(end));
                final List<ClimaticDailyData> phaseClimate = climate.stream()
                        .filter((data)
                                -> data.getDate().equals(startDate) || (data.getDate().after(startDate) && data.getDate().before(endDate)))
                        .collect(Collectors.toList());
                double val;
                val = phaseClimate.stream().mapToDouble(ClimaticDailyData::getTmean).average().getAsDouble();
                results.get(phaseId).get(year).put("meant", val);
                val = phaseClimate.stream().filter((data) -> data.getRain() > 0).count();
                results.get(phaseId).get(year).put("raidays", val);
            });
        });
        return results;
    }

    /**
     * @return climatic data from CSV file.
     */
    private static List<ClimaticDailyData> loadClimate() {
        final File file = new File(DIR + "climate/mmarjou_quotidiennes_1958_2017_avignon.csv");
        assertNotNull("climate file must exist!", file);
        assertTrue("climate file must exist!", file.exists());
        final ClimateFileLoader loader = new ClimateFileLoader();
        loader.setBaseDirectory(Paths.get("."));
        loader.setFile(file);
        final String[] headers = {"", "", "", "rain", "tmin", "tmax", "tmean",
                "radiation", "etp", "wind", "rh", "day", "month", "year", "",
        "solwatercontent"};
        loader.setEtpCalculator(new EtpPenmanMonteithFAO());
        loader.setHeaders(headers);
        loader.setSeparator(";");
        return loader.load();
    }

    /**
     * @return phenological data from CSV.
     */
    private static List<AnnualStageData> loadPheno() {
        final File file = new File(DIR + "phenology/mmarjou_stades_pheno_phases_simple_0604.csv");
        assertNotNull("pheno file must exist!", file);
        assertTrue("pheno file must exist! " + file.getAbsolutePath(), file.exists());
        final PhenologyFileLoader loader = new PhenologyFileLoader();
        loader.setBaseDirectory(Paths.get("."));
        loader.setFile(file);
        final String[] headers = {"year", "s0", "s1", "s2", "s3", "s4"};
        loader.setHeaders(headers);
        loader.setSeparator(";");
        return loader.load();
    }

    /**
     * @param date date to format
     * @return formatted date
     */
    private static String dateRepr(@NonNull final Date date) {
        return SDF.format(date);
    }

    /**
     * Format date.
     *
     * @param year year
     * @param doy day of year
     * @return formatted date
     */
    private static String dateRepr(final int year, final int doy) {
        final Date date = DateUtils.getDate(year, doy);
        return dateRepr(date);
    }

    /**
     * Evaluation from good XML file.
     */
    private Evaluation evaluation;

    /**
     * Set up Evaluation to test.
     */
    @Before
    public void beforeTest() {
        final File xmlFile = getFile("xml/raidays-meant.xml");
        EvaluationSettings settings;
        try {
            settings = (EvaluationSettings) XMLUtil.load(xmlFile,
                    EvaluationSettings.CLASSES_FOR_JAXB);
            settings.setFilePath(xmlFile.getAbsolutePath());
        } catch (final IndicatorsException ex) {
            LOGGER.error(ex.getLocalizedMessage());
            return;
        }
        final Map<String, String> errors = settings.getConfigurationErrors();
        if (errors != null && !errors.isEmpty()) {
            LOGGER.error(errors.toString());
            return;
        }
        evaluation = new Evaluation(settings.getEvaluation());
        evaluation.setSettings(settings);
    }

    /**
     * Check if evaluation is isComputable.
     */
    @Test
    public void computable() {
        assertTrue("Evaluation must not be null!", evaluation != null);
        final boolean found = evaluation.isComputable();
        assertTrue("evaluation must be computable!", found);
    }

    /**
     * Check if evaluation computes using a phenology calculator.
     */
    @Test
    public void computeUsingPhenologyCalculator() {
        assertTrue("Evaluation must not be null!", evaluation != null);
        final List<AnnualStageData> annualStageDatas;
        final Map<Integer, EvaluationResult> results;
        try {
            final EvaluationSettings settings = evaluation.getSettings();
            // wheat Soissons
            final PhenologyCalculatorTest test = new PhenologyCalculatorTest("pheno_linear_4_stages-soissons");
            final PhenologyCalculator calc = test.getCalc();
            calc.setClimaticDailyData(evaluation.getResourceManager().getClimaticResource().getData());
            // -
            settings.getPhenologyLoader().setAnnualStageBuilder(null);
            settings.getPhenologyLoader().setCalculator(calc);
            settings.getPhenologyLoader().setFile(null);
            evaluation.initializeResources();
            results = evaluation.compute();
            annualStageDatas = evaluation.getResourceManager().getPhenologicalResource().getData();
        } catch (final IndicatorsException | IOException e) {
            final String error = e.getClass() + " : " + e.getLocalizedMessage();
            fail(error);
            return;
        }

        // first year in phenology file
        final int firstYear = 1959;
        assertNotNull(annualStageDatas);
        assertFalse(annualStageDatas.isEmpty());
        assertTrue(firstYear == annualStageDatas.get(0).getYear());
        final List<AnnualPhase> computedPhases = results.values().stream() //
                .flatMap(r -> r.getPhaseResults().stream()) //
                .map(r -> r.getAnnualPhase()) //
                .toList();
        assertNotNull(computedPhases);
        assertFalse(computedPhases.isEmpty());
        assertTrue(firstYear == computedPhases.get(0).getHarvestYear());
        compareStagesAndPhases(annualStageDatas, computedPhases);

        assertFalse(results.isEmpty());
    }

    /**
     * Check if evaluation computes.
     */
    @Test
    public void compute() {
        assertTrue("Evaluation must not be null!", evaluation != null);
        final Map<Integer, EvaluationResult> results;
        try {
            evaluation.initializeResources();
            results = evaluation.compute();
        } catch (final IndicatorsException e) {
            final String error = e.getClass() + " : " + e.getLocalizedMessage();
            fail(error);
            return;
        }
        // extract stages to compute number of days
        final Map<Integer, Map<String, Integer>> stageDates = new HashMap<>();
        List<AnnualStageData> aSDatas;
        aSDatas = evaluation.getResourceManager().getPhenologicalResource().getData();
        aSDatas.forEach((aSData) -> {
            final Integer year = aSData.getYear();
            if (!stageDates.containsKey(year)) {
                stageDates.put(year, new HashMap<>());
            }
            aSData.getStages().forEach((stage) -> {
                stageDates.get(year).put(stage.getName(), stage.getValue());
            });
        });

        // check excraidays, hraidays and raidays
        assertFalse(results.isEmpty());
        final String fmt = "%d | %s %s : %d | %s=%.4f";
        for (final Map.Entry<Integer, EvaluationResult> entry : results.entrySet()) {
            final Integer year = entry.getKey();
            if (!stageDates.containsKey(year)) {
                LOGGER.trace("No stage date for year {}", year);
                continue;
            }
            final EvaluationResult result = entry.getValue();

            if (result == null) {
                LOGGER.warn("Strange, no value for the result!");
                continue;
            }
            for (final PhaseResult phase : result.getPhaseResults()) {
                List<IndicatorResult> processes;
                processes = phase.getIndicatorResults();
                if (processes == null) {
                    LOGGER.trace("Strange, result list for year {} is null!", year);
                    continue;
                }
                for (final IndicatorResult process : processes) {
                    for (final IndicatorResult ind : process.getIndicatorResults()) {
                        if (ind == null) {
                            LOGGER.trace("Strange indicatorResult is null!");
                            continue;
                        }
                        final String indicatorId = ind.getIndicatorId();
                        if ("raidays".equals(indicatorId)
                                || "excraidays".equals(indicatorId)
                                || "hraidays".equals(indicatorId)) {
                            final String end = phase.getAnnualPhase().getEndStage();
                            final String start = phase.getAnnualPhase().getStartStage();
                            final int duration = stageDates.get(year).get(end)
                                    - stageDates.get(year).get(start);
                            final String res = String.format(fmt, year, start, end,
                                    duration, indicatorId,
                                    ind.getRawValue());
                            assertTrue(res, duration >= ind.getRawValue());
                        }
                    }
                }
            }
        }

        assertNotNull(aSDatas);
        final List<AnnualPhase> computedPhases = results.values().stream() //
                .flatMap(r -> r.getPhaseResults().stream()) //
                .map(r -> r.getAnnualPhase()) //
                .toList();
        compareStagesAndPhases(aSDatas, computedPhases);

        final Map<String, Map<Integer, Map<String, Double>>> computation = new HashMap<>();

        results.forEach((year, r) -> {
            r.getPhaseResults().forEach((phase) -> {
                final String phaseId = phase.getPhaseId();
                if (!computation.containsKey(phaseId)) {
                    computation.put(phaseId, new HashMap<>());
                }
                computation.get(phaseId).put(year, new HashMap<>());
                phase.getIndicatorResults().forEach((processes) -> {
                    processes.getIndicatorResults().forEach((effect) -> {
                        effect.getIndicatorResults().forEach((ind) -> {
                            final String indId = ind.getIndicatorId();
                            if ("raidays".equals(indId) || "meant".equals(indId)) {
                                computation.get(phaseId).get(year).put(indId, ind.getRawValue());
                            }
                        });
                    });
                });
            });
        });

        final Map<String, Map<Integer, Map<String, Double>>> expected = hardCodedComputation();
        final double delta = 0.001;
        expected.keySet().forEach(phaseId -> {
            expected.get(phaseId).keySet().forEach(year -> {
                String ind;
                ind = "meant";
                assertEquals(phaseId + " " + year + " " + ind, expected.get(phaseId).get(year).get(ind), computation.get(phaseId).get(year).get(ind), delta);
                ind = "raidays";
                assertEquals(phaseId + " " + year + " " + ind, expected.get(phaseId).get(year).get(ind), computation.get(phaseId).get(year).get(ind), delta);
            });
        });

    }

    /**
     * @param annualStageDatas from PhenologyCalculator
     * @param computedPhases from Evaluation
     */
    private void compareStagesAndPhases(
            final List<AnnualStageData> annualStageDatas,
            final List<AnnualPhase> computedPhases) {
        // do nothing now
        final Map<Integer, Map<String, String>> stages = new HashMap<>();
        annualStageDatas.forEach((annual) -> {
            final int year = annual.getYear();
            stages.put(year, new HashMap<>());
            annual.getStages().forEach((stage) -> {
                int dateYear;
                if (annual.existWinterCrop()) {
                    dateYear = year - 1;
                } else {
                    dateYear = year;
                }
                if (stage.getValue() > 0) {
                    stages.get(year).put(stage.getName(), dateRepr(dateYear, stage.getValue()));
                }
            });
        });
        //
        final Map<Integer, Map<String, String>> phases = new HashMap<>();
        computedPhases.forEach((phase) -> {
            final String start = phase.getUid().substring(0, 2);
            final String end = phase.getUid().substring(2, 4);
            final Integer year = phase.getHarvestYear();
            if (!phases.containsKey(year)) {
                phases.put(year, new HashMap<>());
            }
            if (phase.getStart() != null) {
                phases.get(year).put(start, dateRepr(phase.getStart()));
            }
            if (phase.getEnd() != null) {
                phases.get(year).put(end, dateRepr(phase.getEnd()));
            }
        });

        stages.keySet().forEach((year) -> {
            assertEquals(stages.get(year), phases.get(year));
        });

    }

    /**
     * Check criteria of raidays is well set.
     */
    @Test
    public void criteria() {
        final CompositeIndicator eval = evaluation; //.getSettings().getEvaluation();
        int last = eval.getIndicators().size() - 1;
        final CompositeIndicator phase = (CompositeIndicator) eval.getIndicators().get(last);
        assertEquals("s3s4", phase.getId());
        last = phase.getIndicators().size() - 1;
        final CompositeIndicator ecoprocesses = (CompositeIndicator) phase.getIndicators().get(last);
        assertEquals("growth", ecoprocesses.getId());
        last = ecoprocesses.getIndicators().size() - 1;
        final CompositeIndicator watdef = (CompositeIndicator) ecoprocesses.getIndicators().get(last);
        assertEquals("watdef", watdef.getId());
        last = watdef.getIndicators().size() - 1;
        final NumberOfDays raidays = (NumberOfDays) watdef.getIndicators().get(last);
        assertEquals("raidays", raidays.getId());
        final SimpleCriteria criteria = (SimpleCriteria) raidays.getCriteria();
        assertEquals(Variable.RAIN, criteria.getVariable());
        assertEquals(RelationalOperator.GT, criteria.getOperator());
        assertEquals(0.0, criteria.getThreshold(), 0.01);
    }
}
