package fr.inrae.agroclim.indicators.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.File;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import fr.inrae.agroclim.indicators.model.data.DataTestHelper;
import fr.inrae.agroclim.indicators.util.Doublet;

/**
 * Test that {@link Parameter} are well read in an evaluation.
 */
public class EvaluationParametersTest extends DataTestHelper {
    /**
     * Evaluation to test.
     */
    private Evaluation evaluation;

    @Before
    public void beforeTest() {
        final File xmlFile = getFile("xml/cultural-practices.gri");
        evaluation = getEvaluation(xmlFile);
    }

    @Test
    public void getParameterDefaults() {
        List<Doublet<Parameter, Number>> defaults = evaluation.getParameterDefaults();
        assertNotNull(defaults);
        assertEquals(1, defaults.size());
        assertEquals("threshold", defaults.get(0).first().getAttribute());
        assertEquals(35., (Double) defaults.get(0).second().doubleValue(), 0.001);
    }
}
