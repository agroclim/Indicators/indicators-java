package fr.inrae.agroclim.indicators.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.File;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

import fr.inrae.agroclim.indicators.exception.IndicatorsException;
import fr.inrae.agroclim.indicators.model.data.DataTestHelper;
import fr.inrae.agroclim.indicators.model.result.EvaluationResult;
import fr.inrae.agroclim.indicators.model.result.IndicatorResult;
import fr.inrae.agroclim.indicators.model.result.PhaseResult;

/**
 * Test for Evaluation at hourly timescale.
 *
 * Last change $Date$
 *
 * @author $Author$
 * @version $Revision$
 */
public class EvaluationHourlyTest extends DataTestHelper {
    /**
     * Evaluation to test.
     */
    private Evaluation evaluation;

    @Before
    public void beforeTest() {
        final File xmlFile = getFile("xml/test-evaluation-hourly.xml");
        evaluation = getEvaluation(xmlFile);
    }
    /**
     * Check if evaluation is isComputable.
     */
    @Test
    public void computable() {
        assertTrue("Evaluation must not be null!", evaluation != null);
        final boolean found = evaluation.isComputable();
        assertTrue("evaluation must be computable!", found);
    }
    /**
     * Check if evaluation computes.
     */
    @Test
    public void compute() {
        assertTrue("Evaluation must not be null!", evaluation != null);
        final Map<Integer, EvaluationResult> results;
        try {
            evaluation.initializeResources();
            results = evaluation.compute();
        } catch (final IndicatorsException e) {
            final String error = e.getClass() + " : " + e.getLocalizedMessage();
            fail(error);
            return;
        }

        assertNotNull(results);
        assertEquals(1, results.keySet().size());
        assertTrue(results.containsKey(2015));

        final List<PhaseResult> phaseResults = results.get(2015).getPhaseResults();
        assertNotNull(phaseResults);
        assertEquals(1, phaseResults.size());
        final PhaseResult phaseResult = phaseResults.get(0);
        assertNotNull(phaseResult);
        final List<IndicatorResult> ecoprocesses = phaseResult.getIndicatorResults();
        assertNotNull(ecoprocesses);
        assertEquals(1, ecoprocesses.size());
        final IndicatorResult ecoprocess = ecoprocesses.get(0);
        assertNotNull(ecoprocess);
        assertEquals("mort", ecoprocess.getIndicatorId());
        final IndicatorResult practices = ecoprocesses.get(0);
        final IndicatorResult climatik = practices.getIndicatorResults().get(0);
        final IndicatorResult indResult = climatik.getIndicatorResults().get(0);
        assertEquals("coldhours", indResult.getIndicatorId());
        assertEquals(7., indResult.getRawValue(), 0.0001);
    }

    @Test
    public void getTimeScale() {
        assertEquals(TimeScale.HOURLY, evaluation.getTimescale());
    }
}
