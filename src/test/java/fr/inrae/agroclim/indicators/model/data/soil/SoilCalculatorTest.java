/*
 * This file is part of Indicators.
 *
 * Indicators is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Indicators is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Indicators. If not, see <https://www.gnu.org/licenses/>.
 */
package fr.inrae.agroclim.indicators.model.data.soil;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Properties;
import java.util.TimeZone;

import org.junit.Before;
import org.junit.Test;

import fr.inrae.agroclim.indicators.exception.ErrorMessage;
import fr.inrae.agroclim.indicators.exception.type.ResourceErrorType;
import fr.inrae.agroclim.indicators.model.Evaluation;
import fr.inrae.agroclim.indicators.model.EvaluationSettings;
import fr.inrae.agroclim.indicators.model.data.DataTestHelper;
import fr.inrae.agroclim.indicators.model.data.ResourceManager;
import fr.inrae.agroclim.indicators.model.data.Variable;
import fr.inrae.agroclim.indicators.model.data.climate.ClimaticDailyData;
import fr.inrae.agroclim.indicators.util.DateUtils;

/**
 * Test Soil data calculator from ClimaticDailyData.
 *
 * @author Olivier Maury
 */
public final class SoilCalculatorTest extends DataTestHelper {

    /**
     * @param year
     *            YYYY
     * @param doy
     *            day of year
     * @return Date date for the parameters
     */
    private static Date helperDate(final int year, final int doy) {
        final Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        cal.set(Calendar.YEAR, year);
        cal.set(Calendar.DAY_OF_YEAR, doy);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return cal.getTime();
    }

    /**
     * @param year
     *            YYYY
     * @param month
     *            MM (1-12)
     * @param day
     *            DD (1-31)
     * @return Date date for the parameters
     */
    private static Date helperDate(final int year, final int month,
            final int day) {
        final Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        cal.set(year, month - 1, day);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return cal.getTime();
    }

    /**
     * Define parameters with values from R test code.
     *
     * @param calc
     *            calculator to set
     */
    private static void setReferenceParams(final SoilCalculator calc) {
        final double kcIni = 0.300000;
        final double kcLate = 0.450000;
        final double kcMid = 0.700000;
        final double p = 0.450000;
        final double soilDepth = 140.000000;
        final double ru = 226.000000;
        final double swcFc = 19.500000;
        final double swcWp = 7.300000;
        final double swcMax = 25.000000;
        calc.setKcIni(kcIni);
        calc.setKcLate(kcLate);
        calc.setKcMid(kcMid);
        calc.setP(p);
        calc.setSoilDepth(soilDepth);
        calc.setRu(ru);
        calc.setSwcFc(swcFc);
        calc.setSwcMax(swcMax);
        calc.setSwcWp(swcWp);
    }

    /**
     * Calculator for soil data.
     */
    private SoilCalculator calculator;

    /**
     * Climatic daily data used to compute data.
     */
    private final List<ClimaticDailyData> climaticDailyData;

    /**
     * 4 stages for 2015.
     */
    private final List<Date> stages2015;

    /**
     * Error message.
     */
    private final String waterReserveMsg = "At %d-%d-%d, "
            + "water reserve must not be null!";

    /**
     * Error message.
     */
    private final String swcMsg = "At %d-%d-%d, swc and r must not be null!";

    /**
     * Constructor.
     */
    public SoilCalculatorTest() {
        climaticDailyData = getClimatic2015Data();
        stages2015 = Arrays.asList(
                // semis
                helperDate(2015, 3, 15),
                // levée
                helperDate(2015, 3, 25),
                // floraison
                helperDate(2015, 6, 1),
                // récolte
                helperDate(2015, 7, 20));
    }

    /**
     * Define calculator before each test.
     */
    @Before
    public void before() {
        calculator = new SoilCalculator();
        calculator.setClimaticDailyData(climaticDailyData);
        calculator.setStages(stages2015);
        setSoilCalculatorParams(calculator);
    }

    /**
     * No data given.
     */
    @Test
    public void climaticDailyDataNull() {
        calculator.setClimaticDailyData(null);
        setSoilCalculatorParams(calculator);
        Map<String, String> errors = calculator.getConfigurationErrors();
        assertTrue(errors != null);
        assertFalse(errors.isEmpty());
        assertTrue(helperErrors(errors), errors.size() == 1);
        assertTrue(errors.containsKey("soil.climaticDailyData"));
    }

    /**
     * Compare calculation with results from R code.
     */
    @Test
    public void comparisonWithReference() {
        /*
         * Set input data.
         */
        setReferenceParams(calculator);

        /*
         * Load climatic daily data used to compute data.
         */
        final String climate = CLIMATE_1951;
        final String separator = ",";
        final String[] headers = getClimate1951Headers();
        List<ClimaticDailyData> referenceClimaticDailyData;
        referenceClimaticDailyData = getClimaticData(climate, separator, headers);

        calculator.setClimaticDailyData(referenceClimaticDailyData);

        /*
         * Stages
         */
        List<Date> stages = Arrays.asList(
                // S1
                helperDate(1950, 217),
                // S2
                helperDate(1950, 218),
                // S3
                helperDate(1950, 219),
                // S4
                helperDate(1950, 222),
                // S1
                helperDate(1951, 217),
                // S2
                helperDate(1951, 218),
                // S3
                helperDate(1951, 219),
                // S4
                helperDate(1951, 222));
        calculator.setStages(stages);

        /*
         * Checks.
         */
        Map<String, String> errors = calculator.getConfigurationErrors();
        assertTrue(helperErrors(errors), errors == null);

        /*
         * Compute
         */
        List<SoilDailyData> data = calculator.load();
        assertTrue("Computed soil data must not be null!", data != null);
        assertEquals("Number of computed data must equal climatic data.",
                referenceClimaticDailyData.size(), data.size());

        /*
         * Set reference data
         */
        final File soilFile = getFile(getSoil1951Path());
        assertTrue(soilFile.exists());
        final String[] soilHeaders = getSoilHeaders();
        final String soilSeparator = ",";
        SoilFileLoader loader = new SoilFileLoader(soilFile, soilHeaders,
                soilSeparator);
        errors = loader.getConfigurationErrors();
        assertTrue(helperErrors(errors), errors == null);
        List<SoilDailyData> reference = loader.load();
        assertTrue("Reference for soil data must not be null!",
                reference != null);
        assertEquals("Number of reference data must equal climatic data.",
                referenceClimaticDailyData.size(), reference.size());
        assertEquals("Number of computed data must equal reference data.",
                reference.size(), data.size());

        /*
         * Compare
         */
        // Stop at S1 1951, bug in R code.
        final int haltComparison = 582;
        final double tolerance = 0.01;
        final String diffMsg = """

                               At %d-%02d-%02d (%d), %s=%.3f != expected %.3f!""";
        List<String> computationErrors = new ArrayList<>();
        int i = 0;
        for (final SoilDailyData ddata : data) {
            if (i == haltComparison) {
                break;
            }
            SoilDailyData rdata = reference.get(i);
            if (ddata.getDay() != rdata.getDay()) {
                computationErrors.add("days are not the same!");
                break;
            }
            if (ddata.getSwc() == null) {
                computationErrors.add(String.format(swcMsg, ddata.getYear(),
                        ddata.getMonth(), ddata.getDay()));
            }
            if (ddata.getWaterReserve() == null) {
                computationErrors.add(String.format(waterReserveMsg,
                        ddata.getYear(), ddata.getMonth(), ddata.getDay()));
            }
            final int doy = i % DateUtils.nbOfDays(ddata.getYear());
            double difference;
            difference = Math.abs(ddata.getSwc() - rdata.getSwc());
            if (difference > tolerance) {
                computationErrors.add(String.format(diffMsg, ddata.getYear(),
                        ddata.getMonth(), ddata.getDay(), doy, "SWC",
                        ddata.getSwc(), rdata.getSwc()));
            }

            difference = Math.abs(ddata.getWaterReserve()
                    - rdata.getWaterReserve());
            if (difference > tolerance) {
                computationErrors.add(String.format(diffMsg, ddata.getYear(),
                        ddata.getMonth(), ddata.getDay(), doy, "WaterReserve",
                        ddata.getWaterReserve(), rdata.getWaterReserve()));
                if (computationErrors.size() / 2 > 2) {
                    computationErrors.add("\nAnd so on...");
                    break;
                }
            }
            i++;
        }
        assertTrue(computationErrors.toString(), computationErrors.isEmpty());
    }

    /**
     * Test load().
     */
    @Test
    public void load() {
        Map<String, String> errors = calculator.getConfigurationErrors();
        assertTrue(helperErrors(errors), errors == null);
        List<SoilDailyData> data = calculator.load();
        assertTrue("Computed soil data must not be null!", data != null);
        assertEquals("Number of computed data must equal climatic data.",
                climaticDailyData.size(), data.size());

        List<String> computationErrors = new ArrayList<>();
        for (SoilDailyData ddata : data) {
            if (ddata.getSwc() == null) {
                computationErrors.add(String.format(swcMsg, ddata.getYear(),
                        ddata.getMonth(), ddata.getDay()));
            }
            if (ddata.getWaterReserve() == null) {
                computationErrors.add(String.format(waterReserveMsg,
                        ddata.getYear(), ddata.getMonth(), ddata.getDay()));
            }
        }
        assertTrue(computationErrors.toString(), computationErrors.isEmpty());
    }

    /**
     * Test integration into {@link Evaluation}.
     *
     * @throws IOException while loading phenology properties
     */
    @Test
    public void loadInEvaluation() throws IOException {
        final String baseName = "pheno_curve_grapevine_sw_4_stages-chardonnay";
        final Properties ini = getPhenologyProperties(baseName);
        // 1. load Evaluation
        final File xmlFile = getFile("model/data/phenology/" + baseName + ".gri");
        final Evaluation evaluation = getEvaluation(xmlFile, false);
        // 2. adjust ResourceManager
        final ResourceManager mgr = evaluation.getResourceManager();
        mgr.setCropDevelopmentYears(getInt(ini, "nbOfYears"));
        // check
        assertTrue("There must be SOIL variables in ResourceManager.", mgr.hasSoilVariables());
        // 3. set SoilCalculator as it is not defined in XML
        final EvaluationSettings settings = evaluation.getSettings();
        settings.setSoilLoader(new SoilLoaderProxy());
        final SoilCalculator calc = new SoilCalculator();
        setSoilCalculatorParams(calc);
        settings.getSoilLoader().setCalculator(calc);
        // 4. initialize resources
        evaluation.initializeResources();
        final Map<ResourceErrorType, ErrorMessage> errors;
        errors = evaluation.getResourceManager().getConsistencyErrors();
        assertNull("No error must be found", errors);
        // Tests
        final List<ClimaticDailyData> data = evaluation.getResourceManager().getClimaticResource().getData();
        assertFalse("Climatic data must not be empty after initializeResources()", data.isEmpty());
        final List<ClimaticDailyData> dataAfter = evaluation.getClimaticResource().getData();
        assertEquals(data.size(), dataAfter.size());
        assertTrue("All values of SWC must be defined",
                dataAfter.stream().map(d -> d.getRawValue(Variable.SOILWATERCONTENT)).allMatch(Objects::nonNull));
        assertTrue("All values of WATER_RESERVE must be defined",
                dataAfter.stream().map(d -> d.getRawValue(Variable.WATER_RESERVE)).allMatch(Objects::nonNull));
        // Ensure values differs
        long nb = dataAfter.stream().map(d -> d.getRawValue(Variable.SOILWATERCONTENT)).distinct().count();
        assertNotEquals(1L, nb);
        nb = dataAfter.stream().map(d -> d.getRawValue(Variable.WATER_RESERVE)).distinct().count();
        assertNotEquals(1L, nb);
    }

    /**
     * No data given.
     */
    @Test
    public void stagesNull() {
        calculator.setStages(null);
        setSoilCalculatorParams(calculator);
        Map<String, String> errors = calculator.getConfigurationErrors();
        assertTrue(errors != null);
        assertFalse(errors.isEmpty());
        assertTrue(helperErrors(errors), errors.size() == 1);
        assertTrue(errors.containsKey("soil.stages"));
    }

    /**
     * Check helperDate().
     */
    @Test
    public void testHelperDate() {
        final int year = 2016;
        final int month = 2;
        final int day = 3;
        Date date = helperDate(year, month, day);
        SimpleDateFormat sdf = new SimpleDateFormat("MM");
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        String expected = "02";
        String actual = sdf.format(date);
        assertEquals(expected, actual);
    }

}
