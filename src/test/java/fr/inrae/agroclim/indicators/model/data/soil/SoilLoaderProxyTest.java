/*
 * This file is part of Indicators.
 *
 * Indicators is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Indicators is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Indicators. If not, see <https://www.gnu.org/licenses/>.
 */
package fr.inrae.agroclim.indicators.model.data.soil;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.Map;

import org.junit.Test;

import fr.inrae.agroclim.indicators.model.data.DataTestHelper;

/**
 * Test proxy of soil loaders.
 *
 * Last changed : $Date$
 *
 * @author $Author$
 * @version $Revision$
 */
public final class SoilLoaderProxyTest extends DataTestHelper {

    /**
     * Set calculator settings, so no errors.
     */
    @Test
    public void calculatorSettings() {
        final SoilLoaderProxy builder = new SoilLoaderProxy();
        setSoilCalculatorParams(builder);

        builder.setClimaticDailyData(null);
        builder.setStages(null);

        final Map<String, String> errors = builder.getConfigurationErrors();
        assertTrue(helperErrors(errors), errors.size() == 2);
        assertTrue(helperErrors(errors),
                errors.containsKey("soil.climaticDailyData"));
        assertTrue(helperErrors(errors), errors.containsKey("soil.stages"));
    }

    /**
     * Set file loader settings, so no errors.
     */
    @Test
    public void fileSettings() {
        final SoilLoaderProxy builder = new SoilLoaderProxy();
        builder.setFile(getClimate2015File());
        builder.setHeaders(getClimate2015Headers());
        builder.setSeparator(getClimate2015Separator());

        final Map<String, String> errors = builder.getConfigurationErrors();
        assertTrue(helperErrors(errors), errors == null);
    }

    /**
     * No settings, so errors are thrown.
     */
    @Test
    public void noSettings() {
        final SoilLoaderProxy builder = new SoilLoaderProxy();

        final Map<String, String> errors = builder.getConfigurationErrors();
        assertFalse(helperErrors(errors), errors == null);
        assertFalse(helperErrors(errors), errors.isEmpty());
    }
}
