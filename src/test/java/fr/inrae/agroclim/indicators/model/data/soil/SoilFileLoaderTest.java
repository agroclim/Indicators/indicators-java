/*
 * This file is part of Indicators.
 *
 * Indicators is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Indicators is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Indicators. If not, see <https://www.gnu.org/licenses/>.
 */
package fr.inrae.agroclim.indicators.model.data.soil;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.util.List;
import java.util.Map;

import org.junit.BeforeClass;
import org.junit.Test;

import fr.inrae.agroclim.indicators.model.data.DataTestHelper;
import fr.inrae.agroclim.indicators.model.data.Resource;

/**
 * Test file loader for Soil data.
 *
 * Last changed : $Date$
 *
 * @author $Author$
 * @version $Revision$
 */
public final class SoilFileLoaderTest extends DataTestHelper {
    /**
     * Empty Soil file.
     */
    private static File emptyFile;

    /**
     * CSV Soil file OK.
     */
    private static File soilFile;

    /**
     * Define Files.
     */
    @BeforeClass
    public static void setupBeforeClass() {
        final String csvPath = "model/data/soil/soil-ok.csv";
        soilFile = getFile(csvPath);
        emptyFile = getFile(getEmptyPath());
    }

    /**
     * CSV headers OK, even in different case.
     */
    private final String[] headersDifferentCase = {"Year", "Month", "Day", "SWC", "waterReserve"};

    /**
     * Soil file which does not exist.
     */
    private final File notexistantFile = new File("/does/not/exist");

    /**
     * CSV separator OK.
     */
    private final String separator = Resource.DEFAULT_SEP;

    /**
     * Test what happens with empty file.
     */
    @Test
    public void emptyFile() {
        final String[] headers = getSoilHeaders();
        final SoilFileLoader loader = new SoilFileLoader(emptyFile, headers,
                separator);
        final Map<String, String> errors = loader.getConfigurationErrors();
        assertTrue(errors != null);
        assertFalse(errors.isEmpty());
        assertTrue(errors.containsKey("soil.file"));
    }

    /**
     * Test what happens with empty headers.
     */
    @Test
    public void emptyHeaders() {
        final SoilFileLoader loader = new SoilFileLoader(soilFile, null, separator);
        final Map<String, String> errors = loader.getConfigurationErrors();
        assertTrue(errors != null);
        assertFalse(errors.isEmpty());
        assertTrue(errors.containsKey("soil.header"));
    }

    /**
     * Test what happens with empty separator.
     */
    @Test
    public void emptySeparator() {
        final String[] headers = getSoilHeaders();
        final SoilFileLoader loader = new SoilFileLoader(soilFile, headers, null);
        final Map<String, String> errors = loader.getConfigurationErrors();
        assertTrue(errors != null);
        assertFalse(errors.isEmpty());
        assertTrue(errors.containsKey("soil.separator"));
    }

    /**
     * Test loading with good file.
     */
    @Test
    public void load() {
        final String[] headers = getSoilHeaders();
        final SoilFileLoader loader = new SoilFileLoader(soilFile, headers, separator);
        final Map<String, String> errors = loader.getConfigurationErrors();
        assertTrue(errors == null);
        final List<SoilDailyData> data = loader.load();
        assertTrue("soil data must not be null!", data != null);
        assertEquals("soil-ok.csv has only 1 line.", 1, data.size());
        final Double expected = 12.3;
        final SoilDailyData ddata = data.get(0);
        assertEquals(ddata.toString(), expected, ddata.getSwc());
        final Double expected2 = 23.4;
        assertEquals(ddata.toString(), expected2, ddata.getWaterReserve());
    }

    /**
     * Test what happens with headers in different case.
     */
    @Test
    public void loadDifferentCaseHeaders() {
        final SoilFileLoader loader = new SoilFileLoader(soilFile,
                headersDifferentCase, separator);
        final Map<String, String> errors = loader.getConfigurationErrors();
        assertTrue(errors == null);
        final List<SoilDailyData> data = loader.load();
        assertTrue("soil data must not be null!", data != null);
        assertEquals("soil-ok.csv has only 1 line.", 1, data.size());
    }

    /**
     * Test what happens with missing file.
     */
    @Test
    public void notexistantFile() {
        final String[] headers = getSoilHeaders();
        final SoilFileLoader loader = new SoilFileLoader(notexistantFile, headers,
                separator);
        final Map<String, String> errors = loader.getConfigurationErrors();
        assertTrue(errors != null);
        assertFalse(errors.isEmpty());
        assertTrue(errors.containsKey("soil.file"));
    }

    /**
     * Test the Files.
     */
    @Test
    public void soilFile() {
        assertTrue(soilFile.exists());
        assertTrue(emptyFile.exists());
        assertFalse(notexistantFile.exists());
    }
}
