/*
 * This file is part of Indicators.
 *
 * Indicators is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Indicators is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Indicators. If not, see <https://www.gnu.org/licenses/>.
 */
package fr.inrae.agroclim.indicators.model.data;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.junit.Test;

import fr.inrae.agroclim.indicators.exception.ErrorMessage;
import fr.inrae.agroclim.indicators.exception.type.ResourceErrorType;
import fr.inrae.agroclim.indicators.model.data.phenology.AnnualStageData;
import fr.inrae.agroclim.indicators.resources.Messages;

/**
 * Test the resource manager.
 *
 * Last changed : $Date: 2023-03-16 17:36:45 +0100 (jeu. 16 mars 2023) $
 *
 * @author $Author: omaury $
 * @version $Revision: 644 $
 */
public final class ResourceManagerTest extends DataTestHelper {

    /**
     * Resource manager without data.
     */
    @Test
    public void consistencyEmptyResources() {
        final ResourceManager mgr = new ResourceManager();

        // without variables
        Map<ResourceErrorType, ErrorMessage> errors = mgr.getConsistencyErrors();
        assertNotNull("Empty ResourceManager must return consistency errors!",
                errors);
        final Set<ResourceErrorType> expectedErrors = new HashSet<>();
        expectedErrors.add(ResourceErrorType.VARIABLES);
        assertEquals("ResourceManager without variables must have errors!",
                expectedErrors, errors.keySet());
        expectedErrors.clear();

        // with variables
        final Set<Variable> variables = new HashSet<>();
        variables.add(Variable.TMEAN);
        variables.add(Variable.SOILWATERCONTENT);
        mgr.setVariables(variables);
        expectedErrors.addAll(Arrays.asList(ResourceErrorType.CLIMATE,
                ResourceErrorType.PHENO));
        errors = mgr.getConsistencyErrors();
        assertNotNull("Empty ResourceManager must return consistency errors!",
                errors);
        assertEquals("Empty ResourceManager must have errors on all resources",
                expectedErrors, errors.keySet());
    }

    /**
     * Missing days in climatic resource.
     */
    @Test
    public void consistencyMissingClimaticDays() {
        final ResourceManager mgr = new ResourceManager();
        final Set<Variable> variables = new HashSet<>();
        variables.add(Variable.TMEAN);
        mgr.setVariables(variables);
        final String[] headers = {"NUM_POSTE", "year", "month", "day", "etp",
                "radiation", "rain", "tmean", "tmin", "tmax", "rh", "wind"};
        mgr.getClimaticResource().setData(getClimaticData(
                "climate-missing-data.csv", ";", headers));
        final Map<ResourceErrorType, ErrorMessage> errors = mgr.getConsistencyErrors();
        final String subject = "ResourceManager with missing days in climatic "
                + "resource ";
        assertNotNull(subject + "must return consistency errors!", errors);
        assertTrue(subject + "must have errors on climatic resources!",
                errors.keySet().contains(ResourceErrorType.CLIMATE));
        assertEquals(subject + "must have errors on climatic resources!",
                errors.get(ResourceErrorType.CLIMATE).getType().getI18nKey(), ResourceErrorType.CLIMATE_SIZE_WRONG.getI18nKey());
    }

    /**
     * No errors with right files.
     */
    @Test
    public void consistencyNoErrors() {
        final ResourceManager mgr = new ResourceManager();
        mgr.setCropDevelopmentYears(1);
        final Set<Variable> variables = new HashSet<>();
        variables.add(Variable.TMEAN);
        mgr.setVariables(variables);
        mgr.getClimaticResource().setData(getClimatic2015Data());
        mgr.getPhenologicalResource().setData(getPheno2015Data());
        final Map<ResourceErrorType, ErrorMessage> errors = mgr.getConsistencyErrors();
        assertNull(errors);
    }

    /**
     * Resource manager with data only in climatic resource.
     */
    @Test
    public void consistencyOnlyClimatic() {
        final ResourceManager mgr = new ResourceManager();
        final Set<Variable> variables = new HashSet<>();
        variables.add(Variable.TMEAN);
        mgr.setVariables(variables);
        mgr.getClimaticResource().setData(getClimatic2015Data());
        final Map<ResourceErrorType, ErrorMessage> errors = mgr.getConsistencyErrors();
        final String subject = "ResourceManager with only climatic resource ";
        assertNotNull(subject + "must return consistency errors!", errors);
        final Set<ResourceErrorType> expectedErrors = new HashSet<>();
        expectedErrors.add(ResourceErrorType.PHENO);
        assertEquals(subject + "must have errors on pheno resources",
                expectedErrors, errors.keySet());
    }

    /**
     * Years are different between climate and pheno.
     */
    @Test
    public void consistencyYears() {
        final ResourceManager mgr = new ResourceManager();
        mgr.setCropDevelopmentYears(1);
        final Set<Variable> variables = new HashSet<>();
        variables.add(Variable.TMEAN);
        mgr.setVariables(variables);
        mgr.getClimaticResource().setData(getClimatic2015Data());
        final List<AnnualStageData> data = getPhenoSampleData();
        data.remove(data.size() - 1);
        mgr.getPhenologicalResource().setData(data);
        final Map<ResourceErrorType, ErrorMessage> errors = mgr.getConsistencyErrors();
        assertNotNull(errors);
        final Set<ResourceErrorType> expectedErrors = new HashSet<>();
        expectedErrors.add(ResourceErrorType.CLIMATE_YEARS);
        final String subject = "ResourceManager with only climatic data in 2015 "
                + "must have errors on climatic resources";
        assertEquals(subject, expectedErrors, errors.keySet());
    }

    /**
     * Ensure all translations exist.
     */
    @Test
    public void error18nKey() {
        for (final ResourceErrorType val : ResourceErrorType.values()) {
            if (val.getParent() != null) {
                assertFalse(val.getI18nKey() + " must be translated!", Messages.get(val.getI18nKey()).startsWith("!"));
            }
        }
    }
}
