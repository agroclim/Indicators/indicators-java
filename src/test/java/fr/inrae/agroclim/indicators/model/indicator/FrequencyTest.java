/*
 * Copyright (C) 2020 INRAE AgroClim
 *
 * This file is part of Indicators.
 *
 * Indicators is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Indicators is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Indicators. If not, see <http://www.gnu.org/licenses/>.
 */
package fr.inrae.agroclim.indicators.model.indicator;

import static junit.framework.TestCase.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import fr.inrae.agroclim.indicators.exception.IndicatorsException;
import fr.inrae.agroclim.indicators.model.criteria.NoCriteria;
import fr.inrae.agroclim.indicators.model.criteria.SimpleCriteria;
import fr.inrae.agroclim.indicators.model.data.DataTestHelper;
import fr.inrae.agroclim.indicators.model.data.Variable;
import fr.inrae.agroclim.indicators.model.data.climate.ClimaticResource;

/**
 * Test {@link Frequency} indicator.
 *
 * Last changed : $Date$
 *
 * @author $Author$
 * @version $Revision$
 */
public class FrequencyTest extends DataTestHelper {

    /**
     * climate-2015.csv.
     */
    private final ClimaticResource climaticData;

    /**
     * Indicator to test.
     */
    private final Frequency indicator;

    /**
     * Constructor.
     */
    public FrequencyTest() {
        climaticData = new ClimaticResource();
        climaticData.setData(getClimatic2015Data());
        indicator = new Frequency();
    }

    @Test
    public void computeSingleValueWithoutCriteria() {
        NumberOfDays nbOfDays = new NumberOfDays();
        indicator.setNumberOfDays(nbOfDays);
        final double expected = 100.;
        double result = -1;
        String error = null;
        try {
            result = indicator.computeSingleValue(climaticData);
        } catch (final IndicatorsException ex) {
            error = ex.getClass().getCanonicalName() + " : " + ex.getMessage();
        }
        assertTrue(error, error == null);
        assertEquals(expected, result, 0.0);

        nbOfDays.setCriteria(new NoCriteria());
        try {
            result = indicator.computeSingleValue(climaticData);
        } catch (final IndicatorsException ex) {
            error = ex.getClass().getCanonicalName() + " : " + ex.getMessage();
        }
        assertTrue(error, error == null);
        assertEquals(expected, result, 0.0);
    }

    @Test
    public void isComputableWithCriteria() {
        assertTrue(indicator.isComputable());
        NumberOfDays nbOfDays = new NumberOfDays();
        SimpleCriteria criteria = new SimpleCriteria();
        criteria.setVariable(Variable.TMAX);
        nbOfDays.setCriteria(criteria);
        indicator.setNumberOfDays(nbOfDays);
        assertTrue(indicator.isComputable(climaticData));
    }

    @Test
    public void isComputableWithNoCriteria() {
        assertTrue(indicator.isComputable());
        NumberOfDays nbOfDays = new NumberOfDays();
        NoCriteria criteria = new NoCriteria();
        criteria.setVariable(Variable.TMAX);
        nbOfDays.setCriteria(criteria);
        indicator.setNumberOfDays(nbOfDays);
        assertTrue(indicator.isComputable(climaticData));
    }

    @Test(expected = NullPointerException.class)
    public void isComputableWithoutVariable() {
        assertTrue(indicator.isComputable());
        NumberOfDays nbOfDays = new NumberOfDays();
        NoCriteria criteria = new NoCriteria();
        criteria.setVariable(null);
        nbOfDays.setCriteria(criteria);
        indicator.setNumberOfDays(nbOfDays);
        indicator.isComputable(climaticData);
    }
}
