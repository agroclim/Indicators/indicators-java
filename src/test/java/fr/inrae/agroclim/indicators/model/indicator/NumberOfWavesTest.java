/*
 * Copyright (C) 2020 INRAE AgroClim
 *
 * This file is part of Indicators.
 *
 * Indicators is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Indicators is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Indicators. If not, see <http://www.gnu.org/licenses/>.
 */
package fr.inrae.agroclim.indicators.model.indicator;

import static junit.framework.TestCase.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import fr.inrae.agroclim.indicators.exception.IndicatorsException;
import fr.inrae.agroclim.indicators.model.criteria.Criteria;
import fr.inrae.agroclim.indicators.model.criteria.RelationalOperator;
import fr.inrae.agroclim.indicators.model.criteria.SimpleCriteria;
import fr.inrae.agroclim.indicators.model.data.DataTestHelper;
import fr.inrae.agroclim.indicators.model.data.climate.ClimaticResource;

/**
 * Test "number of waves" indicator.
 *
 * Last changed : $Date$
 *
 * @author $Author$
 * @version $Revision$
 */
public final class NumberOfWavesTest extends DataTestHelper {

    /**
     * climate-2015.csv.
     */
    private final ClimaticResource climaticData;

    /**
     * tmax > -20°C.
     */
    private final Criteria criteria;

    /**
     * Constructor.
     */
    public NumberOfWavesTest() {
        criteria = new SimpleCriteria("tmax", RelationalOperator.GT, -20);
        climaticData = new ClimaticResource();
        climaticData.setData(getClimatic2015Data());
    }

    /**
     * Test of compute method, of class NumberOfWaves.
     */
    @Test
    public void computeSingleValue() {
        final NumberOfWaves instance = new NumberOfWaves();
        instance.setCriteria(criteria);
        final int nbDays = 7;
        instance.setNbDays(nbDays);

        final double expected = climaticData.getData().size() / nbDays;
        double result = -1;
        String error = null;
        try {
            result = instance.computeSingleValue(climaticData);
        } catch (final IndicatorsException ex) {
            error = ex.getClass().getCanonicalName() + " : " + ex.getMessage();
        }
        assertTrue(error, error == null);
        assertEquals(expected, result, 0.0);
    }

    /**
     * Without criteria: raise exception.
     * @throws IndicatorsException expected exception while compute
     */
    @Test(expected = IndicatorsException.class)
    public void withoutCriteria() throws IndicatorsException {
        final NumberOfWaves instance = new NumberOfWaves();
        instance.computeSingleValue(climaticData);
    }
}
