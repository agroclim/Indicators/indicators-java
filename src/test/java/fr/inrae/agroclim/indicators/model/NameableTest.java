/*
 * Copyright (C) 2020 INRAE AgroClim
 *
 * This file is part of Indicators.
 *
 * Indicators is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Indicators is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Indicators. If not, see <http://www.gnu.org/licenses/>.
 */
package fr.inrae.agroclim.indicators.model;

import static org.junit.Assert.assertFalse;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import fr.inrae.agroclim.indicators.model.function.normalization.Exponential;
import fr.inrae.agroclim.indicators.model.function.normalization.Linear;
import fr.inrae.agroclim.indicators.model.function.normalization.MultiLinear;
import fr.inrae.agroclim.indicators.model.function.normalization.Normal;
import fr.inrae.agroclim.indicators.model.function.normalization.Sigmoid;
import lombok.RequiredArgsConstructor;

/**
 * Test model of translations for Nameable enum or classes.
 *
 * Last changed : $Date$
 *
 * @author $Author$
 * @version $Revision$
 */
@RequiredArgsConstructor
@RunWith(Parameterized.class)
public class NameableTest {

    /**
     * @return Enum to test.
     */
    @Parameterized.Parameters
    public static List<Nameable> data() {
        List<Nameable> data = new ArrayList<>();
        data.addAll(Arrays.asList(EvaluationType.values()));
        data.addAll(Arrays.asList(TimeScale.values()));
        data.add(new Exponential());
        data.add(new Linear());
        data.add(new MultiLinear());
        data.add(new Normal());
        data.add(new Sigmoid());
        return data;
    }

    /**
     * Enum or class to test.
     */
    private final Nameable type;

    @Test
    public void getName() {
        Arrays.asList(Locale.ENGLISH, Locale.FRENCH).forEach(locale -> {
            String msg = String.format("%s does not have localized name for %s", type, locale);
            assertFalse(msg, ((Nameable) type).getName(locale).contains("!"));
        });
    }
}
