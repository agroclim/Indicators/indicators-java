/*
 * This file is part of Indicators.
 *
 * Indicators is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Indicators is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Indicators. If not, see <https://www.gnu.org/licenses/>.
 */
package fr.inrae.agroclim.indicators.model.data.phenology;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

/**
 * Test model of stage builder.
 *
 * Last changed : $Date$
 *
 * @author $Author$
 * @version $Revision$
 */
@RunWith(Parameterized.class)
public class AnnualStageBuilderTest {
    /**
     * @return combinaisons of years, doys and expectations
     */
    @Parameterized.Parameters
    public static List<Object[]> data() {
        return Arrays.asList(new Object[][]{
            {
                Arrays.asList(2000, 2001, 2002),
                new HashMap<String, Integer>() {
                    private static final long serialVersionUID = 1L;
                    {
                        put("s0", 200);
                        put("s1", 426); // 366+60
                        put("s2", 486); // 366+120
                    }
                },
                Arrays.asList(
                        // 2000
                        "2000 s0 200", "2000 s1 426", "2000 s2 486",
                        // 2001
                        "2001 s0 200", "2001 s1 426", "2001 s2 486",
                        // 2002
                        "2002 s0 200", "2002 s1 426", "2002 s2 486"
                        ),
                // no error
                null
            },
            {
                null,
                null,
                Arrays.asList(),
                new HashMap<String, String>() {
                    private static final long serialVersionUID = 1L;
                    {
                        put("phenology.years",
                                "error.evaluation.phenology.years.missing");
                        put("phenology.doys",
                                "error.evaluation.phenology.doys.missing");
                    }
                }
            },
        });

    }

    /**
     * @param stages AnnualStageData to represent
     * @return representation
     */
    private static List<String> repr(final List<AnnualStageData> stages) {
        final List<String> repr = new ArrayList<>();
        stages.forEach((annualStageData) -> {
            annualStageData.getStages().forEach((stage) -> {
                repr.add(annualStageData.getYear() + " " + stage.getName()
                + " " + stage.getValue());
            });
        });
        Collections.sort(repr);
        return repr;
    }

    /**
     * Errors or null.
     */
    private final Map<String, String> error;

    /**
     * Instance to test.
     */
    private final AnnualStageBuilder builder;

    /**
     * Representations of expected stages.
     */
    private final List<String> expected;

    /**
     * Constructor.
     *
     * @param theYears List of years on which build stages.
     * @param theDoys Day of year for each stage to build.
     * @param theExpected Representations of expected stages.
     * @param theError Errors or null.
     */
    public AnnualStageBuilderTest(final List<Integer> theYears,
            final Map<String, Integer> theDoys, final List<String> theExpected,
            final Map<String, String> theError) {
        builder = new AnnualStageBuilder();
        builder.setDoys(theDoys);
        builder.setYears(theYears);
        expected = theExpected;
        error = theError;
    }

    /**
     * Test getConfigurationErrors().
     */
    @Test
    public void getConfigurationErrors() {
        final Map<String, String> actual = builder.getConfigurationErrors();
        assertEquals(error, actual);
    }

    /**
     * Test load().
     */
    @Test
    public void load() {
        // no test if expected error.
        if (error != null) {
            return;
        }
        //-
        final List<AnnualStageData> stages = builder.load();
        final List<String> actual = repr(stages);
        assertEquals(expected, actual);
    }
}
