package fr.inrae.agroclim.indicators.model.indicator;

import static junit.framework.TestCase.assertEquals;
import static org.junit.Assert.assertNull;

import org.junit.Test;

import fr.inrae.agroclim.indicators.exception.IndicatorsException;
import fr.inrae.agroclim.indicators.model.Knowledge;
import fr.inrae.agroclim.indicators.model.criteria.SimpleCriteria;
import fr.inrae.agroclim.indicators.model.data.DataTestHelper;
import fr.inrae.agroclim.indicators.model.data.Variable;
import fr.inrae.agroclim.indicators.model.data.climate.ClimaticResource;

/**
 * Test "diffOfSum" indicator.
 *
 * Last changed : $Date$
 *
 * @author $Author$
 * @version $Revision$
 */
public class DiffOfSumTest extends DataTestHelper {
    /**
     * climate-2015.csv.
     */
    private final ClimaticResource climaticData;

    /**
     * Constructor.
     */
    public DiffOfSumTest() {
        climaticData = new ClimaticResource();
        climaticData.setData(getClimatic2015Data());
    }

    /**
     * sumwd uses diffOfSum and SimpleCriteria.
     */
    @Test
    public void sumwd() {
        final DiffOfSum indicator = new DiffOfSum();
        final Sum sumVariable1 = new Sum();
        final SimpleCriteria criteria1 = new SimpleCriteria();
        criteria1.setVariable(Variable.RAIN);
        criteria1.setNoThreshold(true);
        sumVariable1.setCriteria(criteria1);
        indicator.setSumVariable1(sumVariable1);
        final Sum sumVariable2 = new Sum();
        final SimpleCriteria criteria2 = new SimpleCriteria();
        criteria2.setVariable(Variable.ETP);
        criteria2.setNoThreshold(true);
        sumVariable2.setCriteria(criteria2);
        indicator.setSumVariable2(sumVariable2);

        final double expected = climaticData.getData().stream()
                .map(data -> data.getRain() - data.getEtp())
                .reduce((v1, v2) -> v1 + v2)
                .get();
        double result = -1;
        String error = null;
        try {
            result = indicator.computeSingleValue(climaticData);
        } catch (final IndicatorsException ex) {
            error = ex.getClass().getCanonicalName() + " : " + ex.getMessage();
        }
        assertNull(error, error);
        assertEquals(expected, result, 0.001);

    }

    /**
     * sumwd is also defined in knowledge.xml
     * @throws IndicatorsException while reading the XML
     */
    public void sumwdFromKnowledge() throws IndicatorsException {
        final Knowledge knowledge = Knowledge.load();
        final SimpleIndicator sumwd = (SimpleIndicator) knowledge.getIndicator("sumwd");
        final double expected = climaticData.getData().stream()
                .map(data -> data.getRain() - data.getEtp())
                .reduce((v1, v2) -> v1 + v2)
                .get();
        double result = -1;
        String error = null;
        try {
            result = sumwd.computeSingleValue(climaticData);
        } catch (final IndicatorsException ex) {
            error = ex.getClass().getCanonicalName() + " : " + ex.getMessage();
        }
        assertNull(error, error);
        assertEquals(expected, result, 0.001);
    }
}
