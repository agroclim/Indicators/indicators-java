/*
 * This file is part of Indicators.
 *
 * Indicators is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Indicators is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Indicators. If not, see <https://www.gnu.org/licenses/>.
 */
package fr.inrae.agroclim.indicators.model.data.climate;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.io.File;
import java.util.List;
import java.util.Map;

import org.junit.Test;

import fr.inrae.agroclim.indicators.exception.IndicatorsException;
import fr.inrae.agroclim.indicators.model.EvaluationSettings;
import fr.inrae.agroclim.indicators.model.data.DataTestHelper;

/**
 * Test Climate class.
 *
 * Last changed : $Date$
 *
 * @author $Author$
 * @version $Revision$
 */
public final class ClimateTest extends DataTestHelper {
    /**
     * Test loading test-evaluation.xml.
     */
    @Test
    public void load() {
        String error = null;
        final File xmlFile = getEvaluationTestFile();
        try {
            final EvaluationSettings e = (EvaluationSettings) fr.inrae.agroclim.indicators.xml.XMLUtil.load(
                    xmlFile, EvaluationSettings.CLASSES_FOR_JAXB);
            assertNotNull("evaluationSettings must not be null", e);
            e.setFilePath(xmlFile.getAbsolutePath());
            assertNotNull("evaluationSettings.climate must not be null", e.getClimateLoader());
            final Map<String, String> errors = e.getClimateLoader()
                    .getConfigurationErrors();
            assertNull(helperErrors(errors), errors);
            assertNotNull("evaluationSettings.climate.file must not be null", e.getClimateLoader().getFile());
            assertNotNull("evaluationSettings.climate.file.header must not be null",
                    e.getClimateLoader().getFile().getHeaders());
            final List<ClimaticDailyData> data = e.getClimateLoader().load();
            assertNotNull("Loaded ClimaticDailyData list must not be null", data);
            assertFalse("Loaded ClimaticDailyData list must not be empty",
                    data.isEmpty());
        } catch (final IndicatorsException ex) {
            error = ex.getMessage() + " " + ex.getCause().getMessage();
        }
        assertNull(error, error);
    }

}
