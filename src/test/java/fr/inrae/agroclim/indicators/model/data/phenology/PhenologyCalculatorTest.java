/*
 * This file is part of Indicators.
 *
 * Indicators is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Indicators is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Indicators. If not, see <https://www.gnu.org/licenses/>.
 */
package fr.inrae.agroclim.indicators.model.data.phenology;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import fr.inrae.agroclim.indicators.model.data.DataTestHelper;
import lombok.Getter;
import lombok.extern.log4j.Log4j2;

/**
 * Test model of Phenology calculator.
 *
 * Last changed : $Date$
 *
 * @author $Author$
 * @version $Revision$
 */
@Log4j2
@RunWith(Parameterized.class)
public final class PhenologyCalculatorTest extends DataTestHelper {

    /**
     * Directory for data files.
     */
    private static final String DIR = "src/test/resources/fr/inrae/agroclim/indicators/model/data/phenology/";

    /**
     * @return combinations of model type and number of stages
     */
    @Parameters(name = "{index}: {0}")
    public static Collection<Object> data() {
        final Object[] data = new Object[]{
                // blé
                "pheno_linear_4_stages-soissons",
                "pheno_linear_4_stages-charger",
                // prairie
                "pheno_richardson_3_stages-gramineesa",
                // vigne
                "pheno_curve_4_stages-cabernetS",
                //
                "pheno_curve_4_stages-chardonnay",
                //
                "pheno_curve_4_stages-syrah",
                //
                "pheno_linear_4_stages-cabernetS",
                //
                "pheno_linear_5_stages-chardonnay",
                //
                "pheno_linear_6_stages-syrah",
                //
                "pheno_curve_grapevine_5_stages-cabernetS",
                //
                "pheno_curve_grapevine_5_stages-syrah",
                //
                "pheno_curve_grapevine_sw_4_stages-cabernetS",
                //
                "pheno_curve_grapevine_sw_4_stages-chardonnay",
                // mais
                "pheno_curve_4_stages-dkc5783",
                //
                "pheno_curve_4_stages-meribel",
                //
                "pheno_linear_4_stages-meribel",
                //
                "pheno_linear_5_stages-dkc5783"
        };
        return Arrays.asList(data);
    }

    /**
     * Instance for the parametrized run.
     */
    @Getter
    private final PhenologyCalculator calc;

    /**
     * Last year of period.
     */
    private final int endYear;

    /**
     * First year of period.
     */
    private final int startYear;

    /**
     * Base name for test files.
     */
    private final String test;

    /**
     * Constructor with parametrized arguments.
     *
     * @param iniFile base name of INI file
     * @throws IOException Exception while reading properties
     */
    public PhenologyCalculatorTest(final String iniFile) throws IOException {
        LOGGER.trace("{}", iniFile);
        final Properties ini = getPhenologyProperties(iniFile);
        calc = getPhenologyCalculator(ini);
        test = iniFile;
        startYear = getInt(ini, "startYear");
        endYear = getInt(ini, "endYear");
        // Finally set data.
        calc.setClimaticDailyData(getPhenologyClimaticDailyData(iniFile, calc, startYear, endYear, etpCalculator));
    }

    /**
     * @return stages from CSV file
     * @throws IOException exception while reading file
     */
    private Map<Integer, List<Integer>> getExpectedStages() throws IOException {
        final Map<Integer, List<Integer>> expected = new HashMap<>();
        final String csvFilePath = String.format("%s%s.csv", DIR, test);
        LOGGER.trace("Loading {}", csvFilePath);
        final Charset charset = Charset.forName("UTF-8");
        final Path path = Paths.get(csvFilePath);
        final BufferedReader reader = Files.newBufferedReader(path, charset);
        String line;
        boolean first = true;
        while ((line = reader.readLine()) != null) {
            if (first) {
                first = false;
                continue;
            }
            final String[] parts = line.split(",");
            final Integer year = Integer.valueOf(parts[0]);
            if (!expected.containsKey(year)) {
                final List<Integer> days = new ArrayList<>();
                expected.put(year, days);
            }
            for (int i = 1; i < parts.length; i++) {
                final Integer doy;
                if (parts[i].equals("NA")) {
                    doy = 0;
                } else {
                    doy = Integer.valueOf(parts[i]);
                }
                expected.get(year).add(doy);
            }
        }
        return expected;
    }

    /**
     * Test PhenologyCalculator.load().
     *
     * @throws IOException exception while loading CSV
     */
    @Test
    public void load() throws IOException {
        // 1/ Configuration is OK
        final Map<String, String> errors = calc.getConfigurationErrors();
        String msg = "No configuration error must be raised! ";
        if (errors != null) {
            msg += errors.toString();
        }
        assertTrue(msg, errors == null);

        // 2/ Result must be provided
        final List<AnnualStageData> result = calc.load();
        assertTrue("List of stages must not be null!", result != null);

        // 3/ Right number of results
        final int nbOfYears;
        if (calc.getNbOfYears() == 1) {
            nbOfYears = endYear - startYear + calc.getNbOfYears();
        } else {
            nbOfYears = endYear - startYear + calc.getNbOfYears() - 1;
        }
        assertEquals("One list of stages per year!", nbOfYears, result.size());

        // 4/ Right stages dates
        final Map<Integer, List<Integer>> expected = getExpectedStages();
        assertTrue("Stages from CSV must not be null!", expected != null);
        assertTrue("Stages from CSV must not be empty!", !expected.isEmpty());
        assertEquals("Stages from CSV must have one list of stages per year!",
                nbOfYears, expected.size());
        final Map<Integer, List<Integer>> actual = new HashMap<>();
        result.forEach((data) -> {
            final Integer year = data.getYear();
            actual.put(year, new ArrayList<>());
            data.getStages().forEach((stage) -> {
                actual.get(year).add(stage.getValue());
            });
        });
        assertEquals("Stages years from CSV vs computed aren't good!",
                expected.keySet(), actual.keySet());
        LOGGER.trace("expected = {}", expected);
        LOGGER.trace("actual = {}", actual);
        assertEquals("Computed stage dates aren't good!", expected, actual);
    }
}
