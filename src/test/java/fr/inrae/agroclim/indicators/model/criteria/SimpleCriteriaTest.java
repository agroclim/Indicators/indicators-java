package fr.inrae.agroclim.indicators.model.criteria;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;

import org.junit.Test;

import fr.inrae.agroclim.indicators.exception.IndicatorsException;
import fr.inrae.agroclim.indicators.model.data.Variable;
import fr.inrae.agroclim.indicators.model.data.climate.ClimaticDailyData;
import fr.inrae.agroclim.indicators.xml.XMLUtil;

/**
 * Test simple criteria.
 *
 * Last changed : $Date$
 *
 * @author $Author$
 * @version $Revision$
 */
public class SimpleCriteriaTest {
    /**
     * th = 0.
     */
    private final ClimaticDailyData data0;

    /**
     * th = 40.
     */
    private final ClimaticDailyData data40;

    /**
     * Constructor.
     */
    public SimpleCriteriaTest() {
        data0 = new ClimaticDailyData();
        data0.setTh(0.);
        data40 = new ClimaticDailyData();
        data40.setTh(40.);
    }

    @Test
    public void gt() throws IndicatorsException {
        final double dek = 10.;
        final SimpleCriteria criteria = new SimpleCriteria();
        criteria.setOperator(RelationalOperator.GT);
        criteria.setThreshold(dek);
        criteria.setVariable(Variable.TH);
        final boolean actual = criteria.eval(data0);
        assertFalse(actual);
    }

    @Test
    public void le() throws IndicatorsException {
        final double dek = 10.;
        final SimpleCriteria criteria = new SimpleCriteria();
        criteria.setOperator(RelationalOperator.LE);
        criteria.setThreshold(dek);
        criteria.setVariable(Variable.TH);
        assertTrue(criteria.eval(data0));
        assertFalse(criteria.eval(data40));
    }

    @Test
    public void inferiorToThreshold() throws IndicatorsException {
        final double dek = 10.;
        final SimpleCriteria criteria = new SimpleCriteria();
        criteria.setInferiorToThreshold(true);
        criteria.setStrict(false);
        criteria.setThreshold(dek);
        criteria.setVariable(Variable.TH);
        assertTrue(criteria.eval(data0));
        assertFalse(criteria.eval(data40));

        criteria.setInferiorToThreshold(false);
        assertFalse(criteria.eval(data0));
        assertTrue(criteria.eval(data40));
    }

    @Test(expected = Test.None.class)
    public void serializeInferiorToThreshold() throws IndicatorsException, UnsupportedEncodingException {
        final Class<?>[] classes = new Class<?>[]{ Criteria.class, SimpleCriteria.class };
        final String expected = """
                                <?xml version="1.0" encoding="UTF-8" standalone="yes"?>
                                <simpleCriteria>
                                    <variable>th</variable>
                                    <operator>GT</operator>
                                    <threshold>35.0</threshold>
                                    <noThreshold>false</noThreshold>
                                </simpleCriteria>
                                """;

        final SimpleCriteria criteria = new SimpleCriteria();
        criteria.setInferiorToThreshold(false);
        criteria.setStrict(true);
        criteria.setThreshold(35.0);
        criteria.setVariable(Variable.TH);
        final ByteArrayOutputStream baos = new ByteArrayOutputStream();
        XMLUtil.serialize(criteria, baos, classes);

        final String actual = baos.toString(StandardCharsets.UTF_8.name());
        assertEquals(expected, actual);
    }

    @Test(expected = Test.None.class)
    public void unserializeGT() throws IndicatorsException {
        final Class<?>[] classes = new Class<?>[]{ Criteria.class, SimpleCriteria.class };
        final String xml = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>"
                + "<criteria xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:type=\"simpleCriteria\">"
                + "<variable>th</variable><operator>GT</operator><threshold>35.0</threshold>"
                + "</criteria>";

        final InputStream stream = new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8));
        final Object actual = XMLUtil.loadResource(stream, classes);
        assertNotNull(actual);
        assertTrue(actual instanceof SimpleCriteria);
        final SimpleCriteria criteria = (SimpleCriteria) actual;
        assertEquals(RelationalOperator.GT, criteria.getOperator());
        assertEquals(35.0, criteria.getThreshold(), 0.01);
        assertEquals(Variable.TH, criteria.getVariable());
        assertFalse(criteria.eval(data0));
        assertTrue(criteria.eval(data40));
    }

    @Test(expected = Test.None.class)
    public void unserializeInferiorToThreshold() throws IndicatorsException {
        final Class<?>[] classes = new Class<?>[]{ Criteria.class, SimpleCriteria.class };
        final String xml = """
                           <?xml version="1.0" encoding="UTF-8" ?><criteria xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="simpleCriteria">    <variable>th</variable>
                               <inferiorToThreshold>false</inferiorToThreshold>
                               <strict>false</strict>
                               <threshold>35.0</threshold>
                               <noThreshold>false</noThreshold>
                           </criteria>""";

        final InputStream stream = new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8));
        final Object actual = XMLUtil.loadResource(stream, classes);
        assertNotNull(actual);
        assertTrue(actual instanceof SimpleCriteria);
        final SimpleCriteria criteria = (SimpleCriteria) actual;
        assertEquals(RelationalOperator.GE, criteria.getOperator());
        assertEquals(35.0, criteria.getThreshold(), 0.01);
        assertEquals(Variable.TH, criteria.getVariable());
        assertFalse(criteria.eval(data0));
        assertTrue(criteria.eval(data40));
    }
}
