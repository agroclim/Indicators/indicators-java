/*
 * This file is part of Indicators.
 *
 * Indicators is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Indicators is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Indicators. If not, see <https://www.gnu.org/licenses/>.
 */
package fr.inrae.agroclim.indicators.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

import fr.inrae.agroclim.indicators.exception.IndicatorsException;
import fr.inrae.agroclim.indicators.model.data.DataTestHelper;
import fr.inrae.agroclim.indicators.xml.XMLUtil;
import lombok.extern.log4j.Log4j2;

/**
 * Test that a Getari file with stageDeltas.
 */
@Log4j2
public class StageDeltaEvaluationTest extends DataTestHelper {

    /**
     * Evaluation from good XML file.
     */
    private Evaluation evaluation;

    /**
     * Set up Evaluation to test.
     */
    @Before
    public void beforeTest() {
        final File xmlFile = getFile("xml/evaluation-test-delta.gri");
        EvaluationSettings settings;
        try {
            settings = (EvaluationSettings) XMLUtil.load(xmlFile,
                    EvaluationSettings.CLASSES_FOR_JAXB);
            settings.initializeKnowledge();
            settings.setFilePath(xmlFile.getAbsolutePath());
        } catch (final IndicatorsException ex) {
            LOGGER.error(ex.getLocalizedMessage());
            return;
        }
        final Map<String, String> errors = settings.getConfigurationErrors();
        if (errors != null && !errors.isEmpty()) {
            LOGGER.error(errors.toString());
            return;
        }
        evaluation = new Evaluation(settings.getEvaluation());
        evaluation.setSettings(settings);
        evaluation.getSettings().setFilePath(xmlFile.getAbsolutePath());
    }

    /**
     * Check if evaluation is isComputable.
     */
    @Test
    public void computable() {
        assertTrue("Evaluation must not be null!", evaluation != null);
        final boolean found = evaluation.isComputable();
        assertTrue("evaluation must be computable!", found);
    }

    /**
     * Check if evaluation computes and number of days is less or equal to number of days in phase.
     */
    @Test
    public void compute() {
        assertTrue("Evaluation must not be null!", evaluation != null);
        String error = null;
        try {
            evaluation.initializeResources();
            final long start = System.currentTimeMillis();
            evaluation.compute();
            LOGGER.info("Evaluation.compute() last {} seconds", (System.currentTimeMillis() - start) / 1000.);
        } catch (final IndicatorsException e) {
            error = e.getClass() + " : " + e.getLocalizedMessage();
        }
        assertNull(error, error);
    }

    @Test
    public void hasStageDelta() {
        assertNotNull(evaluation.getSettings().getPhenologyLoader().getDeltas());
        assertEquals(1, evaluation.getSettings().getPhenologyLoader().getDeltas().size());
        assertEquals("s1", evaluation.getSettings().getPhenologyLoader().getDeltas().get(0).getStage());
    }
}
