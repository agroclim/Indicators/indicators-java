/*
 * This file is part of Indicators.
 *
 * Indicators is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Indicators is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Indicators. If not, see <https://www.gnu.org/licenses/>.
 */
package fr.inrae.agroclim.indicators.model;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

import fr.inrae.agroclim.indicators.exception.IndicatorsException;
import fr.inrae.agroclim.indicators.model.data.DataTestHelper;
import fr.inrae.agroclim.indicators.model.data.phenology.AnnualStageData;
import fr.inrae.agroclim.indicators.model.result.EvaluationResult;
import fr.inrae.agroclim.indicators.model.result.IndicatorResult;
import fr.inrae.agroclim.indicators.model.result.PhaseResult;
import lombok.extern.log4j.Log4j2;
import static org.junit.Assert.fail;

/**
 * Test that Marine Marjou's Getari file computes.
 *
 * Times of Evaluation.compute():
 *
 * 2018-06-29 : 0.32s on core i7-4770.
 */
@Log4j2
public class MMarjouTest extends DataTestHelper {

    /**
     * Evaluation from XML file.
     */
    private Evaluation evaluation;

    /**
     * Set up Evaluation to test.
     */
    @Before
    public void beforeTest() {
        final File xmlFile = getFile("xml/mmarjou.xml");
        evaluation = getEvaluation(xmlFile);
    }

    /**
     * Check if evaluation is isComputable.
     */
    @Test
    public void computable() {
        assertTrue("Evaluation must not be null!", evaluation != null);
        boolean found = evaluation.isComputable();
        assertTrue("evaluation must be computable!", found);
    }

    /**
     * Check if evaluation computes and number of days is less or equal to number of days in phase.
     */
    @Test
    public void raidaysInfNbDaysInPhase() {
        assertTrue("Evaluation must not be null!", evaluation != null);
        final Map<Integer, EvaluationResult> results;
        try {
            evaluation.initializeResources();
            long start = System.currentTimeMillis();
            results = evaluation.compute();
            LOGGER.info("Evaluation.compute() last {} seconds", (System.currentTimeMillis() - start) / 1000.);
        } catch (final IndicatorsException e) {
            final String error = e.getClass() + " : " + e.getLocalizedMessage();
            fail(error);
            return;
        }
        // extract stages to compute number of days
        Map<Integer, Map<String, Integer>> stageDates = new HashMap<>();
        List<AnnualStageData> aSDatas;
        aSDatas = evaluation.getSettings().getPhenologyLoader().load();
        aSDatas.forEach((aSData) -> {
            Integer year = aSData.getYear();
            if (!stageDates.containsKey(year)) {
                stageDates.put(year, new HashMap<>());
            }
            aSData.getStages().forEach((stage) -> {
                stageDates.get(year).put(stage.getName(), stage.getValue());
            });
        });
        // check excraidays, hraidays and raidays
        assertFalse(results.isEmpty());
        String fmt = "%d | %s %s : %d | %s=%.4f";
        for (Map.Entry<Integer, EvaluationResult> entry : results.entrySet()) {
            Integer year = entry.getKey();
            if (!stageDates.containsKey(year)) {
                LOGGER.trace("No stage date for year {}", year);
                continue;
            }
            EvaluationResult result = entry.getValue();

            if (result == null) {
                LOGGER.warn("Strange, no value for the result!");
                continue;
            }
            for (final PhaseResult phase : result.getPhaseResults()) {
                List<IndicatorResult> processes;
                processes = phase.getIndicatorResults();
                if (processes == null) {
                    LOGGER.trace("Strange, result list for year " + year
                            + " is null!");
                    continue;
                }
                for (final IndicatorResult process : processes) {
                    for (IndicatorResult ind : process.getIndicatorResults()) {
                        if (ind == null) {
                            LOGGER.trace("Strange indicatorResult is null!");
                            continue;
                        }
                        String indicatorId = ind.getIndicatorId();
                        if ("raidays".equals(indicatorId)
                                || "excraidays".equals(indicatorId)
                                || "hraidays".equals(indicatorId)) {
                            String end = phase.getAnnualPhase().getEndStage();
                            String start = phase.getAnnualPhase().getStartStage();
                            int duration = stageDates.get(year).get(end)
                                    - stageDates.get(year).get(start);
                            String res = String.format(fmt, year, start, end,
                                    duration, indicatorId,
                                    ind.getRawValue());
                            assertTrue(res,
                                    duration >= ind.getRawValue());
                        }
                    }
                }
            }
        }
    }
}
