package fr.inrae.agroclim.indicators.model.data.phenology;

import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.junit.Test;

/**
 * Test StageDelta.
 */
public final class StageDeltaTest {
    @Test
    public void compareTo() {
        final List<StageDelta> list = Arrays.asList(
                new StageDelta("s0", -1),
                new StageDelta("s0", 1));
        Collections.sort(list);
        assertTrue(list.get(0).getDays() == -1);
        assertTrue(list.get(1).getDays() == 1);
        final List<StageDelta> list2 = Arrays.asList(
                new StageDelta("s1", 1),
                new StageDelta("s0", 1),
                new StageDelta("s0", -1));
        Collections.sort(list2);
        assertTrue(list2.get(0).getDays() == -1);
        assertTrue(list2.get(1).getDays() == 1);
    }

}
