/*
 * This file is part of Indicators.
 *
 * Indicators is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Indicators is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Indicators. If not, see <https://www.gnu.org/licenses/>.
 */
package fr.inrae.agroclim.indicators.model.data.phenology;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

/**
 * Test computation of relative stages.
 *
 * Last changed : $Date$
 *
 * @author $Author$
 * @version $Revision$
 */
@RunWith(Parameterized.class)
public class RelativeAnnualStageCalculatorTest {
    /**
     * @return combinaisons of deltas, stages and expectations
     */
    @Parameterized.Parameters
    public static List<Object[]> data() {
        return Arrays.asList(new Object[][]{
            {
                // deltas :
                Arrays.asList(
                new StageDelta("s0", -1),
                new StageDelta("s0", 1),
                new StageDelta("s2", -8),
                new StageDelta("s2", 9)
                ),
                // Stages  :
                Arrays.asList(
                // - 2000
                "2000 s0 200", "2000 s1 426", "2000 s2 486",
                // - 2001
                "2001 s0 200", "2001 s1 426", "2001 s2 486",
                // - 2002
                "2002 s0 200", "2002 s1 426", "2002 s2 486"
                ),
                // Expected stages :
                Arrays.asList(
                // - 2000
                "2000 s0-1 199", "2000 s0 200", "2000 s0+1 201",
                //
                "2000 s1 426",
                //
                "2000 s2-8 478", "2000 s2 486", "2000 s2+9 495",
                // - 2001
                "2001 s0-1 199", "2001 s0 200", "2001 s0+1 201",
                //
                "2001 s1 426",
                //
                "2001 s2-8 478", "2001 s2 486", "2001 s2+9 495",
                // - 2002
                "2002 s0-1 199", "2002 s0 200", "2002 s0+1 201",
                //
                "2002 s1 426",
                //
                "2002 s2-8 478", "2002 s2 486", "2002 s2+9 495"
                )
            }
        });
    }

    /**
     * Parametrized object.
     */
    private static List<AnnualStageData> from(final List<String> stages) {
        Map<Integer, AnnualStageData> annualStageDatas = new HashMap<>();
        Integer previousYear = null;
        for (final String arg : stages) {
            String parts[] = arg.split(" ");
            final Integer year = Integer.parseInt(parts[0]);
            final String stageName = parts[1];
            final Integer doy = Integer.parseInt(parts[2]);
            if (!Objects.equals(previousYear, year)) {
                AnnualStageData data = new AnnualStageData();
                data.setYear(year);
                annualStageDatas.put(year, data);
            }
            annualStageDatas.get(year).add(stageName, doy);
            previousYear = year;
        }
        return new ArrayList<>(annualStageDatas.values());
    }

    /**
     * @param stages AnnualStageData to represent
     * @return represenation
     */
    private static List<String> repr(final List<AnnualStageData> stages) {
        List<String> repr = new ArrayList<>();
        stages.forEach((annualStageData) -> {
            annualStageData.getStages().forEach((stage) -> {
                repr.add(annualStageData.getYear() + " " + stage.getName()
                        + " " + stage.getValue());
            });
        });
        Collections.sort(repr);
        return repr;
    }

    /**
     * Instance for the given parameters to test.
     */
    private final RelativeAnnualStageCalculator calc;

    /**
     * Expected stages.
     */
    private final List<String> expected;

    /**
     * Number of provided stages.
     */
    private final int nbOfStages;

    /**
     * Constructor.
     *
     * @param deltas number of days after and before stages (stage=>days).
     * @param stages stages to handle.
     * @param theExpected expected stages after handling
     */
    public RelativeAnnualStageCalculatorTest(final List<StageDelta> deltas,
            final List<String> stages, final List<String> theExpected) {
        expected = theExpected;
        calc = new RelativeAnnualStageCalculator();
        calc.setDeltas(deltas);
        List<AnnualStageData> annualStageDatas = from(stages);
        calc.setAnnualStageDatas(annualStageDatas);
        nbOfStages = stages.size();
    }

    /**
     * Test that from() well parses the provided stages.
     */
    @Test
    public void from() {
        assertEquals(nbOfStages, repr(calc.getAnnualStageDatas()).size());
    }

    /**
     * Test load().
     */
    @Test
    public void load() {
        List<AnnualStageData> stages = calc.load();
        List<String> actual = repr(stages);
        Collections.sort(actual);
        Collections.sort(expected);
        assertEquals(expected, actual);
    }
}
