/*
 * Copyright (C) 2021 INRAE AgroClim
 *
 * This file is part of Indicators.
 *
 * Indicators is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Indicators is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Indicators. If not, see <http://www.gnu.org/licenses/>.
 */
package fr.inrae.agroclim.indicators.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.BeforeClass;
import org.junit.Test;

import fr.inrae.agroclim.indicators.exception.IndicatorsException;
import fr.inrae.agroclim.indicators.model.criteria.Criteria;
import fr.inrae.agroclim.indicators.model.criteria.FormulaCriteria;
import fr.inrae.agroclim.indicators.model.indicator.CompositeIndicator;
import fr.inrae.agroclim.indicators.model.indicator.Formula;
import fr.inrae.agroclim.indicators.model.indicator.Indicator;
import fr.inrae.agroclim.indicators.model.indicator.NumberOfDays;

/**
 * Test the class vs the XML file.
 *
 * Last change $Date$
 *
 * @author $Author$
 * @version $Revision$
 */
public class KnowledgeHourlyTest {

    /**
     * Knowledge loaded from XML.
     */
    private static Knowledge knowledge;

    @BeforeClass
    public static void loadXml() {
        try {
            knowledge = Knowledge.load(TimeScale.HOURLY);
        } catch (final IndicatorsException ex) {
            assertTrue("Loading XML file from Knowledge.load() should work!", false);
        }
    }

    @Test
    public void avgThi() {
        final Indicator ind = knowledge.getIndicator("avgthi_idele");
        assertNotNull(ind);
        assertEquals(Formula.class, ind.getClass());
        final Formula thi = (Formula) ind;
        assertNotNull(thi.getAggregation());
        assertNotNull(thi.getExpression());
    }

    /**
     * Test cloning indicators.
     * @throws CloneNotSupportedException should never be thrown
     */
    @Test
    public void cloneTest() throws CloneNotSupportedException {
        assertTrue("Loading XML file from Knowledge.load() should work!", knowledge != null);
        for (final CompositeIndicator comp : knowledge.getIndicators()) {
            for (final Indicator ind : comp.getIndicators()) {
                ind.clone();
            }
        }
    }

    @Test
    public void thihours() {
        final Indicator ind = knowledge.getIndicator("thihours_idele");
        assertNotNull(ind);
        assertEquals(NumberOfDays.class, ind.getClass());
        final NumberOfDays thi = (NumberOfDays) ind;
        Criteria criteria = thi.getCriteria();
        assertNotNull(criteria);
        assertEquals(FormulaCriteria.class, criteria.getClass());
        FormulaCriteria formulaCriteria = (FormulaCriteria) criteria;
        assertNotNull(formulaCriteria.getExpression());
        assertNotNull(formulaCriteria.getExpressionParameters());
        assertFalse(formulaCriteria.getExpressionParameters().isEmpty());
        assertEquals("min", formulaCriteria.getExpressionParameters().get(0).getName());
        assertEquals(68d, formulaCriteria.getExpressionParameters().get(0).getValue(), 0.01);
    }
}
