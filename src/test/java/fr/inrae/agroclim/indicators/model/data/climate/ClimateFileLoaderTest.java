/*
 * This file is part of Indicators.
 *
 * Indicators is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Indicators is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Indicators. If not, see <https://www.gnu.org/licenses/>.
 */
package fr.inrae.agroclim.indicators.model.data.climate;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.text.DateFormat;
import java.text.ParseException;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import fr.inrae.agroclim.indicators.model.TimeScale;
import fr.inrae.agroclim.indicators.model.data.DataTestHelper;
import fr.inrae.agroclim.indicators.resources.Messages;
import fr.inrae.agroclim.indicators.util.DateUtils;

/**
 * Test file loader for Climate data.
 *
 * Last changed : $Date$
 *
 * @author $Author$
 * @version $Revision$
 */
public final class ClimateFileLoaderTest extends DataTestHelper {

    /**
     * Empty file.
     */
    private static String emptyFile;

    /**
     * Soil file which does not exist.
     */
    private static String notexistantFile;

    /**
     * CSV Climate file OK.
     */
    private static String climateFile;

    /**
     * CSV separator for the climate file.
     */
    private static String separator;

    /**
     * Create a data for a date
     * @param string date string
     * @return data
     */
    private static ClimaticDailyData createClimaticDailyData(final String string) {
        final ClimaticDailyData data = new ClimaticDailyData();
        final LocalDate date = LocalDate.parse(string);
        data.setDay(date.getDayOfMonth());
        data.setMonth(date.getMonthValue());
        data.setYear(date.getYear());
        return data;
    }

    /**
     * Define Files.
     */
    @BeforeClass
    public static void setupBeforeClass() {
        Locale.setDefault(Locale.ENGLISH);
        Messages.refresh();
        climateFile = getClimate2015File().toString();
        emptyFile = getFile(getEmptyPath()).toString();
        if (System.getProperty("os.name") != null
                && System.getProperty("os.name").startsWith("Windows")) {
            notexistantFile = new File("C:\\does\\not\\exist").toString();
        } else {
            notexistantFile = new File("/does/not/exist").toString();
        }
        separator = getClimate2015Separator();
    }

    @Test
    public void checkDateSuccession() throws ParseException {
        final String path = "/path/to/csv";
        final int line = 2;
        final ClimaticDailyData yesterday = createClimaticDailyData("2020-01-05");
        final ClimaticDailyData today = createClimaticDailyData("2020-01-06");
        final ClimaticDailyData tomorrow = createClimaticDailyData("2020-01-07");

        final ClimateFileLoader loader = new ClimateFileLoader();
        loader.setTimeScale(TimeScale.DAILY);
        assertTrue(today.getErrors().isEmpty());

        // OK
        loader.checkDate(yesterday, today, line, path);
        assertTrue(today.getErrors().isEmpty());

        // missing
        assertTrue(tomorrow.getErrors().isEmpty());
        loader.checkDate(yesterday, tomorrow, line, path);
        assertFalse(tomorrow.getErrors().isEmpty());

        // order
        assertTrue(yesterday.getErrors().isEmpty());
        loader.checkDate(tomorrow, yesterday, line, path);
        assertFalse(yesterday.getErrors().isEmpty());

        // duplicates
        assertTrue(today.getErrors().isEmpty());
        loader.checkDate(today, today, line, path);
        assertFalse(today.getErrors().isEmpty());
    }

    /**
     * Test the Files.
     */
    @Test
    public void climateFile() {
        assertTrue(new File(climateFile).exists());
        assertTrue(new File(emptyFile).exists());
        assertFalse(new File(notexistantFile).exists());
    }

    /**
     * Test what happens with empty file.
     */
    @Test
    public void emptyFile() {
        final String[] headers = getClimate2015Headers();
        final ClimateFileLoader loader = new ClimateFileLoader(emptyFile, headers,
                separator);
        final Map<String, String> errors = loader.getConfigurationErrors();
        assertNotNull(errors);
        assertFalse(errors.isEmpty());
        assertTrue(errors.containsKey("climate.file"));
    }

    /**
     * Test what happens with empty headers.
     */
    @Test
    public void emptyHeaders() {
        final ClimateFileLoader loader = new ClimateFileLoader(climateFile, null,
                separator);
        final Map<String, String> errors = loader.getConfigurationErrors();
        assertNotNull(errors);
        assertFalse(errors.isEmpty());
        assertTrue(helperErrors(errors), errors.containsKey("climate.header"));
    }

    /**
     * Test what happens with empty separator.
     */
    @Test
    public void emptySeparator() {
        final String[] headers = getClimate2015Headers();
        final ClimateFileLoader loader = new ClimateFileLoader(climateFile, headers,
                null);
        final Map<String, String> errors = loader.getConfigurationErrors();
        assertNotNull(errors);
        assertFalse(errors.isEmpty());
        assertTrue(helperErrors(errors),
                errors.containsKey("climate.separator"));
    }

    /**
     * Test loading with good file.
     */
    @Test
    public void load() {
        final String[] headers = getClimate2015Headers();
        final ClimateFileLoader loader = new ClimateFileLoader(climateFile, headers,
                separator);
        assertTrue(loader.getVariables().isEmpty());
        final EtpCalculator etpCalculator = new EtpPenmanMonteithFAO(false);
        loader.setEtpCalculator(etpCalculator);
        assertEquals(etpCalculator.getVariables(), loader.getVariables());
        final Map<String, String> errors = loader.getConfigurationErrors();
        assertNull(helperErrors(errors), errors);

        final List<ClimaticDailyData> data = loader.load();
        assertNotNull("climate data must not be null!", data);
        final int nbOfData = 365;
        assertEquals("climate-2015.csv has only 365 lines.", nbOfData,
                data.size());
        final Double expected = 0.6;
        ClimaticDailyData ddata = data.get(0);
        assertEquals(ddata.toString(), expected, ddata.getTmin());
        ddata = data.get(nbOfData - 1);
        final Double expected2 = 16.32;
        assertEquals(ddata.toString(), expected2, ddata.getRadiation());
    }

    /**
     * Ensure date strictly anterior to 1970 works.
     */
    @Test
    public void load1950() {
        final String climate = CLIMATE_1951;
        final String[] headers = getClimate1951Headers();
        final List<ClimaticDailyData> data = getClimaticData(climate, ",", headers);
        assertNotNull("Year must be not null for « " + climate + " »", data.get(0).getYear());
        assertEquals(1950, data.get(0).getYear().intValue());
    }

    /**
     * Test loading with filter.
     */
    @Test
    public void loadFilteringPeriod() {
        final String[] headers = getClimate2015Headers();
        final ClimateFileLoader loader = new ClimateFileLoader(climateFile, headers,
                separator);
        final int year = 2015;
        loader.setStartYear(year);
        loader.setEndYear(year);
        loader.setEtpCalculator(new EtpPenmanMonteithFAO(false));
        final Map<String, String> errors = loader.getConfigurationErrors();
        assertNull(helperErrors(errors), errors);

        List<ClimaticDailyData> data = loader.load();
        assertNotNull("climate data must not be null!", data);
        final int nbOfData = 365;
        assertEquals("climate-2015.csv has only 365 lines.", nbOfData,
                data.size());

        loader.setStartYear(year + 1);
        loader.setEndYear(year + 1);
        data = loader.load();
        assertNotNull("climate data must not be null!", data);
        assertTrue("climate data must be empty for " + (year + 1),
                data.isEmpty());
    }

    /**
     * Test with trailing separators.
     */
    @Test
    public void loadTrailingSeparators() {
        final Map<String, String> files = new HashMap<>();
        files.put(separator, "climate-with-trailing-tab.csv");
        files.put(",", "climate-with-trailing-comma.csv");
        files.forEach((sep, fileName) -> {
            final String[] headers = Stream.concat(
                    Arrays.stream(getClimate2015Headers()),
                    Arrays.stream(new String[]{sep}))
                    .toArray(String[]::new);
            final String file = getFile("model/data/climate/" + fileName)
                    .toString();
            final ClimateFileLoader loader = new ClimateFileLoader(file, headers, sep);
            assertTrue(loader.getVariables().isEmpty());
            final EtpCalculator etpCalculator = new EtpPenmanMonteithFAO(false);
            loader.setEtpCalculator(etpCalculator);
            assertEquals(etpCalculator.getVariables(), loader.getVariables());
            final Map<String, String> errors = loader.getConfigurationErrors();
            assertNull(helperErrors(errors));

            final List<ClimaticDailyData> data = loader.load();
            assertNotNull("climate data must not be null!");
            final int nbOfData = 365;
            assertEquals(fileName + " has only 365 lines.", nbOfData, data.size());
            final Double expected = 0.6;
            ClimaticDailyData ddata = data.get(0);
            assertEquals(ddata.toString(), expected, ddata.getTmin());
            ddata = data.get(nbOfData - 1);
            final Double expected2 = 16.32;
            assertEquals(ddata.toString(), expected2, ddata.getRadiation());
        });
    }

    /**
     * Exception is raised if no file is defined.
     */
    @Test
    public void loadWithoutFile() {
        final String[] headers = getClimate2015Headers();
        final ClimateFileLoader loader = new ClimateFileLoader(null, headers,
                separator);
        loader.setEtpCalculator(new EtpPenmanMonteithFAO(false));
        try {
            loader.load();
            Assert.fail("Expected an RuntimeException to be thrown");
        } catch (final RuntimeException e) {
            assertTrue(e.getMessage(),
                    e.getMessage().startsWith("no file defined for climate."));
        }
    }

    /**
     * Test loading with good file without defining headers.
     */
    @Test
    public void loadWithoutHeaders() {
        final String[] headers = null;
        final ClimateFileLoader loader = new ClimateFileLoader(climateFile, headers,
                separator);
        loader.setEtpCalculator(new EtpPenmanMonteithFAO(false));

        final List<ClimaticDailyData> data = loader.load();
        assertNotNull("climate data must not be null!", data);
        final int nbOfData = 365;
        assertEquals("climate-2015.csv has only 365 lines.", nbOfData,
                data.size());
        final Double expected = 0.6;
        ClimaticDailyData ddata = data.get(0);
        assertEquals(ddata.toString(), expected, ddata.getTmin());
        ddata = data.get(nbOfData - 1);
        final Double expected2 = 16.32;
        assertEquals(ddata.toString(), expected2, ddata.getRadiation());
    }

    /**
     * Exception is raised if no separator is defined.
     */
    @Test
    public void loadWithoutSeparator() {
        final String[] headers = getClimate2015Headers();
        final ClimateFileLoader loader = new ClimateFileLoader(climateFile, headers,
                null);
        loader.setEtpCalculator(new EtpPenmanMonteithFAO(false));
        try {
            loader.load();
            Assert.fail("Expected an RuntimeException to be thrown");
        } catch (final RuntimeException e) {
            assertEquals(RuntimeException.class, e.getClass());
            assertEquals("no separator defined for climate.", e.getMessage());
        }
    }

    /**
     * Exception is raised if wrong number of headers is used.
     */
    @Test
    public void loadWithWrongHeaders() {
        final String[] headers = new String[]{"a"};
        final ClimateFileLoader loader = new ClimateFileLoader(climateFile, headers,
                separator);
        loader.setEtpCalculator(new EtpPenmanMonteithFAO(false));
        try {
            loader.load();
            Assert.fail("Expected an RuntimeException to be thrown");
        } catch (final RuntimeException e) {
            assertTrue(e.getMessage(),
                    e.getMessage().startsWith("Wrong number of headers!"));
        }
    }

    /**
     * Test what happens when data is missing.
     */
    @Test
    public void missingData() {
        String csvPath;
        csvPath = "model/data/climate/climate-missing-data.csv";
        final String file = getFile(csvPath).toString();

        final String[] headers = {"NUM_POSTE", "year", "month", "day", "etp",
                "radiation", "rain", "tmean", "tmin", "tmax", "rh", "wind"};
        final ClimateFileLoader loader = new ClimateFileLoader(file, headers, ";");
        loader.setEtpCalculator(new EtpPenmanMonteithFAO(false));
        final Map<String, String> errors = loader.getConfigurationErrors();
        assertNull(helperErrors(errors), errors);

        loader.load();
    }

    /**
     * Test what happens when a date is missing.
     */
    @Test
    public void missingDate() {
        String csvPath;
        csvPath = "model/data/climate/climate-missing-date.csv";
        final String file = getFile(csvPath).toString();

        final String[] headers = {"NUM_POSTE", "year", "month", "day", "etp",
                "radiation", "rain", "tmean", "tmin", "tmax", "rh", "wind"};
        final ClimateFileLoader loader = new ClimateFileLoader(file, headers, ";");
        loader.setLocale(Locale.ENGLISH);
        loader.setEtpCalculator(new EtpPenmanMonteithFAO(false));
        final Map<String, String> errors = loader.getConfigurationErrors();
        assertNull(helperErrors(errors), errors);

        final List<ClimaticDailyData> data = loader.load();
        final List<String> dataErrors = data.stream().flatMap(d -> d.getErrors().stream()).collect(Collectors.toList());
        assertFalse(dataErrors.isEmpty());
        final DateFormat df = DateFormat.getDateInstance(DateFormat.SHORT, Locale.ENGLISH);
        final Date date = DateUtils.getDate(1990, 11);
        final String msg = Messages.format("error.day.missing", "climate-missing-date.csv", 11, df.format(date));
        assertEquals(1, dataErrors.size());
        assertEquals(msg, dataErrors.get(0));
    }

    /**
     * Test when a file contains not used headers.
     */
    @Test
    public void moreHeaders() {
        String csvPath;

        csvPath = "model/data/climate/climate-with-more-headers.csv";
        final String file = getFile(csvPath).toString();

        final String[] headers = {"NUM_POSTE", "year", "month", "day", "etp",
                "radiation", "rain", "tmean", "tmin", "tmax", "rh", "wind"};
        final ClimateFileLoader loader = new ClimateFileLoader(file, headers, ";");
        loader.setEtpCalculator(new EtpPenmanMonteithFAO(false));
        final Map<String, String> errors = loader.getConfigurationErrors();
        assertNull(helperErrors(errors), errors);

        final List<ClimaticDailyData> data = loader.load();
        assertNotNull("climate data must not be null!", data);
        final int nbOfData = 12;
        assertEquals(csvPath + " has only 12 lines.", nbOfData,
                data.size());
        final Double expected = 3.2;
        ClimaticDailyData ddata = data.get(0);
        assertEquals(ddata.toString(), expected, ddata.getTmin());
        ddata = data.get(nbOfData - 1);
        final Double expected2 = 380.;
        assertEquals(ddata.toString(), expected2, ddata.getRadiation());
    }

    /**
     * Test what happens with missing file.
     */
    @Test
    public void notexistantFile() {
        final String[] headers = getClimate2015Headers();
        final ClimateFileLoader loader = new ClimateFileLoader(notexistantFile,
                headers, separator);
        loader.setEtpCalculator(new EtpPenmanMonteithFAO(false));
        final Map<String, String> errors = loader.getConfigurationErrors();
        assertNotNull(errors);
        assertFalse(errors.isEmpty());
        assertTrue(errors.containsKey("climate.file"));
    }
}
