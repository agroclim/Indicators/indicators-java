/*
 * This file is part of Indicators.
 *
 * Indicators is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Indicators is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Indicators. If not, see <https://www.gnu.org/licenses/>.
 */
package fr.inrae.agroclim.indicators.model.indicator;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.junit.BeforeClass;
import org.junit.Test;

import fr.inrae.agroclim.indicators.exception.IndicatorsException;
import fr.inrae.agroclim.indicators.model.EvaluationType;
import fr.inrae.agroclim.indicators.model.Knowledge;
import fr.inrae.agroclim.indicators.model.Parameter;
import fr.inrae.agroclim.indicators.model.TimeScale;
import fr.inrae.agroclim.indicators.model.data.DailyData;
import fr.inrae.agroclim.indicators.model.data.DataTestHelper;
import fr.inrae.agroclim.indicators.model.data.Resource;
import fr.inrae.agroclim.indicators.model.data.Variable;
import fr.inrae.agroclim.indicators.util.Doublet;

/**
 * Test common methods of Indicator.
 *
 * Last change $Date$
 *
 * @author $Author$
 * @version $Revision$
 */
public class IndicatorTest extends DataTestHelper {
    public class Common extends Indicator {

        private static final long serialVersionUID = 1L;

        @Override
        public Double compute(final Resource<? extends DailyData> data) throws IndicatorsException {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        public void fireValueUpdated() {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        public final List<Doublet<Parameter, Number>> getParameterDefaults() {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        public Map<String, Double> getParametersValues() {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        public EvaluationType getType() {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        public Set<Variable> getVariables() {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        public boolean isComputable(final Resource<? extends DailyData> res) {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        public void setParametersFromKnowledge(final Knowledge knowledge) {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        public void setParametersValues(final Map<String, Double> values) {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        public String toStringTree(final String indent) {
            throw new UnsupportedOperationException("Not supported yet.");
        }

		@Override
		public void removeParameter(final Parameter param) {
			throw new UnsupportedOperationException("Not supported yet.");

		}

    }
    /**
     * Knowledge loaded from XML.
     */
    private static Knowledge knowledge;

    @BeforeClass
    public static void loadXml() {
        try {
            knowledge = Knowledge.load(TimeScale.DAILY);
        } catch (final IndicatorsException ex) {
            fail("Loading XML file from Knowledge.load() should work!");
        }
    }
    @Test
    public void readIndicatorNotes() {
        final String indicatorToTest = "monilia";
        final Indicator ind = knowledge.getIndicator(indicatorToTest);
        assertNotNull("indicator « " + indicatorToTest + " » is null", ind);
        if (ind.getNotes() != null) {   // si il y a des noeuds notes dans l'indicateur
            assertFalse("notes list is empty", ind.getNotes().isEmpty());
        }
    }

    @Test
    public void readKnowledgeNotes() {
        assertNotNull("notes list is null", knowledge.getNotes());
        assertFalse("notes list is empty", knowledge.getNotes().isEmpty());
    }

    @Test
    public void setIndicatorCategory() {
        final IndicatorCategory actual = IndicatorCategory.getByTag(null);
        assertNull("getByTag(null) must return null", actual);
        final Common ind = new Common();
        assertNull("category must be null by default", ind.getCategory());
        assertNull("indicatorCategory must be null by default",
                ind.getIndicatorCategory());
    }
}
