/*
 * This file is part of Indicators.
 *
 * Indicators is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Indicators is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Indicators. If not, see <https://www.gnu.org/licenses/>.
 */
package fr.inrae.agroclim.indicators.model.function.aggregation;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.util.HashMap;

import org.junit.Test;

import fr.inrae.agroclim.indicators.exception.IndicatorsException;

/**
 * Test JEXLFunction.
 *
 * Last changed : $Date$
 *
 * @author $Author$
 * @version $Revision$
 */
public final class JEXLFunctionTest {

    /**
     * Test an expression.
     */
    @Test
    public void expression() {
        String error = null;
        try {
            final HashMap<String, Double> data = new HashMap<>();
            final Double a = 50d;
            final Double b = 10d;
            data.put("a", a);
            data.put("b", b);
            final JEXLFunction func = new JEXLFunction();
            func.setExpression("a * math:min(b, 2) - 10");
            final Double expected = a * MathMethod.min(b, 2.) - 10;
            final Double actual = func.aggregate(data);
            assertEquals(expected, actual);
        } catch (final IndicatorsException ex) {
            error = ex.getMessage();
        }
        assertNull(error);
    }

    /**
     * Test expression with variable names starting with $.
     */
    @Test
    public void dollarVariableNames() {
        String error = null;
        try {
            final HashMap<String, Double> data = new HashMap<>();
            final Double one = 50d;
            final Double two = 10d;
            data.put("$1", one);
            data.put("$2", two);
            final JEXLFunction func = new JEXLFunction();
            func.setExpression("$1 * math:min($2, 2) - 10");
            final Double expected = one * MathMethod.min(two, 2.) - 10;
            final Double actual = func.aggregate(data);
            assertEquals(expected, actual);
        } catch (final IndicatorsException ex) {
            error = ex.getMessage();
        }
        assertNull(error);
    }

    /**
     * Test expression with variable names starting with dollar and variables
     * with number.
     */
    @Test
    public void dollarVariableNamesWithNumberParams() {
        String error = null;
        try {
            final HashMap<String, Double> data = new HashMap<>();
            final Double one = 50d;
            final Double two = 10d;
            data.put("1", one);
            data.put("2", two);
            final JEXLFunction func = new JEXLFunction();
            func.setExpression("$1 * math:min($2, 2) - 10");
            final Double expected = one * MathMethod.min(two, 2) - 10;
            final Double actual = func.aggregate(data);
            assertEquals(expected, actual);
        } catch (final IndicatorsException ex) {
            error = ex.getMessage();
        }
        assertNull(error);
    }

    /**
     * Test numeric value.
     */
    @Test
    public void numericValue() {
        String error = null;
        try {
            final HashMap<String, Double> data = new HashMap<>();
            final JEXLFunction f = new JEXLFunction();
            f.setExpression("0.0d");
            final Double actual = f.aggregate(data);
            final Double expected = 0.;
            assertEquals(expected, actual);

        } catch (final IndicatorsException ex) {
            error = ex.getMessage();
        }
        assertNull(error);
    }
}
