/*
 * This file is part of Indicators.
 *
 * Indicators is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Indicators is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Indicators. If not, see <https://www.gnu.org/licenses/>.
 */
package fr.inrae.agroclim.indicators.xml;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

import org.junit.Test;

import fr.inrae.agroclim.indicators.exception.IndicatorsException;
import fr.inrae.agroclim.indicators.model.Evaluation;
import fr.inrae.agroclim.indicators.model.EvaluationSettings;
import fr.inrae.agroclim.indicators.model.Knowledge;
import fr.inrae.agroclim.indicators.model.data.DataTestHelper;
import fr.inrae.agroclim.indicators.model.data.climate.ClimateFileLoader;
import fr.inrae.agroclim.indicators.model.data.climate.ClimateLoaderProxy;
import fr.inrae.agroclim.indicators.model.data.phenology.PhenologyFileLoader;
import fr.inrae.agroclim.indicators.model.data.phenology.PhenologyLoaderProxy;
import fr.inrae.agroclim.indicators.model.indicator.CompositeIndicator;
import fr.inrae.agroclim.indicators.model.indicator.Indicator;
import fr.inrae.agroclim.indicators.model.indicator.NumberOfWaves;
import lombok.extern.log4j.Log4j2;

/**
 * Test XMLUtil.
 *
 * Last changed : $Date$
 *
 * @author $Author$
 * @version $Revision$
 */
@Log4j2
public final class XMLUtilTest extends DataTestHelper {
    /**
     * Test load() with file from Getari.
     */
    @Test
    public void loadEvaluationSettings() {
        String message = "";
        try {
            final File xmlFile = getEvaluationTestFile();
            XMLUtil.load(xmlFile, EvaluationSettings.CLASSES_FOR_JAXB);
        } catch (final IndicatorsException e) {
            final StringWriter sw = new StringWriter();
            final PrintWriter pw = new PrintWriter(sw);
            e.printStackTrace(pw);
            message = sw.toString();
        }
        assertTrue(message, message.isEmpty());
    }

    /**
     * Test load()/save() a file.
     */
    @Test
    public void loadEvaluationSettingsAndSave() {
        String message = "";
        try {
            final File xmlFile = getEvaluationTestFile();
            EvaluationSettings settings;
            settings = (EvaluationSettings) XMLUtil.load(xmlFile,
                    EvaluationSettings.CLASSES_FOR_JAXB);
            final ByteArrayOutputStream baos = new ByteArrayOutputStream();
            XMLUtil.serialize(settings, baos,
                    EvaluationSettings.CLASSES_FOR_JAXB);
            final String path = xmlFile.getAbsoluteFile() + ".new";
            try (OutputStream outputStream = new FileOutputStream(path)) {
                baos.writeTo(outputStream);
                LOGGER.trace("File saved at {}", path);
            }
            EvaluationSettings saved;
            saved = (EvaluationSettings) XMLUtil.load(new File(path),
                    EvaluationSettings.CLASSES_FOR_JAXB);
            assertEquals("EtpCalculator must be equal",
                    settings.getClimateLoader().getEtpCalculator(),
                    saved.getClimateLoader().getEtpCalculator());
            assertEquals("ClimateFileLoaders must be equal",
                    settings.getClimateLoader().getFile(),
                    saved.getClimateLoader().getFile());
            assertEquals("ClimateLoaders must be equal",
                    settings.getClimateLoader(), saved.getClimateLoader());
            final CompositeIndicator s1 = (CompositeIndicator) settings.getEvaluation().getIndicators().get(0);
            final CompositeIndicator savedS1 = (CompositeIndicator) saved.getEvaluation().getIndicators().get(0);
            final List<Indicator> indicators = s1.getIndicators();
            final List<Indicator> savedIndicators = savedS1.getIndicators();
            final List<Indicator> mortIndicators = ((CompositeIndicator) indicators.get(1)).getIndicators();
            final List<Indicator> savedMortIndicators = ((CompositeIndicator) savedIndicators.get(1)).getIndicators();
            final CompositeIndicator heat = (CompositeIndicator) mortIndicators.get(1);
            final CompositeIndicator savedHeat = (CompositeIndicator) savedMortIndicators.get(1);
            final NumberOfWaves numbheatwav = (NumberOfWaves) heat.getIndicators().get(0);
            final NumberOfWaves savedNumbheatwav = (NumberOfWaves) savedHeat.getIndicators().get(0);
            assertEquals("Indicators s1 of s1 must be equal",
                    indicators.get(0),
                    savedIndicators.get(0)
                    );
            assertEquals("Indicators s1/mort/watdef must be equal",
                    mortIndicators.get(0),
                    savedMortIndicators.get(0)
                    );
            assertEquals("Criteria of s1/mort/heat/numbheatwav must be equal",
                    numbheatwav.getCriteria(),
                    savedNumbheatwav.getCriteria()
                    );
            assertEquals("Names s1/mort/heat/numbheatwav must be equal",
                    numbheatwav.getNames(),
                    savedNumbheatwav.getNames()
                    );
            assertEquals("Id s1/mort/heat/numbheatwav must be equal",
                    numbheatwav.getId(),
                    savedNumbheatwav.getId()
                    );
            assertEquals("Indicators s1/mort/heat/numbheatwav must be equal",
                    numbheatwav,
                    savedNumbheatwav
                    );
            assertEquals("Indicators s1/mort/heat must be equal",
                    heat,
                    savedHeat
                    );
            assertEquals("Indicators mort of s1 must be equal",
                    indicators.get(1),
                    savedIndicators.get(1)
                    );
            assertEquals("Indicators of s1 must be equal",
                    indicators,
                    savedIndicators
                    );
            assertEquals("Phases s1 must be equal", s1, savedS1);
            assertEquals("#5F9EA0", s1.getColor());
            assertEquals("Phases must be equal",
                    settings.getEvaluation().getIndicators(),
                    saved.getEvaluation().getIndicators());
            assertEquals("Evaluations must be equal",
                    settings.getEvaluation(), saved.getEvaluation());
            assertEquals("FilePaths must be equal",
                    settings.getFilePath(), saved.getFilePath());
            assertEquals("Knowledge must be equal",
                    settings.getKnowledge(), saved.getKnowledge());
            assertEquals("Names must be equal",
                    settings.getName(), saved.getName());
            assertEquals("PhenologyLoaders must be equal",
                    settings.getPhenologyLoader(), saved.getPhenologyLoader());
            assertEquals("SoilLoaders must be equal",
                    settings.getSoilLoader(), saved.getSoilLoader());
            assertEquals("SoilPhenologyCalculators must be equal",
                    settings.getSoilPhenologyCalculator(), saved.getSoilPhenologyCalculator());
            assertEquals(settings, saved);
        } catch (final IOException | IndicatorsException e) {
            final StringWriter sw = new StringWriter();
            final PrintWriter pw = new PrintWriter(sw);
            e.printStackTrace(pw);
            message = sw.toString();
        }
        assertTrue(message, message.isEmpty());
    }

    /**
     * Test load() with missing file.
     */
    @Test
    public void loadFileNotFound() {
        boolean exception = false;
        boolean fileNotFoundException = false;
        try {
            final File xmlFile = new File(System.getProperty("user.dir")
                    + File.separator + "test/xml/does-not-exist.xml");
            XMLUtil.load(xmlFile, EvaluationSettings.CLASSES_FOR_JAXB);
        } catch (final IndicatorsException e) {
            exception = true;
            if (e.getCause() instanceof FileNotFoundException) {
                fileNotFoundException = true;
            }
        }
        assertTrue("An Exception must be thrown", exception);
        assertTrue("A FileNotFoundException must be the root exception.",
                fileNotFoundException);
    }
    /**
     * Test load() with embedded knowledge.xml.
     */
    @Test
    public void loadKnowledge() {
        String message = "";
        try {
            final File xmlFile = getFile("knowledge.xml");
            assertNotNull("knowledge.xml must be found!", xmlFile);
            XMLUtil.load(xmlFile, Knowledge.CLASSES_FOR_JAXB);
        } catch (final IndicatorsException e) {
            LOGGER.catching(e);
            final StringBuilder sb = new StringBuilder();
            sb.append(e.getClass().getCanonicalName()).append(": ");
            sb.append(e.toString());
            Throwable cause = e.getCause();
            while (cause != null) {
                sb.append(" : ").append(cause.getClass().getCanonicalName())
                .append(": ").append(cause.getMessage());
                cause = cause.getCause();
            }
            message = sb.toString();
        }
        assertTrue(message, message.isEmpty());

    }

    /**
     * Test saving a new evaluation.
     *
     * @throws java.io.IOException while creating tmp file.
     * @throws IndicatorsException while serializing
     */
    @Test
    public void saveEvaluation() throws IOException, IndicatorsException {
        final String name = "évaluation";
        final File clim = getClimate1951File();
        final File phen = getPhenoSampleFile();
        final Evaluation evaluation = new Evaluation();
        final EvaluationSettings settings = new EvaluationSettings();
        settings.setName(name);
        evaluation.setSettings(settings);
        settings.setClimate(new ClimateLoaderProxy());
        settings.getClimateLoader().setFile(new ClimateFileLoader());
        settings.getClimateLoader().getFile().setSeparator("\t");
        settings.getClimateLoader().getFile().setFile(clim);
        settings.setPhenologyLoader(new PhenologyLoaderProxy());
        settings.getPhenologyLoader().setFile(new PhenologyFileLoader());
        settings.getPhenologyLoader().getFile().setSeparator(";");
        settings.getPhenologyLoader().getFile().setFile(phen);
        final Path tmp = Files.createTempFile("evaluation", ".gri");
        final String fileName = tmp.toString();
        XMLUtil.serialize(settings, fileName, EvaluationSettings.CLASSES_FOR_JAXB);
        EvaluationSettings loaded;
        loaded = (EvaluationSettings) XMLUtil.load(new File(fileName), EvaluationSettings.CLASSES_FOR_JAXB);
        assertEquals(name, loaded.getName());
    }
}
