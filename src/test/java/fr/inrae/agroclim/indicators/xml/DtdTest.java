/*
 * Copyright (C) 2021 INRAE AgroClim
 *
 * This file is part of Indicators.
 *
 * Indicators is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Indicators is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Indicators. If not, see <http://www.gnu.org/licenses/>.
 */
package fr.inrae.agroclim.indicators.xml;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.junit.Test;
import org.w3c.dom.Document;
import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

import fr.inrae.agroclim.indicators.model.data.DataTestHelper;
import lombok.RequiredArgsConstructor;

/**
 * Test evaluation.dtd.
 *
 * Last changed : $Date$
 *
 * @author $Author$
 * @version $Revision$
 */
public class DtdTest extends DataTestHelper {

	@RequiredArgsConstructor
	public static class SaxErrorHandler implements ErrorHandler {

		private final List<String> errors;

		@Override
		public void error(final SAXParseException e) throws SAXException {
			errors.add("ERROR at " + e.getLineNumber() + "-" + e.getColumnNumber() + " : " + e.getLocalizedMessage() + "\n");
		}

		@Override
		public void fatalError(final SAXParseException e) throws SAXException {
			errors.add("FATAL at " + e.getLineNumber() + "-" + e.getColumnNumber() + " : " + e.getLocalizedMessage() + "\n");
		}

		@Override
		public void warning(final SAXParseException e) throws SAXException {
			errors.add("WARN at " + e.getLineNumber() + "-" + e.getColumnNumber() + " : " + e.getLocalizedMessage() + "\n");
		}
	}

	@Test
	public void validate() throws SAXException, IOException, ParserConfigurationException {

		final DocumentBuilderFactory domFactory = DocumentBuilderFactory.newInstance();
		domFactory.setValidating(true);
		final DocumentBuilder builder = domFactory.newDocumentBuilder();
                final DtdResolver dtdResolver = new DtdResolver();
                dtdResolver.setDtds(XMLUtil.getDtds());
		builder.setEntityResolver(dtdResolver);
		final List<String> errors = new ArrayList<>();
		builder.setErrorHandler(new SaxErrorHandler(errors));
		final File xmlFile = getEvaluationTestFile();
		final Document doc = builder.parse(xmlFile);
		assertNotNull(doc);
		assertTrue(xmlFile.getAbsolutePath() + "\n" + errors.toString(), errors.isEmpty());
	}

}
