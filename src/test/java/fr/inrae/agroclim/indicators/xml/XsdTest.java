package fr.inrae.agroclim.indicators.xml;

import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import javax.xml.XMLConstants;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.transform.Source;
import javax.xml.transform.sax.SAXSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

import org.junit.Test;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;

import fr.inrae.agroclim.indicators.model.data.DataTestHelper;
import lombok.extern.log4j.Log4j2;

/**
 * Test evaluation.xsd.
 *
 * Last changed : $Date$
 *
 * @author $Author$
 * @version $Revision$
 */
@Log4j2
public class XsdTest extends DataTestHelper {
	@Test
	public void validate() {
		final List<String> errors = new ArrayList<>();
		final File xmlFile = getEvaluationTestFile();
		try {
                        SAXParserFactory parserFactory = SAXParserFactory.newInstance();
                        parserFactory.setNamespaceAware(true);
                        SAXParser parser = parserFactory.newSAXParser();
                        parser.setProperty(XMLConstants.ACCESS_EXTERNAL_DTD, "");
                        parser.setProperty(XMLConstants.ACCESS_EXTERNAL_SCHEMA, "");
                        XMLReader xmlreader = parser.getXMLReader();
                        final DtdResolver dtdResolver = new DtdResolver();
                        dtdResolver.setDtds(XMLUtil.getDtds());
                        xmlreader.setEntityResolver(dtdResolver);
			final InputStream inputStream = new FileInputStream(xmlFile);
			final InputSource inputSource = new InputSource(inputStream);
			final Source source = new SAXSource(xmlreader, inputSource);

			final SchemaFactory sf = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
			final Schema schema = sf.newSchema(getClass().getResource("/fr/inrae/agroclim/indicators/evaluation.xsd"));
			final Validator validator = schema.newValidator();
			validator.setResourceResolver(new XMLUtil.CustomLSResourceResolver());
			validator.setErrorHandler(new DtdTest.SaxErrorHandler(errors));
			validator.validate(source);
		} catch (IOException | ParserConfigurationException | SAXException e) {
			LOGGER.catching(e);
			errors.add(e.getMessage());
		}
		assertTrue(errors.toString(), errors.isEmpty());
	}
}
