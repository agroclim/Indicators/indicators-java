/*
 * This file is part of Indicators.
 *
 * Indicators is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Indicators is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Indicators. If not, see <https://www.gnu.org/licenses/>.
 */
package fr.inrae.agroclim.indicators.resources;

import java.util.Locale;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * Test Messages.
 *
 * Last changed : $Date$
 *
 * @author $Author$
 * @version $Revision$
 */
public final class MessagesTest {

    /**
     * Locale before test.
     */
    private static Locale originalLocale;

    /**
     * Define English as locale for tests.
     */
    @BeforeClass
    public static void setLocale() {
        originalLocale = Locale.getDefault();
        Locale.setDefault(Locale.ENGLISH);
        Messages.refresh();
    }

    /**
     * Reset locale.
     */
    @AfterClass
    public static void resetLocale() {
        Locale.setDefault(originalLocale);
        Messages.refresh();
    }

    /**
     * Test format with existing key.
     */
    @Test
    public void formatExists() {
        final String key = "error.climate.jdbc.query";
        final String expected = "Error while query execution \"SELECT 1\"!";
        final String actual = Messages.format(key, "SELECT 1");
        Assert.assertEquals(expected, actual);
    }

    /**
     * Test with existing key.
     */
    @Test
    public void getStringExists() {
        final String key = "error.title";
        final String expected = "Error";
        final String actual = Messages.get(key);
        Assert.assertEquals(expected, actual);
    }

    /**
     * Test with missing key.
     */
    @Test
    public void getStringMissing() {
        final String key = "this.key.does.not.exist";
        final String expected = "!" + key + "!";
        final String actual = Messages.get(key);
        Assert.assertEquals(expected, actual);
    }

}
