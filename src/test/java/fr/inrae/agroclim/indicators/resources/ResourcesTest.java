/*
 * This file is part of Indicators.
 *
 * Indicators is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Indicators is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Indicators. If not, see <https://www.gnu.org/licenses/>.
 */
package fr.inrae.agroclim.indicators.resources;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.time.LocalDateTime;
import java.time.Month;
import java.time.format.DateTimeParseException;
import java.util.Locale;

import org.junit.Test;

/**
 * Test Resources.
 *
 * Last changed : $Date$
 *
 * @author $Author$
 * @version $Revision$
 */
public class ResourcesTest {

    /**
     * Date.
     */
    private static final LocalDateTime DATE;

    static {
        final int year = 2018;
        final Month month = Month.AUGUST;
        final int day = 27;
        final int hours = 11;
        final int minutes = 46;
        final int seconds = 30;
        DATE = LocalDateTime.of(year, month, day, hours, minutes, seconds);
    }

    /**
     * Name of existing bundle.
     */
    private static final String NAME
    = "fr.inrae.agroclim.indicators.resources.test";

    @Test
    public void format() {
        final Resources res = new Resources(NAME, Locale.FRENCH);
        final String actual = res.format("warning.missing", "climat", 1, "WIND");
        final String expected = "climat : ligne 1 : WIND manque.";
        assertEquals(expected, actual);
    }

    @Test
    public void formatMissing() {
        final Resources res = new Resources(NAME, Locale.FRENCH);
        final String actual = res.format("key.missing", "a", "b", "c");
        final String expected = "!key.missing : [a, b, c]!";
        assertEquals(expected, actual);
    }

    @Test
    public void getBundleName() {
        final Resources res = new Resources(NAME, Locale.FRENCH);
        assertEquals(NAME, res.getBundleName());
    }

    @Test
    public void getLocalDatetimeISO() {
        final Resources res = new Resources(NAME, Locale.FRENCH);
        final LocalDateTime actual = res.getLocalDateTime("date.iso_local_date_time");
        assertEquals(DATE, actual);
    }

    @Test(expected = DateTimeParseException.class)
    public void getLocalDatetimeWrong() {
        final Resources res = new Resources(NAME, Locale.FRENCH);
        res.getLocalDateTime("error.title");
    }

    @Test
    public void getLocalDatetimeYyyyMMddHHmmss() {
        final Resources res = new Resources(NAME, Locale.FRENCH);
        final LocalDateTime actual = res.getLocalDateTime("date.yyyyMMddHHmmss");
        assertEquals(DATE, actual);
    }

    @Test
    public void getStringExisting() {
        final Resources res = new Resources(NAME, Locale.FRENCH);
        final String actual = res.getString("error.title");
        final String expected = "Erreur";
        assertEquals(expected, actual);
    }

    @Test
    public void getStringExistingDefault() {
        final Resources res = new Resources(NAME, Locale.ROOT);
        final String actual = res.getString("error.title");
        final String expected = "Error";
        assertEquals(expected, actual);
    }

    @Test
    public void getStringExistingEnglish() {
        final Resources res = new Resources(NAME, Locale.ENGLISH);
        final String actual = res.getString("error.title");
        final String expected = "Error";
        assertEquals(expected, actual);
    }

    @Test
    public void getStringMissing() {
        final Resources res = new Resources(NAME, Locale.FRENCH);
        final String actual = res.getString("key.missing");
        final String expected = "!key.missing!";
        assertEquals(expected, actual);
    }

    @Test
    public void getVersionAndBuildDate() {
        final Resources res = new Resources(NAME, Locale.FRENCH);
        final String actual = res.getVersionAndBuildDate();
        final String expected = "1.2.3+20180828145930";
        assertEquals(expected, actual);
    }

    @Test
    public void setBundleName() {
        final Resources res = new Resources(NAME, Locale.FRENCH);
        String actual = res.getString("error.title");
        final String expected = "Erreur";
        assertEquals(expected, actual);
        res.setBundleName("fr.inrae.agroclim.indicators.resources.version");
        actual = res.getString("build.date");
        assertTrue(!actual.startsWith("!"));
    }

    @Test
    public void setLocale() {
        Resources res;
        String actual;
        String expected;

        res = new Resources(NAME, Locale.FRENCH);
        actual = res.getString("error.title");
        expected = "Erreur";
        assertEquals(expected, actual);

        res.setLocale(Locale.FRENCH);
        actual = res.getString("error.title");
        expected = "Erreur";
        assertEquals(expected, actual);

        res.setLocale(Locale.ROOT);
        actual = res.getString("error.title");
        expected = "Error";
        assertEquals(expected, actual);
    }
}
