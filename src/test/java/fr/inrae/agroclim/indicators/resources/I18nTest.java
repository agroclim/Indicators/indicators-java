/*
 * This file is part of Indicators.
 *
 * Indicators is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Indicators is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Indicators. If not, see <https://www.gnu.org/licenses/>.
 */
package fr.inrae.agroclim.indicators.resources;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.ResourceBundle;

import org.junit.Test;

import fr.inrae.agroclim.indicators.resources.I18n.Operator;

/**
 * Test plural form handling with I18n.
 *
 * Last changed : $Date$
 *
 * @author $Author$
 * @version $Revision$
 */
public class I18nTest {

    /**
     * Name of test bundle.
     */
    private static final String NAME
            = "fr.inrae.agroclim.indicators.resources.test";

    /**
     * Key for plural messages to test.
     */
    private static final String KEY = "cartItems";

    @Test
    public void format() {
        final I18n res = new I18n(NAME, Locale.FRENCH);
        final String actual = res.format("warning.missing", "climat", 1, "WIND");
        final String expected = "climat : ligne 1 : WIND manque.";
        assertEquals(expected, actual);
    }

    /**
     * Variation is not present, default value.
     */
    @Test
    public void formatPluralMissing() {
        int nb = 1;
        final I18n res = new I18n(NAME, Locale.FRENCH);
        String actual;
        String expected;
        actual = res.format(nb, "cats", nb);
        expected = "Un chat";
        assertEquals(expected, actual);

        nb = 2;
        actual = res.format(nb, "cats", nb);
        expected = "2 chats";
        assertEquals(expected, actual);
    }

    @Test
    public void formatPluralCompare() {
        final I18n res = new I18n(NAME, Locale.FRENCH);
        final Map<Integer, String> values = new HashMap<>();
        values.put(1, "Valeur == 1");
        values.put(10, "Valeur >= 10");
        values.put(11, "Valeur > 10");
        values.put(-10, "Valeur <= -10");
        values.put(-101, "Valeur < -100");
        values.forEach((nb, expected) -> {
            final String actual = res.format(nb, "comparison", nb);
            assertEquals(expected, actual);
        });
    }

    @Test
    public void formatPluralDefault() {
        final int nb = 101;
        final I18n res = new I18n(NAME, Locale.FRENCH);
        final String actual = res.format(nb, KEY, nb);
        final String expected = "Il y a 101 produits dans votre panier.";
        assertEquals(expected, actual);
    }

    @Test
    public void formatPluralOne() {
        final int nb = 1;
        final I18n res = new I18n(NAME, Locale.FRENCH);
        final String actual = res.format(nb, KEY, nb);
        final String expected = "Il y a un produit dans votre panier.";
        assertEquals(expected, actual);
    }

    @Test
    public void formatPluralTwo() {
        final int nb = 2;
        final I18n res = new I18n(NAME, Locale.FRENCH);
        final String actual = res.format(nb, KEY, nb);
        final String expected = "Il y a deux produits dans votre panier.";
        assertEquals(expected, actual);
    }

    @Test
    public void formatPluralFew() {
        final int nb = 3;
        final I18n res = new I18n(NAME, Locale.FRENCH);
        final String actual = res.format(nb, KEY, nb);
        final String expected = "Il y a 3 produits dans votre panier, ce qui est peu.";
        assertEquals(expected, actual);
    }

    @Test
    public void formatPluralMany() {
        final int nb = 1314;
        final I18n res = new I18n(NAME, Locale.FRENCH);
        final String actual = res.format(nb, KEY, nb);
        String expected;
        expected = switch (System.getProperty("java.specification.version")) {
            case "1.8", "9", "10", "11" -> "Il y a 1 314 produits dans votre panier, ce qui est beaucoup.";
            default -> "Il y a 1 314 produits dans votre panier, ce qui est beaucoup.";
        };
        assertEquals(expected, actual);
    }

    @Test
    public void formatPluralSpecial() {
        final int nb = 42;
        final I18n res = new I18n(NAME, Locale.FRENCH);
        final String actual = res.format(nb, KEY, nb);
        final String expected = "Il y a 42 produits dans votre panier, ce qui est un nombre spécial.";
        assertEquals(expected, actual);
    }

    @Test
    public void formatPluralSpecialDefaultProperties() {
        final int nb = 42;
        final I18n res = new I18n(NAME, Locale.KOREA);
        final String actual = res.format(nb, KEY, nb);
        final String expected = "There are 42 items in your cart, a special number!";
        assertEquals(expected, actual);
    }

    @Test
    public void getStringDefault() {
        final I18n res = new I18n(NAME, Locale.FRENCH);
        final String actual = res.get("not.translated");
        final String expected = "This message is not translated.";
        assertEquals(expected, actual);
    }

    @Test
    public void getStringExisting() {
        final I18n res = new I18n(NAME, Locale.FRENCH);
        final String actual = res.get("error.title");
        final String expected = "Erreur";
        assertEquals(expected, actual);
    }

    @Test
    public void constructorWithResourceBundle() {
        final ResourceBundle bundle = ResourceBundle.getBundle(NAME, Locale.FRENCH);
        final I18n res = new I18n(bundle);
        String actual;
        String expected;
        actual = res.get("error.title");
        expected = "Erreur";
        assertEquals(expected, actual);

        actual = res.get("not.translated");
        expected = "This message is not translated.";
        assertEquals(expected, actual);
    }

    @Test
    public void matches() {
        final Map<String, Integer> doesMatch = new HashMap<>();
        doesMatch.put("=1", 1);
        doesMatch.put(">0", 1);
        doesMatch.put(">=0", 1);
        doesMatch.put(">=1", 1);
        doesMatch.put("<0", -1);
        doesMatch.put("<=0", -1);
        doesMatch.put("<=1", -1);
        doesMatch.put("<-100", -101);
        doesMatch.forEach((comparison, plural) -> {
            final boolean actual = I18n.matches(comparison, plural);
            assertTrue(comparison + " must be true for " + plural, actual);
        });
        final Map<String, Integer> dontMatch = new HashMap<>();
        dontMatch.put("=0", 1);
        dontMatch.put(">1", 1);
        dontMatch.put(">2", 1);
        dontMatch.put(">=2", 1);
        dontMatch.put("<-1", -1);
        dontMatch.put("<-2", -1);
        dontMatch.put("<=-2", -1);
        dontMatch.forEach((comparison, plural) -> {
            final boolean actual = I18n.matches(comparison, plural);
            assertFalse(comparison + " must be false for " + plural, actual);
        });
    }

    @Test
    public void operatorExtract() {
        final Map<String, I18n.Operator> operators = new HashMap<>();
        operators.put("1", null);
        operators.put("=1", I18n.Operator.AEQ);
        operators.put(">0", I18n.Operator.ESUP);
        operators.put(">=0", I18n.Operator.DSUPEQ);
        operators.put(">=1", I18n.Operator.DSUPEQ);
        operators.put("<0", I18n.Operator.CINF);
        operators.put("<=0", I18n.Operator.BINFEQ);
        operators.forEach((comparison, expected) -> {
            final Optional<Operator> res = I18n.Operator.extract(comparison);
            final I18n.Operator actual = res.orElse(null);
            assertEquals(expected + " must be extracted from " + comparison, expected, actual);
        });
    }
}
