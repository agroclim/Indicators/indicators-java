/*
 * This file is part of Indicators.
 *
 * Indicators is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Indicators is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Indicators. If not, see <https://www.gnu.org/licenses/>.
 */
package fr.inrae.agroclim.indicators.resources;

import static fr.inrae.agroclim.indicators.resources.I18n.PluralSuffix.FEW;
import static fr.inrae.agroclim.indicators.resources.I18n.PluralSuffix.ONE;
import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import fr.inrae.agroclim.indicators.resources.I18n.PluralSuffix;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@RunWith(Parameterized.class)
public final class I18nPluralSuffixTest {

    /**
     * {@link Parameters} annotation marks this method as parameters provider.
     *
     * @return combinaisons of years, doys and expectations
     */
    @Parameters(name = "Run {0} ({1}) => {2}")
    public static Iterable<Object[]> data() {
        return Arrays.asList(
                new Object[][]{
                    {ONE, -1, true},
                    {ONE, 1, true},
                    {ONE, 2, false},
                    {ONE, 3, false},
                    {ONE, 11, false},
                    {ONE, null, false},
                    {FEW, 3, true},
                    {FEW, 10, true},
                    {FEW, 103, true},
                    {FEW, 113, false},
                    {FEW, 13, false}
                }
                );
    }
    private final PluralSuffix suffix;
    private final Integer value;
    private final boolean expected;

    @Test
    public void getSuffix() {
        final String expectedSuffix = suffix.name().toLowerCase();
        final Optional<String> actual = PluralSuffix.getSuffix(value);
        if (expected) {
            assertEquals(actual.toString(), expected, actual.isPresent());
            assertEquals(expectedSuffix, actual.get());
        }
    }

    @Test
    public void test() {
        final boolean actual = suffix.test(value);
        assertEquals(expected, actual);
    }
}
